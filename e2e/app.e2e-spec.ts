import { SilverBrickPage } from './app.po';

describe('SilverBrickPage', () => {
  let page: SilverBrickPage;

  beforeEach(() => {
    page = new SilverBrickPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
  });
});
