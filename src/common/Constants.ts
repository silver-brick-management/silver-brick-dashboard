// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Headers } from '@angular/http';
import { ConfigContext } from '../environments/environment';

export class Constants {
    static readonly AUTH_TOKEN_STORAGE_NAME: string = "silverbrick:id_token";
    static readonly AUTH_PROFILE_STORAGE_NAME: string = "silverbrick:profile";
    static readonly REFRESH_THRESHOLD_30_MIN: number = 30 * 60 * 1000; // in milliseconds (30 min * 60 seconds * 1000 milliseconds)
    static readonly ERROR_MESSAGE_CLEAR_STORAGE_FAIL = "Clear storage failed";

    static readonly DIALOG_RESPONSE_ACTION_OK: string = "ok";
    static readonly DIALOG_RESPONSE_ACTION_SECOND_OK: string = 'second_ok';
    static readonly DIALOG_RESPONSE_ACTION_CANCEL: string = "cancel";
    static readonly DIALOG_RESPONSE_ACTION_ADD: string = "Added";
    static readonly DIALOG_RESPONSE_ACTION_UPDATE: string = "Updated";
    static readonly DIALOG_RESPONSE_ACTION_ACTIVATE: string = "Activated";
    static readonly DIALOG_RESPONSE_ACTION_DEACTIVATE: string = "Deactivated";
    static readonly DIALOG_RESPONSE_ACTION_DELETE: string = "Deleted";
    static readonly DIALOG_RESPONSE_ACTION_SHIP: string = "Shipped";
    static readonly STR_LOADING: string = "LOADING...";
    static readonly DEFAULT_USER_POSITION: string = "User";
    static readonly ERROR_MESSAGE_NO_PROFILE: string = "Profile Not Found";
    static readonly DEFAULT_PROFILE_PHOTO_PATH: string = "https://firebasestorage.googleapis.com/v0/b/silver-brick.appspot.com/o/Defaults%2Fdefault-user.png?alt=media&token=864efc3d-7b31-4f68-8401-965e5c33f734";
    static readonly STR_ORGS: string = "orgs";
    static readonly STR_PNG = "png";
    static readonly STR_FINISHING: string = "Finishing...";
    static readonly STR_AUDIENCE_TENANT_ONLY: string = "tenant";
    static readonly STR_AUDIENCE_LANDLORD_ONLY: string = "landlord";
    static readonly STR_AUDIENCE_TENANT_AND_LANDLORD: string = "tenant_landlord";
    static readonly STR_AUDIENCE_TENANT_AND_STAFF: string = "tenant_staff";
    static readonly STR_AUDIENCE_STAFF_ONLY: string = "staff";

    // Errors
    static readonly ERROR_MESSAGE_IONIC_STORAGE_FAIL: string = "Ionic storage failed";
    static readonly ERROR_TYPE_STORAGE_UNAUTHORIZED: string = "storage/unauthorized";
    static readonly ERROR_TYPE_STORAGE_CANCELED: string = "storage/canceled";

    // Socket actions
    static readonly SOCKET_CONNECT = "connect";
    static readonly SOCKET_DISCONNECT = "disconnect";
    static readonly SOCKET_DISCONNECTING = "disconnecting";
    static readonly SOCKET_RECONNECT = "reconnect";
    static readonly SOCKET_RECONNECT_ATTEMPT = "reconnect_attempt";
    static readonly SOCKET_ERROR = "error";
    static readonly SOCKET_AUTHENTICATE = "authenticate";
    static readonly SOCKET_AUTHENTICATED = "authenticated";
    static readonly SOCKET_UNAUTHORIZED = "unauthorized";
    static readonly SOCKET_JOIN = "join";
    static readonly SOCKET_JOINED = "joined";
    static readonly SOCKET_LEAVE = "leave";



    static readonly STR_NA: string = "N/A";
    static readonly STR_DISMISS: string = "Dismiss";

    static readonly STR_ZERO: string = "0";
    static readonly STR_ONE: string = "1";

    static readonly PATCH_ACTIVATE_ACTION: string = "activate";
    static readonly PATCH_DEACTIVATE_ACTION: string = "deactivate";

    // Messages
    static readonly UNSAVED_CHANGES_TITLE: string = "You have unsaved changes";
    static readonly UNSAVED_CHANGES_MSG: string = "Are you sure you want to leave this page?";
    static readonly UNSAVED_CHANGES_OK: string = "Leave";
    static readonly UNSAVED_CHANGES_CANCEL: string = "Stay";
    static readonly MESSAGE_IMAGE_UPLOAD_ERROR: string = "There was an error uploading your image";
    static readonly MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED: string = "Image upload not authorized";
    static readonly MESSAGE_IMAGE_UPLOAD_CANCELED: string = "Image upload canceled";
    static readonly MESSAGE_IMAGE_UPLOAD_FAILURE: string = "Chat Image Upload failure";

static readonly STRIPE_SECRET_KEY: string = "sk_live_s5IU4SsrjRxnnS4ZvLUxR5V7";
}

// API Urls
export class Urls {
    static readonly SERVER_BASE: string = ConfigContext.SilverBrickApiURL;
    static readonly AUTH_BASE_V1: string = `${Urls.SERVER_BASE}auth/v1/dba/`;
    static readonly API_BASE_V1: string = `${Urls.SERVER_BASE}dba/v1/`;

    static readonly API_LOGIN_V1: string = `${Urls.AUTH_BASE_V1}login/`;
    static readonly API_REFRESH_V1: string = `${Urls.AUTH_BASE_V1}refreshtoken/`;
    static readonly API_PROFILE_V1: string = `${Urls.AUTH_BASE_V1}profile/`;

    static readonly API_USERS_V1: string = `${Urls.API_BASE_V1}users/`;
    static readonly API_USER_IMPORT_V1 = `${Urls.API_BASE_V1}userimport/`;
    static readonly API_BUILDING_USERS_V1 = `${Urls.API_BASE_V1}buildingusers/`;
    static readonly API_BUILDING_TENANTS_V1 = `${Urls.API_BASE_V1}buildingtenants/`;
    static readonly API_ALL_TENANTS_V1 = `${Urls.API_BASE_V1}alltenants/`;
    static readonly API_BUILDING_INFO_V1 = `${Urls.API_BASE_V1}buildinginfo/`;
    static readonly API_ORGS_V1 = `${Urls.API_BASE_V1}orgs/`;
    static readonly API_CHAT_ROOMS_V1 = `${Urls.API_BASE_V1}messages/rooms/`;
    static readonly API_SERVICES_V1 = `${Urls.API_BASE_V1}services/`;
    static readonly API_BOOKINGS_V1 = `${Urls.API_BASE_V1}bookings/`;
    static readonly API_BLDGS_V1 = `${Urls.API_BASE_V1}buildings/`;
    static readonly API_TASKS_V1 = `${Urls.API_BASE_V1}staffBookings/`;
    static readonly API_TASK_HISTORY_V1 = `${Urls.API_BASE_V1}taskHistory/`;
    static readonly API_UPDATE_HISTORY_V1 = `${Urls.API_BASE_V1}updateHistory/`;

    static readonly API_ASSIGN_TASKS_V1 = `${Urls.API_BASE_V1}assignTasks/`;
    static readonly API_DASHBOARD_RECURRING_TASKS_V1 = `${Urls.API_BASE_V1}dashboardRecurringTasks/`;

    static readonly API_CERTIFICATIONS_V1 = `${Urls.API_BASE_V1}certifications/`;  
    static readonly DEFAULT_PROFILE_URL: string = "https://firebasestorage.googleapis.com/v0/b/silverbrick-live.appspot.com/o/Defaults%2FgkoXOHyv4kWmN67ttHUMjtiMAFI3.png?alt=media&token=7f75beaa-ac2c-4452-b391-667f4a179ca3";
    static readonly SOCKET_MESSAGE_V1 = `${Urls.SERVER_BASE}messages`;
}

export const JSON_CONTENT_TYPE = { 'Content-Type': 'application/json' };
export const JSON_HEADER = { headers: new Headers(JSON_CONTENT_TYPE) };
export const CONTENT_TYPE_X_WWW_FORM = { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' };

export const STATE_LIST = [
  "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
  "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
  "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
  "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
  "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"
];

export const PROPERTY_TYPES = [
    "Residential",
    "Single-Family",
    "Condo/Townhome",
    "Multi-Family",
    "Commercial",
    "Industrial",
    "Office",
    "Shopping Center",
    "Retail",
];

export const TASK_TYPES = [
    "General Inquiry",
    "Maintenance Request"
];

export const MAINTENANCE_REQUESTS = [
    "General",
    "Applicances",
    "Electrical",
    "HVAC",
    "Key & Lock",
    "Lighting",
    "Outside",
    "Plumbing"
];

export const TASK_STATUS = [
    "New",
    "In Progress",
    "Completed",
    "Deferred",
    "Assigned",
    "Closed"
];

export const URGENCY = [
    "Normal",
    "High",
    "Low"
];

export const colors: any = {
    red: {
        primary: "#ad2121",
        secondary: "#FAE3E3"
    },
    blue: {
        primary: "#1e90ff",
        secondary: "#D1E8FF"
    },
    yellow: {
        primary: "#e3bc08",
        secondary: "#FDF1BA"
    }
};

export const staffColors: any = {
    "ZG4D0TFYG2VY0FuQaoBmQ5by5vw2": {
        primary: "#ff885c",
        secondary: "#000"
    },
    "UBAkHLKTuGUFnSwmrZXi37MzUPD2": {
        primary: "#a9efa9",
        secondary: "#000"
    },
    "Zk8kplp7R1hISEVsfm2WfescaJ12": {
        primary: "#c0b359",
        secondary: "#000"
    },
    "I41o6F08A7cClZh8sIqXFs39Rk02": {
        primary: "#87cefa",
        secondary: "#000"
    },
    "SDp6u7LC0zR2T6Y8BrwCSV8sp8Z2": {
        primary: "#deb887",
        secondary: "#000"
    },
    "qfD6dkLEyrUf3JzXfIScK68XaBh2": {
        primary: "#660099",
        secondary: "#fff"
    },
    "HaJsDQiiWDglxcAWcIcQ4xD1dJ42": {
        primary: "#deb887",
        secondary: "#000"
    },
    "notFound": {
        primary: "rgb(217, 217, 217)",
        secondary: "#000"
    },
    "sCL3x6K4vqeNMn1XH1BwVtarE1T2": {
        primary: "rgb(0, 0, 0)",
        secondary: "#fff"
    },
    "lEkZQcZNgVe54yCQGWCelx3qPRF3": {
        primary: "rgb(229, 230, 46)",
        secondary: "#000"
    },
    "hb7m6CJ91ydD6VbJIlIiGY5shu52": {
        primary: "rgb(230, 162, 46)",
        secondary: "#000"
    },
    "aAEzjUa46bSzVWwQOd5cWe5KYCH2": {
        primary: "rgb(18, 228, 233)",
        secondary: "#000"
    },
    "ZvY8ck96v2XID3CFoxvVbjsoWib2": {
        primary: "rgb(18, 228, 233)",
        secondary: "#000"
    },
    "YXCHgSu3gQXaLezWhsrxenhtdqr1": {
        primary: "rgb(251, 100, 100)",
        secondary: "#fff"
    },
    "JcnB7Git6zUrqS4gfDgfcScFwSR2": {
        primary: "rgb(20, 86, 143)",
        secondary: "#fff"
    },
    "EE0vk3d483XV7njC9ycdQnrRlhp2": {
        primary: "rgb(20, 86, 143)",
        secondary: "#fff"
    },
    "7VUXqqjXP8ZvsoPTOpRdlDxwwqH2": {
        primary: "rgb(143, 33, 20)",
        secondary: "#fff"
    },
    "0gQOWQmcq0ZBJ8WTU7Yx3bqmxAu2": {
        primary: "rgb(170, 131, 14)",
        secondary: "#fff"
    },
    "0K2swEAq2vTQrPkrYFvADQFqHcs2": {
        primary: "rgb(21, 134, 15)",
        secondary: "#fff"
    }
    
};