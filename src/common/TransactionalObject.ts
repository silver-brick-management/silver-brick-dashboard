import * as _ from 'lodash';

export class TransactionalObject<T> {
    public Source: T = null;
    public Current: T = null;

    private constructor(objRef: T) {
        this.Source = objRef;
    }

    GetSource(): T {
        return this.Source;
    }

    Get(): T {
        return this.Current;
    }

    public Reset(): void {
        // Make a deep 'duplicate' clone, that creates a separate object
        // from the source
        this.Current = _.cloneDeep(this.Source);
    }

    public Commit(): void {
        // Shallow copy so we keep the same object ref
        this.Source = Object.assign(this.Source, this.Current);
    }

    private _initialize(): void {
        this.Reset();
    }

    static CreateInstance<T>(objRef: T): TransactionalObject<T> {
        let objInstance: TransactionalObject<T> = new TransactionalObject<T>(objRef);

        objInstance._initialize();

        return objInstance;
    }
}