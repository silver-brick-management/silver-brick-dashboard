import { IUser, ITaskFeed, ITask, IAssignee, RecurringEvent, IMapIcon } from '../shared/SilverBrickTypes';
import { UserChatRoom } from "../interfaces/IResponse";
import { RouteUtil } from './RouteUtil';
import { ArrayUtil } from './ArrayUtil';
import { Constants, colors, staffColors } from "./Constants";
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay, CalendarWeekViewEvent } from "angular-calendar";
import * as moment from "moment";
import RRule from "rrule";

declare var google: any;

export class UserUtil {
    static DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)', 'forever'];
    static actions: CalendarEventAction[] = [
        {
            label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event, date }: { event: RecurringEvent; date: Date }): void => {
                // this.RecurringTask(event.task, date);
            },
        }

    ];

	static Transform(value: any): any[] {
        let keys = Object.keys(value);
        let newKeys: any[] = keys.map(k => value[k]);
        
        return newKeys;
    }

    static SortByName(users: IUser[]): IUser[] {
        let newUsers: IUser[] = users.sort((user1, user2) => {
            if (`${user1.firstName} ${user1.lastName}` < `${user2.firstName} ${user2.lastName}`) {
                return -1;
            }

            if (`${user1.firstName} ${user1.lastName}` > `${user2.firstName} ${user2.lastName}`) {
                return 1;
            }

            return 0;
        });
        return newUsers;
    }

    static ScopeTasks(tasks: ITask[], scope: string): ITask[] {
        let scopeTask: ITask[] = tasks.filter((task: ITask) => {
            return (task.status === scope);
        });
        return scopeTask;
    }

    static ScopeTasksByType(tasks: ITask[], type: string): ITask[] {
        let scopeTask: ITask[] = tasks.filter((task: ITask) => {
            return (task.taskType === type);
        });
        return scopeTask;
    }

    static ScopeStaffTasks(tasks: ITask[], assignee: IAssignee): ITask[] {
        let scopeTask: ITask[] = tasks.filter((task: ITask) => {
            if ((null != task.assignees) && (task.assignees.length > 0)) {
                let uids: string[] = task.assignees.map((assignee, index: number) => {
                    return assignee.uid;
                });
                return (uids.includes(assignee.uid));
            } else {
                return false;
            }
        });
        return scopeTask;
    }

    static ScopeUnAssignedStaffTasks(tasks: ITask[]): ITask[] {
        let scopeTask: ITask[] = tasks.filter((task: ITask) => {
            return ((task.assignees == null) || ((task.assignees != null) && (task.assignees.length === 0)));
        });
        return scopeTask;
    }

    static SetupEvents(tasks: ITask[], activeDate: Date, calendarColors: any): RecurringEvent[] {
        if (null == activeDate) {
            activeDate = new Date(Date.now());
        }
        let allEvents: any[] = [];
        let recurringEvents = [];
        let allTasks: ITask[] = tasks;
        let assignCount: number = 0;
        let date: Date = new Date(Date.now());
        allTasks.forEach((task: ITask) => {
            if ((null != task.assignees) && (task.assignees.length > 0)) {

            } else {
                assignCount += 1;
            }
            let matchDate = `${activeDate.getMonth()}_${activeDate.getDate()}_${activeDate.getFullYear()}`;
            if ((null != task.skipDates && !task.skipDates.includes(matchDate)) || null == task.skipDates) {
                let startHours: number = Number(task.startTime.split(":", 2)[0]);
                let endHours: number = Number(task.endTime.split(":", 2)[0]);
                let startMinutes: number = Number(task.startTime.split(":", 2)[1]);
                let endMinutes: number = Number(task.endTime.split(":", 2)[1]);
                let currentDate: Date = new Date(Date.now());
                let taskDate: Date = new Date(task.startDate);
                let shouldAddEvent: boolean = false;
                if (null == task.endDate) {
                    switch (task.durationAmount) {
                        case this.DurationOptions[0]:
                        {
                            let endDate: Date = new Date(task.startDate);
                            endDate.setDate(endDate.getDate() + Number(task.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            task.endDate = endDate.getTime();
                            break;
                        }

                        case this.DurationOptions[1]:
                        {
                            let endDate: Date = new Date(task.startDate);
                            endDate.setDate(endDate.getDate() + (Number(task.durationQuantity) * 7));
                            endDate.setHours(endDate.getHours() - 4);
                            task.endDate = endDate.getTime();
                            break;
                        }

                        case this.DurationOptions[2]:
                        {
                            let date: Date = new Date(task.startDate);
                            let currentDate = date.getDate();
                            // Set to day 1 to avoid forward
                            date.setDate(1);
                            // Increase month by 1
                            date.setMonth(date.getMonth() + Number(task.durationQuantity) + 1);
                            // Get max # of days in this new month
                            let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
                            // Set the date to the minimum of current date of days in month
                            date.setDate(Math.min(currentDate, daysInMonth));
                            date.setHours(date.getHours() - 4);
                            task.endDate = date.getTime();
                            break;
                        }

                        case this.DurationOptions[3]:
                        {
                            let endDate: Date = new Date(task.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(task.durationQuantity));
                            endDate.setHours(endDate.getHours() - 4);
                            task.endDate = endDate.getTime();
                            break;
                        }

                        case this.DurationOptions[4]:
                        {
                            let endDate: Date = new Date(task.startDate);
                            endDate.setFullYear(endDate.getFullYear() + Number(20));
                            endDate.setHours(endDate.getHours() - 4);
                            task.endDate = endDate.getTime();
                            break;
                        }
                        
                        default:
                        {
                            break;
                        }
                    }
                }
                let taskEndDate: Date = new Date(task.endDate);
                let color: string = staffColors["notFound"];
                if (null != task.assignees && task.assignees.length > 0) {
                    if (null != calendarColors) {
                        color =
                            null != calendarColors[task.assignees[0].uid]
                                ? calendarColors[task.assignees[0].uid]
                                : staffColors["notFound"];
                    } else {
                        color =
                            null != staffColors[task.assignees[0].uid]
                                ? staffColors[task.assignees[0].uid]
                                : staffColors["notFound"];
                    }
                }
                let mapIcon: IMapIcon = {
                    fillColor: color,
                    strokeColor: color
                };
                let newEvent: any = {
                    start: new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), startHours, startMinutes),
                    end: new Date(taskEndDate.getFullYear(), taskEndDate.getMonth(), taskEndDate.getDate(), endHours, endMinutes),
                    title: `${task.subject}`,
                    actions: this.actions,
                    color: color,
                    // icon: pinViewBackground,
                    resizable: {
                        beforeStart: true,
                        afterEnd: true,
                    },
                    meta: `${task.subject}`,
                    displayTitle: `${task.subject}`,
                    startTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), startHours, startMinutes),
                    endTime: new Date(date.getFullYear(), date.getMonth(), date.getDate(), endHours, endMinutes),
                };
                let matchDate: string = `${activeDate.getMonth() + 1}_${activeDate.getDate()}_${activeDate.getFullYear()}`;
                if ((null != task.completedDates) && (task.completedDates.includes(matchDate))) {
                    newEvent.completedToday = true;
                }
                Object.assign(newEvent, {
                    task: task,
                });
                let startDate: Date = new Date(task.startDate);
                let endDate: Date = new Date(task.endDate);
                    switch (task.repeatInterval) {
                        case "Daily": {
                            newEvent.rrule = {
                                freq: RRule.DAILY,
                                interval: task.repeatQuantity,
                            };
                            allEvents.push(newEvent);
                            break;
                        }

                        case "Weekly": {
                            // // console.log("tas", task.id);
                            let newWeeklyMonthlyDays = [];
                            let weeklyMonthDays: string[] = [];
                            let taskIDs: string[] = [];
                            if (null != task.monthlyWeekDays) {
                                if (task.monthlyWeekDays.includes(activeDate.getDay())) {
                                    shouldAddEvent = true;
                                    if (!taskIDs.includes(newEvent.task.id)) {
                                        allEvents.push(newEvent);
                                        // allEvents = this._sortBy(allEvents);
                                        taskIDs.push(newEvent.task.id);
                                        return;
                                    }
                                }
                                for (let day of task.monthlyWeekDays) {
                                    weeklyMonthDays.push(this._getDay(day));
                                }
                            }
                            break;
                        }

                        case "Monthly": {
                            if (task.monthlyConvention === "Day of month") {
                                let newMonthlyDays = [];
                                let weeklyMonthDays: string[] = [];
                                // // // console.log("task monthlyDays", task.id, task.orgID, task.buildingID, task.subject);
                                if (null != task.monthlyDays) {
                                    for (let day of task.monthlyDays) {
                                        newMonthlyDays.push(day);
                                        weeklyMonthDays.push(`${day + 1}${this._nth(day + 1)}`);
                                        if (activeDate.getDate() === day + 1) {
                                            shouldAddEvent = true;
                                            allEvents.push(newEvent);
                                            // allEvents = this._sortBy(allEvents);
                                        } else {
                                            shouldAddEvent = false;
                                        }
                                    }
                                    newEvent.title = newEvent.title + ` (Monthly on ${weeklyMonthDays.join(", ")})`;
                                    newEvent.rrule = {
                                        freq: RRule.MONTHLY,
                                        interval: task.repeatQuantity,
                                        bymonthday: newMonthlyDays,
                                    };
                                }
                            } else {
                                let year = activeDate.getFullYear();
                                let month = activeDate.getMonth();
                                let day = new Date(year + "-" + month + "-01").getDay();
                                let weekCount = 1;
                                let currentWeek: number = this._weekCount(moment(activeDate));
                                let dayCount = 1;
                                let taskIDs: string[] = [];
                                newEvent.rrule = {
                                    freq: RRule.MONTHLY,
                                    interval: task.repeatQuantity,
                                    byweekday: [],
                                };
                                let firstDay = new Date(activeDate.getFullYear(), activeDate.getMonth(), 1);
                                let firstDayNumber = firstDay.getDay();
                                let addCount: number = 0;
                                taskIDs = [];
                                let finalCurrentWeek: number = activeDate.getDay() >= firstDayNumber ? currentWeek : currentWeek - 1;
                                if ((null != task.monthlyNWeekDays) && (null != task.monthlyNWeekDays[finalCurrentWeek])) {
                                    let keys = task.monthlyNWeekDays[finalCurrentWeek];
                                    for (let day of keys) {
                                        let result = this._nthDayOfMonth(moment(), day, currentWeek);
                                        let matchDate: Date = new Date(result);
                                        if (keys.includes(activeDate.getDay())) {
                                            shouldAddEvent = true;
                                            addCount = 1;
                                            if (!taskIDs.includes(newEvent.task.id)) {
                                                allEvents.push(newEvent);
                                                // allEvents = this._sortBy(allEvents);
                                                taskIDs.push(newEvent.task.id);
                                                return;
                                            }
                                        }
                                        dayCount += 1;
                                    }
                                }
                                weekCount += 1;
                                dayCount = 1;
                                // }
                                if (addCount > 0) {
                                    addCount = 0;
                                }
                            }
                            break;
                        }

                        case "Yearly": {
                            let newMonthlyDays = [];
                            for (let day of task.monthlyDays) {
                                newMonthlyDays.push(day + 1);
                            }
                            newEvent.rrule = {
                                freq: RRule.YEARLY,
                                interval: task.repeatQuantity,
                                bymonthday: newMonthlyDays,
                                bymonth: new Date(task.startDate).getMonth(),
                            };
                            let startMonth: number = new Date(task.startDate).getMonth();
                            let startDay: number = new Date(task.startDate).getDate();
                            if (activeDate.getDate() === startDay && activeDate.getMonth() === startMonth) {
                                shouldAddEvent = true;
                                allEvents.push(newEvent);
                                
                            } else {
                                shouldAddEvent = false;
                            }
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                // }
                if (task.repeatInterval === "Once") {
                    let newDate: Date = new Date(activeDate.getFullYear(), activeDate.getMonth(), activeDate.getDate());
                    let secondDate: Date = new Date(newEvent.start.getFullYear(), newEvent.start.getMonth(), newEvent.start.getDate());
                    if (newDate.getTime() === secondDate.getTime()) {
                        allEvents.push(newEvent);
                    }
                }
            }
        });
        return allEvents;
    }


    static MatchStaffTasks(tasks: ITask[], users: IUser[]): IUser[] {
        let newUsers: IUser[] =[];
        for (let user of users) {
            user.taskCount = 0;
            user.completedTaskCount = 0;
            user.todayTaskCount = 0;
            user.todayCompletedTaskCount = 0;
            let userTask: ITask[] = [];
            for (let task of tasks) {
                if ((null == task.assignees) || ((null != task.assignees) && (task.assignees.length === 0))) {
                    user.taskCount = user.taskCount + 1;
                    if ((null != task.completedDates) && (task.completedDates.length > 0)) {
                        user.completedTaskCount += 1;
                    } else {

                    }
                    user.todayTaskCount += 1;
                    // // console.log("user", user.completedTaskCount, user.todayTaskCount);
                } else {
                    let uids: string[] = task.assignees.map((assignee, index: number) => {
                        return assignee.uid;
                    });
                    let assigns: IAssignee[] = ArrayUtil.Transform(task.assignees);
                    let assign: IAssignee = {
                        uid: user.uid,
                        name: `${user.firstName} ${user.lastName}`
                    };
                    if (uids.includes(user.uid)) {
                        user.taskCount = user.taskCount + 1;
                        if ((null != task.completedDates) && (task.completedDates.length > 0)) {
                            user.completedTaskCount += 1;
                        }
                        user.todayTaskCount = user.todayTaskCount + 1;
                        user.todayCompletedTaskCount += 1;
                        // // console.log("taskCount", matchDate, ((null != task.task.completedDates) && (task.task.completedDates.includes(matchDate))), user.todayCompletedTaskCount);
                    } else {

                    }
                }
                userTask.push(task);
            }
            newUsers.push(user);            
        }
        return newUsers;
    }

	static SortByCreatedAt(users: IUser[]): IUser[] {
		let newUsers: IUser[] = users.sort((user1, user2) => {
			if (user1.createdAt > user2.createdAt) {
			    return -1;
			}

			if (user1.createdAt < user2.createdAt) {
			    return 1;
			}

			return 0;
		});
		return newUsers;
	}

    static SortByLastActivity(users: IUser[]): IUser[] {
        let newUsers: IUser[] = users.sort((user1, user2) => {
            // console.log("act", (null != user1.lastActivity ? new Date(user1.lastActivity).toDateString(): 0), (null != user2.lastActivity ? new Date(user2.lastActivity).toDateString(): 0))
            if (((null != user1.lastActivity) ? (user1.lastActivity): 0) > ((null != user2.lastActivity) ? (user2.lastActivity): 0)) {
                return -1;
            }

            if (((null != user1.lastActivity) ? (user1.lastActivity): 0) < ((null != user2.lastActivity) ? (user2.lastActivity): 0)) {
                return 1;
            }

            return 0;
        });
        return newUsers;
    }

	static SortByDate(users: ITaskFeed[]): ITaskFeed[] {
		let newTasks: ITaskFeed[] = users.sort((task1, task2) => {
			if (task1.timestamp > task2.timestamp) {
			    return -1;
			}

			if (task1.timestamp < task2.timestamp) {
			    return 1;
			}

			return 0;
		});
		return newTasks;
	}

    static SortTasksByDate(users: ITask[]): ITask[] {
        let newTasks: ITask[] = users.sort((task1, task2) => {
            if (task1.date > task2.date) {
                return -1;
            }

            if (task1.date < task2.date) {
                return 1;
            }

            return 0;
        });
        return newTasks;
    }

    static SortTasksByDateReverse(users: ITask[]): ITask[] {
        let newTasks: ITask[] = users.sort((task1, task2) => {
            if (task1.lastUpdated < task2.lastUpdated) {
                return -1;
            }

            if (task1.lastUpdated > task2.lastUpdated) {
                return 1;
            }

            return 0;
        });
        return newTasks;
    }

    static SortTasksByDateReverseTwice(users: ITask[]): ITask[] {
        let newTasks: ITask[] = users.sort((task1, task2) => {
            if (task1.lastUpdated > task2.lastUpdated) {
                return -1;
            }

            if (task1.lastUpdated < task2.lastUpdated) {
                return 1;
            }

            return 0;
        });
        return newTasks;
    }

    static SortByFirstName(users: IUser[]): IUser[] {
        let newUsers: IUser[] = users.sort((user1, user2) => {
            if (user1.firstName < user2.firstName) {
                return -1;
            }

            if (user1.firstName > user2.firstName) {
                return 1;
            }

            return 0;
        });
        return newUsers;
    }

	static sortChatRooms(rooms: UserChatRoom[]): UserChatRoom[] {
	    let chatRooms: UserChatRoom[] = [];
	    if (null != rooms) {
	      chatRooms = rooms.sort((room1, room2) => {
	        if (room1.lastMessage.date > room2.lastMessage.date) {
	          return -1;
	        }
	        if (room1.lastMessage.date < room2.lastMessage.date) {
	          return 1;
	        }
	        return 0;
	      });
	    }

	    return chatRooms;
	}

    static _getDay(day: number): string {
        switch (day) {
            case 0: {
                return "Sunday";
            }

            case 1: {
                return "Monday";
            }

            case 2: {
                return "Tuesday";
            }

            case 3: {
                return "Wednesday";
            }

            case 4: {
                return "Thursday";
            }

            case 5: {
                return "Friday";
            }

            case 6: {
                return "Saturday";
            }

            default: {
                break;
            }
        }
    }

    static _nth(d: number): string {
        if (d > 3 && d < 21) {
            return "th";
        } else {
            switch (d % 10) {
                case 1: {
                    return "st";
                }

                case 2: {
                    return "nd";
                }

                case 3: {
                    return "rd";
                }

                default: {
                    return "th";
                }
            }
        }
    }

    static _sortBy(users: RecurringEvent[]): RecurringEvent[] {
        let newUsers: RecurringEvent[] = users.sort((user1, user2) => {
            if (user1.start < user2.start) {
                return -1;
            }

            if (user1.start > user2.start) {
                return 1;
            }

            return 0;
        });
        // // // // // console.log("_sortBy", newUsers);
        return newUsers;
    }

    static _weekCount(m: moment.Moment) {
        return m.week() - moment(m).startOf("month").week();
    }

    static _nthDayOfMonth(monthMoment: moment.Moment, day: number, weekNumber: number): string {
        let m = monthMoment.clone().startOf("month").day(day);
        if (m.month() !== monthMoment.month()) m.add(7, "d");
        return m.add(7 * (weekNumber - 1), "d").format("YYYY-MM-DD");
    }
}