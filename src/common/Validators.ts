// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { AbstractControl, FormArray } from '@angular/forms';

export class FormValidators {

    static CheckEmailsMatch(control: AbstractControl) {
        const email = control.get('email');
        const confirm = control.get('confirm');
        if (!email || !confirm) {
            return null;
        }
        return (email.value === confirm.value) ? null : { nomatch: true };
    }

    static CheckBuildingArrayLength(control: AbstractControl) {
        const buildings: FormArray = (control.get('buildings') as FormArray);
        return (buildings.length > 0) ? null : { empty: true };
    }
}
