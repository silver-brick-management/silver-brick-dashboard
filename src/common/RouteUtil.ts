/* eslint-disable no-undef, @typescript-eslint/no-unused-vars, no-unused-vars */

import { Constants } from '../common/Constants';
import { ITask, RecurringEvent } from '../shared/SilverBrickTypes';

declare var google: any;

export class RouteUtil {
   
    public initMap(): void {

    }

    static CalculateRoute(method: google.maps.TravelMode, tasks: RecurringEvent[]): RecurringEvent[] {
        const directionsService = new google.maps.DirectionsService();
        const directionsRenderer = new google.maps.DirectionsRenderer();
        const polylineOptionsActual = {
            strokeColor: '#0096FF',
            strokeOpacity: 1.0,
            strokeWeight: 10
        };
        let waypts: google.maps.DirectionsWaypoint[] = [];
        let newTasks: RecurringEvent[] = [];
        console.log("method", method);
        for (let task of tasks) {
            let address: string = task.task.buildingName + ', Brooklyn, NY';
            waypts.push({
                location: address,
                stopover: true,
            });
            // console.log("waypts", waypts);
        }
        directionsService
            .route({
                origin: '68 4th Street, Brooklyn, NY',
                destination: '68 4th Street, Brooklyn, NY',
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: ((null != method)) ? method : google.maps.TravelMode.BICYCLING,
            })
            .then(async (response: any) => {
                // console.log("CalculateRoute", response);
                directionsRenderer.setDirections(response);
                let route = response.routes[0];
                for (let i = 0; i < (route.legs.length - 1); i++) {
                    let routeSegment = i + 1;
                    let matchingEvent: RecurringEvent = tasks.find((event: RecurringEvent) => {
                        let parts: string[] = (i === 0) ? route.legs[i].end_address.split(", Brooklyn, NY", 2) : ((i === (route.legs.length - 1)) ? route.legs[i].start_address.split(", Brooklyn, NY", 2) : route.legs[i].end_address.split(", Brooklyn, NY", 2));
                        // let criteria = RegExp('\d{1,5}\s\w.\s(\b\w*\b\s){1,2}\w*\.');

                        // 1. First test for address validation via a Regex
                        let businessNameCheck: string[] = parts[0].split(",");
                        if ((null != businessNameCheck) && (businessNameCheck.length > 1)) {
                            parts[0] = businessNameCheck[businessNameCheck.length - 1];
                        }


                        // Remove periods from Google's street name
                        parts[0] = parts[0].replace(".", "");

                        // New name to compare the Google name too
                        let newName: string = (event.task.buildingName);

                        // Testing the number part of the street name for dashes (ex. 62-64 ABC Street)
                        let numberParts: string[] = event.task.buildingName.split(" ");
                        let newNumbers: string[] = numberParts[0].split("-");
                        if ((null == newNumbers) || ((null != newNumbers && (newNumbers.length === 1)))) {
                            newNumbers = numberParts[0].split("+");
                        }
                        let buildingNameParts: string[] = newName.split(" ");
                        
                        // If there is a dash in the number, Google will return the number before the dash
                        // so we remove the first number and make it the street number, and get rid of the -
                        let buildingNumber: string = buildingNameParts.shift();
                        buildingNameParts = buildingNameParts.filter((part: string) => {
                            return ((part !== "-") && (isNaN(Number(part))));
                        });
                        buildingNumber = buildingNumber.replace(" ", "");

                        let newerNumber: string[] = parts[0].split(" ")[0].split("-");
                        // Check to see
                        if ((null != newNumbers) && (null != buildingNameParts)) {
                            if (newNumbers.includes(newerNumber[0])) {
                                let matching = newNumbers.find((an: string) => {
                                    return an === newerNumber[0];
                                });
                                if (null != matching) {
                                    newName = `${matching} ${buildingNameParts.join(' ')}`;
                                }
                            } else if (newNumbers.includes(newerNumber[1])) {
                                let matching = newNumbers.find((an: string) => {
                                    return an === newerNumber[1];
                                });
                                if (null != matching) {
                                    newName = `${matching} ${buildingNameParts.join(' ')}`;
                                }
                            }
                        }
                        newName = newName.replace(".", "");
                        newName = newName.replace(" Avenue", " Ave");
                        newName = newName.replace(" Street", " St");
                        newName = newName.replace(" Place", " Pl");
                        newName = newName.replace(" Lane", "Ln");
                        newName = newName.replace(" Boulevard", " Blvd");
                        newName = newName.replace(" Court", " Ct");
                        newName = newName.replace(" Road", " Rd");
                        newName = newName.replace("North ", "N ");
                        newName = newName.replace("South ", "S ");
                        newName = newName.replace("West ", "W ");
                        newName = newName.replace("East ", "E ");
                        newName = newName.replace("Saint ", "St ");
                        // console.log("match? Google Name: ", parts[0]);
                        // console.log("match? New building name: ", newName);
                        return (newName.toLowerCase().trim() === parts[0].toLowerCase().trim());
                    });
                    // console.log("matchingEvent", matchingEvent);
                    let secondMatchingEvent: RecurringEvent = newTasks.find((event: RecurringEvent) => {
                        return ((null != matchingEvent) && (event.task.id === matchingEvent.task.id));
                    });
                    if ((null != matchingEvent) && (null == secondMatchingEvent)) {
                        let allEvents: RecurringEvent[] = tasks.filter((event: RecurringEvent) => {
                            return ((event.task.buildingID === matchingEvent.task.buildingID) || event.task.buildingName === matchingEvent.task.buildingName);
                        });
                        // console.log("allEvents", allEvents);
                        if (null != allEvents) {
                            for (let newTask of allEvents) {
                                let minutes: string = String(i);
                                if (minutes.length === 1) {
                                    minutes = `0${i}`;
                                }
                                let endMinutes: string = String(i + 1);
                                if (endMinutes.length === 1) {
                                    endMinutes = `0${i}`;
                                }
                                newTask.task.startTime = `7:${minutes}`;
                                newTask.task.endTime = `7:${endMinutes}`;
                                let taskDate: Date = new Date(newTask.task.startDate);
                                let taskEndDate: Date = new Date(newTask.task.endDate);
                                newTask.start =  new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), 7, Number(minutes));
                                newTask.end = new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), 7, Number(endMinutes));
                                newTask.task.location = {
                                    lat: 0,
                                    lng: 0
                                };
                                newTask.task.location.lat = await route.legs[i].end_location.lat();
                                newTask.task.location.lng = await route.legs[i].end_location.lng();
                                newTasks.push(newTask);
                            }
                        }
                    }
                }
                return newTasks;
            })
            .catch((e: any) => {
                console.error("Directions request failed due to " + e);
            });
            return newTasks;
    }

    static CalculateRouteNoEvent(method: string, tasks: ITask[]): ITask[] {
        const directionsService = new google.maps.DirectionsService();
        const directionsRenderer = new google.maps.DirectionsRenderer();
        const polylineOptionsActual = {
            strokeColor: '#0096FF',
            strokeOpacity: 1.0,
            strokeWeight: 10
        };
        let waypts: google.maps.DirectionsWaypoint[] = [];
        let newTasks: ITask[] = [];
        // console.log("tasks", tasks);
        for (let task of tasks) {
            let address: string = task.buildingName + ', Brooklyn, NY';
            waypts.push({
                location: address,
                stopover: true,
            });
            // console.log("waypts", waypts);
        }
        directionsService
            .route({
                origin: '68 4th Street, Brooklyn, NY',
                destination: '68 4th Street, Brooklyn, NY',
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: ((null != method) && (method === 'bicycle')) ? google.maps.TravelMode.BICYCLING : google.maps.TravelMode.WALKING,
            })
            .then((response: any) => {
                // console.log("CalculateRoute", response);
                directionsRenderer.setDirections(response);
                let route = response.routes[0];
                for (let i = 0; i < (route.legs.length); i++) {
                    let routeSegment = i + 1;
                    let matchingEvent: ITask = tasks.find((event: ITask) => {
                        let parts: string[] = route.legs[i].end_address.split(", NY", 2);
                        let newName: string = (event.buildingName + ", Brooklyn");
                        newName = newName.replace("Avenue", "Ave");
                        newName = newName.replace("Street", "St");
                        newName = newName.replace("Place", "Pl");
                        newName = newName.replace("North", "N");
                        newName = newName.replace("South", "S");
                        // console.log("parts", parts, newName);
                        return (newName === parts[0]);
                    })
                    let secondMatchingEvent: ITask = newTasks.find((event: ITask) => {
                        return (event.subject === matchingEvent.subject);
                    });
                    if ((null != matchingEvent) && (null == secondMatchingEvent)) {
                        let newTask: ITask = matchingEvent;

                        let minutes: string = String(i);
                        if (minutes.length === 1) {
                            minutes = `0${i}`;
                        }
                        let endMinutes: string = String(i + 1);
                        if (endMinutes.length === 1) {
                            endMinutes = `0${i}`;
                        }
                        newTask.startTime = `7:${minutes}`;
                        newTask.endTime = `7:${endMinutes}`;
                        let taskDate: Date = new Date(newTask.startDate);
                        let taskEndDate: Date = new Date(newTask.endDate);
                        newTask.start =  new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), 7, Number(minutes));
                        newTask.end = new Date(taskDate.getFullYear(), taskDate.getMonth(), taskDate.getDate(), 7, Number(endMinutes));
                        newTasks.push(newTask);
                        // console.log("newTasks", newTask);
                    }
                }

            })
            .catch((e: any) => {
                // console.error("Directions request failed due to " + e);
                throw new Error("Directions request failed due to " + e);
            });
            return newTasks;
    }
}