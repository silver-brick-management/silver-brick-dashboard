// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which can be found at the root of this
// project.
//
// *********************************************************


export enum TimeZoneType {
    EST,
    EST_NO_DSV
}

export class TimeUtil {

    // Note! The granularity of time in javascript is milliseconds!
    static A_SECOND_IN_MILLISECONDS = 1000;
    static AN_HOUR_IN_MINUTES = 60;
    static AN_HOUR_IN_SECONDS = TimeUtil.AN_HOUR_IN_MINUTES * 60; // 60 * 60 = 3600
    static AN_HOUR_IN_MILLISECONDS = TimeUtil.AN_HOUR_IN_SECONDS * TimeUtil.A_SECOND_IN_MILLISECONDS; // 3600 * 1000 = 3,600,000
    static A_DAY_IN_MILLISECONDS = TimeUtil.AN_HOUR_IN_MILLISECONDS * 24 /* 24 hours in a day */; // 3,600,000 * 24 = 86,400,000
    static END_OF_DAY_IN_MILLISECONDS = TimeUtil.A_DAY_IN_MILLISECONDS - 1;
    static START_OF_DAY_IN_MILLISECONDS = 0 /* 12 AM in epoch milliseconds*/;

    static IsTimeInRange(targetTime: Date, startTime: Date, endTime: Date): boolean {
        let normalizedTargetTime: number = targetTime.getTime() % this.A_DAY_IN_MILLISECONDS;
        let normalizedStartTime: number = startTime.getTime() % this.A_DAY_IN_MILLISECONDS;
        let normalizedEndTime: number = endTime.getTime() % this.A_DAY_IN_MILLISECONDS;
        console.log("normalizedTargetTime: ", normalizedTargetTime, " normalizedStartTime: ", normalizedStartTime, " normalizedEndTime: ", normalizedEndTime);
        return (normalizedTargetTime <= normalizedEndTime) && (normalizedTargetTime >= normalizedStartTime);
    }

    static ConvertLocalTimeToUtc(localTimeInSeconds: any | string, timeZone: TimeZoneType): number {
        let adjustmentHours: number = 0;
        switch (timeZone) {
            case TimeZoneType.EST:
            {
                adjustmentHours = 5;
                break;
            }

            case TimeZoneType.EST_NO_DSV:
            {
                adjustmentHours = 4;
                break;
            }

            default:
            {
                throw new Error("Invalid Time Zone!");
            }

        }

        return (localTimeInSeconds * this.A_SECOND_IN_MILLISECONDS) + (adjustmentHours * this.AN_HOUR_IN_MILLISECONDS);
    }

    /**
   * Returns a string indicating how long ago a post was made (i.e: "2 hours ago")
   * @param  {Date} date - timestamp of post
   *
   * @returns {String} - new formatted time ago
   */
  public static timeSince(date: number) {
    const seconds = Math.floor((+new Date() - date) / 1000); // coerce date to number
    let interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
      return `${interval} years ago`;
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return `${interval} months ago`;
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return `${interval} days ago`;
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return `${interval} hours ago`;
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return `${interval} minutes ago`;
    }
    return "Just now";
  }

    static GetMonthDayYear(): string {
        let newDate: string = "";
        const curDate: Date = new Date(Date.now());
        newDate = curDate.getMonth().toString() + curDate.getDay().toString() + curDate.getFullYear().toString();
        return newDate;
    }
}