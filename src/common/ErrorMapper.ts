// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { FirebaseError } from 'firebase';
import { IError, ErrorType } from '../interfaces/IError';
import { ErrorFactory } from '../factories/ErrorFactory';
import { IServerResponseMessage } from '../interfaces/IResponse';
import { Constants } from './Constants';

export class ErrorMapper {
    static GetErrorFromFirebaseAuthError(firebaseAuthError: FirebaseError): IError {
        let errorInstance: IError = null;

        switch (firebaseAuthError.code) {
            case 'auth/user-not-found':
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_USER_NOT_FOUND,
                    firebaseAuthError,
                    firebaseAuthError.message);
                break;

            case "auth/wrong-password":
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_WRONG_PASSWORD,
                    firebaseAuthError,
                    firebaseAuthError.message);
                break;

            default:
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_UNKNOWN_ERROR,
                    firebaseAuthError,
                    firebaseAuthError.message);
                break;
        }

        return errorInstance;
    }

    static GetErrorFromServerResponseError(status: number, serverResponseError: IServerResponseMessage): IError {
        let errorInstance: IError = null;

        switch (status) {
            case 401:
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_UNAUTHORIZED,
                    serverResponseError,
                    serverResponseError.description ? serverResponseError.description : serverResponseError.message);
                break;

            default:
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_UNKNOWN_ERROR,
                    serverResponseError,
                    serverResponseError.description ? serverResponseError.description : serverResponseError.message);
                break;
        }

        return errorInstance;
    }

    static GetErrorFromSocketError(serverResponseError: IServerResponseMessage): IError {
        let errorInstance: IError = null;

        switch (serverResponseError.message) {
            case 'UnauthorizedError':
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_UNAUTHORIZED,
                    serverResponseError,
                    serverResponseError.description ? serverResponseError.description : serverResponseError.message);
                break;

            default:
                errorInstance = ErrorFactory.CreateInstance(
                    ErrorType.AUTH_UNKNOWN_ERROR,
                    serverResponseError,
                    serverResponseError.description ? serverResponseError.description : serverResponseError.message);
                break;
        }

        return errorInstance;
    }

    static GetErrorFromStorageError(error: any): IError {
        let errorInstance: IError = null;

        errorInstance = ErrorFactory.CreateInstance(
            ErrorType.AUTH_UNKNOWN_ERROR,
            error,
            Constants.ERROR_MESSAGE_CLEAR_STORAGE_FAIL);

        return errorInstance;
    }
}
