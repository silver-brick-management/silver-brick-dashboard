// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

export enum LogLevel {
    STACKTRACE,
    VERBOSE,
    INFO,
    ERROR,
    STATUS
}

export type ReportMap = {
    [key: string]: any[];
};

export class Logger {
    private static _loggerInstance: Logger = null;

    static GetInstance(): Logger {
        if (null == this._loggerInstance) {
            this._loggerInstance = new Logger();
        }

        return this._loggerInstance;
    }

    static log(message: string, logLevel: LogLevel): void {
        this.GetInstance()._log(message, logLevel);
    }

    static Status(message: string) {
        this.log(message, LogLevel.STATUS);
    }

    static ErrorWithPayload(tag: string, payloadData: string) {
        this.PayloadData(tag, payloadData, LogLevel.ERROR);
    }

    static Error(message: string) {
        this.log(message, LogLevel.ERROR);
    }

    static Info(message: string) {
        this.log(message, LogLevel.INFO);
    }

    static Verbose(message: string) {
        this.log(message, LogLevel.VERBOSE);
    }

    static StackTrace(message: string) {
        this.log(message, LogLevel.STACKTRACE);
    }

    static PayloadData(tag: string, payloadData: string, logLevel: LogLevel = LogLevel.VERBOSE) {
        const message = "__ start:" + tag + " __\n" + payloadData + "\n__ end:" + tag + " __";
        this.log(message, logLevel);
    }

    constructor() {
    }

    private _log(message: string, logLevel: LogLevel): void {
        if (null == message) {
            message = "";
        }

        switch (logLevel) {
            case LogLevel.STATUS:
                console.info(message);
                break;

            case LogLevel.ERROR:
                console.error(message);
                break;

            case LogLevel.VERBOSE:
            case undefined: /* Default to verbose logging for this case*/
                console.log(message);
                break;

            case LogLevel.STACKTRACE:
                console.trace(message);
                break;

            case LogLevel.INFO:
                console.info(message);
                break;

            default:
                console.error("Logger: LogLevel out of range!");
                console.trace();
                break;
        }
    }
}
