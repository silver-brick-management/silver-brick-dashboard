// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

// Packaging the action to ensure cleaner code in the components
@Injectable()
export class UrlRedirectActions {
    public static SAVE_REDIRECT = 'SAVE_REDIRECT';
    public static CLEAR_REDIRECT = 'CLEAR_REDIRECT';

    SaveRedirect(url: string): Action {
        return {
            type: UrlRedirectActions.SAVE_REDIRECT,
            payload: url
        };
    }

    ClearRedirect(): Action {
        return {
            type: UrlRedirectActions.CLEAR_REDIRECT
        };
    }
}
