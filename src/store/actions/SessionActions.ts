/** @format */

// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from "@ngrx/store";
import { Injectable } from "@angular/core";
import { ICard, cards, customers, IShippingInformation } from "stripe";

// Local modules
import {
    IPaginateQueryStrings,
    ITaskDialogResponse,
    IGetBldgHelper,
} from "../../interfaces/IParams";
import {
    AssignTaskHelper,
    AddMessageHelper,
    ITaskFeedJoin,
    IServerResponseGetUsers,
} from "../../interfaces/IResponse";
import {
    IUser,
    ITaskFeed,
    IService,
    IBooking,
    ITask,
} from "../../shared/SilverBrickTypes";

export enum SessionActionType {
    SESSION_INIT,
    SESSION_NO_ACTION,
    GET_USERS_SCOPE,
    GET_USERS_SCOPE_SUCCEEDED,
    GET_USERS_SCOPE_FAILED,
    UPDATE_STRIPE_TOKEN,
    UPDATE_STRIPE_TOKEN_SUCCESS,
    UPDATE_STRIPE_TOKEN_FAIL,
    GET_BOOKINGS,
    GET_BOOKINGS_SUCCESS,
    GET_BOOKINGS_FAIL,
    GET_STAFF_BOOKINGS,
    GET_STAFF_BOOKINGS_SUCCESS,
    GET_STAFF_BOOKINGS_FAIL,
    GET_BUILDING_TASKS,
    GET_BUILDING_TASKS_SUCCESS,
    GET_BUILDING_TASKS_FAIL,
    GET_SERVICES,
    GET_SERVICES_SUCCESS,
    GET_SERVICES_FAIL,
    GET_TASKS,
    GET_TASKS_SUCCESS,
    GET_TASKS_FAIL,
    GET_LANDLORD_TASKS,
    GET_LANDLORD_TASKS_SUCCESS,
    GET_LANDLORD_TASKS_FAIL,
    GET_LANDLORD_TASKS_COMPLETED,
    GET_LANDLORD_TASKS_COMPLETED_SUCCESS,
    GET_LANDLORD_TASKS_COMPLETED_FAIL,
    GET_TENANT_TASKS,
    GET_TENANT_TASKS_SUCCESS,
    GET_TENANT_TASKS_FAIL,
    CREATE_TASK,
    CREATE_TASK_SUCCESS,
    CREATE_TASK_FAIL,
    UPDATE_TASK,
    UPDATE_TASK_SUCCESS,
    UPDATE_TASK_FAIL,
    CREATE_RECURRING_TASK,
    CREATE_RECURRING_TASK_SUCCESS,
    CREATE_RECURRING_TASK_FAIL,
    UPDATE_RECURRING_TASK,
    UPDATE_RECURRING_TASK_SUCCESS,
    UPDATE_RECURRING_TASK_FAIL,
    DELETE_TASK,
    DELETE_TASK_SUCCESS,
    DELETE_TASK_FAIL,
    GET_CLOSED_TASKS,
    GET_CLOSED_TASKS_SUCCESS,
    GET_CLOSED_TASKS_FAILED,
    ASSIGN_TASK,
    ASSIGN_TASK_SUCCESS,
    ASSIGN_TASK_FAIL,
    GET_TASK_FEED,
    GET_TASK_FEED_SUCCESS,
    GET_TASK_FEED_FAIL,
    ADD_TASK_MESSAGE,
    ADD_TASK_MESSAGE_SUCCESS,
    ADD_TASK_MESSAGE_FAIL,
    GET_ALL_HISTORY,
    GET_ALL_HISTORY_SUCCESS,
    GET_ALL_HISTORY_FAIL,
    GET_STAFF_HISTORY,
    GET_STAFF_HISTORY_SUCCESS,
    GET_STAFF_HISTORY_FAIL,
    SCOPE_BUIDLING_TASKS,
    SCOPE_RECURRING_TASKS,
    SCOPE_BUILDING_HISTORY,
    GET_RECURRING_TASKS,
    GET_RECURRING_TASKS_SUCCESS,
    GET_RECURRING_TASKS_FAIL,
    GET_RECURRING_STAFF_TASKS,
    GET_RECURRING_STAFF_TASKS_SUCCESS,
    GET_RECURRING_STAFF_TASKS_FAIL,
    COMPLETE_TASK,
    COMPLETE_TASK_SUCCESS,
    COMPLETE_TASK_FAIL,
    COMPLETE_RECURRING_TASK,
    COMPLETE_RECURRING_TASK_SUCCESS,
    COMPLETE_RECURRING_TASK_FAIL,
    REOPEN_TASK,
    REOPEN_TASK_SUCCESS,
    REOPEN_TASK_FAIL,
    REOPEN_RECURRING_TASK,
    REOPEN_RECURRING_TASK_SUCCESS,
    REOPEN_RECURRING_TASK_FAIL,
    SCOPE_STAFF,
    SCOPE_STAFF_TASKS,
    APPROVE_HISTORY,
    APPROVE_HISTORY_SUCCESS,
    APPROVE_HISTORY_FAIL,
    UPDATE_HISTORY,
    UPDATE_HISTORY_SUCCESS,
    UPDATE_HISTORY_FAIL,
    CLEAN_UP_SEARCH,
    CHOOSE_DATE,
    SORT_REVERSE_TASKS,
    SEARCH_TASKS,
    UPDATE_CALENDAR_EVENTS,
    GET_CALENDAR_COLORS,
    GET_CALENDAR_COLORS_SUCCESS,
    GET_CALENDAR_COLORS_FAIL,
    UPDATE_MAP_EVENTS,
    CHANGE_SEARCH,
    CHANGE_ACTIVE_DATE,
    SCOPE_TASK_TYPE
}

@Injectable()
export class SessionActions {
    InitializeSession(user: IUser): Action {
        return {
            type: SessionActionType[SessionActionType.SESSION_INIT],
            payload: user,
        };
    }

    GetUsersScope(queryStrings: IPaginateQueryStrings): Action {
        return {
            type: SessionActionType[SessionActionType.GET_USERS_SCOPE],
            payload: queryStrings,
        };
    }

    GetUsersScopeSuccess(getResponse: IServerResponseGetUsers): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_USERS_SCOPE_SUCCEEDED
            ],
            payload: getResponse,
        };
    }

    GetUsersScopeFail(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_USERS_SCOPE_FAILED],
            payload: error,
        };
    }

    GetClosedTasks(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_CLOSED_TASKS],
        };
    }

    GetClosedTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_CLOSED_TASKS_SUCCESS],
            payload: tasks,
        };
    }

    GetClosedTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_CLOSED_TASKS_FAILED],
            payload: error,
        };
    }

    GetBookings(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS],
        };
    }

    GetBookingsSucceeded(bookings: ITask[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS_SUCCESS],
            payload: bookings,
        };
    }

    GetBookingsFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS_FAIL],
            payload: error,
        };
    }

    GetStaffBookings(helper: IGetBldgHelper): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS],
            payload: helper,
        };
    }

    GetStaffBookingsSucceeded(bookings: ITask[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS_SUCCESS],
            payload: bookings,
        };
    }

    GetStaffBookingsFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BOOKINGS_FAIL],
            payload: error,
        };
    }

    GetBuildingTasks(helper: IGetBldgHelper): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BUILDING_TASKS],
            payload: helper,
        };
    }

    GetBuildingTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_BUILDING_TASKS_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetBuildingTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_BUILDING_TASKS_FAIL],
            payload: error,
        };
    }

    GetTasks(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASKS],
        };
    }

    GetTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASKS_SUCCESS],
            payload: tasks,
        };
    }

    GetTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASKS_FAIL],
            payload: error,
        };
    }

    GetLandlordTasks(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_LANDLORD_TASKS],
        };
    }

    GetLandlordTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_LANDLORD_TASKS_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetLandlordTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_LANDLORD_TASKS_FAIL],
            payload: error,
        };
    }

    GetLandlordTasksCompleted(): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_LANDLORD_TASKS_COMPLETED
            ],
        };
    }

    GetLandlordTasksCompletedSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_LANDLORD_TASKS_COMPLETED_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetLandlordTasksCompletedFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_LANDLORD_TASKS_COMPLETED_FAIL
            ],
            payload: error,
        };
    }

    GetRecurringTasks(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_RECURRING_TASKS],
        };
    }

    GetRecurringTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_RECURRING_TASKS_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetRecurringTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_RECURRING_TASKS_FAIL],
            payload: error,
        };
    }

    GetRecurringStaffTasks(helper: IGetBldgHelper): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_RECURRING_STAFF_TASKS
            ],
            payload: helper,
        };
    }

    GetRecurringStaffTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_RECURRING_STAFF_TASKS_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetRecurringStaffTasksFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_RECURRING_STAFF_TASKS_FAIL
            ],
            payload: error,
        };
    }

    GetStaffTasks(id: string): Action {
        return {
            type: SessionActionType[SessionActionType.GET_STAFF_BOOKINGS],
            payload: id,
        };
    }

    GetStaffTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_STAFF_BOOKINGS_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetStaffTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_FAIL],
            payload: error,
        };
    }

    GetTenantTasks(id: string): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TENANT_TASKS],
            payload: id,
        };
    }

    GetTenantTasksSucceeded(tasks: ITask[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TENANT_TASKS_SUCCESS],
            payload: tasks,
        };
    }

    GetTenantTasksFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TENANT_TASKS_FAIL],
            payload: error,
        };
    }

    GetTaskFeed(helper: ITaskFeedJoin): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASK_FEED],
            payload: helper,
        };
    }

    GetTaskFeedSucceeded(tasks: ITaskFeed[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASK_FEED_SUCCESS],
            payload: tasks,
        };
    }

    GetTaskFeedFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_TASK_FEED_FAIL],
            payload: error,
        };
    }

    GetAllHistory(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_ALL_HISTORY],
        };
    }

    GetAllHistorySucceeded(tasks: ITaskFeed[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_ALL_HISTORY_SUCCESS],
            payload: tasks,
        };
    }

    GetAllHistoryFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_ALL_HISTORY_FAIL],
            payload: error,
        };
    }

    GetStaffHistory(id: string): Action {
        return {
            type: SessionActionType[SessionActionType.GET_STAFF_HISTORY],
            payload: id,
        };
    }

    GetStaffHistorySucceeded(tasks: ITaskFeed[]): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_STAFF_HISTORY_SUCCESS
            ],
            payload: tasks,
        };
    }

    GetStaffHistoryFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_STAFF_HISTORY_FAIL],
            payload: error,
        };
    }

    CreateTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.CREATE_TASK],
            payload: task,
        };
    }

    CreateTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.CREATE_TASK_SUCCESS],
            payload: task,
        };
    }

    CreateTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.CREATE_TASK_FAIL],
            payload: error,
        };
    }

    UpdateTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_TASK],
            payload: task,
        };
    }

    UpdateTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_TASK_SUCCESS],
            payload: task,
        };
    }

    UpdateTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_TASK_FAIL],
            payload: error,
        };
    }

    CreateRecurringTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.CREATE_RECURRING_TASK],
            payload: task,
        };
    }

    CreateRecurringTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[
                SessionActionType.CREATE_RECURRING_TASK_SUCCESS
            ],
            payload: task,
        };
    }

    CreateRecurringTaskFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.CREATE_RECURRING_TASK_FAIL
            ],
            payload: error,
        };
    }

    UpdateRecurringTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_RECURRING_TASK],
            payload: task,
        };
    }

    UpdateRecurringTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[
                SessionActionType.UPDATE_RECURRING_TASK_SUCCESS
            ],
            payload: task,
        };
    }

    UpdateRecurringTaskFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.UPDATE_RECURRING_TASK_FAIL
            ],
            payload: error,
        };
    }

    AssignTask(task: AssignTaskHelper): Action {
        return {
            type: SessionActionType[SessionActionType.ASSIGN_TASK],
            payload: task,
        };
    }

    AssignTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.ASSIGN_TASK_SUCCESS],
            payload: task,
        };
    }

    AssignTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.ASSIGN_TASK_FAIL],
            payload: error,
        };
    }

    AddTaskMessage(task: AddMessageHelper): Action {
        return {
            type: SessionActionType[SessionActionType.ADD_TASK_MESSAGE],
            payload: task,
        };
    }

    AddTaskMessageSucceeded(task: ITaskFeed): Action {
        return {
            type: SessionActionType[SessionActionType.ADD_TASK_MESSAGE_SUCCESS],
            payload: task,
        };
    }

    AddTaskMessageFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.ADD_TASK_MESSAGE_FAIL],
            payload: error,
        };
    }

    GetServices(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_SERVICES],
        };
    }

    GetServicesSucceeded(services: IService[]): Action {
        return {
            type: SessionActionType[SessionActionType.GET_SERVICES_SUCCESS],
            payload: services,
        };
    }

    GetServicesFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_SERVICES_FAIL],
            payload: error,
        };
    }

    DeleteTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.DELETE_TASK],
            payload: task,
        };
    }

    DeleteTaskSucceeded(taskID: string): Action {
        return {
            type: SessionActionType[SessionActionType.DELETE_TASK_SUCCESS],
            payload: taskID
        };
    }

    DeleteTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.DELETE_TASK_FAIL],
            payload: error,
        };
    }

    ScopeByTaskType(type: string): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_TASK_TYPE],
            payload: type,
        };
    }

    ScopeBuildingTasks(helper: IGetBldgHelper): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_BUIDLING_TASKS],
            payload: helper,
        };
    }

    ScopeBuildingHistory(helper: IGetBldgHelper): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_BUILDING_HISTORY],
            payload: helper,
        };
    }

    ScopeRecurringTasks(user: string): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_RECURRING_TASKS],
            payload: user,
        };
    }

    ScopeStaffTasks(user: IUser): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_STAFF_TASKS],
            payload: user,
        };
    }

    ScopeStaff(searchStr: string): Action {
        return {
            type: SessionActionType[SessionActionType.SCOPE_STAFF],
            payload: searchStr,
        };
    }

    CleanUpSearch(): Action {
        return {
            type: SessionActionType[SessionActionType.CLEAN_UP_SEARCH],
        };
    }

    SearchTasks(searchStr: string): Action {
        return {
            type: SessionActionType[SessionActionType.SEARCH_TASKS],
            payload: searchStr,
        };
    }

    UpdateEvents(): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_CALENDAR_EVENTS],
        };
    }

    UpdateMapEvents(): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_MAP_EVENTS],
        };
    }

    CompleteTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.COMPLETE_TASK],
            payload: task,
        };
    }

    CompleteTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.COMPLETE_TASK_SUCCESS],
            payload: task,
        };
    }

    CompleteTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.COMPLETE_TASK_FAIL],
            payload: error,
        };
    }

    CompleteRecurringTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK],
            payload: task,
        };
    }

    CompleteRecurringTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[
                SessionActionType.COMPLETE_RECURRING_TASK_SUCCESS
            ],
            payload: task,
        };
    }

    CompleteRecurringTaskFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.COMPLETE_RECURRING_TASK_FAIL
            ],
            payload: error,
        };
    }

    ReopenTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.REOPEN_TASK],
            payload: task,
        };
    }

    ReopenTaskSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.REOPEN_TASK_SUCCESS],
            payload: task,
        };
    }

    ReopenTaskFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.REOPEN_TASK_FAIL],
            payload: error,
        };
    }

    ReopenRecurringTask(task: ITask): Action {
        return {
            type: SessionActionType[SessionActionType.REOPEN_RECURRING_TASK],
            payload: task,
        };
    }

    ReopenRecurringSucceeded(task: ITask): Action {
        return {
            type: SessionActionType[
                SessionActionType.REOPEN_RECURRING_TASK_SUCCESS
            ],
            payload: task,
        };
    }

    ReopenRecurringFailed(error: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.REOPEN_RECURRING_TASK_FAIL
            ],
            payload: error,
        };
    }

    ApproveHistory(helper: AddMessageHelper): Action {
        return {
            type: SessionActionType[SessionActionType.APPROVE_HISTORY],
            payload: helper,
        };
    }

    ApproveHistorySucceeded(task: ITaskFeed): Action {
        return {
            type: SessionActionType[SessionActionType.APPROVE_HISTORY_SUCCESS],
            payload: task,
        };
    }

    ApproveHistoryFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.APPROVE_HISTORY_FAIL],
            payload: error,
        };
    }

    UpdateHistory(helper: AddMessageHelper): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_HISTORY],
            payload: helper,
        };
    }

    UpdateHistorySucceeded(task: ITaskFeed): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_HISTORY_SUCCESS],
            payload: task,
        };
    }

    UpdateHistoryFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.UPDATE_HISTORY_FAIL],
            payload: error,
        };
    }

    GetCalendarColors(): Action {
        return {
            type: SessionActionType[SessionActionType.GET_CALENDAR_COLORS],
        };
    }

    GetCalendarColorsSucceeded(colors: any): Action {
        return {
            type: SessionActionType[
                SessionActionType.GET_CALENDAR_COLORS_SUCCESS
            ],
            payload: colors,
        };
    }

    GetCalendarColorsFailed(error: any): Action {
        return {
            type: SessionActionType[SessionActionType.GET_CALENDAR_COLORS_FAIL],
            payload: error,
        };
    }

    SortTasksByDateReverseTwice(): Action {
        return {
            type: SessionActionType[SessionActionType.SORT_REVERSE_TASKS]
        };
    }

    ChangeSearch(search: string): Action {
        return {
            type: SessionActionType[SessionActionType.CHANGE_SEARCH],
            payload: search
        }
    }

    ChangeActiveDate(date: Date): Action {
        return {
            type: SessionActionType[SessionActionType.CHANGE_ACTIVE_DATE],
            payload: date
        }
    }

    SessionNoAction(): Action {
        return {
            type: SessionActionType[SessionActionType.SESSION_NO_ACTION],
        };
    }
}
