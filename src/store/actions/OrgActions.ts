// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

// Local modules
import { IAddBldgHelper, IGetBldgHelper } from '../../interfaces/IParams';
import { IOrgBasic, IOrgInfo, IBuilding } from '../../shared/SilverBrickTypes';
import { IServerResponseCreateOrg } from '../../interfaces/IResponse';

// Packaging the action to ensure cleaner code in the components
@Injectable()
export class OrgActions {
    public static GET_ALL_ORGS = 'GET_ALL_ORGS';
    public static GET_ALL_ORGS_SUCCESS = 'GET_ALL_ORGS_SUCCESS';
    public static GET_ALL_ORGS_FAILURE = 'GET_ALL_ORGS_FAILURE';
    public static GET_ALL_ORGS_SCOPE = 'GET_ALL_ORGS_SCOPE';
    public static GET_ALL_ORGS_SCOPE_SUCCESS = 'GET_ALL_ORGS_SCOPE_SUCCESS';
    public static GET_ALL_ORGS_SCOPE_FAILURE = 'GET_ALL_ORGS_SCOPE_FAILURE';
    public static GET_ORGS_INFO = 'GET_ALL_ORGS_INFO';
    public static GET_ORGS_INFO_SUCCESS = 'GET_ALL_ORGS_INFO_SUCCESS';
    public static GET_ORGS_INFO_FAILURE = 'GET_ALL_ORGS_INFO_FAILURE';
    public static ADD_ORG = 'ADD_ORG';
    public static ADD_ORG_SUCCESS = 'ADD_ORG_SUCCESS';
    public static ADD_ORG_FAILURE = 'ADD_ORG_FAILURE';
    public static UPDATE_ORG = 'UPDATE_ORG';
    public static UPDATE_ORG_SUCCESS = 'UPDATE_ORG_SUCCESS';
    public static UPDATE_ORG_FAILURE = 'UPDATE_ORG_FAILURE';
    public static DELETE_ORG = 'DELETE_ORG';
    public static DELETE_ORG_SUCCESS = 'DELETE_ORG_SUCCESS';
    public static DELETE_ORG_FAILURE = 'DELETE_ORG_FAILURE';
    public static ADD_BLDG = 'ADD_BLDG';
    public static ADD_BLDG_SUCCESS = 'ADD_BLDG_SUCCESS';
    public static ADD_BLDG_FAILURE = 'ADD_BLDG_FAILURE';
    public static UPDATE_BLDG = 'UPDATE_BLDG';
    public static UPDATE_BLDG_SUCCESS = 'UPDATE_BLDG_SUCCESS';
    public static UPDATE_BLDG_FAILURE = 'UPDATE_BLDG_FAILURE';
    public static GET_BLDG = 'GET_BLDG';
    public static GET_BLDG_SUCCESS = 'GET_BLDG_SUCCESS';
    public static GET_BLDG_FAILURE = 'GET_BLDG_FAILURE';
    public static IMPORT_USERS = 'IMPORT_USERS';
    public static IMPORT_USERS_SUCCESS = 'IMPORT_USERS_SUCCESS';
    public static ORG_STATE_LOADING = 'ORG_STATE_LOADING';
    public static SEARCH_BUILDINGS = 'SCOPE_BUILDINGS';
    public static SEARCH_ORGS = 'SEARCH_ORGS';
    public static CLEAN_UP_SEARCH = "CLEAN_UP_SEARCH";

    GetAllOrgs(): Action {
        return {
            type: OrgActions.GET_ALL_ORGS
        };
    }

    GetAllOrgsSuccess(orgs: IOrgBasic[]): Action {
        return {
            type: OrgActions.GET_ALL_ORGS_SUCCESS,
            payload: orgs
        };
    }

    GetAllOrgsFail(error: any): Action {
        return {
            type: OrgActions.GET_ALL_ORGS_FAILURE,
            payload: error
        };
    }

    GetAllOrgsScope(): Action {
        return {
            type: OrgActions.GET_ALL_ORGS_SCOPE
        };
    }

    GetAllOrgsScopeSuccess(orgs: IOrgBasic[]): Action {
        return {
            type: OrgActions.GET_ALL_ORGS_SCOPE_SUCCESS,
            payload: orgs
        };
    }

    GetAllOrgsScopeFail(error: any): Action {
        return {
            type: OrgActions.GET_ALL_ORGS_SCOPE_FAILURE,
            payload: error
        };
    }

    GetOrgInfo(orgID: string): Action {
        return {
            type: OrgActions.GET_ORGS_INFO,
            payload: orgID
        };
    }

    GetOrgSuccessInfo(orgs: IOrgInfo): Action {
        return {
            type: OrgActions.GET_ORGS_INFO_SUCCESS,
            payload: orgs
        };
    }

    GetOrgFailInfo(error: any): Action {
        return {
            type: OrgActions.GET_ORGS_INFO_FAILURE,
            payload: error
        };
    }

    AddOrg(org: IOrgInfo): Action {
        return {
            type: OrgActions.ADD_ORG,
            payload: org
        };
    }

    AddOrgSuccess(newOrg: IOrgBasic): Action {
        console.log("AddOrgSuccess", newOrg)
        return {
            type: OrgActions.ADD_ORG_SUCCESS,
            payload: newOrg
        };
    }

    AddOrgFail(error: any): Action {
        return {
            type: OrgActions.ADD_ORG_FAILURE,
            payload: error
        };
    }

    UpdateOrg(org: IOrgInfo): Action {
        return {
            type: OrgActions.UPDATE_ORG,
            payload: org
        };
    }

    UpdateOrgSuccess(newOrg: IOrgBasic): Action {
        return {
            type: OrgActions.UPDATE_ORG_SUCCESS,
            payload: newOrg
        };
    }

    UpdateOrgFail(error: any): Action {
        return {
            type: OrgActions.UPDATE_ORG_FAILURE,
            payload: error
        };
    }

    DeleteOrg(orgID: string): Action {
        return {
            type: OrgActions.DELETE_ORG,
            payload: orgID
        };
    }

    DeleteOrgSuccess(orgID: string): Action {
        return {
            type: OrgActions.DELETE_ORG_SUCCESS,
            payload: orgID
        };
    }

    DeleteOrgFail(error: any): Action {
        return {
            type: OrgActions.DELETE_ORG_FAILURE,
            payload: error
        };
    }

    AddBldg(bldg: IAddBldgHelper): Action {
        return {
            type: OrgActions.ADD_BLDG,
            payload: bldg
        };
    }

    AddBldgSuccess(newBldg: IBuilding): Action {
        return {
            type: OrgActions.ADD_BLDG_SUCCESS,
            payload: newBldg
        };
    }

    AddBldgFail(error: any): Action {
        return {
            type: OrgActions.ADD_BLDG_FAILURE,
            payload: error
        };
    }

    UpdateBldg(bldg: IAddBldgHelper): Action {
        return {
            type: OrgActions.UPDATE_BLDG,
            payload: bldg
        };
    }

    UpdateBldgSuccess(bldg: IBuilding): Action {
        return {
            type: OrgActions.UPDATE_BLDG_SUCCESS,
            payload: bldg
        };
    }

    UpdateBldgFail(error: any): Action {
        return {
            type: OrgActions.UPDATE_BLDG_FAILURE,
            payload: error
        };
    }

    GetBldg(bldg: IGetBldgHelper): Action {
        return {
            type: OrgActions.GET_BLDG,
            payload: bldg
        };
    }

    GetBldgSuccess(bldg: IBuilding): Action {
        return {
            type: OrgActions.GET_BLDG_SUCCESS,
            payload: bldg
        };
    }

    GetBldgFail(error: any): Action {
        return {
            type: OrgActions.GET_BLDG_FAILURE,
            payload: error
        };
    }

    SearchBuildings(str: string): Action {
        return {
            type: OrgActions.SEARCH_BUILDINGS,
            payload: str
        };
    }

    SearchOrgs(str: string): Action {
        return {
            type: OrgActions.SEARCH_ORGS,
            payload: str
        };
    }

    CleanUpSearch(): Action {
        return {
            type: OrgActions.CLEAN_UP_SEARCH
        }
    }


    // ImportUsers(): Action {
    //     return {
    //         type: OrgActions.IMPORT_USERS
    //     };
    // }

    // ImportUsersSuccess(): Action {
    //     return {
    //         type: OrgActions.IMPORT_USERS_SUCCESS
    //     };
    // }

    OrgStateLoading(): Action {
        return {
            type: OrgActions.ORG_STATE_LOADING
        };
    }
}
