// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

// Local modules
import { IUser } from '../../shared/SilverBrickTypes';
import { IPaginateQueryStrings, IUserActivationParam, IGetBldgHelper } from '../../interfaces/IParams';
import {
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseImportUsers,
    IServerResponseGetBuildingUsers,
    ITimeCardToday,
    ITimeCardRoster,
    GetTimeCardHelper
} from '../../interfaces/IResponse';

// Packaging the action to ensure cleaner code in the components
@Injectable()
export class UserActions {
    public static GET_USERS = 'GET_USERS';
    public static GET_USERS_FROM_FULL_URL = 'GET_USERS_FROM_FULL_URL';
    public static GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
    public static GET_USERS_FAIL = 'GET_USERS_FAIL';
    public static GET_USERS_SCOPE = 'GET_USERS_SCOPE';
    public static GET_USERS_FAIL_SCOPE = 'GET_USERS_FAIL_SCOPE';
    public static GET_USERS_SUCCESS_SCOPE = 'GET_USERS_SUCCESS_SCOPE';
    public static GET_BUILDING_USERS = 'GET_BUILDING_USERS';
    public static GET_BUILDING_USERS_SUCCESS = 'GET_BUILDING_USERS_SUCCESS';
    public static GET_BUILDING_USERS_FAIL = 'GET_BUILDING_USERS_FAIL';
    public static GET_BUILDING_TENANTS = 'GET_BUILDING_TENANTS';
    public static GET_BUILDING_TENANTS_SUCCESS = 'GET_BUILDING_TENANTS_SUCCESS';
    public static GET_BUILDING_TENANTS_FAIL = 'GET_BUILDING_TENANTS_FAIL';
    public static GET_USER = 'GET_USER';
    public static GET_USER_SUCCESS = 'GET_USER_SUCCESS';
    public static GET_USER_FAIL = 'GET_USER_FAIL';
    public static ADD_USER = 'ADD_USER';
    public static ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';
    public static ADD_USER_FAIL = 'ADD_USER_FAIL';
    public static IMPORT_USERS = 'IMPORT_USERS';
    public static IMPORT_USERS_SUCCESS = 'IMPORT_USERS_SUCCESS';
    public static IMPORT_USERS_FAIL = 'IMPORT_USERS_FAIL';
    public static UPDATE_USER = 'UPDATE_USER';
    public static UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
    public static UPDATE_USER_FAIL = 'UPDATE_USER_FAIL';
    public static ACTIVATE_USER = 'ACTIVATE_USER';
    public static ACTIVATE_USER_SUCCESS = 'ACTIVATE_USER_SUCCESS';
    public static ACTIVATE_USER_FAIL = 'ACTIVATE_USER_FAIL';
    public static DEACTIVATE_USER = 'DEACTIVATE_USER';
    public static DEACTIVATE_USER_SUCCESS = 'DEACTIVATE_USER_SUCCESS';
    public static DEACTIVATE_USER_FAIL = 'DEACTIVATE_USER_FAIL';
    public static DELETE_USER = 'DELETE_USER';
    public static DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
    public static DELETE_USER_FAIL = 'DELETE_USER_FAIL';
    public static SORT_USERS_BY_NAME = 'SORT_USERS_BY_NAME';
    public static SORT_USERS_BY_ACTIVITY = 'SORT_USERS_BY_ACTIVITY';
    public static GET_ALL_TENANTS = 'GET_ALL_TENANTS';
    public static GET_ALL_TENANTS_SUCCESS = 'GET_ALL_TENANTS_SUCCESS';
    public static GET_ALL_TENANTS_FAIL = 'GET_ALL_TENANTS_FAIL';
    public static GET_ALL_TENANTS_LANDLORD = 'GET_ALL_TENANTS_LANDLORD';
    public static GET_ALL_TENANTS_LANDLORD_SUCCESS = 'GET_ALL_TENANTS_LANDLORD_SUCCESS';
    public static GET_ALL_TENANTS_LANDLORD_FAIL = 'GET_ALL_TENANTS_LANDLORD_FAIL';
    public static GET_ALL_LANDLORDS = 'GET_ALL_LANDLORDS';
    public static GET_ALL_LANDLORDS_SUCCESS = 'GET_ALL_LANDLORDS_SUCCESS';
    public static GET_ALL_LANDLORDS_FAIL = 'GET_ALL_LANDLORDS_FAIL';
    public static SEARCH_ALL_TENANTS = 'SEARCH_ALL_TENANTS';
    public static SEARCH_ALL_STAFF = 'SEARCH_ALL_STAFF';
    public static UPDATE_USER_PASSWORD = 'UPDATE_USER_PASSWORD';
    public static UPDATE_USER_PASSWORD_SUCCESS = 'UPDATE_USER_PASSWORD_SUCCESS';
    public static UPDATE_USER_PASSWORD_FAIL = 'UPDATE_USER_PASSWORD_FAIL';
    public static GET_ALL_TIME_CARDS = 'GET_ALL_TIME_CARDS';
    public static GET_ALL_TIME_CARDS_SUCCESS = 'GET_ALL_TIME_CARDS_SUCCESS';
    public static GET_ALL_TIME_CARDS_FAIL = 'GET_ALL_TIME_CARDS_FAIL';
    public static SCOPE_BUILDING_STAFF = 'SCOPE_BUILDING_STAFF';
    public static SCOPE_BUILDING_TENANTS = 'SCOPE_BUILDING_TENANTS';
    public static SCOPE_BUILDING_LANDLORDS = 'SCOPE_BUILDING_LANDLORDS';
    public static CLEAN_UP_SEARCH = "CLEAN_UP_SEARCH";
    public static SEND_USER_INVITE = 'SEND_USER_INVITE';
    public static SEND_USER_INVITE_SUCCESS = 'SEND_USER_INVITE_SUCCESS';
    public static SEND_USER_INVITE_FAIL = 'SEND_USER_INVITE_FAIL';

    GetUsers(queryStrings: IPaginateQueryStrings): Action {
        return {
            type: UserActions.GET_USERS,
            payload: queryStrings
        };
    }

    GetUsersFromFullUrl(url: string): Action {
        return {
            type: UserActions.GET_USERS_FROM_FULL_URL,
            payload: url
        };
    }

    GetUsersSuccess(getResponse: IServerResponseGetUsers): Action {
        return {
            type: UserActions.GET_USERS_SUCCESS,
            payload: getResponse
        };
    }

    GetUsersFail(error: any): Action {
        return {
            type: UserActions.GET_USERS_FAIL,
            payload: error
        };
    }

    // GetUsersScope(queryStrings: IPaginateQueryStrings): Action {
    //     return {
    //         type: UserActions.GET_USERS_SCOPE,
    //         payload: queryStrings
    //     };
    // }

    // GetUsersScopeSuccess(getResponse: IServerResponseGetUsers): Action {
    //     return {
    //         type: UserActions.GET_USERS_SUCCESS_SCOPE,
    //         payload: getResponse
    //     };
    // }

    // GetUsersScopeFail(error: any): Action {
    //     return {
    //         type: UserActions.GET_USERS_FAIL_SCOPE,
    //         payload: error
    //     };
    // }

    GetBuildingUsers(helper: IGetBldgHelper): Action {
        return {
            type: UserActions.GET_BUILDING_USERS,
            payload: helper
        };
    }

    GetBuildingUsersSuccess(getResponse: IServerResponseGetBuildingUsers): Action {
        return {
            type: UserActions.GET_BUILDING_USERS_SUCCESS,
            payload: getResponse
        };
    }

    GetBuildingUsersFail(error: any): Action {
        return {
            type: UserActions.GET_BUILDING_USERS_FAIL,
            payload: error
        };
    }

    GetAllTenants(): Action {
        return {
            type: UserActions.GET_ALL_TENANTS
        };
    }

    GetAllTenantsSuccess(getResponse: IUser[]): Action {
        return {
            type: UserActions.GET_ALL_TENANTS_SUCCESS,
            payload: getResponse
        };
    }

    GetAllTenantsFail(error: any): Action {
        return {
            type: UserActions.GET_ALL_TENANTS_FAIL,
            payload: error
        };
    }

    GetAllTenantsLandlord(): Action {
        return {
            type: UserActions.GET_ALL_TENANTS_LANDLORD
        };
    }

    GetAllTenantsLandlordSuccess(getResponse: IUser[]): Action {
        return {
            type: UserActions.GET_ALL_TENANTS_LANDLORD_SUCCESS,
            payload: getResponse
        };
    }

    GetAllTenantsLandlordFail(error: any): Action {
        return {
            type: UserActions.GET_ALL_TENANTS_LANDLORD_FAIL,
            payload: error
        };
    }

    GetAllLandlords(): Action {
        return {
            type: UserActions.GET_ALL_LANDLORDS
        };
    }

    GetAllLandlordsSuccess(getResponse: IUser[]): Action {
        return {
            type: UserActions.GET_ALL_LANDLORDS_SUCCESS,
            payload: getResponse
        };
    }

    GetAllLandlordsFail(error: any): Action {
        return {
            type: UserActions.GET_ALL_LANDLORDS_FAIL,
            payload: error
        };
    }

    GetBuildingTenants(helper: IGetBldgHelper): Action {
        return {
            type: UserActions.GET_BUILDING_TENANTS,
            payload: helper
        };
    }

    GetBuildingTenantsSuccess(getResponse: IServerResponseGetBuildingUsers): Action {
        return {
            type: UserActions.GET_BUILDING_TENANTS_SUCCESS,
            payload: getResponse
        };
    }

    GetBuildingTenantsFail(error: any): Action {
        return {
            type: UserActions.GET_BUILDING_TENANTS_FAIL,
            payload: error
        };
    }

     GetUser(uidOrEmail: string): Action {
        return {
            type: UserActions.GET_USER,
            payload: uidOrEmail
        };
    }

    GetUserSuccess(addResponse: IServerResponseAddOrUpdateOrGetUser): Action {
        return {
            type: UserActions.GET_USER_SUCCESS,
            payload: addResponse
        };
    }

    GetUserFail(error: any): Action {
        return {
            type: UserActions.GET_USER_FAIL,
            payload: error
        };
    }

    AddUser(user: IUser): Action {
        return {
            type: UserActions.ADD_USER,
            payload: user
        };
    }

    AddUserSuccess(addResponse: IServerResponseAddOrUpdateOrGetUser): Action {
        return {
            type: UserActions.ADD_USER_SUCCESS,
            payload: addResponse
        };
    }

    AddUserFail(error: any): Action {
        return {
            type: UserActions.ADD_USER_FAIL,
            payload: error
        };
    }

    ImportUsers(users: IUser[]): Action {
        return {
            type: UserActions.IMPORT_USERS,
            payload: users
        };
    }

    ImportUsersSuccess(importSuccess: IUser[]): Action {
        return {
            type: UserActions.IMPORT_USERS_SUCCESS,
            payload: importSuccess
        };
    }

    ImportUsersFail(error: any): Action {
        return {
            type: UserActions.IMPORT_USERS_FAIL,
            payload: error
        };
    }

    UpdateUser(user: IUser): Action {
        return {
            type: UserActions.UPDATE_USER,
            payload: user
        };
    }

    UpdateUserSuccess(updateResponse: IServerResponseAddOrUpdateOrGetUser): Action {
        return {
            type: UserActions.UPDATE_USER_SUCCESS,
            payload: updateResponse
        };
    }

    UpdateUserFail(error: any): Action {
        return {
            type: UserActions.UPDATE_USER_FAIL,
            payload: error
        };
    }

    UpdateUserPassword(user: IUser): Action {
        return {
            type: UserActions.UPDATE_USER_PASSWORD,
            payload: user
        };
    }

    UpdateUserPasswordSuccess(updateResponse: IServerResponseAddOrUpdateOrGetUser): Action {
        return {
            type: UserActions.UPDATE_USER_PASSWORD_SUCCESS,
            payload: updateResponse
        };
    }

    UpdateUserPasswordFail(error: any): Action {
        return {
            type: UserActions.UPDATE_USER_PASSWORD_FAIL,
            payload: error
        };
    }

    ActivateUser(uid: string): Action {
        return {
            type: UserActions.ACTIVATE_USER,
            payload: uid
        };
    }

    ActivateUserSuccess(activationParam: IUserActivationParam): Action {
        return {
            type: UserActions.ACTIVATE_USER_SUCCESS,
            payload: activationParam
        };
    }

    ActivateUserFail(error: any): Action {
        return {
            type: UserActions.ACTIVATE_USER_FAIL,
            payload: error
        };
    }

    DeactivateUser(uid: string): Action {
        return {
            type: UserActions.DEACTIVATE_USER,
            payload: uid
        };
    }

    DeactivateUserSuccess(activationParam: IUserActivationParam): Action {
        return {
            type: UserActions.DEACTIVATE_USER_SUCCESS,
            payload: activationParam
        };
    }

    DeactivateUserFail(error: any): Action {
        return {
            type: UserActions.DEACTIVATE_USER_FAIL,
            payload: error
        };
    }

    DeleteUser(uid: string): Action {
        return {
            type: UserActions.DELETE_USER,
            payload: uid
        };
    }

    DeleteUserSuccess(uid: string): Action {
        return {
            type: UserActions.DELETE_USER_SUCCESS,
            payload: uid
        };
    }

    DeleteUserFail(error: any): Action {
        return {
            type: UserActions.DELETE_USER_FAIL,
            payload: error
        };
    }

    SendUserInvite(user: IUser): Action {
        return {
            type: UserActions.SEND_USER_INVITE,
            payload: user
        };
    }

    SendUserInviteSuccess(user: IUser): Action {
        return {
            type: UserActions.SEND_USER_INVITE_SUCCESS,
            payload: user
        };
    }

    SendUserInviteFail(error: any): Action {
        return {
            type: UserActions.SEND_USER_INVITE_FAIL,
            payload: error
        };
    }

    GetAllTimeCards(helper: GetTimeCardHelper): Action {
        return {
            type: UserActions.GET_ALL_TIME_CARDS,
            payload: helper
        };
    }

    GetAllTimeCardsSuccess(cards: ITimeCardRoster): Action {
        return {
            type: UserActions.GET_ALL_TIME_CARDS_SUCCESS,
            payload: cards
        };
    }

    GetAllTimeCardsFail(error: any): Action {
        return {
            type: UserActions.GET_ALL_TIME_CARDS_FAIL,
            payload: error
        };
    }

    SearchAllTenants(str: string): Action {
        return {
            type: UserActions.SEARCH_ALL_TENANTS,
            payload: str
        };
    }

    SearchAllStaff(str: string): Action {
        return {
            type: UserActions.SEARCH_ALL_STAFF,
            payload: str
        };
    }

    ScopeBuildingStaff(helper: IGetBldgHelper): Action {
        return {
            type: UserActions.SCOPE_BUILDING_STAFF,
            payload: helper
        }
    }

    ScopeBuildingLandlords(helper: IGetBldgHelper): Action {
        return {
            type: UserActions.SCOPE_BUILDING_LANDLORDS,
            payload: helper
        }
    }

    ScopeBuildingTenants(helper: IGetBldgHelper): Action {
        return {
            type: UserActions.SCOPE_BUILDING_TENANTS,
            payload: helper
        }
    }

    CleanUpSearch(): Action {
        return {
            type: UserActions.CLEAN_UP_SEARCH
        }
    }

    SortByName(): Action {
        return {
            type: UserActions.SORT_USERS_BY_NAME
        };
    }

    SortByActivity(): Action {
        return {
            type: UserActions.SORT_USERS_BY_ACTIVITY
        };
    }
}
