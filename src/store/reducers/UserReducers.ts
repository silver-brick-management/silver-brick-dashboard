// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action, Reducer } from '@ngrx/store';

// Local modules
import { UserActions } from '../actions/UserActions';
import { IUser } from '../../shared/SilverBrickTypes';
import { IUserState, UserStateType } from '../../interfaces/IStoreState';
import { IUserActivationParam, ICredentials, IGetBldgHelper } from '../../interfaces/IParams';
import { ArrayUtil } from "../../common/ArrayUtil";

import {
    IServerResponseGetProfile,
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseGetBuildingUsers,
    ITimeCardToday,
    ITimeCardRoster
} from '../../interfaces/IResponse';
import { UserUtil } from '../../common/UserUtil';

const initialUsersState: IUserState = {
    error: null,
    loading: null,
    limit: "25",
    next: null,
    previous: null,
    invitedUser: null,
    prevCursor: null,
    nextCursor: null,
    users: [],
    searchStaff: [],
    buildingUsers: [],
    searchTenants: [],
    tenants: [],
    allLandlords: [],
    masterLandlords: [],
    masterTenants: [],
    timeCards: [],
    allTenants: [],
    allUsers: [],
    usersScope: [],
    credentials: null,
    createdUser: null,
    type: UserStateType.NONE
};

export class UserReducers {
    static Reduce(usersState: IUserState = initialUsersState, action: any): IUserState {
        switch (action.type) {

            case UserActions.ADD_USER:
            case UserActions.GET_USER:
            case UserActions.GET_USERS:
            case UserActions.GET_ALL_TENANTS_LANDLORD:
            case UserActions.GET_ALL_TENANTS:
            case UserActions.UPDATE_USER:
            case UserActions.UPDATE_USER_PASSWORD:
            case UserActions.GET_ALL_TIME_CARDS:
            case UserActions.DELETE_USER:
            case UserActions.GET_ALL_LANDLORDS:
            case UserActions.ACTIVATE_USER:
            case UserActions.DEACTIVATE_USER:
            case UserActions.GET_USERS_FROM_FULL_URL:
            case UserActions.SEND_USER_INVITE:
            case UserActions.GET_BUILDING_USERS:
            case UserActions.GET_BUILDING_TENANTS:
            {
                return Object.assign({}, usersState, { error: null, loading: true, type: UserStateType.LOADING });
            }

            case UserActions.GET_USERS_SUCCESS:
            {
                const getManyResponse: IServerResponseGetUsers = action.payload;
                let sortedUsers: IUser[] = UserUtil.SortByLastActivity(getManyResponse.data);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    next: getManyResponse.paging.next,
                    previous: getManyResponse.paging.previous,
                    prevCursor: getManyResponse.paging.cursors.prevCursor,
                    nextCursor: getManyResponse.paging.cursors.nextCursor,
                    allUsers: (null != getManyResponse.data) ? UserUtil.SortByLastActivity(getManyResponse.data).map(user => user) : usersState.allUsers,
                    users: (null != getManyResponse.data) ? UserUtil.SortByLastActivity(getManyResponse.data).map(user => user) : usersState.users,
                    searchStaff: ((null != getManyResponse.data) && (getManyResponse.data.length > 0)) ? getManyResponse.data : usersState.searchStaff,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_BUILDING_USERS_SUCCESS:
            {
                const getBldgUsersResponse: IServerResponseGetBuildingUsers = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    next: usersState.next,
                    previous: usersState.previous,
                    prevCursor: usersState.prevCursor,
                    nextCursor: usersState.nextCursor,
                    buildingUsers: (null != getBldgUsersResponse.data) ? UserUtil.SortByLastActivity(getBldgUsersResponse.data).map(user => user) : [],
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_USERS_SUCCESS_SCOPE:
            {
                const getAllOrgsResponse: IServerResponseGetUsers = action.payload;
                let allBuildings: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }, { uid: 'none', firstName: "Un", lastName: "Assigned" }, ...UserUtil.SortByFirstName(getAllOrgsResponse.data)];
                
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    usersScope: ((null != getAllOrgsResponse.data) && (getAllOrgsResponse.data.length > 0)) ? allBuildings : usersState.usersScope,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_BUILDING_TENANTS_SUCCESS:
            {
                const getBldgUsersResponse: IServerResponseGetBuildingUsers = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    tenants: (null != getBldgUsersResponse.data) ? UserUtil.SortByLastActivity(getBldgUsersResponse.data).map(user => user) : [],
                    searchTenants: getBldgUsersResponse.data,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_ALL_TENANTS_LANDLORD_SUCCESS:
            {
                const getBldgUsersResponse: IUser[] = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    tenants:getBldgUsersResponse,
                    searchTenants: getBldgUsersResponse,
                    type: UserStateType.GET_USERS_SUCCEEDED
                });
            }

            case UserActions.GET_ALL_TENANTS_SUCCESS:
            {
                const getBldgUsersResponse: IUser[] = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    masterTenants: (null != getBldgUsersResponse) ? UserUtil.SortByLastActivity(getBldgUsersResponse).map(user => user) : [],
                    allTenants: (null != getBldgUsersResponse) ? UserUtil.SortByLastActivity(getBldgUsersResponse).map(user => user) : [],
                    searchTenants: getBldgUsersResponse,
                    type: UserStateType.GET_ALL_TENANTS_SUCCEEDED
                });
            }

            case UserActions.GET_ALL_LANDLORDS_SUCCESS:
            {
                const getBldgUsersResponse: IUser[] = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    masterLandlords: (null != getBldgUsersResponse) ? UserUtil.SortByLastActivity(getBldgUsersResponse).map(user => user) : [],
                    allLandlords: (null != getBldgUsersResponse) ? UserUtil.SortByLastActivity(getBldgUsersResponse).map(user => user) : [],
                    type: UserStateType.GET_ALL_LANDLORDS_SUCCEEDED
                });
            }

            case UserActions.GET_USER_SUCCESS:
            {
                const getOneResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;

                return Object.assign({}, initialUsersState, {
                    loading: false,
                    users: [...initialUsersState.users, Object.assign({}, getOneResponse.data)]
                });
            }

            case UserActions.SORT_USERS_BY_NAME:
            {

                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    allUsers: UserUtil.SortByName(usersState.allUsers),
                    users: UserUtil.SortByName(usersState.allUsers)
                });
            }

            case UserActions.SORT_USERS_BY_ACTIVITY:
            {
                console.log("SORT_USERS_BY_ACTIVITY");
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    allUsers: UserUtil.SortByLastActivity(usersState.allUsers),
                    users: UserUtil.SortByLastActivity(usersState.allUsers)
                });
            }

            case UserActions.GET_ALL_TIME_CARDS_SUCCESS:
            {
                const timeCards: ITimeCardRoster = action.payload;
                let keys: string[] = Object.keys(timeCards);
                let rosters: ITimeCardToday[] = [];
                for (let key of keys) {
                    timeCards[key].cards = ArrayUtil.Transform(timeCards[key].cards);
                    rosters.push(timeCards[key]);
                }
                return Object.assign({}, initialUsersState, {
                    loading: false,
                    timeCards: rosters,
                    type: UserStateType.GET_TIME_CARDS_SUCCEEDED
                });
            }

            case UserActions.ADD_USER_SUCCESS:
            {
                const addResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                console.log("User", addResponse.data);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: [...usersState.users, Object.assign({}, addResponse.data)]
                });
            }

            case UserActions.IMPORT_USERS_SUCCESS:
            {
                const importedUsers: IUser[] = action.payload;
                // console.log("IMPORT_USERS_SUCCESS", importedUsers.length);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: [...usersState.users, Object.assign({}, importedUsers)]
                });
            }

            case UserActions.SEND_USER_INVITE_SUCCESS:
            {
                const user: IUser = action.payload;
                // console.log("IMPORT_USERS_SUCCESS", importedUsers.length);
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    invitedUser: user,
                    type: UserStateType.SEND_USER_INVITE_SUCCEEDED
                });
            }

            case UserActions.UPDATE_USER_SUCCESS:
            {
                const updateResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: usersState.users.map((user: IUser) => {
                        return user.uid === updateResponse.data.uid ? Object.assign({}, user, updateResponse.data) : user;
                    })
                });
            }

            case UserActions.UPDATE_USER_PASSWORD_SUCCESS:
            {
                const updateResponse: IServerResponseAddOrUpdateOrGetUser = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    type: UserStateType.UPDATE_USER_SUCCEEDED
                });
            }

            case UserActions.DELETE_USER_SUCCESS:
            {
                const deleteUid: string = action.payload;
                let match: IUser = null;
                let tenantMatch: IUser = null;
                let staffMatch: IUser = null;
                let partialState: any = {
                    error: null,
                    loading: false
                };
                match = usersState.allLandlords.find((usr: IUser) => {
                    return (usr.uid === deleteUid);
                });
                if (null != match) {
                    partialState.masterLandlords = usersState.masterLandlords.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                    partialState.allLandlords = usersState.allLandlords.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                }

                tenantMatch = usersState.allTenants.find((usr: IUser) => {
                    return (usr.uid === deleteUid);
                });
                if (null != tenantMatch) {
                    partialState.allTenants = usersState.allTenants.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                    partialState.masterTenants = usersState.masterTenants.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                }

                staffMatch = usersState.allUsers.find((usr: IUser) => {
                    return (usr.uid === deleteUid);
                });
                if (null != staffMatch) {
                    partialState.allUsers = usersState.allUsers.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                    partialState.users = usersState.users.filter((user: IUser) => {
                        return user.uid !== deleteUid;
                    });
                }

                return Object.assign({}, usersState, partialState);
            }

            case UserActions.ACTIVATE_USER_SUCCESS:
            case UserActions.DEACTIVATE_USER_SUCCESS:
            {
                const activationParam: IUserActivationParam = action.payload;
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    users: usersState.users.map((user: IUser) => {
                        return (user.uid === activationParam.uid) ?
                            Object.assign({}, user, {isActive: activationParam.activationStatus }) :
                            user;
                    })
                });
            }

            case UserActions.SEARCH_ALL_TENANTS:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = usersState.allTenants;
                let search: IUser[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = usersState.allTenants;
                }
                console.log("SEARCH_ALL_TENANTS", search, userVals, searchStr);

                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchTenants: (null != search) ? search : []
                };
                return Object.assign({}, usersState, partialFilterState);
            }

            case UserActions.SEARCH_ALL_STAFF:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = usersState.allUsers;
                let search: IUser[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = usersState.users;
                }
                console.log("SEARCH_ALL_STAFF", search, userVals, searchStr);
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchStaff: (null != search) ? search : []
                };
                return Object.assign({}, usersState, partialFilterState);
            }

            case UserActions.SCOPE_BUILDING_TENANTS: {
                const helper: IGetBldgHelper= action.payload;
                console.log("SCOPE_BUILDING_HISTORY", helper);
                let scopeTask: IUser[] = [];
                if ((null != usersState.masterTenants) && (usersState.masterTenants.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = usersState.masterTenants;
                    } else {
                        usersState.masterTenants.forEach((task: IUser) => {
                            if ((null != task.orgs) && (null != task.orgs.buildings) && (null != task.orgs.buildings[0])) {
                                if (task.orgs.buildings[0].id === helper.buildingID) {
                                    scopeTask.push(task);
                                }
                            }
                        })
                    }
                }
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    allTenants: (null != scopeTask) ? UserUtil.SortByDate(ArrayUtil.Transform(scopeTask)) : []
                });
            }

            case UserActions.SCOPE_BUILDING_LANDLORDS: {
                const helper: IGetBldgHelper= action.payload;
                let scopeTask: IUser[] = [];
                if ((null != usersState.masterLandlords) && (usersState.masterLandlords.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = usersState.masterLandlords;
                    } else {
                        usersState.masterLandlords.forEach((task: IUser) => {
                            if ((null != task.orgs) && (null != task.orgs.buildings) && (null != task.orgs.buildings[0])) {
                                if (task.orgs.buildings[0].id === helper.buildingID) {
                                    scopeTask.push(task);
                                }
                            }
                        })
                    }
                }
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    allLandlords: (null != scopeTask) ? UserUtil.SortByDate(ArrayUtil.Transform(scopeTask)) : []
                });
            }

            case UserActions.CLEAN_UP_SEARCH: 
            {
                return Object.assign({}, usersState, {
                    error: null,
                    loading: false,
                    searchStaff: usersState.allUsers,
                    searchTenants: usersState.allTenants
                });
            }

            case UserActions.ADD_USER_FAIL:
            case UserActions.GET_USER_FAIL:
            case UserActions.GET_USERS_FAIL:
            case UserActions.UPDATE_USER_FAIL:
            case UserActions.DELETE_USER_FAIL:
            case UserActions.ACTIVATE_USER_FAIL:
            case UserActions.DEACTIVATE_USER_FAIL:
            case UserActions.GET_BUILDING_USERS_FAIL:
            case UserActions.GET_ALL_LANDLORDS_FAIL:
            case UserActions.UPDATE_USER_PASSWORD_FAIL:
            case UserActions.GET_ALL_TENANTS_FAIL:
            {
                return Object.assign({}, usersState, { error: action.payload, loading: false });
            }

            default:
            {
                return usersState;
            }
        }
    }
}
