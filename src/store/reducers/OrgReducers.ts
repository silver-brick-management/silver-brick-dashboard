// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action, Reducer } from '@ngrx/store';

// Local modules
import { OrgActions } from '../actions/OrgActions';
import { IOrgBasic, IBuildingSimple, IBuildingBasic, IBuilding } from '../../shared/SilverBrickTypes';
import { IOrgState, IStateBase, OrgStateType } from '../../interfaces/IStoreState';
import {
    IServerResponseGetAllOrgs,
    IServerResponseCreateOrg,
    IServerResponseGetOrgInfo
} from '../../interfaces/IResponse';
import { ArrayUtil } from "../../common/ArrayUtil";
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

const initialOrgState: IOrgState = {
    error: null,
    loading: null,
    orgs: [],
    orgsInfo: [],
    allBuildings: [],
    allOrgs: [],
    searchOrgs: [],
    searchBuildings: [],
    selectedOrg: null,
    selectedOrgBldgs: [],
    selectedBldg: null,
    type: OrgStateType.NONE
};

export class OrgReducers {
    static Reduce(orgState: IOrgState = initialOrgState, action: any): IOrgState {
        switch (action.type) {
            case OrgActions.GET_ALL_ORGS:
            case OrgActions.ADD_ORG:
            case OrgActions.GET_ORGS_INFO:
            case OrgActions.UPDATE_ORG:
            case OrgActions.DELETE_ORG:
            case OrgActions.ADD_BLDG:
            case OrgActions.UPDATE_BLDG:
            case OrgActions.GET_BLDG:
            case OrgActions.ORG_STATE_LOADING:
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: true,
                    type: OrgStateType.LOADING
                });
            }

            case OrgActions.GET_ALL_ORGS_SUCCESS:
            {
                let getAllOrgsResponse: IOrgBasic[] = action.payload;
                getAllOrgsResponse = OrgBldgUtil.SortByName(getAllOrgsResponse);
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    orgs: getAllOrgsResponse,
                    allOrgs: getAllOrgsResponse,
                    searchOrgs: getAllOrgsResponse,
                    type: OrgStateType.GET_ALL_ORGS_SUCCEEDED
                });
            }

            case OrgActions.GET_ALL_ORGS_SCOPE_SUCCESS:
            {
                let getAllOrgsResponse: IOrgBasic[] = action.payload;
                getAllOrgsResponse = OrgBldgUtil.SortByName(getAllOrgsResponse);
                let allBuildings: any[] = [{ id: 'all', name: "All" }];
                if (null != getAllOrgsResponse) {
                    for (let org of getAllOrgsResponse) {
                        if (null != org.buildings) {
                            allBuildings = [...allBuildings, ...org.buildings];
                            // for (let building of buildings) {
                            //     building.orgID = org.id;
                            //     allBuildings.push(building);
                            // }
                        }
                    }
                }
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    allBuildings: allBuildings,
                    searchBuildings: allBuildings,
                    allOrgs: getAllOrgsResponse,
                    searchOrgs: getAllOrgsResponse,
                    type: OrgStateType.GET_ALL_ORGS_SUCCEEDED
                });
            }

            case OrgActions.GET_ORGS_INFO_SUCCESS:
            {
                const getOrgInfoResponse: IServerResponseGetOrgInfo = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedOrg: getOrgInfoResponse 
                });
            }

            case OrgActions.ADD_ORG_SUCCESS:
            {
                const createOrgResponse: IOrgBasic = action.payload;
                const newOrgs: IOrgBasic[] = [...orgState.orgs, createOrgResponse];
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    orgs: newOrgs,
                    searchOrgs: newOrgs,
                    type: OrgStateType.ADD_ORG_SUCCEEDED
                });
            }

            case OrgActions.UPDATE_ORG_SUCCESS:
            {
                const updateOrgResponse: IOrgBasic = action.payload;
                let partialUpdateState: IStateBase = {
                    loading: false,
                    error: null
                };

                 Object.assign(partialUpdateState, {
                        orgs: orgState.orgs.map((org: IOrgBasic) => {
                            return (org.id === updateOrgResponse.id) ? Object.assign({}, org, updateOrgResponse) : org;
                        }),
                        type: OrgStateType.UPDATE_ORG_SUCCEEDED

                    });
                return Object.assign({}, orgState, partialUpdateState);
            }

            case OrgActions.DELETE_ORG_SUCCESS:
            {
                const orgID: string = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    orgs: orgState.orgs.filter((org: IOrgBasic) => {
                        return org.id !== orgID;
                    }),
                    type: OrgStateType.DELETE_ORG_SUCCEEDED
                });
            }

            case OrgActions.ADD_BLDG_SUCCESS:
            {
                const createBldgResponse: IBuilding = action.payload;
                const bldgs: IBuilding[] = [...orgState.selectedOrgBldgs, createBldgResponse];
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedOrgBldgs: bldgs,
                    type: OrgStateType.ADD_BLDG_SUCCEEDED
                });
            }

            case OrgActions.GET_BLDG_SUCCESS:
            {
                const getBldgResponse: IBuilding = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedBldg: getBldgResponse,
                    type: OrgStateType.GET_BUILDING_SUCCEEDED
                });
            }

            case OrgActions.UPDATE_BLDG_SUCCESS:
            {
                const updateOrgResponse: IBuilding = action.payload;
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    selectedBldg: updateOrgResponse,
                    type: OrgStateType.UPDATE_BLDG_SUCCEEDED
                });
            }

            case OrgActions.SEARCH_BUILDINGS:
            {
                const searchStr: string = action.payload;
                let userVals: IBuildingSimple[] = orgState.allBuildings;
                let search: IBuildingSimple[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IBuildingSimple[] = userVals.filter((map: IBuildingSimple) => {
                        return ((null != map) && (null != map.name)) ? (map.name.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1) : null;
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = orgState.allBuildings;
                }
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchBuildings: (null != search) ? search : []
                };
                return Object.assign({}, orgState, partialFilterState);
            }

            case OrgActions.SEARCH_ORGS:
            {
                const searchStr: string = action.payload;
                let userVals: IOrgBasic[] = orgState.allOrgs;
                let search: IOrgBasic[] = [];
                console.log("SearchOrgs", userVals);
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IOrgBasic[] = userVals.filter((map: IOrgBasic) => {
                        return ((null != map) && (null != map.name)) ? (map.name.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1) : null;
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = orgState.allOrgs;
                }
                console.log("search", search);
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    searchOrgs: (null != search) ? search : []
                };
                return Object.assign({}, orgState, partialFilterState);
            }

            case OrgActions.CLEAN_UP_SEARCH: 
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    searchOrgs: orgState.allOrgs,
                    searchBuildings: orgState.allBuildings
                });
            }

            case OrgActions.IMPORT_USERS_SUCCESS:
            {
                return Object.assign({}, orgState, {
                    error: null,
                    loading: false,
                    type: OrgStateType.IMPORT_USERS_SUCCEEDED
                });
            }
            
            default:
            {
                return orgState;
            }
        };
    }
}
