// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action, Reducer } from '@ngrx/store';

// Local modules
import { UrlRedirectActions } from '../actions/UrlRedirectActions';

export class UrlRedirectReducers {
    static Reduce(urlRedirectState: string = null, action: any): string {
        switch (action.type) {
            case UrlRedirectActions.SAVE_REDIRECT:
            {
                console.log("SAVE_REDUR", action.payload);
                return action.payload;
            }

            case UrlRedirectActions.CLEAR_REDIRECT:
            {
                return null;
            }

            default:
            {
                return urlRedirectState;
            }
        }
    }
}
