// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Action, Reducer } from "@ngrx/store";
import { ICard, IShippingInformation, customers } from "stripe";

// Local modules
import { SessionActions, SessionActionType } from "../actions/SessionActions";
import { IUser, IAssignee, RecurringEvent, ITaskFeed, IUserOrg, IUserBuilding, IUserAudience, IUserOrgBuildingBase, IService, IBooking, ITask } from "../../shared/SilverBrickTypes";
import { SessionStateType, IStateBase, ISessionState } from "../../interfaces/IStoreState";
import { ArrayUtil } from "../../common/ArrayUtil";
import { RouteUtil } from "../../common/RouteUtil";
import { UserUtil } from "../../common/UserUtil";
import { IGetBldgHelper } from '../../interfaces/IParams';
import { IServerResponseGetUsers } from '../../interfaces/IResponse';

interface IPartialSessionState extends IStateBase {
    isOrgAdmin?: boolean;
    isBldgAdmin?: boolean;
    username?: string;
    org?: IUserOrg;
    userPosition?: string;
    uid?: string;
    orgID?: string;
    buildingID?: string;
    user?: IUser;
    type?: SessionStateType;
    services?: IService[];
    bookings?: IBooking[];
    tenantBookings?: IBooking[];
    staffBookings?: IBooking[];
    tasks?: ITask[];
    taskFeed?: ITaskFeed[];
    allHistory?: ITaskFeed[];
    allTasks?: ITask[];
    recurringTasks?: ITask[];
    allRecurringTasks?: ITask[];
    searchTasks?: ITask[];
    selectedStaff?: IUser;
    usersScope?: Array<IUser>;
    completedTasks?: Array<ITask>;
    allUsersScope?: Array<IUser>;
    calendarColors?: any;
    closedTasks?: ITask[];
    closedRecurringTasks?: ITask[];
    searchType?: string;
    activeDate?: Date;
    mapEvents?: Array<RecurringEvent>;
    selectedBuilding?: string;
    deletedTaskID?: string;
    updatedTask?: ITask;
    taskTypeScope?: string;
}

const initialSessionState: ISessionState = {
    error: null,
    loading: null,
    username: null,
    uid: null,
    orgID: null,
    updatedTask: null,
    isBldgAdmin: null,
    isOrgAdmin: null,
    buildingID: null,
    userPosition: null,
    email: null,
    user: null,
    org: null,
    services: [],
    bookings: [],
    tenantBookings: [],
    tasks: [],
    allHistory: [],
    taskFeed: [],
    allTasks: [],
    recurringTasks: [],
    completedTasks: [],
    mapEvents: [],
    activeDate: null,
    allRecurringTasks: [],
    closedTasks: [],
    calendarColors: null,
    allUsersScope: [],
    usersScope: [],
    searchTasks: [],
    searchType: "Staff",
    selectedBuilding: null,
    deletedTaskID: null,
    selectedStaff: null,
    closedRecurringTasks: [],
    staffBookings: [],
    taskTypeScope: "All",
    type: SessionStateType.NONE,
};

export class SessionReducers {
    static Reduce(sessionState: ISessionState = initialSessionState, action: any): ISessionState {
        let partialState: IPartialSessionState;
        switch (action.type) {
            case SessionActionType[SessionActionType.SESSION_INIT]:
            {
                const user: IUser = action.payload;
                console.log("SESSION_DBA_INIT", user);
                let org: IUserOrg = user.orgs;
                const buildings: IUserBuilding[] = org.buildings;
                let isBldgAdmin: boolean = false;

                // Looking for an 'Any' isAdmin relationship
                for (let building of buildings) {
                    if (building.isAdmin) {
                        isBldgAdmin = true;
                        break;
                    } else {
                        // remains false
                    }
                }
                partialState = {
                    error: null,
                    loading: false,
                    userPosition: user.role ? user.role : "",
                    uid: user.uid,
                    isOrgAdmin: org.isAdmin,
                    isBldgAdmin: isBldgAdmin,
                    username: `${ user.firstName } ${ user.lastName}`,
                    org: org,
                    orgID: org.id,
                    buildingID: buildings[0].id,
                    user: user,
                    type: SessionStateType.SESSION_INIT_SUCCESS,
                };
                return Object.assign({}, sessionState, partialState);
            }

            case SessionActionType[SessionActionType.GET_USERS_SCOPE_SUCCEEDED]:
            {
                const getAllOrgsResponse: IServerResponseGetUsers = action.payload;
                let newerUsers = UserUtil.SortByFirstName(getAllOrgsResponse.data);
                let allBuildings: any[] = [{ uid: 'all', firstName: "All", lastName: "Staff" }, { uid: 'none', firstName: "Un", lastName: "Assigned" }, ...newerUsers];
                console.log("allBuildings", allBuildings);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    usersScope: allBuildings,
                    allUsersScope: ((null != getAllOrgsResponse.data) && (getAllOrgsResponse.data.length > 0)) ? allBuildings : sessionState.allUsersScope,
                    type: SessionStateType.GET_USERS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.GET_BOOKINGS_SUCCESS]: {
                let bookings: IBooking[] = action.payload;
                console.log("bookings", bookings);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    bookings: bookings,
                    type: SessionStateType.GET_BOOKINGS_SUCCESS,
                });
            }

            case SessionActionType[SessionActionType.GET_CALENDAR_COLORS_SUCCESS]: {
                let colors: any = action.payload;
                console.log("colors", colors);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    calendarColors: colors
                });
            }

            case SessionActionType[SessionActionType.GET_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                let finalTasks: ITask[] = [];
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                    if (sessionState.user.isAdmin) {
                        for (let task of tasks) {
                            if ((null != task.scheduled) && (!task.scheduled)) {
                                finalTasks.push(task);
                            }
                        }
                    } else {
                        finalTasks = tasks;
                    }
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: finalTasks,
                    allTasks: finalTasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_CLOSED_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                console.log("tasks", tasks);
                let finalTasks: ITask[] = [];
                let finalRecTasks: ITask[] = [];
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                    for (let task of tasks) {
                        if (task.recurring) {
                            finalRecTasks.push(task);
                        } else {
                            finalTasks.push(task);
                        }
                    }
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    closedRecurringTasks: finalRecTasks,
                    closedTasks: finalTasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_BUILDING_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    allTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_COMPLETED_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    completedTasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_RECURRING_TASKS_SUCCESS]:
            case SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS_SUCCESS]:
            {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }

                let scopeTask: ITask[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(tasks, assignee);
                    console.log("ScopeStaffTasks", tasks);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(tasks);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'all')) {
                    scopeTask = tasks;
                } else {
                    scopeTask = tasks;
                }
                if ((null != sessionState.selectedBuilding) && (sessionState.selectedBuilding !== "")) {
                    if (sessionState.selectedBuilding === "all") {
                    } else {
                        let newScopeTasks: ITask[] = scopeTask.filter((tsk: ITask) => {
                            return (tsk.buildingID === sessionState.selectedBuilding);
                        });
                        scopeTask = newScopeTasks;
                    }
                }
                let scopeUsers: IUser[] = sessionState.allUsersScope.filter((user: IUser) => {
                    return ((user.firstName !== 'All'));
                });
                if ((null != sessionState.user) && (!sessionState.user.isAdmin) && (sessionState.user.role === 'Staff')) {
                    scopeUsers = [ sessionState.user ];
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecurringTasks: UserUtil.SortTasksByDateReverse(tasks),
                    // usersScope: allBuildings,
                    // allUsersScope: allBuildings,
                    recurringTasks: UserUtil.SortTasksByDateReverse(scopeTask),
                    allTasks: tasks,
                    tasks: scopeTask,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.ASSIGN_TASK_SUCCESS]:
            {
                let assignedTask: ITask = action.payload;
                return Object.assign({}, sessionState, {
                    loading: false,
                    error: null,
                    recurringTasks: sessionState.recurringTasks.map((task: ITask) => {
                        return task.id === assignedTask.id ? Object.assign({}, task, assignedTask) : task;
                    })
                });
            }

            case SessionActionType[SessionActionType.UPDATE_CALENDAR_EVENTS]:
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecurringTasks: UserUtil.SortTasksByDateReverse(sessionState.allRecurringTasks),
                    recurringTasks: UserUtil.SortTasksByDateReverse(sessionState.recurringTasks),
                    allTasks: sessionState.allTasks,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.UPDATE_MAP_EVENTS]:
            {
                let scopeTask: ITask[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allRecurringTasks, assignee);
                }
                let scopedEvents: RecurringEvent[] = UserUtil.SetupEvents(scopeTask, sessionState.activeDate, sessionState.calendarColors);
                let finalEvents: RecurringEvent[] = [];
                if (scopedEvents.length < 25) {
                    finalEvents = RouteUtil.CalculateRoute(null, scopedEvents);
                } else {
                    finalEvents = scopedEvents;
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    mapEvents: scopedEvents,
                    type: SessionStateType.GET_MAP_EVENTS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.CHANGE_ACTIVE_DATE]:
            {
                let date: Date = action.payload;
                let scopeTask: ITask[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTask = UserUtil.ScopeStaffTasks(sessionState.allRecurringTasks, assignee);
                }
                let scopedEvents: RecurringEvent[] = UserUtil.SetupEvents(scopeTask, sessionState.activeDate, sessionState.calendarColors);
                let finalEvents: RecurringEvent[] = [];
                if (scopedEvents.length < 25) {
                    finalEvents = RouteUtil.CalculateRoute(null, scopedEvents);
                } else {
                    finalEvents = scopedEvents;
                }
                console.log("finalEvents", finalEvents);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    activeDate: date,
                    mapEvents: scopedEvents,
                    type: SessionStateType.CHANGE_ACTIVE_DATE_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.GET_TENANT_TASKS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: tasks,
                    type: SessionStateType.GET_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_SUCCESS]: {
                let tasks: ITask[] = action.payload;
                if (null != tasks) {
                    tasks = UserUtil.SortTasksByDate(tasks);
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecurringTasks: UserUtil.SortTasksByDateReverse(tasks),
                    recurringTasks: UserUtil.SortTasksByDateReverse(tasks),
                    tasks: tasks,
                    allTasks: tasks,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_TASK_FEED_SUCCESS]: {
                let tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.ADD_TASK_MESSAGE_SUCCESS]: {
                let tasks: ITaskFeed = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: [tasks, ...sessionState.taskFeed]
                });
            }

            case SessionActionType[SessionActionType.CREATE_TASK_SUCCESS]: {
                const task: ITask = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: ((null != task.scheduled) && (!task.scheduled) && (!task.recurring)) ? [task, ...sessionState.tasks] : sessionState.tasks,
                    allTasks: ((null != task.scheduled) && (!task.scheduled) && (!task.recurring)) ? [task, ...sessionState.allTasks] : sessionState.allTasks,
                    type: SessionStateType.CREATE_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.CREATE_RECURRING_TASK_SUCCESS]:
            {
                const task: ITask = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    recurringTasks: [task, ...sessionState.recurringTasks],
                    allRecurringTasks: [task, ...sessionState.allRecurringTasks],
                    type: SessionStateType.CREATE_RECURRING_TASKS_SUCCEEDED,
                });
            }

            case SessionActionType[SessionActionType.GET_ALL_HISTORY_SUCCESS]: {
                const tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allHistory: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : [],
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.GET_STAFF_HISTORY_SUCCESS]: {
                const tasks: ITaskFeed[] = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allHistory:  (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : [],
                    taskFeed: (null != tasks) ? UserUtil.SortByDate(ArrayUtil.Transform(tasks)) : []
                });
            }

            case SessionActionType[SessionActionType.APPROVE_HISTORY_SUCCESS]: {
                const taskFeed: ITaskFeed = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allHistory:  sessionState.allHistory.map((tsk: ITaskFeed) => {
                        return tsk.id === taskFeed.id ? Object.assign({}, tsk, taskFeed) : tsk;
                    }),
                    taskFeed: sessionState.taskFeed.map((tsk: ITaskFeed) => {
                        return tsk.id === taskFeed.id ? Object.assign({}, tsk, taskFeed) : tsk;
                    })
                });
            }

            case SessionActionType[SessionActionType.SCOPE_BUIDLING_TASKS]: {
                const helper: IGetBldgHelper = action.payload;
                let scopeTask: ITask[] = [];
                let scopeRegTasks: ITask[] = [];
                if ((null != sessionState.allTasks) && (sessionState.allTasks.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeRegTasks = sessionState.allTasks;
                    } else {
                        sessionState.allTasks.forEach((task: ITask) => {
                            if (task.buildingID === helper.buildingID) {
                                scopeRegTasks.push(task);
                            }
                        });
                    }
                }
                if ((null != sessionState.allRecurringTasks) && (sessionState.allRecurringTasks.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = sessionState.allRecurringTasks;
                    } else {
                        sessionState.allRecurringTasks.forEach((task: ITask) => {
                            if (task.buildingID === helper.buildingID) {
                                scopeTask.push(task);
                            }
                        });
                    }
                }
                let scopeTaskStaff: ITask[] = scopeTask;
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    let assignee: IAssignee = {
                        name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`,
                        uid: sessionState.selectedStaff.uid
                    };
                    scopeTaskStaff = UserUtil.ScopeStaffTasks(scopeTask, assignee);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTaskStaff = UserUtil.ScopeUnAssignedStaffTasks(scopeTask);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'all')) {
                    scopeTaskStaff = scopeTask;
                } else {
                    scopeTaskStaff = scopeTask;
                }
                console.log("SCOPE_BUIDLING_TASKS", scopeTask);
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: scopeRegTasks,
                    allTasks: sessionState.allTasks,
                    recurringTasks: UserUtil.SortTasksByDateReverse(scopeTaskStaff),
                    selectedBuilding: helper.buildingID,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.SEARCH_TASKS]: {
                const searchStr: string = action.payload;
                let tasks: ITask[] = sessionState.allTasks;
                let scopeTask: ITask[] = [];
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    if (sessionState.searchType === 'Staff') {
                        const search2: ITask[] = tasks.filter((map: ITask) => {
                            let assignees: IAssignee[] = map.assignees;
                            let fullName: string = `${map.subject};`;
                            let matches: IAssignee[] = assignees.filter((assign: IAssignee) => {
                                return (assign.name.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1)
                            });
                            return ((null != matches) && (matches.length > 0) ? map : null);
                        });
                        if (null != search2) {
                            scopeTask = search2;
                        }
                    } else {
                        const search2: ITask[] = tasks.filter((map: ITask) => {
                            let fullName: string = `${map.subject};`;
                            return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                        });
                        if (null != search2) {
                            scopeTask = search2;
                        }
                    }
                    
                } else {
                    scopeTask = sessionState.allTasks;
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    type: SessionStateType.SEARCH_TASKS_SUCCEEDED,
                    recurringTasks: UserUtil.SortTasksByDateReverse(scopeTask)
                });
            }

            case SessionActionType[SessionActionType.CHANGE_SEARCH]: {
                const searchStr: string = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    searchType: searchStr
                });
            }

            case SessionActionType[SessionActionType.SCOPE_TASK_TYPE]: {
                const taskType: string = action.payload;
                let tasks: ITask[] = sessionState.allTasks;
                let allTasks: ITask[] = sessionState.allTasks;
                let scopeTask: ITask[] = [];
                if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid !== 'all') && (sessionState.selectedStaff.uid !== 'none')) {
                    scopeTask = UserUtil.ScopeStaffTasks(tasks, { name: `${sessionState.selectedStaff.firstName} ${sessionState.selectedStaff.lastName}`, uid: sessionState.selectedStaff.uid });
                    console.log("scopeTask", scopeTask);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'none')) {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(tasks);
                } else if ((null != sessionState.selectedStaff) && (sessionState.selectedStaff.uid === 'all')) {
                    scopeTask = sessionState.allRecurringTasks;
                } else {
                    scopeTask = sessionState.allTasks;
                }
                if ((null != sessionState.selectedBuilding) && (sessionState.selectedBuilding !== "")) {
                    if (sessionState.selectedBuilding === "all") {
                    } else {
                        let newScopeTasks: ITask[] = scopeTask.filter((tsk: ITask) => {
                            return (tsk.buildingID === sessionState.selectedBuilding);
                        });
                        scopeTask = newScopeTasks;
                    }
                }
                if (taskType !== "All") {
                    scopeTask = UserUtil.ScopeTasksByType(scopeTask, taskType);
                } else {
                    
                }
                console.log("SCOPE_TASK_TYPE", taskType, scopeTask.length);
                let scopedEvents: RecurringEvent[] = UserUtil.SetupEvents(scopeTask, sessionState.activeDate, sessionState.calendarColors);
                let finalEvents: RecurringEvent[] = [];
                if (scopedEvents.length < 25) {
                    finalEvents = RouteUtil.CalculateRoute(null, scopedEvents);
                } else {
                    finalEvents = scopedEvents;
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskType: taskType,
                    allTasks: sessionState.allTasks,
                    recurringTasks: UserUtil.SortTasksByDateReverse(scopeTask),
                });
            }

            case SessionActionType[SessionActionType.SCOPE_STAFF_TASKS]: {
                const user: IUser = action.payload;
                let tasks: ITask[] = sessionState.allTasks;
                let allTasks: ITask[] = sessionState.allTasks;
                let scopeTask: ITask[] = [];
                if ((user.uid !== 'all') && (user.uid !== 'none')) {
                    scopeTask = UserUtil.ScopeStaffTasks(tasks, { name: `${user.firstName} ${user.lastName}`, uid: user.uid });
                    console.log("scopeTask", scopeTask);
                } else if (user.uid === 'none') {
                    scopeTask = UserUtil.ScopeUnAssignedStaffTasks(tasks);
                } else if (user.uid === 'all') {
                    scopeTask = sessionState.allRecurringTasks;
                } else {
                    scopeTask = sessionState.allTasks;
                }
                let scopedEvents: RecurringEvent[] = UserUtil.SetupEvents(scopeTask, sessionState.activeDate, sessionState.calendarColors);
                let finalEvents: RecurringEvent[] = [];
                if (scopedEvents.length < 25) {
                    finalEvents = RouteUtil.CalculateRoute(null, scopedEvents);
                } else {
                    finalEvents = scopedEvents;
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecurringTasks: UserUtil.SortTasksByDateReverse(allTasks),
                    recurringTasks: UserUtil.SortTasksByDateReverse(scopeTask),
                    selectedStaff: user,
                    mapEvents: scopedEvents,
                    type: SessionStateType.GET_RECURRING_TASKS_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.SCOPE_STAFF]:
            {
                const searchStr: string = action.payload;
                let userVals: IUser[] = sessionState.allUsersScope;
                let search: IUser[] = [];
                console.log("SCOPE_STAFF", searchStr);
                if ((null != searchStr) && (searchStr.trim() !== "")) {
                    const search2: IUser[] = userVals.filter((map: IUser) => {
                        let fullName: string = `${map.firstName} ${map.lastName};`
                        return (fullName.toLowerCase().indexOf(searchStr.toLowerCase()) > - 1);
                    });
                    if (null != search2) {
                        search = search2;
                    }
                } else {
                    search = sessionState.allUsersScope;
                }
                const partialFilterState: any = {
                    error: null,
                    loading: false,
                    usersScope: (null != search) ? search : []
                };
                return Object.assign({}, sessionState, partialFilterState);
            }

            case SessionActionType[SessionActionType.SCOPE_BUILDING_HISTORY]: {
                const helper: IGetBldgHelper= action.payload;
                let scopeTask: ITaskFeed[] = [];
                if ((null != sessionState.allHistory) && (sessionState.tasks.length > 0)) {
                    if (helper.buildingID === "all") {
                        scopeTask = sessionState.allHistory;
                    } else {
                        sessionState.allHistory.forEach((task: ITaskFeed) => {
                            if (task.buildingID === helper.buildingID) {
                                scopeTask.push(task);
                            }
                        })
                    }
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    taskFeed: (null != scopeTask) ? UserUtil.SortByDate(ArrayUtil.Transform(scopeTask)) : []
                });
            }

            case SessionActionType[SessionActionType.CLEAN_UP_SEARCH]: 
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    usersScope: sessionState.allUsersScope
                });
            }

            case SessionActionType[SessionActionType.UPDATE_TASK_SUCCESS]: 
            {
                let updatedTask: ITask = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: sessionState.tasks.map((task: ITask) => {
                        return task.id === updatedTask.id ? Object.assign({}, task, updatedTask) : task;
                    }),
                    allTasks: sessionState.allTasks.map((task: ITask) => {
                        return task.id === updatedTask.id ? Object.assign({}, task, updatedTask) : task;
                    }),
                });
            }

            case SessionActionType[SessionActionType.UPDATE_RECURRING_TASK_SUCCESS]: 
            {
                let updatedTask: ITask = action.payload;
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    deletedTaskID: updatedTask.id,
                    updatedTask: updatedTask,
                    type: SessionStateType.UPDATE_RECURRING_TASKS_SUCCEEDED,
                    recurringTasks: sessionState.recurringTasks.map((task: ITask) => {
                        return task.id === updatedTask.id ? Object.assign({}, task, updatedTask) : task;
                    }),
                    allRecurringTasks: sessionState.allRecurringTasks.map((task: ITask) => {
                        return task.id === updatedTask.id ? Object.assign({}, task, updatedTask) : task;
                    }),
                });
            }

            case SessionActionType[SessionActionType.SORT_REVERSE_TASKS]: 
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    tasks: UserUtil.SortTasksByDateReverseTwice(sessionState.tasks),
                    allTasks: UserUtil.SortTasksByDateReverseTwice(sessionState.allTasks),
                    closedRecurringTasks: UserUtil.SortTasksByDateReverseTwice(sessionState.closedRecurringTasks),
                    closedTasks: UserUtil.SortTasksByDateReverseTwice(sessionState.closedTasks),
                });
            }

            case SessionActionType[SessionActionType.DELETE_TASK]: 
            {
                let task: ITask = action.payload;
                let recurringTasks: ITask[] = sessionState.recurringTasks;
                console.log("DELETE_TASK", task.id);
                recurringTasks = recurringTasks.filter((tsk: ITask) => {
                    return (tsk.id !== task.id);
                });
                let allRecurringTasks: ITask[] = sessionState.allRecurringTasks;
                allRecurringTasks = allRecurringTasks.filter((tsk: ITask) => {
                    return (tsk.id !== task.id);
                });
                let allTasks: ITask[] = sessionState.allTasks;
                if (null != sessionState.allTasks) {
                    allTasks = allTasks
                }
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    allRecuringTasks: sessionState.allRecurringTasks.filter((tsk: ITask) => {
                        return (tsk.id !== task.id);
                    }),
                    recurringTasks: sessionState.recurringTasks.filter((tsk: ITask) => {
                        return (tsk.id !== task.id);
                    }),
                    allTasks: sessionState.allTasks.filter((tsk: ITask) => {
                        return (tsk.id !== task.id);
                    }),
                    deletedTaskID: task.id,
                    type: SessionStateType.DELETE_RECURRING_TASK_SUCCEEDED
                });
            }

            case SessionActionType[SessionActionType.SESSION_NO_ACTION]: 
            {
                return Object.assign({}, sessionState, {
                    error: null,
                    loading: false,
                    type: SessionStateType.LOADING,
                });
            }

            case SessionActionType[SessionActionType.GET_TASKS_FAIL]:
            case SessionActionType[SessionActionType.ADD_TASK_MESSAGE_FAIL]:
            case SessionActionType[SessionActionType.CREATE_RECURRING_TASK_FAIL]:
            case SessionActionType[SessionActionType.CREATE_TASK_FAIL]:
            case SessionActionType[SessionActionType.GET_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS_FAIL]:
            case SessionActionType[SessionActionType.UPDATE_TASK_FAIL]:
            case SessionActionType[SessionActionType.UPDATE_RECURRING_TASK_FAIL]:
            case SessionActionType[SessionActionType.GET_ALL_HISTORY_FAIL]:
            case SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_RECURRING_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_BUILDING_TASKS_FAIL]:
            case SessionActionType[SessionActionType.GET_STAFF_HISTORY_FAIL]:
            case SessionActionType[SessionActionType.GET_CLOSED_TASKS_FAILED]:
            case SessionActionType[SessionActionType.ASSIGN_TASK_FAIL]: 
            {
                return Object.assign({}, sessionState, { error: action.payload, loading: false });
            }

            case SessionActionType[SessionActionType.GET_TASKS]:
            case SessionActionType[SessionActionType.ADD_TASK_MESSAGE]:
            case SessionActionType[SessionActionType.CREATE_TASK]:
            case SessionActionType[SessionActionType.GET_TASKS]:
            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS]:
            case SessionActionType[SessionActionType.GET_CLOSED_TASKS]:
            case SessionActionType[SessionActionType.GET_LANDLORD_TASKS_COMPLETED]:
            case SessionActionType[SessionActionType.GET_STAFF_HISTORY]:
            case SessionActionType[SessionActionType.GET_TENANT_TASKS]:
            case SessionActionType[SessionActionType.GET_STAFF_BOOKINGS]:
            case SessionActionType[SessionActionType.GET_BUILDING_TASKS]:
            case SessionActionType[SessionActionType.REOPEN_TASK]:
            case SessionActionType[SessionActionType.REOPEN_RECURRING_TASK]:
            case SessionActionType[SessionActionType.COMPLETE_TASK]:
            case SessionActionType[SessionActionType.COMPLETE_RECURRING_TASK]:
            case SessionActionType[SessionActionType.UPDATE_TASK]:
            case SessionActionType[SessionActionType.GET_RECURRING_TASKS]:
            case SessionActionType[SessionActionType.GET_RECURRING_STAFF_TASKS]:
            case SessionActionType[SessionActionType.CREATE_RECURRING_TASK]:
            case SessionActionType[SessionActionType.UPDATE_RECURRING_TASK]:
            case SessionActionType[SessionActionType.GET_ALL_HISTORY]:
            case SessionActionType[SessionActionType.ASSIGN_TASK]: 
            {
                return Object.assign({}, sessionState, { error: null, loading: true });
            }

            default: 
            {
                return sessionState;
            }
        }
    }
}

