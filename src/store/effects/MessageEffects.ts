// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/fromPromise";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

// Local modules
import { AuthHttpWrapper } from '../../services/AuthHttpWrapper';
import { IMessage } from '../../shared/SilverBrickTypes';
import { AddMessageHelper, NewChatRoomHelper, UserChatRoom } from '../../interfaces/IResponse';
import { MessageActionTypes, MessageActions } from '../actions/MessageActions';
import { MessageSocket } from '../../services/MessageSocket';
import { OrgApi } from '../../services/OrgApi';
import { AccountApi } from '../../services/AccountApi';
import { IAddBldgHelper, IGetBldgHelper } from '../../interfaces/IParams';

@Injectable()
export class MessageEffects {

  constructor(
    private actions$: Actions,
    private _messageSocket: MessageSocket,
    private _messageActions: MessageActions,
    private _accountApi: AccountApi,
    private _orgApi: OrgApi,
    private _authHttpWrapper: AuthHttpWrapper) {

  }

  @Effect()
  joinChannel = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.JOIN_MESSAGE_CHANNEL])
    .switchMap((action) => {
      console.log("Joining message room", action.payload);
      const roomID: string = action.payload;
      return this._messageSocket.JoinRoom(roomID)
        .map(serverResponse => this._messageActions.GetMessageHistorySuccess(serverResponse.data))
        .catch(error => Observable.of(this._messageActions.MessageNoAction()));
    });

  @Effect()
  leaveChannel = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.LEAVE_MESSAGE_CHANNEL])
    .switchMap((action) => {
      return this._messageSocket.LeaveRoom()
        .map(() => this._messageActions.MessageNoAction());
    });

  @Effect()
  getHistory = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY])
    .switchMap((action) => {
      const roomID: string = action.payload.roomID;
      const cursor: string = action.payload.cursor;
      return this._messageSocket.GetMessageHistory(roomID, cursor)
        .map(() => this._messageActions.MessageNoAction());
    });

  @Effect()
  onGetHistorySuccess = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.GET_MESSAGE_HISTORY_LISTENER])
    .switchMap((action) => {
      return this._messageSocket.OnGetMessageHistorySuccess()
        .map(serverResponse => {
          return this._messageActions.GetMessageHistorySuccess(serverResponse.data);
        })
        .catch(error => Observable.of(this._messageActions.GetMessageHistoryFail(error)));
    });

  @Effect()
  addMessage = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.ADD_MESSAGE])
    .switchMap((action) => {
      const messageHelper: AddMessageHelper = action.payload;
      return this._messageSocket.AddNewMessage(messageHelper)
        .map(() => this._messageActions.MessageNoAction());
    });

  @Effect()
  onAddMessageSuccess = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.ADD_MESSAGE_LISTENER])
    .switchMap((action) => {
      return this._messageSocket.OnAddMessageSuccess()
        .map(serverResponse => {
          return this._messageActions.AddMessageSuccess(serverResponse.data);
        })
        .catch(error => Observable.of(this._messageActions.AddMessageFail(error)));
    });

  @Effect()
  newChatRoom = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM])
    .switchMap((action) => {
      const messageHelper: NewChatRoomHelper = action.payload;
      return this._messageSocket.NewChatRoom(messageHelper)
        .map(() => this._messageActions.MessageNoAction());
    });

  @Effect()
  onNewChatRoomSuccess = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.NEW_CHAT_ROOM_LISTENER])
    .switchMap((action) => {
      return this._messageSocket.OnNewChatRoomSuccess()
        .map(serverResponse => {
          return this._messageActions.NewChatRoomSuccess(serverResponse.data);
        })
        .catch(error => Observable.of(this._messageActions.NewChatRoomFail(error)));
    });

  @Effect()
  getChatRooms = this.actions$
    .ofType(MessageActionTypes[MessageActionTypes.GET_CHAT_ROOMS])
    .switchMap((action) => {
      return this._orgApi.GetChatRooms()
        .map(serverResponse => this._messageActions.GetChatRoomsSuccess(serverResponse.data))
        .catch(error => Observable.of(this._messageActions.GetChatRoomsFail(error)));
    });
}
