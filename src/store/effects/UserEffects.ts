// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/switchMap";

import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

// Local modules
import { UserActions } from '../actions/UserActions';
import { UserApi } from '../../services/UserApi';
import { IUser } from '../../shared/SilverBrickTypes';
import { IPaginateQueryStrings, IUserActivationParam, IGetBldgHelper } from '../../interfaces/IParams';
import {
    IServerResponseGetUsers,
    IServerResponseAddOrUpdateOrGetUser,
    IServerResponseMessage,
    IServerResponseImportUsers,
    IServerResponseGetBuildingUsers,
    GetTimeCardHelper
} from '../../interfaces/IResponse';

@Injectable()
export class UserEffects {
    constructor(
        private actions$: Actions,
        private _userApi: UserApi,
        private _userActions: UserActions) { }

    @Effect()
    getUsers = this.actions$
        .ofType(UserActions.GET_USERS)
        .switchMap((action) => {
            const queryStrings: IPaginateQueryStrings = action.payload;
            return this._userApi.GetUsers(queryStrings)
                .map(serverResponseGetUser => this._userActions.GetUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetUsersFail(error)));
        });

    // @Effect()
    // getUsersScope = this.actions$
    //     .ofType(UserActions.GET_USERS_SCOPE)
    //     .switchMap((action) => {
    //         const queryStrings: IPaginateQueryStrings = action.payload;
    //         return this._userApi.GetUsers(queryStrings)
    //             .map(serverResponseGetUser => this._userActions.GetUsersScopeSuccess(serverResponseGetUser))
    //             .catch(error => Observable.of(this._userActions.GetUsersScopeFail(error)));
    //     });

    @Effect()
    getBuildingUsers = this.actions$
        .ofType(UserActions.GET_BUILDING_USERS)
        .switchMap((action) => {
            const helper: IGetBldgHelper = action.payload;
            return this._userApi.GetBuildingUsers(helper)
                .map(serverResponseGetUser => this._userActions.GetBuildingUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetBuildingUsersFail(error)));
        });

    @Effect()
    getBuildingTenants = this.actions$
        .ofType(UserActions.GET_BUILDING_TENANTS)
        .switchMap((action) => {
            const helper: IGetBldgHelper = action.payload;
            return this._userApi.GetBuildingTenants(helper)
                .map(serverResponseGetUser => this._userActions.GetBuildingTenantsSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetBuildingTenantsFail(error)));
        });

    @Effect()
    getAllTenants = this.actions$
        .ofType(UserActions.GET_ALL_TENANTS)
        .switchMap((action) => {
            return this._userApi.GetAllTenants()
                .map(serverResponseGetUser => this._userActions.GetAllTenantsSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllTenantsFail(error)));
        });

    @Effect()
    getAllTenantsLandlord = this.actions$
        .ofType(UserActions.GET_ALL_TENANTS_LANDLORD)
        .switchMap((action) => {
            return this._userApi.GetLandlordTenants()
                .map(serverResponseGetUser => this._userActions.GetAllTenantsLandlordSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllTenantsLandlordFail(error)));
        });

    @Effect()
    getAllLandlords = this.actions$
        .ofType(UserActions.GET_ALL_LANDLORDS)
        .switchMap((action) => {
            return this._userApi.GetAllLandlords()
                .map(serverResponseGetUser => this._userActions.GetAllLandlordsSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllLandlordsFail(error)));
        });

    @Effect()
    getUsersFromFullUrl = this.actions$
        .ofType(UserActions.GET_USERS_FROM_FULL_URL)
        .switchMap((action) => {
            const url: string = action.payload;
            return this._userApi.GetUsersFromFullUrl(url)
                .map(serverResponseGetUser => this._userActions.GetUsersSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.GetUsersFail(error)));
        });

    @Effect()
    getUser = this.actions$
        .ofType(UserActions.GET_USER)
        .switchMap((action) => {
            const uidOrEmail: string = action.payload;
            return this._userApi.GetUser(uidOrEmail)
                .map(serverResponseGetUser => this._userActions.GetUserSuccess(serverResponseGetUser))
                .catch(error => Observable.of(this._userActions.AddUserFail(error)));
        });

    @Effect()
    addUser = this.actions$
        .ofType(UserActions.ADD_USER)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.AddUser(user)
                .map(serverResponseAddUser => this._userActions.AddUserSuccess(serverResponseAddUser))
                .catch(error => Observable.of(this._userActions.AddUserFail(error)));
        });

    @Effect()
    importUser = this.actions$
        .ofType(UserActions.IMPORT_USERS)
        .switchMap((action) => {
            const users: IUser[] = action.payload;
            return this._userApi.ImportUsers(users)
                .map(serverResponseImportUsers => this._userActions.ImportUsersSuccess(serverResponseImportUsers.data))
                .catch(error => Observable.of(this._userActions.ImportUsersFail(error)));
        });

    @Effect()
    updateUser = this.actions$
        .ofType(UserActions.UPDATE_USER)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.UpdateUser(user)
                .map((serverResponseUpdateUser) => this._userActions.UpdateUserSuccess(serverResponseUpdateUser))
                .catch(error => Observable.of(this._userActions.UpdateUserFail(error)));
        });

    @Effect()
    updateUserPassword = this.actions$
        .ofType(UserActions.UPDATE_USER_PASSWORD)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.UpdateUserPassword(user)
                .map((serverResponseUpdateUser) => this._userActions.UpdateUserPasswordSuccess(serverResponseUpdateUser))
                .catch(error => Observable.of(this._userActions.UpdateUserPasswordFail(error)));
        });

    @Effect()
    activateUser = this.actions$
        .ofType(UserActions.ACTIVATE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.ActivateUser(uid)
                .map((serverResponseMessage) => {
                    const activationParam: IUserActivationParam = {
                        uid: uid,
                        activationStatus: true
                    };
                    return this._userActions.ActivateUserSuccess(activationParam);
                })
                .catch(error => Observable.of(this._userActions.ActivateUserFail(error)));
        });

    @Effect()
    deactivateUser = this.actions$
        .ofType(UserActions.DEACTIVATE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.DeactivateUser(uid)
                .map((serverResponseMessage) => {
                    const activationParam: IUserActivationParam = {
                        uid: uid,
                        activationStatus: false
                    };
                    return this._userActions.DeactivateUserSuccess(activationParam);
                })
                .catch(error => Observable.of(this._userActions.DeactivateUserFail(error)));
        });

    @Effect()
    deleteUser = this.actions$
        .ofType(UserActions.DELETE_USER)
        .switchMap((action) => {
            const uid: string = action.payload;
            return this._userApi.DeleteUser(uid)
                .map((serverResponseMessage) => {
                    return this._userActions.DeleteUserSuccess(uid);
                })
                .catch(error => Observable.of(this._userActions.DeleteUserFail(error)));
        });

    @Effect()
    sendUserInvite = this.actions$
        .ofType(UserActions.SEND_USER_INVITE)
        .switchMap((action) => {
            const user: IUser = action.payload;
            return this._userApi.SendUserInvite(user)
                .map((serverResponseMessage) => {
                    return this._userActions.SendUserInviteSuccess(serverResponseMessage.data);
                })
                .catch(error => Observable.of(this._userActions.SendUserInviteFail(error)));
        });

    @Effect()
    getAllTimeCards = this.actions$
        .ofType(UserActions.GET_ALL_TIME_CARDS)
        .switchMap((action) => {
            let helper: GetTimeCardHelper = action.payload;
            return this._userApi.GetTimeCards(helper)
                .map(serverResponseGetUser => this._userActions.GetAllTimeCardsSuccess(serverResponseGetUser.data))
                .catch(error => Observable.of(this._userActions.GetAllTimeCardsFail(error)));
        });
}
