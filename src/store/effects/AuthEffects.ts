// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/fromPromise";

import { User } from 'firebase';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';

// Local modules
import { AuthActions, AuthActionType } from '../actions/AuthActions';
import { AuthApi } from '../../services/AuthApi';
import { UserApi } from '../../services/UserApi';
import { AuthSocket } from '../../services/AuthSocket';
import { MessageSocket } from '../../services/MessageSocket';

import { ICredentials } from '../../interfaces/IParams';
import { FirebaseAuth } from '../../services/FirebaseAuth';
import { StorageHelper } from '../../services/StorageHelper';
import { IServerResponseLogin, IServerResponseMessage } from '../../interfaces/IResponse';
import { IError, ErrorType } from '../../interfaces/IError';
import { ErrorMapper } from '../../common/ErrorMapper';

@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions,
        private _authApi: AuthApi,
        private _userApi: UserApi,
        private _authSocket: AuthSocket,
        private _messageSocket: MessageSocket,
        private _authActions: AuthActions,
        private _firebaseAuth: FirebaseAuth,
        private _storageHelper: StorageHelper) { }

    @Effect()
    init = this.actions$
        .ofType(AuthActionType[AuthActionType.INIT])
        .switchMap((action: Action) => {
            return Observable.fromPromise(this._firebaseAuth.Initialize())
                .map(firebaseUser => this._authActions.LoginServer(firebaseUser))
                .catch(error => Observable.of(this._authActions.LoginFail(error)));
        });

    @Effect()
    LoginFirebase = this.actions$
        .ofType(AuthActionType[AuthActionType.LOGIN_FIREBASE])
        .switchMap((action: Action) => {
            const credentials: ICredentials = action.payload;
            return Observable.fromPromise(this._firebaseAuth.Login(credentials.email, credentials.password))
                .map(firebaseUser => this._authActions.LoginServer(firebaseUser))
                .catch(error => Observable.of(this._authActions.LoginFail(error)));
        });

    @Effect()
    loginServer = this.actions$
        .ofType(AuthActionType[AuthActionType.LOGIN_SERVER])
        .switchMap((action: Action) => {
            const firebaseUser: User = action.payload;
            return this._authApi.Login(firebaseUser)
                .map((serverResponseLogin) => {
                    this._storageHelper.StoreAuth(serverResponseLogin.accessToken, serverResponseLogin.data);
                    return this._authActions.LoginServerSucceeded(serverResponseLogin);
                })
                .catch(error => Observable.of(this._authActions.LoginFail(error)));
        });

    @Effect()
    InitAuthSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.INIT_AUTH_SOCKET])
        .switchMap((action: Action) => {
            const token: string = action.payload;
            return this._authSocket.Initialize(token)
                .map(socketResponse => this._authActions.InitAuthSocketSucceeded(socketResponse))
                .catch(error => Observable.of(this._authActions.LoginFail(ErrorMapper.GetErrorFromSocketError(error))));
        });

    // Best-effort
    @Effect()
    ForceReconnectSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.FORCE_RECONNECT])
        .switchMap((action: Action) => {
            return this._authSocket.Reconnect()
                .map(() => this._authActions.AuthNoAction())
                .catch(error => Observable.of(this._authActions.LoginFail(ErrorMapper.GetErrorFromSocketError(error))));
        });

    @Effect()
    ForceReconnectFollowUpSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.FORCE_RECONNECT_FOLLOWUP])
        .switchMap((action: Action) => {
            return this._authSocket.ReconnectFollowUp()
                .map((isReconnectRequested) => {
                    if (isReconnectRequested) {
                        return this._authActions.Initialize();
                    }
                    else {
                        return this._authActions.AuthNoAction();
                    }
                })
                .catch(error => Observable.of(this._authActions.LoginFail(ErrorMapper.GetErrorFromSocketError(error))));
        });

    @Effect()
    InitMessageSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.INIT_MESSAGE_SOCKET])
        .switchMap((action: Action) => this._messageSocket.Initialize())
        .map((socketResponse: IServerResponseMessage)  => this._authActions.InitMessageSocketSucceeded(socketResponse))
        .catch((error: IServerResponseMessage) => Observable.of(this._authActions.LoginFail(ErrorMapper.GetErrorFromSocketError(error))));

    @Effect()
    OnSocketDisconnect = this.actions$
        .ofType(AuthActionType[AuthActionType.SOCKET_DISCONNECT_LISTENER])
        .switchMap((action: Action) => this._authSocket.OnDisconnect())
        .switchMap(() => this._messageSocket.Disconnect())
        .map(() => this._authActions.SocketDisconnected());

    @Effect()
    ReconnectAuthSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.AUTH_SOCKET_RECONNECT_LISTENER])
        .switchMap((action: Action) => this._authSocket.OnAuthenticated())
        .map((socketResponse: IServerResponseMessage) => this._authActions.InitAuthSocketSucceeded(socketResponse))
        .catch((error: IError) => Observable.of(this._authActions.LoginFail(error)));

    @Effect()
    ReconnectMessageSocket = this.actions$
        .ofType(AuthActionType[AuthActionType.MESSAGE_SOCKET_RECONNECT_LISTENER])
        .switchMap((action: Action) => this._messageSocket.OnAuthenticated())
        .map((socketResponse: IServerResponseMessage) => this._authActions.InitMessageSocketSucceeded(socketResponse))
        .catch((error: IError) => Observable.of(this._authActions.LoginFail(error)));

    @Effect()
    changePassword = this.actions$
        .ofType(AuthActionType[AuthActionType.CHANGE_PASSWORD])
        .switchMap((action: Action) => {
            const pass: string = action.payload;
            return this._userApi.ChangePassword(pass)
                .map(serverResponse => this._authActions.ChangePasswordSuccess())
                .catch(error => Observable.of(this._authActions.ChangePasswordFail(error)));
        });

    @Effect()
    logout = this.actions$
        .ofType(AuthActionType[AuthActionType.LOGOUT])
        .switchMap(() => Observable.fromPromise(this._firebaseAuth.Logout()))
        .switchMap(() => Observable.fromPromise(this._storageHelper.ClearAuth()))
        .switchMap(() => this._authSocket.Disconnect())
        .switchMap(() => this._messageSocket.Disconnect())
        .map(() => { return this._authActions.LogoutSuccess() })
        .catch((error: IError) => Observable.of(this._authActions.LogoutFail(error)));

}
