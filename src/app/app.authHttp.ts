// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http, RequestOptions } from '@angular/http';

// Local modules
import { Constants, Urls } from '../common/Constants';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
    return new AuthHttp(
        new AuthConfig({
            tokenName: Constants.AUTH_TOKEN_STORAGE_NAME,
            globalHeaders: [{ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' }]
        }),
        http,
        options);
}

@NgModule({
    providers: [
        {
            provide: AuthHttp,
            useFactory: authHttpServiceFactory,
            deps: [Http, RequestOptions]
        }
    ]
})
export class AppAuthHttpModule { }
