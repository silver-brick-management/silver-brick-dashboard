// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'hammerjs';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MaterialModule, MdDatepickerModule, MdNativeDateModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2MapModule} from 'ng2-map';
import { CalendarModule } from 'angular-calendar';
import { DndModule } from 'ngx-drag-drop';
import { NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { ColorPickerModule } from 'ngx-color-picker';

// Local modules
import { AppStateModule } from './app.state';
import { AppComponent } from './app.component';
import { AppAuthHttpModule } from './app.authHttp';
import { AuthApi } from '../services/AuthApi';
import { UserApi } from '../services/UserApi';
import { AccountApi } from '../services/AccountApi';
import { OrgApi } from '../services/OrgApi';
import { AuthSocket } from '../services/AuthSocket';
import { MessageSocket } from '../services/MessageSocket';

import { FirebaseAuth } from '../services/FirebaseAuth';
import { FirebaseStorage } from '../services/FirebaseStorage';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { StorageHelper } from '../services/StorageHelper';
import { ConfigContext } from '../environments/environment';
import { AuthHttpWrapper } from '../services/AuthHttpWrapper';
import { AppDialogModule, DialogComponents } from './app.dialogs';
import { AppRoutingModule, RoutingComponents } from './app.routes';
import { AppControlModule, ControlComponents } from './app.controls';
import { Controls } from '../providers/Controls';
import { SessionData } from '../providers/SessionData';
import { CalendarProvider } from '../providers/CalendarProvider';

export const firebaseConfig = {
  apiKey: ConfigContext.FirebaseAPIKey,
  authDomain: ConfigContext.FirebaseAuthDomain,
  databaseURL: ConfigContext.FirebaseDatabaseURL,
  storageBucket: ConfigContext.FirebaseStorageBucket,
  messagingSenderId: ConfigContext.FirebaseMessengerSenderID
};

@NgModule({
  imports: [
    HttpModule,
    FormsModule,
    BrowserModule,
    /* NGRX State management */
    AppStateModule,
    /* Dialog declarations */
    AppDialogModule,
    /* Routing definitions */
    AppRoutingModule,
    AppControlModule,
    /* Register AuthHttp */
    AppAuthHttpModule,
    DndModule,
    ReactiveFormsModule,
    NgbModalModule.forRoot(),
    BrowserAnimationsModule,
    CalendarModule.forRoot(),
    ColorPickerModule,
    Ng2MapModule.forRoot({ apiUrl: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD0C_SCgt4kdYqR31VCcCJXrkLbDcck28g&libraries=places,directions&v=weekly'}),
    /* Angular bootstrap Imports */
    MaterialModule,
    MdDatepickerModule,
    MdNativeDateModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  declarations: [
    AppComponent,
    DialogComponents,
    RoutingComponents,
    ControlComponents
  ],
  bootstrap: [AppComponent],
  providers: [
    AuthApi,
    AccountApi,
    UserApi,
    OrgApi,
    AuthSocket,
    MessageSocket,
    FirebaseAuth,
    SessionData,
    FirebaseStorage,
    StorageHelper,
    CalendarProvider,
    AuthHttpWrapper,
    AngularFireAuth,
    Controls
  ],
  entryComponents: [DialogComponents],
  schemas:      [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
