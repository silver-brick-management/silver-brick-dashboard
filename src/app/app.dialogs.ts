// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';

// Local modules
import { ConfirmDialogComponent } from '../components/confirm-dialog/confirm-dialog.component';
import { UserDetailComponent } from '../components/user-details/user-details.component';
import { OrgDetailComponent } from '../components/org-details/org-details.component';
import { BldgDetailComponent } from '../components/building-details/building-details.component';
import { BuildingUsersComponent } from '../components/building-users/building-users.component';
import { TenantDetailComponent } from '../components/tenant-details/tenant-details.component';
import { TaskDetailsComponent } from '../components/task-details/task-details.component';
import { RecurringTaskDetailsComponent } from '../components/recurring-task-details/recurring-task-details.component';
import { LandlordDetailComponent } from '../components/landlord-details/landlord-details.component';
import { AudienceChooserComponent } from '../components/audience-chooser/audience-chooser.component';

@NgModule({
  entryComponents: [
    ConfirmDialogComponent,
    UserDetailComponent,
    BuildingUsersComponent,
    AudienceChooserComponent,
    LandlordDetailComponent,
    TenantDetailComponent,
    TaskDetailsComponent,
    RecurringTaskDetailsComponent
  ]
})
export class AppDialogModule { }

export const DialogComponents = [
	ConfirmDialogComponent, 
	OrgDetailComponent,
	BldgDetailComponent,
    AudienceChooserComponent,
    LandlordDetailComponent,
	BuildingUsersComponent,
	UserDetailComponent,
    TenantDetailComponent,
    RecurringTaskDetailsComponent,
    TaskDetailsComponent
];
