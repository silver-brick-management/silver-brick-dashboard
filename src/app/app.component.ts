// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { tokenNotExpired } from "angular2-jwt";
import { Observable, Subscription } from "rxjs/Rx";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { MdDialog, MdSnackBar, MdDialogConfig } from "@angular/material";

// Local modules
import { AppState } from "../app/app.state";
import { IAuthState, AuthStateType, ISessionState, SessionStateType } from "../interfaces/IStoreState";
import { AuthActions } from "../store/actions/AuthActions";
import { UrlRedirectActions } from "../store/actions/UrlRedirectActions";
import { MessageActions } from "../store/actions/MessageActions";

import { SessionActions } from "../store/actions/SessionActions";
import { ConfirmDialogComponent } from "../components/confirm-dialog/confirm-dialog.component";
import { Constants, Urls } from "../common/Constants";
import { IUserDialogBase } from "../interfaces/IParams";
import { SessionData } from "../providers/SessionData";
import { CalendarProvider } from "../providers/CalendarProvider";
import { ICredentials } from '../interfaces/IParams';
import { Controls } from "../providers/Controls";
import { UserActions } from "../store/actions/UserActions";

import { OrgActions } from "../store/actions/OrgActions";

@Component({
    styleUrls: ['./app.component.scss'],
    selector: "app-root",
    templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit, OnDestroy {
    private _isInitialized: boolean = false;
    private _allSubscription: Subscription = new Subscription();
    private _wasAlreadyLoggedIn: boolean = false;
    private _authStateSubscription: Subscription;
    public authState$: Observable<IAuthState>;
    public SessionState$: Observable<ISessionState>;
    public urlRedirectState$: Observable<string>;
    private _shouldSetupSocketListeners: boolean;
    private _socketsConnected: boolean;
    private _alreadyLoggedIn: boolean = false;
    private _sessionStateSubscription: Subscription;
    private _isLoginLoadComplete: boolean;
    private _setUpSession: boolean;
    private _loadedTasks: boolean = false;

    constructor(
        private _dialog: MdDialog,
        private _router: Router,
        private _snackbar: MdSnackBar,
        private _userActions: UserActions,
        private _orgActions: OrgActions,
        private _store: Store<AppState>,
        private _authActions: AuthActions,
        private _messageActions: MessageActions,
        private _urlRedirectActions: UrlRedirectActions,
        private _sessionActions: SessionActions,
        private _sessionData: SessionData,
        private _controls: Controls,
        public calendarProvider: CalendarProvider) {
        this.SessionState$ = this._store.select((state) => state.sessionState);
        this.authState$ = this._store.select((state) => state.authState);
        this.urlRedirectState$ = this._store.select((state) => state.urlRedirectState);
    }

    ngOnInit(): void {
        this._initiate();
        let self = this;
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
          history.forward();
          self._snackbar.open("Back button disabled, Navigate using the arrows on screen", "Dismiss", { duration: 2000,extraClasses: ["browser-class"] });
        };
        this._alreadyLoggedIn = false;
        let token: string = window.location.search.substring(1);
        let parts: string[] = token.split("customerEmail=", 2);
        if (null != parts) {
            let email: string = parts[1];
            let credentials: ICredentials = {
                email: email,
                password: "silverbrick"
            };
            if ((null != credentials) && (null != credentials.email) && (null != credentials.password) && (credentials.password !== '') && (credentials.email !== '')) {
                this._store.dispatch(this._authActions.LoginFirebase(credentials));
            } else {
            }
        } else {
        }
        
    }

    ngOnDestroy(): void {
        this._authStateSubscription.unsubscribe();
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
    }

    _initiate(): void {
        this._sessionStateSubscription = this.SessionState$
        .subscribe((state: ISessionState) => {
            if (null != state) {
                switch (state.type) {

                    case SessionStateType.GET_USERS_SUCCEEDED:
                    {
                        if (null != state.user) {
                            if (!this._loadedTasks) {
                                if (!state.user.isAdmin) {
                                   
                                } else {
                                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                    this._loadedTasks = true;
                                }
                            }
                        }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            }
        });
        this._socketsConnected = false;
        this._isLoginLoadComplete = false;
        this._shouldSetupSocketListeners = true;
        this._setUpSession = true;
        // Skipping the first emit as its the initialization emit
        this._allSubscription.add(
            this.authState$.subscribe(
                (currentAuthState: IAuthState) => {
                    console.log("Current", currentAuthState.type, AuthStateType[currentAuthState.type]);
                    switch (currentAuthState.type) {
                        case AuthStateType.SOCKET_DISCONNECTED: {
                            console.log("SOCKET_DISCONNECTED");
                            // Only if you are logged in will these actions be called
                            if (this._isLoginLoadComplete) {
                                /* Publish an event that tells the pages / components listening for the 
                     EVENT_SOCKET_DISCONNECTED event, that the socket is connected, parameter - false
                     This disables certain UI capabilities to ensure nothing crashes for sockets.
                  */
                                // Ensures the listeners are reconnected upon reconnection
                                this._shouldSetupSocketListeners = true;
                                // Toast only works with cordova. Need to ensure it exist or it crashes the code
                                // this._snackbar.open("Reconnecting...", Constants.STR_DISMISS, { duration: 1500, extraClasses: ["reconnect-class"] });

                                this._store.dispatch(this._authActions.ForceReconnectFollowUp());
                            } else {
                            }
                            break;
                        }

                        case AuthStateType.LOGOUT_SUCCEEDED:
                            {
                            this._shouldSetupSocketListeners = true;
                            this._sessionData.UnInitialize();
                            this._isLoginLoadComplete = false;
                            this._wasAlreadyLoggedIn = false;
                            this._setUpSession = true;

                            // // NEVER DO THIS, EVER. Talk with someone for details.
                            this._router.navigate(["/entry"]);

                            // fallback to below
                            break;
                        }

                        case AuthStateType.AUTH_REFRESH_INITIATED: {
                            this._shouldSetupSocketListeners = true;
                            this._sessionData.UnInitialize();
                            this._wasAlreadyLoggedIn = false;
                            this._setUpSession = true;

                            // // NEVER DO THIS, EVER. Talk with someone for details.
                            this._router.navigate(["/entry"]);

                            // fallback to below
                            break;
                        }

                        case AuthStateType.LOADING: {
                            this._router.navigate(["/entry"]);
                            break;
                        }

                        case AuthStateType.NONE: {
                            this._store.dispatch(this._authActions.Initialize());
                            break;
                        }

                        case AuthStateType.SOCKET_UNABLE_TO_CONNECT: {
                            // NOTE: I dont think this path will work as we have to uninitialize everything
                            // before intitializing again or we will be in a bizzare state
                            // Bayo to investigate what the original intent was
                            // Could potentially pass in the error to the login page
                            // as a param
                            this._store.dispatch(this._authActions.Initialize());
                            break;
                        }

                        case AuthStateType.SERVER_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && !currentAuthState.isAuthSocketConnected) {
                                this._store.dispatch(this._authActions.InitAuthSocket(currentAuthState.token));
                                // this._sessionData.Initialize();
                            } else {
                                this._store.dispatch(
                                    this._authActions.LoginFail({
                                        error: "Socket auth failed!",
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.LOGIN_FAILED: {
                            // this._controls.ShowDialog('Login Failed', ((null != currentAuthState.error) ? (currentAuthState.error.Message) : 'There was an error while attempting to connect'), 'Try Again', 'Start Over')
                            // .subscribe((response) => {
                            //     if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                            //         this._store.dispatch(this._authActions.Initialize());
                            //     } else {
                            //         this._router.navigate(['/login']);
                            //     }
                            // });
                            this._router.navigate(["/login"]);
                            this._setUpSession = true;
                            break;
                        }

                        case AuthStateType.AUTH_SOCKET_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected) {
                                // Initialize session state
                                if (this._setUpSession) {
                                    if (currentAuthState.profile.isAdmin) {
                                        this._store.dispatch(this._sessionActions.InitializeSession(currentAuthState.profile));
                                        this._setUpSession = false;
                                    } else {
                                        this._store.dispatch(this._sessionActions.InitializeSession(currentAuthState.profile));
                                        this._setUpSession = false;
                                    }
                                }
                                // Urls.Initialize(currentAuthState.profile.isDBA);
                                this._store.dispatch(this._authActions.InitMessageSocket());
                            } else {
                                this._store.dispatch(
                                    this._authActions.LoginFail({
                                        error: "Socket Publisher failed!",
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.MESSAGE_SOCKET_READY: {
                            // Initiate the socket connection
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected && currentAuthState.isMessageSocketConnected) {
                                this._store.dispatch(this._authActions.LoginSuccess());
                                this._sessionData.Initialize();
                                if (currentAuthState.profile.isFirstTime) {

                                } else {
                                    if (!currentAuthState.profile.isAdmin) {
                                        if ((currentAuthState.profile.role === "Tenant")) {
                                            this._store.dispatch(this._sessionActions.GetTenantTasks(currentAuthState.profile.uid));
                                        } else if (currentAuthState.profile.role === "Landlord") {
                                            this._store.dispatch(this._sessionActions.GetLandlordTasks());
                                        } else {
                                            let bldgHelper: any = {
                                                orgID: currentAuthState.profile.orgs.id,
                                                buildingID: currentAuthState.profile.orgs.buildings[0].id
                                            };
                                            console.log("bldgHelper", bldgHelper);
                                            this._store.dispatch(this._orgActions.GetAllOrgsScope());
                                            this._store.dispatch(this._sessionActions.GetStaffTasks(currentAuthState.profile.uid));
                                            this._store.dispatch(this._sessionActions.GetStaffHistory(currentAuthState.profile.uid));
                                        }
                                    } else {
      
                                        this._store.dispatch(this._userActions.GetUsers(null));
                                        this._store.dispatch(this._sessionActions.GetUsersScope(null));
                                        this._store.dispatch(this._orgActions.GetAllOrgsScope());
                                        let date: Date = new Date(Date.now());
                                        let helper: any = {
                                            day: date.getDate(),
                                            month: date.getMonth() + 1,
                                            year: date.getFullYear(),
                                            uid: this._sessionData.userID,
                                            date: date.getTime()
                                        };
                                        this._store.dispatch(this._sessionActions.GetTasks());
                                    }
                                }
                                // this.calendarProvider.Initialize();
                            } else {
                                this._store.dispatch(
                                    this._authActions.LoginFail({
                                        error: "Socket warnable failed!",
                                    })
                                );
                            }
                            break;
                        }

                        case AuthStateType.SERVER_AND_SOCKET_READY: {
                            if (currentAuthState.token && tokenNotExpired(null, currentAuthState.token) && currentAuthState.profile && currentAuthState.isAuthSocketConnected && currentAuthState.isMessageSocketConnected) {
                                if (this._shouldSetupSocketListeners) {
                                    // Ensures we dont dupe the listners as they
                                    // remain active during the lifetime of the app
                                    // Init socket disconnect & reconnect listener
                                    this._store.dispatch(this._authActions.SocketDisconnectListener());
                                    this._store.dispatch(this._authActions.AuthSocketReconnectListener());
                                    this._store.dispatch(this._authActions.MessageSocketReconnectListener());
                                    this._store.dispatch(this._messageActions.GetMessageHistoryListener());
                                    this._store.dispatch(this._messageActions.AddMessageListener());
                                    this._store.dispatch(this._messageActions.NewChatRoomListener());
                                    this._store.dispatch(this._messageActions.GetChatRoomsListener());
                                    this._store.dispatch(this._sessionActions.GetCalendarColors());
                                    // this._store.dispatch(this._publisherActions.GetPublisherAllByReader());
                                    // this._socketsConnected = true;
                                   
                                    let lastTab: string = null;
                                    if (this._isLoginLoadComplete) {
                                        this._socketsConnected = true;
                                        // this._snackbar.open("Connected", Constants.STR_DISMISS, { duration: 1500 });
                                    } else {
                                        // this._store.dispatch(this._publisherActions.GetChartData(helper));
                                    }

                                    // this._store.dispatch(this._publisherActions.GetSegments());
                                    this.urlRedirectState$.first().subscribe((url) => {
                                        if (url) {
                                            const redirectTo: string = url;
                                            this._store.dispatch(this._urlRedirectActions.ClearRedirect());

                                            this._router.navigate([redirectTo]);
                                        } else if (window.location.pathname === "/" || window.location.pathname === "/login") {
                                            // User is logged in and the route is leading
                                            // to the entry page which is just a loading UI
                                            // This forces a navigation to the dashboard for
                                            // logged in users and seem to be going to the
                                            // entry component.
                                            if (this._isLoginLoadComplete) {
                                                this._router.navigate(["/dashboard"]);
                                            } else {
                                                this._router.navigate(["/dashboard"]);
                                            }
                                        } else {
                                            console.log("currentAuthState.type", currentAuthState.type);
                                        }
                                    });
                                    this._shouldSetupSocketListeners = false;
                                } else {
                                }
                                if (!this._isLoginLoadComplete) {
                                    this._isLoginLoadComplete = true;
                                    this._wasAlreadyLoggedIn = true;
                                    this._router.navigate(["/dashboard"]);
                                } else {
                                    // For socket reconnect, we do not need to rerun
                                    // these actions. But we do for logout -> login
                                }
                            } else {
                                this._router.navigate(["/login"]);
                            }
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                },
                (error) => {
                    console.log("App Component: Auth subscription failed! Go log in");
                    console.log(error);
                    this._router.navigate(["/login"]);
                    this._setUpSession = true;
                }
            )
        );
        // Try loging in if a firebase object already exist
        this._store.dispatch(this._authActions.Initialize());
    }
}