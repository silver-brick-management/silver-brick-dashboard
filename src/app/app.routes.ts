// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
// Local modules
import { AuthGuard } from './app.guard';
import { LoginComponent } from '../pages/login/login.component';
import { EntryComponent } from '../pages/entry/entry.component';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { DashboardHomeComponent } from '../pages/dashboard-home/dashboard-home.component';
import { DashboardUsersComponent } from '../pages/dashboard-users/dashboard-users.component';
import { DashboardTenantsComponent } from '../pages/dashboard-tenants/dashboard-tenants.component';
import { DashboardOrgsComponent } from '../pages/dashboard-orgs/dashboard-orgs.component';
import { DashboardChatComponent } from '../pages/dashboard-chat/dashboard-chat.component';
import { DashboardScheduleComponent } from '../pages/dashboard-schedule/dashboard-schedule.component';
import { DashboardTasksComponent } from '../pages/dashboard-tasks/dashboard-tasks.component';
import { DashboardRecurringTasksComponent } from '../pages/dashboard-recurring-tasks/dashboard-recurring-tasks.component';
import { DashboardArchiveComponent } from '../pages/dashboard-archive/dashboard-archive.component';
import { DashboardCalendarComponent } from '../pages/dashboard-calendar/dashboard-calendar.component';
import { DashboardLandlordsComponent } from '../pages/dashboard-landlords/dashboard-landlords.component';

const routes: Routes = [
    { path: '', component: EntryComponent },
    { path: 'login', component: LoginComponent },
    // { path: '', redirectTo: '/login', pathMatch: 'full'},
    { path: 'dashboard', component: DashboardComponent, canActivate: [ AuthGuard ],
        children: [
            { path: '', redirectTo: '/dashboard/home', pathMatch: 'full' },
            { path: 'home', component: DashboardHomeComponent },
            { path: 'users', component: DashboardUsersComponent },
            { path: 'tenants', component: DashboardTenantsComponent },
            { path: 'calendar', component: DashboardCalendarComponent },
            { path: 'chat', component: DashboardChatComponent },
            { path: 'schedule', component: DashboardScheduleComponent },
            { path: 'orgs', component: DashboardOrgsComponent },
            { path: 'landlords', component: DashboardLandlordsComponent },
            { path: 'archive', component: DashboardArchiveComponent },
            { path: 'tasks', component: DashboardTasksComponent },
            { path: 'recurring', component: DashboardRecurringTasksComponent }
        ]
    },
    { path: 'dashboard/home', component: DashboardHomeComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/users', component: DashboardUsersComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/tenants', component: DashboardTenantsComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/orgs', component: DashboardOrgsComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/archive', component: DashboardArchiveComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/schedule', component: DashboardScheduleComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/calendar', component: DashboardCalendarComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/landlords', component: DashboardLandlordsComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/chat', component: DashboardChatComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/tasks', component: DashboardTasksComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/recurring', component: DashboardRecurringTasksComponent, canActivate: [AuthGuard] },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }) ],
    exports: [ RouterModule ],
    providers: [
        AuthGuard,
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ]
})
export class AppRoutingModule { }

export const RoutingComponents = [ 
    EntryComponent,
    DashboardRecurringTasksComponent,
    LoginComponent,
    DashboardCalendarComponent,
    DashboardChatComponent,
    DashboardOrgsComponent,
    DashboardArchiveComponent,
    DashboardLandlordsComponent,
    DashboardTasksComponent,
    DashboardComponent,
    DashboardTenantsComponent,
    DashboardHomeComponent,
    DashboardScheduleComponent,
    DashboardUsersComponent ];
