// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Rx';
import { tokenNotExpired } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

// Local modules
import { AppState } from '../app/app.state';
import { IAuthState } from '../interfaces/IStoreState';
import { UrlRedirectActions } from '../store/actions/UrlRedirectActions';

@Injectable()
export class AuthGuard implements CanActivate {

    public authState: Observable<IAuthState>;

    constructor(
        private _router: Router,
        private _store: Store<AppState>,
        private _urlRedirectActions: UrlRedirectActions) {

        this.authState = this._store.select(state => state.authState);
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authState.map((currentAuthState) => {
            if (currentAuthState.loading) {
                this._store.dispatch(this._urlRedirectActions.SaveRedirect(state.url));
                // Navigate to home
                this._router.navigate(['']);
                return false;
            } else {
                if (currentAuthState.token &&
                    tokenNotExpired(null, currentAuthState.token) &&
                    currentAuthState.profile) {
                    return true;
                } else {
                    this._router.navigate(['/login']);
                    return false;
                }
            }
        }).first();
    }
}
