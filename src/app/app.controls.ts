// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// Local modules
import { CollapsibleControlComponent } from '../components/collapsible-control/collapsible-control.component';
import { CalendarHeaderComponent } from '../components/calendar-components/calendar-header.component';
import { MainCalendarComponent } from "../components/calendar-components/main-calendar-component/main-calendar.component";
import { LetterAvatarComponent } from '../components/letter-avatar/letter-avatar';
import { SearchDropdown } from '../components/search-dropdown/search-dropdown'

@NgModule({
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppControlModule { }

export const ControlComponents = [ SearchDropdown, LetterAvatarComponent, MainCalendarComponent, CalendarHeaderComponent, CollapsibleControlComponent ];
