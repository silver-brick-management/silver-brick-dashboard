// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Local modules
import { AuthEffects } from '../store/effects/AuthEffects';
import { AuthActions } from '../store/actions/AuthActions';
import { AuthReducers } from '../store/reducers/AuthReducers';
import { SessionActions } from '../store/actions/SessionActions';
import { SessionEffects } from '../store/effects/SessionEffects';
import { SessionReducers } from '../store/reducers/SessionReducers';
import { UserEffects } from '../store/effects/UserEffects';
import { UserActions } from '../store/actions/UserActions';
import { UserReducers } from '../store/reducers/UserReducers';
import { OrgEffects } from '../store/effects/OrgEffects';
import { OrgActions } from '../store/actions/OrgActions';
import { OrgReducers } from '../store/reducers/OrgReducers';
import { MessageEffects } from '../store/effects/MessageEffects';
import { MessageActions } from '../store/actions/MessageActions';
import { MessageReducers } from '../store/reducers/MessageReducers';
import { IAuthState, IUserState, ISessionState, IOrgState, IMessageState } from '../interfaces/IStoreState';
import { UrlRedirectActions } from '../store/actions/UrlRedirectActions';
import { UrlRedirectReducers } from '../store/reducers/UrlRedirectReducers';

@NgModule({
    imports: [
        StoreModule.provideStore({
            authState: AuthReducers.Reduce,
            usersState: UserReducers.Reduce,
            urlRedirectState: UrlRedirectReducers.Reduce,
            sessionState: SessionReducers.Reduce,
            orgState: OrgReducers.Reduce,
            messageState: MessageReducers.Reduce
        }),
        EffectsModule.run(AuthEffects),
        EffectsModule.run(UserEffects),
        EffectsModule.run(SessionEffects),
        EffectsModule.run(OrgEffects),
        EffectsModule.run(MessageEffects)
    ],
    exports: [
        StoreModule,
        EffectsModule
    ],
    providers: [
        AuthActions,
        UserActions,
        UrlRedirectActions,
        SessionActions,
        OrgActions,
        MessageActions
     ]
})
export class AppStateModule { }

export interface AppState {
    authState: IAuthState;
    usersState: IUserState;
    urlRedirectState: string;
    sessionState: ISessionState;
    orgState: IOrgState;
    messageState: IMessageState;
}
