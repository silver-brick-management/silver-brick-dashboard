// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import "rxjs/add/operator/map";
import "rxjs/add/operator/switchMap";

import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { URLSearchParams, Response, RequestOptions } from "@angular/http";
import { cards } from "stripe";

// Local modules
import { AuthHttpWrapper } from "./AuthHttpWrapper";
import { ErrorMapper } from "../common/ErrorMapper";
import { IProfile, ITask } from "../shared/SilverBrickTypes";
import { StorageHelper } from "./StorageHelper";
import { Urls, JSON_HEADER } from "../common/Constants";
import { IServerResponseBase, IServerResponseAddTaskHistory, IServerResponseGetCalendarColors, AddMessageHelper, ITaskFeedJoin, IServerResponseGetTaskHistory, IServerResponseAssignTask, AssignTaskHelper, IServerResponseMessage, IServerResponseGetProfile, IServerResponseAddUpdateAssignTask, IServerResponseGetTasks } from "../interfaces/IResponse";
import { SessionData } from "../providers/SessionData";
import { IAddBldgHelper, IGetBldgHelper } from '../interfaces/IParams';

@Injectable()
export class AccountApi {
    private _options: RequestOptions = new RequestOptions(JSON_HEADER);

    constructor(
        private _storageHelper: StorageHelper,
        private _sessionData: SessionData,
        private _authHttp: AuthHttpWrapper) {}

    GetTasks(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_ALL_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetClosedTasks(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_CLOSED_TASK_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetCalendarColors(): Observable<IServerResponseGetCalendarColors> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_CALENDAR_COLORS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetLandlordTasks(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_LANDLORD_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetLandlordTasksCompleted(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Put(this._sessionData.API_LANDLORD_TASKS_V1, null, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    DeleteTask(task: ITask): Observable<IServerResponseBase> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Post(this._sessionData.API_DELETE_TASK_V1, JSON.stringify({ data: task }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetBuildingTasks(helper: IGetBldgHelper): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
        .Put(this._sessionData.API_ALL_TASKS_V1,  JSON.stringify({ data: helper }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetRecurringTasks(): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_RECURRING_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetRecurringStaffTasks(buildingID: string, orgID: string): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        options.search = queryStrings;
        return this._authHttp
            .Put(this._sessionData.API_RECURRING_TASKS_V1, JSON.stringify({ buildingID: buildingID, orgID: orgID }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetTenantTasks(id: string): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("tenant_id", id);
        options.search = queryStrings;
        return this._authHttp
            .Get(this._sessionData.API_TENANT_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetStaffTasks(id: string): Observable<IServerResponseGetTasks> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("staff_id", id);
        options.search = queryStrings;
        return this._authHttp
            .Get(this._sessionData.API_TASKS_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetTaskFeed(helper: ITaskFeedJoin): Observable<IServerResponseGetTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Put(this._sessionData.API_TASK_HISTORY_V1, JSON.stringify({ data: helper }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    ApproveTaskFeed(helper: AddMessageHelper): Observable<IServerResponseAddTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Patch(this._sessionData.API_TASK_HISTORY_V1, JSON.stringify({ data: helper }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateTaskFeed(helper: AddMessageHelper): Observable<IServerResponseAddTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Post(this._sessionData.API_UPDATE_HISTORY_V1, JSON.stringify({ data: helper }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetAllHistory(): Observable<IServerResponseGetTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Get(this._sessionData.API_TASK_HISTORY_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    GetStaffHistory(id: string): Observable<IServerResponseGetTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        const queryStrings = new URLSearchParams();
        queryStrings.set("staff_id", id);
        options.search = queryStrings;
        return this._authHttp
            .Get(this._sessionData.API_STAFF_HISTORY_V1, options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AddTaskFeed(helper: AddMessageHelper): Observable<IServerResponseAddTaskHistory> {
        const options: RequestOptions = new RequestOptions(JSON_HEADER);
        return this._authHttp
            .Post(this._sessionData.API_TASK_HISTORY_V1, JSON.stringify({ data: helper }), options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    CreateTask(task: ITask): Observable<IServerResponseAddUpdateAssignTask> {
        return this._authHttp
            .Post(this._sessionData.API_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    CreateRecurringTask(task: ITask): Observable<IServerResponseAddUpdateAssignTask> {
        return this._authHttp
            .Post(this._sessionData.API_RECURRING_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateRecurringTask(task: ITask): Observable<IServerResponseAddUpdateAssignTask> {
        return this._authHttp
            .Patch(this._sessionData.API_RECURRING_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UpdateTask(task: ITask): Observable<IServerResponseAddUpdateAssignTask> {
        return this._authHttp
            .Put(this._sessionData.API_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    AssignTask(task: AssignTaskHelper): Observable<IServerResponseAssignTask> {
        return this._authHttp
            .Patch(this._sessionData.API_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    CompleteTask(task: ITask): Observable<IServerResponseAssignTask> {
        return this._authHttp
            .Patch(this._sessionData.API_COMPLETE_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    CompleteRecurringTask(task: ITask): Observable<IServerResponseAssignTask> {
        return this._authHttp
            .Patch(this._sessionData.API_COMPLETE_RECURRING_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }

    UnCompleteTask(task: ITask): Observable<IServerResponseAssignTask> {
        return this._authHttp
            .Put(this._sessionData.API_COMPLETE_TASKS_V1, JSON.stringify({ data: task }), this._options)
            .map((response: Response) => response.json())
            .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
    }
}

