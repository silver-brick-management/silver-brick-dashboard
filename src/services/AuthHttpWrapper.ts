// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/fromPromise";

import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { AuthHttp, tokenNotExpired } from 'angular2-jwt';
import { Response, RequestOptionsArgs } from '@angular/http';

// Local modules
import { AuthApi } from '../services/AuthApi';
import { StorageHelper } from '../services/StorageHelper';
import { IServerResponseRefreshToken } from '../interfaces/IResponse';

@Injectable()
export class AuthHttpWrapper {
    private _tokenIsBeingRefreshed: Subject<boolean>;

    constructor(
        private _router: Router,
        private _authHttp: AuthHttp,
        private _authApi: AuthApi,
        private _storageHelper: StorageHelper) {

        this._tokenIsBeingRefreshed = new Subject<boolean>();
        this._tokenIsBeingRefreshed.next(false);
    }

    Get(url: string, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authGet(url, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authGet(url, options);
        }
    }

    Post(url: string, body: any, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authPost(url, body, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authPost(url, body, options);
        }
    }

    Put(url: string, body: any, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authPut(url, body, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authPut(url, body, options);
        }
    }

    Delete(url: string, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authDelete(url, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authDelete(url, options);
        }
    }

    Patch(url: string, body: any, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authPatch(url, body, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authPatch(url, body, options);
        }
    }

    Head(url: string, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authHead(url, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authHead(url, options);
        }
    }

    Options(url: string, options?: RequestOptionsArgs) {
        if (this._authApi.TokenRequiresRefresh()) {
            this._tokenIsBeingRefreshed.next(true);
            return this._authApi.RefreshToken()
                .switchMap((refreshTokenResponse) => {
                    const isRefreshSuccessful: boolean = this.refreshTokenSuccessHandler(refreshTokenResponse);
                    if (isRefreshSuccessful && tokenNotExpired(null, refreshTokenResponse.accessToken)) {
                        this._tokenIsBeingRefreshed.next(false);
                        return this.authOptions(url, options);
                    } else {
                        this._tokenIsBeingRefreshed.next(false);
                        // TODO: Implement sessiontimeout page
                        // this.router.navigate(['/sessiontimeout']);
                        return Observable.throw(refreshTokenResponse);
                    }
                })
                .catch((error) => {
                    this.refreshTokenErrorHandler(error);
                    return Observable.throw(error);
                });
        } else {
            return this.authOptions(url, options);
        }
    }

    private refreshTokenSuccessHandler(refreshTokenResponse: IServerResponseRefreshToken): boolean {
        let isSuccessful = false;

        if (refreshTokenResponse.success) {
            this._storageHelper.StoreAccessToken(refreshTokenResponse.accessToken);
            console.log("Refreshed access token");
            isSuccessful = true;
        } else {
            this._storageHelper.ClearAuth();
            console.log("Refresh failed from server. Removed tokens");
            this._router.navigate(['/login']);
        }
        this._tokenIsBeingRefreshed.next(false);
        return isSuccessful;
    }

    private refreshTokenErrorHandler(error: any): void {
        this._tokenIsBeingRefreshed.next(false);
        this._storageHelper.ClearAuth();
        console.log("Refresh failed from server or client. Removed tokens");
        console.log(error);
        // TODO: Implement sessiontimeout page
        // this._router.navigate(['/sessiontimeout']);
    }

    private authGet(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.get(url, options);
    }

    private authPost(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.post(url, body, options);
    }

    private authPut(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.put(url, body, options);
    }

    private authDelete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.delete(url, options);
    }

    private authPatch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.patch(url, body, options);
    }

    private authHead(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.head(url, options);
    }

    private authOptions(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this._authHttp.options(url, options);
    }
}
