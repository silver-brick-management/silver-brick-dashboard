// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { storage } from 'firebase';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class FirebaseStorage {

    constructor(
        private _angularFireAuth: AngularFireAuth) {
    }

    // https://github.com/apache/cordova-plugin-camera#cameraoptions-errata-
    GetImageOptions(): any {
        return {
            quality: 50,
            allowEdit: true,
            sourceType: 0,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            encodingType: 0,
            destinationType:0
        };
    }
    Upload(path: string, data: File, contentType: string): storage.UploadTask {
        return this._angularFireAuth.app.storage().ref(path).put(
            data,
            {
                contentType: 'image/png'
            }
        );
    }

    UploadTrack(path: string, data: File, contentType: string): storage.UploadTask {
        return this._angularFireAuth.app.storage().ref(path).put(
            data,
            {
                contentType: 'audio/mpeg'
            }
        );
    }
}
