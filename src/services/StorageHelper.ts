// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable } from '@angular/core';

// Local modules
import { Constants } from '../common/Constants';
import { IUser } from '../shared/SilverBrickTypes';
import { ErrorMapper } from '../common/ErrorMapper';

@Injectable()
export class StorageHelper {

    constructor() {

    }

    async StoreAuth(token: string, profile: IUser): Promise<void> {
        try {
            localStorage.setItem(Constants.AUTH_TOKEN_STORAGE_NAME, token);
            localStorage.setItem(Constants.AUTH_PROFILE_STORAGE_NAME, JSON.stringify(profile));
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    async ClearAuth(): Promise<void> {
        try {
            localStorage.removeItem(Constants.AUTH_TOKEN_STORAGE_NAME);
            localStorage.removeItem(Constants.AUTH_PROFILE_STORAGE_NAME);
        }
        catch (error) {
            throw ErrorMapper.GetErrorFromStorageError(error);
        }
    }

    StoreStripeAuth(token: string): void {
        localStorage.setItem("stripe:id_token", token);
    }

    ClearStripeAuth(): void {
        localStorage.removeItem("stripe:id_token");
    }

    StoreAccessToken(token: string): void {
        localStorage.setItem(Constants.AUTH_TOKEN_STORAGE_NAME, token);
    }

    GetAccessToken(): string {
        return localStorage.getItem(Constants.AUTH_TOKEN_STORAGE_NAME) as string;
    }

    StoreStripeAccessToken(token: string): void {
        localStorage.setItem("stripe:id_token", token);
    }

    GetAccessStripeToken(): string {
        return localStorage.getItem("stripe:id_token") as string;
    }
}
