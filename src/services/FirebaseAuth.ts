// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable } from '@angular/core';
import { FirebaseError, User } from 'firebase';
import * as firebase from "firebase";
import { AngularFireAuth } from 'angularfire2/auth';

// Local modules
import { ErrorMapper } from '../common/ErrorMapper';

@Injectable()
export class FirebaseAuth {

    constructor(public angularFire: AngularFireAuth) { }

    Initialize(): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            this.angularFire.authState.subscribe((firebaseState: any) => {
                if (firebaseState) {
                    resolve(firebaseState);
                } else {
                    reject();
                }
            },
            (error: any) => {
                console.error("Firebase exception! " + error);
                reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
            });
        });
    }

    GetToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.angularFire.auth.currentUser.getToken(true)
                .then((idToken: string) => {
                    resolve(idToken);
                })
                .catch((error: FirebaseError) => {
                    reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    Login(username: string, password: string): Promise<User> {
        // Trim, since Android sometimes adds a space unintended by the user to the end of the user name.
        username = username.trim();
        return new Promise<User>((resolve, reject) => {
            this.angularFire.auth
                .signInWithEmailAndPassword(
                username, password)
                .then((firebaseState: any) => {
                    resolve(firebaseState);
                })
                .catch((error: FirebaseError) => {
                    console.error("Firebase exception! " + error);
                    reject(ErrorMapper.GetErrorFromFirebaseAuthError(error));
                });
        });
    }

    async Logout(): Promise<void> {
        try {
            await this.angularFire.auth.signOut();
            console.log("Successfully logged out!");
        }
        catch (error) {
            console.error("There was a problem logging out! Error - " + error);
            throw ErrorMapper.GetErrorFromFirebaseAuthError(<firebase.FirebaseError>error);
        }
    }
}
