// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import 'rxjs/add/operator/map';

import { User } from 'firebase';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { AuthConfigConsts, tokenNotExpired } from 'angular2-jwt';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

// Local modules
import { ErrorMapper } from '../common/ErrorMapper';
import { FirebaseAuth } from '../services/FirebaseAuth';
import { IServerCredentials } from '../interfaces/IParams';
import { StorageHelper } from '../services/StorageHelper';
import { IServerResponseLogin, IServerResponseRefreshToken } from '../interfaces/IResponse';
import { Constants, Urls, JSON_HEADER, JSON_CONTENT_TYPE } from '../common/Constants';
import { SessionData } from "../providers/SessionData";

@Injectable()
export class AuthApi {

    constructor(
        private _http: Http,
        private _sessionData: SessionData,
        private _firebaseAuth: FirebaseAuth,
        private _storageHelper: StorageHelper) { }

    Login(firebaseUser: User): Observable<IServerResponseLogin> {
        return Observable.fromPromise(<Promise<string>>firebaseUser.getToken(true))
        .switchMap((idToken) => {
            const serverCredentials: IServerCredentials = {
                email: firebaseUser.email,
                id_token: idToken
            };
            return this._http.post(this._sessionData.API_LOGIN_V1, JSON.stringify(serverCredentials), JSON_HEADER)
                .map((response: Response) => response.json())
                .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
        });
    }

    RefreshToken(): Observable<IServerResponseRefreshToken> {
        return Observable.fromPromise(this._firebaseAuth.GetToken())
            .switchMap((idToken: string) => {
                const url = this._sessionData.API_REFRESH_V1;
                const body = { id_token: idToken };
                const token: string = this._storageHelper.GetAccessToken();
                const requestHeader = new Headers(JSON_CONTENT_TYPE);
                requestHeader.set(AuthConfigConsts.DEFAULT_HEADER_NAME, AuthConfigConsts.HEADER_PREFIX_BEARER + token);
                const options = new RequestOptions({ headers: requestHeader });

                return this._http.post(url, body, options)
                    .map((response: Response) => response.json())
                    .catch((error) => Observable.throw(ErrorMapper.GetErrorFromServerResponseError(error.status, error.json())));
            });
    }

    TokenRequiresRefresh(): boolean {
        return !tokenNotExpired(Constants.AUTH_TOKEN_STORAGE_NAME);
    }
}
