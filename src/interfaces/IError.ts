// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

export enum ErrorType {
    AUTH_UNAUTHORIZED,
    AUTH_UNKNOWN_ERROR,
    AUTH_USER_NOT_FOUND,
    AUTH_WRONG_PASSWORD
}

export interface IError {
    Data: any;
    Type: ErrorType;
    Message: string;
}
