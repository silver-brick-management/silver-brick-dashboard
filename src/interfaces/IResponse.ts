// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Local modules
import { IUser, IAssignee, ILocation, ITaskFeed, ITask, ITimeCard, IBooking, IService, IMessage, IOrgInfo, IOrgBasic, IBuildingBasic, IBuilding } from "../shared/SilverBrickTypes";

/* COMMON */

export interface IServerResponseBase {
    success: boolean;
    description?: string;
}

export interface IServerResponseMessage extends IServerResponseBase {
    message: string;
}

export interface IServerResponseExisting extends IServerResponseBase {
    baseId: string;
}

export interface IServerResponseGetCalendarColors extends IServerResponseBase {
    data: any;
}

export interface IServerResponseRefreshToken extends IServerResponseBase {
    accessToken: string;
}

export interface IServerResponseLogin extends IServerResponseRefreshToken {
    data: IUser;
}

export interface IServerResponseGetUsers extends IServerResponseBase {
    data: IUser[];
    paging: IPaging;
}

export interface IServerResponseGetBuildingUsers extends IServerResponseBase {
    data: IUser[];
}

export interface IServerResponseAddOrUpdateOrGetUser extends IServerResponseBase {
    data: IUser;
}

export interface IServerResponseImportUsers extends IServerResponseBase {
    data: IUser[];
}

export interface IServerResponseGetProfile extends IServerResponseBase {
    data: IUser;
}

export interface IServerResponseGetAllOrgs extends IServerResponseBase {
    data: IOrgBasic[];
}

export interface IServerResponseGetOrgInfo extends IServerResponseBase {
    data: IOrgInfo;
}

export interface IServerResponseCreateOrg extends IServerResponseBase {
    data: IOrgBasic;
}

export interface IServerResponseCreateBldg extends IServerResponseBase {
    data: IBuilding;
}

export interface IServerResponseAddUpdateUserLocation extends IServerResponseBase {
    data: ILocation;
}

/*
  Messages Related
*/

export interface IServerResponseGetMessageHistory extends IServerResponseBase {
    data: IGetMessageHistoryHelper;
}

export interface IServerResponseAddMessage extends IServerResponseBase {
    data: AddMessageHelper;
}

export interface IServerResponseNewRoom extends IServerResponseBase {
    data: NewChatRoomHelper;
}

export interface IServerResponseGetChatRooms extends IServerResponseBase {
    data: UserChatRoom[];
}

export interface IServerResponseGetBookings extends IServerResponseBase {
    data: IBooking[];
}

export interface IServerResponseAssignBooking extends IServerResponseBase {
    data: IBooking;
}

export interface IServerResponseGetTasks extends IServerResponseBase {
    data: ITask[];
}

export interface IServerResponseAddUpdateAssignTask extends IServerResponseBase {
    data: ITask;
}

export interface IServerResponseAssignTask extends IServerResponseBase {
    data: AssignTaskHelper;
}

export interface IServerResponseGetTaskHistory extends IServerResponseBase {
    data: ITaskFeed[];
}

export interface IServerResponseAddTaskHistory extends IServerResponseBase {
    data: ITaskFeed;
}

export interface IServerResponseChangePassword extends IServerResponseBase {
    data: boolean;
}

export interface IServerResponseGetTimeCardRoster extends IServerResponseBase {
    data: ITimeCardRoster;
}

export interface IServerResponseAddUpdateUserLocation extends IServerResponseBase {
    data: ILocation;
}

export interface ITaskFeedJoin {
    orgID: string;
    buildingID: string;
    taskID: string;
}

/* EXTRAS */

export interface IPaging {
    cursors: ICursors;
    previous: string;
    next: string;
}

export interface ICursors {
    prevCursor: string;
    nextCursor: string;
}

export interface AssignTaskHelper {
    userID: string;
    authorID: string;
    task: ITask;
    message?: string;
}

/*
  Chat Helpers
*/

export interface IGetMessageHistoryHelper {
    messages: IMessage[];
    hasMoreMessages: boolean;
    cursor: string;
    isInitialLoad: boolean;
}
export interface AddMessageHelper {
    taskID: string;
    message: ITaskFeed;
}

export interface NewChatRoomHelper {
    message: IMessage;
    participants: MessageParticipant[];
    roomID?: string;
    authorID: string;
}

export interface UserChatRoom {
    users: MessageParticipant[];
    roomID: string;
    lastMessage: IMessage;
}

export interface MessageParticipant {
    uid: string;
    photoURL: string;
    role: string;
    authour: string;
}

export interface ITimeCardToday {
    name: string;
    uid: string;
    cards: ITimeCard[];
}

export interface ITimeCardRoster {
    [key: string]: ITimeCardToday;
}

export interface GetTimeCardHelper {
    date: number;
    uid: string;
    year?: number;
    month?: number;
    day?: number;
}
