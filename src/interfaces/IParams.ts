// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Local modules
import { ISilverBrickUser, IBuildingSimple, IUser, IOrgInfo, IBuilding, ITask } from '../shared/SilverBrickTypes';

export interface ICredentials {
  email: string;
  password: string;
}

export interface IServerCredentials {
  email: string;
  id_token: string;
}

export interface IPaginateQueryStrings {
  limit?: string;
  isForward?: boolean;
  prevCursor?: string;
  nextCursor?: string;
}

export interface IUserActivationParam {
  uid: string;
  activationStatus: boolean;
}

export interface IUserDialogBase {
  action: string;
}

export interface IInputDialogBase extends IUserDialogBase {
    input: string;
    date?: Date;
}

export interface IUserDialogResponse extends IUserDialogBase {
  user: IUser;
}

export interface ITaskDialogResponse extends IUserDialogBase {
  task: ITask;
}

export interface IAudienceDialogResponse extends IUserDialogBase {
  selected: IUser;
}

export interface IBuildingDialogResponse extends IUserDialogBase {
  selected: IBuildingSimple;
}

export interface IOrgDialogResponse extends IUserDialogBase {
    org: IOrgInfo;
}

export interface IBldgDialogResponse extends IUserDialogBase {
    building: IAddBldgHelper;
}

export interface IAddBldgHelper {
    building: IBuilding;
    orgID: string;
}

export interface IGetBldgHelper {
    buildingID: string;
    orgID: string;
}

export interface IFormSelctedValue {
  id: string;
  index: number;
}
