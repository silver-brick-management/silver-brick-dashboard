// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

export enum ConfigContextType {
    STANDARD_DEV
}

export class IConfigContext {
    static readonly FirebaseAPIKey: string;
    static readonly FirebaseAuthDomain: string;
    static readonly FirebaseDatabaseURL: string;
    static readonly FirebaseStorageBucket: string;
    static readonly FirebaseMessengerSenderID: string;
    static readonly SilverBrickApiURL: string;
}
