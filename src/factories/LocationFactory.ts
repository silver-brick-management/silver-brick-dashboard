import { IAddress } from '../shared/SilverBrickTypes';

export class LocationFactory implements IAddress {
	city: string;
	line1: string;
	line2: string;
	state: string;
	zip: string;

	constructor() {
		this.city = "None";
		this.line1 = "Line 1";
		this.line2 = "Line 2";
		this.state = "NA";
		this.zip = "10010";
	}

	static CreateInstance(): IAddress {
		let newLocation: IAddress = null;
		newLocation = new LocationFactory();
		return newLocation;
	}
}