// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialogRef, MdDialog, MdDialogConfig } from "@angular/material";
import { Component, Optional, ChangeDetectionStrategy } from "@angular/core";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";

// Local modules
import { AppState } from "../../app/app.state";
import { IOrgState } from "../../interfaces/IStoreState";
import { OrgActions } from "../../store/actions/OrgActions";

// Component Modules
import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { CollapsibleControlComponent } from "../collapsible-control/collapsible-control.component";
import { BuildingUsersComponent } from "../building-users/building-users.component";

// Type Related Modules
import {
    IBuildingBasic,
    IBuilding,
    IBuildingSimple,
    IBuildingConfig,
    IBuildingInfo,
} from "../../shared/SilverBrickTypes";
import { IBldgDialogResponse, IUserDialogBase, IFormSelctedValue, IAddBldgHelper } from "../../interfaces/IParams";
import { IError } from "../../interfaces/IError";
import { Constants, STATE_LIST, PROPERTY_TYPES } from "../../common/Constants";

// Utils and Helper modules
import { Controls } from "../../providers/Controls";
import { FormValidators } from "../../common/Validators";
declare var google: any;

@Component({
    templateUrl: "./building-details.component.html",
    styleUrls: ["./building-details.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BldgDetailComponent {
    private _card: HTMLElement = null;
    private _input: HTMLElement = null;
    private _searchBox: any;
    private _orgStateSubscription: Subscription;
    private _screenSize: number = window.screen.width;

    public bldgForm = this._formBuilder.group({
        info: this._formBuilder.group({
            name: ["", Validators.required],
            phone: ["N/A", Validators.required],
            address: this._formBuilder.group({
                line1: ["", Validators.required],
                line2: [""],
                city: ["Brooklyn", Validators.required],
                state: ["NY", Validators.required],
                zip: ["11231", Validators.required],
            }),
            location: this._formBuilder.group({
                lat: [0],
                lng: [0]
            }),
            type: ["Residential", Validators.required],
        }),
        config: this._formBuilder.group({
            isEmailSendingEnabled: [false, Validators.required],
            isSMSSendingEnabled: [false, Validators.required],
            propertyCodes: [""],
        }),
    });

    public HasSelectedAnAddress: boolean = false;

    public orgID: string = "";
    public isNewBuilding: boolean = false;
    public building: IBuilding = null;
    public stateList: string[] = STATE_LIST;
    public selectedState: string = "Choose a State";
    public selectedPropertyType: string = "";
    public userStateError: IError;
    public isOrgStateLoading: boolean;

    public bldgDetailTitle: String = "";
    public allBuildings: IBuildingSimple[] = [];
    public showExtras: boolean = true;
    public ScheduleListForms: FormArray;
    public orgState$: Observable<IOrgState>;
    public propertyTypes: string[] = PROPERTY_TYPES;

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    constructor(
        private _dialog: MdDialog,
        private _controls: Controls,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _orgActions: OrgActions,
        @Optional() public dialogRef: MdDialogRef<BldgDetailComponent>
    ) {
        this.orgState$ = this._store.select((state) => state.orgState);
    }

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {
        this._card = document.getElementById("pac-card") as HTMLElement;
        this._input = document.getElementById("pac-input") as HTMLInputElement;
        const biasInputElement = document.getElementById("use-location-bias") as HTMLInputElement;
        const strictBoundsInputElement = document.getElementById("use-strict-bounds") as HTMLInputElement;
        this._store.dispatch(this._orgActions.GetAllOrgsScope());
        if (!this.isNewBuilding) {
            this._orgStateSubscription = this.orgState$.subscribe((orgState: IOrgState) => {
                if (null != orgState && null != orgState.selectedBldg) {
                    this.building = orgState.selectedBldg;
                    this.selectedState = this.building.info.address.state;

                    this.bldgForm.patchValue({
                        info: {
                            name: this.building.info.name || "",
                            phone: this.building.info.phone || "",
                            address: this.building.info.address,
                            type: this.building.info.type ? this.building.info.type : "",
                        },
                        config: {
                            isEmailSendingEnabled:
                                null != this.building.config ? this.building.config.isEmailSendingEnabled : false,
                            isSMSSendingEnabled:
                                null != this.building.config ? this.building.config.isSMSSendingEnabled : false,
                            propertyCodes:
                                null != this.building.config.propertyCodes ? this.building.config.propertyCodes : "",
                        },
                    });
                }
            });
        } else {
            this.bldgDetailTitle = "New Building";
            this._orgStateSubscription = this.orgState$.subscribe((orgState: IOrgState) => {
                if (null != orgState.allBuildings) {
                    this.allBuildings = orgState.allBuildings;
                }
            });
        }
    }

    public onPropertyChange(type: string): void {
        this.selectedPropertyType = type;
    }

    public onStateChange(state: string): void {
        this.selectedState = state;
    }

    public AutoComplete(search: string): void {
        if (null == this._input) {
            this._card = document.getElementById("pac-card") as HTMLElement;
            this._input = document.getElementById("pac-input") as HTMLInputElement;
        }
        const options = {
            fields: ["formatted_address", "address_component", "geometry", "name"],
            strictBounds: false,
            types: ["address"],
        };
        const autocomplete = new google.maps.places.Autocomplete(this._input , options);

        const infowindow = new google.maps.InfoWindow();
        const infowindowContent = document.getElementById("infowindow-content") as HTMLElement;

        infowindow.setContent(infowindowContent);

        autocomplete.addListener("place_changed", () => {
            infowindow.close();
            const place = autocomplete.getPlace();
            let street_number = place.address_components.find((data: any) => {
                return data.types.includes("street_number");
            });
            let route = place.address_components.find((data: any) => {
                return data.types.includes("route");
            });
            // console.log("route", route, street_number);
            let addressParts: string[] = place.formatted_address.split(",");
            let zipParts: string[] = addressParts[addressParts.length - 2].split(" ");
            let city: string = addressParts[addressParts.length - 3];
            let state: string = zipParts[1];
            let zip: string = zipParts[2];
            let lat = place.geometry.location.lat();
            let lng = place.geometry.location.lng();
            // console.log("addressParts", zipParts, addressParts[addressParts.length - 2], city, state);
            this.bldgForm.patchValue({
                info: {
                    name: `${street_number.short_name} ${route.short_name}`,
                    address: {
                        line1: `${street_number.short_name} ${route.short_name}`,
                        city: city,
                        state: state,
                        zip: zip,
                    },
                    location: {
                        lat: lat,
                        lng: lng
                    }
                },
            });
            this.HasSelectedAnAddress = true;
        });
    }

    public saveAction(): void {
        if (this.bldgForm.valid && this.bldgForm.dirty) {
            const newBldg: IBuilding = this.bldgForm.value;
            console.log("New Building", newBldg);
            let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
            if (this.building) {
                action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                newBldg.id = this.building.id;
            } else {
                // Create a IUser object and add it to the dialogResult
            }
            // if (this.building && this.HasSelectedAnAddress) {
                if (null == this.building) {
                    let match: IBuildingSimple = this.allBuildings.find((org: IBuildingSimple) => {
                        return org.name.toLowerCase() === newBldg.info.name.toLowerCase();
                    });
                    if (null == match || (null != match && null != match.orgID && match.orgID !== this.orgID)) {
                        const addBuildingHelper: IAddBldgHelper = {
                            orgID: this.orgID,
                            building: newBldg,
                        };
                        const dialogResponse: IBldgDialogResponse = {
                            action: action,
                            building: addBuildingHelper,
                        };
                        this._handleOrgDetailAction(dialogResponse);
                        // this.dialogRef.close(dialogResponse);
                    } else {
                        this._controls
                            .ShowDialog(
                                "Duplicate Building",
                                "There is already an building with this name. Please confirm you want to proceed",
                                "Proceed",
                                "Cancel"
                            )
                            .subscribe((action: any) => {
                                if (action && action.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                                    const addBuildingHelper: IAddBldgHelper = {
                                        orgID: this.orgID,
                                        building: newBldg,
                                    };
                                    const dialogResponse: IBldgDialogResponse = {
                                        action: action,
                                        building: addBuildingHelper,
                                    };
                                    this._handleOrgDetailAction(dialogResponse);
                                }
                            });
                    }
                } else {
                    const addBuildingHelper: IAddBldgHelper = {
                        orgID: this.orgID,
                        building: newBldg,
                    };
                    const dialogResponse: IBldgDialogResponse = {
                        action: action,
                        building: addBuildingHelper,
                    };
                    this._handleOrgDetailAction(dialogResponse);
                }
            // } else {
            //     this._controls.ShowAlert(
            //         "Please select an address",
            //         "Please select a Google verified address for this building.",
            //         "Try Again"
            //     );
            // }
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public closeAction(): void {
        if (this.bldgForm.dirty) {
            // Ask user if they want to close since they have changed something
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

    public buildingUsers(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "100%";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "900px";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(BuildingUsersComponent, dialogConfig);
        if (!this.isNewBuilding) {
            dialogRef.componentInstance.orgID = this.orgID;
            dialogRef.componentInstance.buildingID = this.building.id;
            dialogRef.componentInstance.buildingName = this.building.info.name;
        } else {
            // No user to push to the dialog
            // Do nothing
        }
        dialogRef.componentInstance.PopulateForm();
    }

    /* Helper Methods */

    private _bindScheduleListForm(): FormArray {
        this.ScheduleListForms = this._formBuilder.array([]);
        return this.ScheduleListForms;
    }

    private _dismissDialog(): void {
        const dialogResponse: IBldgDialogResponse = {
            action: Constants.DIALOG_RESPONSE_ACTION_CANCEL,
            building: null,
        };
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.dialogRef.close(dialogResponse);
    }

    private _handleOrgDetailAction(response: IBldgDialogResponse) {
        this.orgState$
            .skip(2)
            .take(1)
            .subscribe((currentOrgState) => {
                if (!currentOrgState.error) {
                    this.dialogRef.close(response);
                } else {
                    // There was an error, stay on the page
                }
            });
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD: {
                this._store.dispatch(this._orgActions.AddBldg(response.building));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE: {
                this._store.dispatch(this._orgActions.UpdateBldg(response.building));
                break;
            }

            default: {
                break;
            }
        }
    }
}
