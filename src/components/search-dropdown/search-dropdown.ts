import { Component, Input, forwardRef, HostListener, ElementRef, Output, EventEmitter, ViewChild } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { ArrayUtil } from "../../common/ArrayUtil";

@Component({
    selector: "search-dropdown",
    templateUrl: "search-dropdown.html",
    styleUrls: ["search-dropdown.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SearchDropdown),
            multi: true,
        },
    ],
})
export class SearchDropdown implements ControlValueAccessor {
    @Output() afterChange = new EventEmitter();
    @ViewChild("input", { read: false }) input: ElementRef;
    @Input("size") size;
    @Input("items") set items(value) {
        this.list = ArrayUtil.Transform(value);
        this.temp_list = ArrayUtil.Transform(value);
        console.log("list", this.list);
    }
    @Input("img") img;
    @Input("label") label;
    @Input("uid") uid;
    @Input() Placeholder: string;
    @Input() ShowingPeople: boolean;
    onChange: any = () => {};
    onTouch: any = () => {};

    public list = [];
    private _img: any;
    private _label: any;
    private _uid: any;
    public temp_list = [];
    public keyword = "";
    public value: any = "Search a User";
    public shown = false;
    
    constructor(private ele: ElementRef) {}

    ngOnChanges(): void {
        this._label = typeof this.label !== "undefined" && this.label !== "" ? this.label : "name";
        this._img = typeof this.img !== "undefined" && this.img !== "" ? this.img : "img";
        this._uid = typeof this.uid !== "undefined" && this.uid !== "" ? this.uid : "id";
        this.value = this.ShowingPeople ? 'Filter by Staff' : 'Filter by Property';
    }

    public writeValue(value) {
        if (value) {
            this.temp_list.map((x) => {
                if (x[this._uid] == value) {
                    this.value = x[this._label];
                }
            });
        }
    }

    public registerOnChange(fn: any) {
        this.onChange = fn;
    }

    public registerOnTouched(fn: any) {
        this.onTouch = fn;
    }

    public search(e) {
        const val = e.toLowerCase();
        const temp = this.temp_list.filter((x) => {
            let name: string = this.ShowingPeople ? `${x.firstName} ${x.lastName}` : `${x.name}`;
            if (name.toLowerCase().indexOf(val) !== -1 || !val) {
                return x;
            }
        });
        this.list = temp;
    }

    public select(item) {
        this.onChange(`${item.firstName} ${item.lastName}`);
        this.value = this.ShowingPeople ? `${item.firstName} ${item.lastName}` : `${item.name}`;
        this.shown = false;
        this.afterChange.emit(item);
    }
    
    public show() {
        this.shown = this.shown ? false : true;
        setTimeout(() => {
            this.input.nativeElement.focus();
        }, 200);
    }

    @HostListener("document:click", ["$event"]) onClick(e) {
        if (!this.ele.nativeElement.contains(e.target)) {
            this.shown = false;
        }
    }
}
