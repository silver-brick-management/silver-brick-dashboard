// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, Optional } from '@angular/core';
import { MdDialogRef } from '@angular/material';

// Local modules
import { Constants } from '../../common/Constants';
import { IUserDialogBase, IInputDialogBase } from '../../interfaces/IParams';

@Component({
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {

    public Title: string = "Confirm";
    public Message: string = "Are you sure you want to proceed?";
    public OkString: string = "Proceed";
    public CancelString: string = "Cancel";
    public IsInputting: boolean = false;
    public InputEmail: string = '';
    public AssignMessage: string = '';
    public IsAssigning: boolean = false;
    public SelectedDate: Date = new Date(Date.now());
    public IsRescheduling: boolean = false;
    public SecondOkString: string = '';

    constructor(@Optional() public dialogRef: MdDialogRef<ConfirmDialogComponent>) {

    }

    public onDateSelect(event: any): void {
        console.log("onDateSelect", event);
        this.SelectedDate = event;
    }

    public okAction(): void {
        if (this.IsInputting) {
            const dialogResponse: IInputDialogBase = {
                input: this.InputEmail,
                action: Constants.DIALOG_RESPONSE_ACTION_OK
            };
            this.dialogRef.close(dialogResponse);
        } else if (this.IsAssigning) {
            const dialogResponse: IInputDialogBase = {
                input: this.AssignMessage,
                action: Constants.DIALOG_RESPONSE_ACTION_OK
            };
            this.dialogRef.close(dialogResponse);
        } else if (this.IsRescheduling) {
            const dialogResponse: IInputDialogBase = {
                input: this.AssignMessage,
                date: this.SelectedDate,
                action: Constants.DIALOG_RESPONSE_ACTION_OK
            };
            console.log("SelectedDate", dialogResponse.date);
            this.dialogRef.close(dialogResponse);
        } else {
            const dialogResponse: IUserDialogBase = {
                action: Constants.DIALOG_RESPONSE_ACTION_OK
            };
            this.dialogRef.close(dialogResponse);
        }
    }

    public secondOkAction(): void {
        const dialogResponse: IInputDialogBase = {
            input: this.AssignMessage,
            date: this.SelectedDate,
            action: Constants.DIALOG_RESPONSE_ACTION_OK
        };
        console.log("SelectedDate", dialogResponse.date);
        this.dialogRef.close(dialogResponse);
    }

    public cancelAction(): void {
        if (this.IsInputting) {
            const dialogResponse: IInputDialogBase = {
                input: this.InputEmail,
                action: Constants.DIALOG_RESPONSE_ACTION_CANCEL
            };
            this.dialogRef.close(dialogResponse);
        } else {
            const dialogResponse: IUserDialogBase = {
                action: Constants.DIALOG_RESPONSE_ACTION_CANCEL
            };
            this.dialogRef.close(dialogResponse);
        }
    }
}
