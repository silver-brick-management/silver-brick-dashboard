// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialogRef, MdDialog, MdDialogConfig, MdSnackBar } from '@angular/material';
import { Component, Optional, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

// Local modules
import { AppState } from '../../app/app.state';
import { IOrgState, OrgStateType } from '../../interfaces/IStoreState';

// Action Modules
import { OrgActions } from '../../store/actions/OrgActions';

// Component Modules
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { CollapsibleControlComponent } from '../collapsible-control/collapsible-control.component';
import { BldgDetailComponent } from '../building-details/building-details.component';

// Type related
import { IOrgInfo, IOrgBasic, IBuildingBasic } from '../../shared/SilverBrickTypes';
import { IOrgDialogResponse, IUserDialogBase, IBldgDialogResponse, IFormSelctedValue } from '../../interfaces/IParams';
import { IError } from '../../interfaces/IError';
import { Constants, STATE_LIST } from '../../common/Constants';

// Utils modules
import { Controls } from '../../providers/Controls';
import { FormValidators } from '../../common/Validators';

@Component({
    templateUrl: './org-details.component.html',
    styleUrls: ['./org-details.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrgDetailComponent  {

    public orgForm = this._formBuilder.group({
    	name: ['', Validators.required],
        phone: ['N/A', Validators.required],
        address: this._formBuilder.group({
            line1: ['', Validators.required],
            line2: [''],
            city: ['Brooklyn', Validators.required],
            state: ['NY', Validators.required],
            zip: ['11231', Validators.required]
        })
    });

    public org: IOrgInfo = null;
    public orgID: string;
    public allOrgs: IOrgBasic[] = [];
    public stateList: string[] = STATE_LIST;
    public selectedState: string = "Choose a State";
    public userStateError: IError;
    public isOrgStateLoading: boolean;

    public orgDetailTitle: String = "";
    public showExtras: boolean = true;

    public orgState$: Observable<IOrgState>;
    public selectedOrg$: Observable<IOrgInfo>;
    private _orgStateSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    
    get IsMobile(): boolean {
        return this._screenSize < 768;
    }
    get IsNewOrg(): boolean {
        return null === this.org;
    }

    constructor(
        private _controls: Controls,
        private _dialog: MdDialog,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _orgActions: OrgActions,
        private _snackBar: MdSnackBar,
        @Optional() public dialogRef: MdDialogRef<OrgDetailComponent>) {
        this.selectedOrg$ = this._store.select(state => state.orgState.selectedOrg);
        this.orgState$ = this._store.select(state => state.orgState);
    }

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {

        if (this.org) {
            this._orgStateSubscription = this.orgState$
            .subscribe((orgState: IOrgState) => {
                if (null != orgState.selectedOrg) {
                    this.orgDetailTitle = `${orgState.selectedOrg.name}`;
                    // Set form values from existing
                    this.selectedState = orgState.selectedOrg.address.state;
                    this.org = orgState.selectedOrg;
                    this.orgForm.patchValue({
                        name: this.org.name,
                        phone: this.org.phone,
                        address: this.org.address
                    });
                }
            
            });
        } else {
            this.org = null;
            this.orgDetailTitle = "New Org";
            this._orgStateSubscription = this.orgState$
            .subscribe((orgState: IOrgState) => {
                
                if (null != orgState.allOrgs) {
                    this.allOrgs = orgState.allOrgs;
                }
            });
        }
    }

    public onStateChange(state: string): void {
        console.log("State Change", state);
    	this.selectedState = state;
    }
    
    public saveAction(): void {
        if (this.orgForm.valid && this.orgForm.dirty) {
            const newOrg: IOrgInfo = this.orgForm.value;
            let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
            if (this.org) {
                action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                newOrg.id = this.orgID;
            } else {
                // Create a IUser object and add it to the dialogResult
            }
            if (null == this.org) {
                let match: IOrgBasic = this.allOrgs.find((org: IOrgBasic) => {
                    return (org.name.toLowerCase() === newOrg.name.toLowerCase());
                });
                console.log("match", this.allOrgs);
                if (null == match) {
                     const dialogResponse: IOrgDialogResponse = {
                        action: action,
                        org: newOrg
                    };
                    this._handleOrgDetailAction(dialogResponse);
                    // this.dialogRef.close(dialogResponse);
                   
                } else {
                    this._controls.ShowDialog("Duplicate Org", "There is already an org with this name. Please confirm you want to proceed", "Proceed", "Cancel")
                    .subscribe((action: any) => {
                        if (action && (action.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                             const dialogResponse: IOrgDialogResponse = {
                                action: action,
                                org: newOrg
                            };
                            this._handleOrgDetailAction(dialogResponse);
                            // this.dialogRef.close(dialogResponse);
                        }
                    });
                }
            } else {
                const dialogResponse: IOrgDialogResponse = {
                    action: action,
                    org: newOrg
                };
                this._handleOrgDetailAction(dialogResponse);
            }
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public buildingAction(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '900px';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;
        this._store.dispatch(this._orgActions.GetAllOrgsScope());
        const dialogRef = this._dialog.open(BldgDetailComponent, dialogConfig);
        dialogRef.componentInstance.orgID = this.orgID;

        dialogRef.componentInstance.isNewBuilding = true;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IBldgDialogResponse) => {
            if (response) {
                this._handleBuildingDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public deleteAction(): void {
        this._controls.ShowDialog("Confirm Org Deletion", 'You are about to delete this org, which includes all of its buildings, tasks, tenants & landlords/owners', "Confirm", "Cancel")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    console.log("org", this.org, this.orgID)
                    const dialogResponse: IOrgDialogResponse = {
                        action: Constants.DIALOG_RESPONSE_ACTION_DELETE,
                        org: this.org
                    };
                    this._handleOrgDetailAction(dialogResponse);
                } else {

                }
            }
        });
    }

    public closeAction(): void {
        if (this.orgForm.dirty) {
            // Ask user if they want to close since they have changed something
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

   
    /* Helper Methods */
    
    // Creates a building group in the building
    // form array
    private _createBuilding(id: string, isAdmin: boolean = false, canCreate: boolean = true, hasNonUserDirectoryAccess: boolean = false): FormGroup {
        return this._formBuilder.group({
            id: [id, Validators.minLength(2)],
            isAdmin: [isAdmin, Validators.required],
            canCreate: [canCreate, Validators.required],
            hasNonUserDirectoryAccess: [hasNonUserDirectoryAccess, Validators.required]
        });
    }

    private _dismissDialog(): void {
        const dialogResponse: IOrgDialogResponse = {
            action: Constants.DIALOG_RESPONSE_ACTION_CANCEL,
            org: null
        };
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.dialogRef.close(dialogResponse);
    }

    private _handleOrgDetailAction(response: IOrgDialogResponse) {
        this.orgState$.skip(2).take(1).subscribe((currentOrgState) => {
            if (!currentOrgState.error) {
                this.dialogRef.close(response);
            } else {
                // There was an error, stay on the page
            }
        });
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                this._store.dispatch(this._orgActions.AddOrg(response.org));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                this._store.dispatch(this._orgActions.UpdateOrg(response.org));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE:
            {
                this._store.dispatch(this._orgActions.DeleteOrg(this.orgID));
                break;
            }

            default:
            {
                break;
            }
        }
    }

    private _handleBuildingDetailResponse(response: IBldgDialogResponse) {
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `Addition of a new Building named ${response.building.building.info.name} was successful`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `Update of a existing Building named ${response.building.building.info.name} was successful`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                break;
            }

            default:
            {
                break;
            }
        }
    }
}
