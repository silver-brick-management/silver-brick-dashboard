// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import * as moment from "moment";

import {
    Injectable,
    EventEmitter,
    Component,
    OnInit,
    OnChanges,
    OnDestroy,
    ViewChild,
    TemplateRef,
    Output,
    Optional,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { ICard, customers, IShippingInformation } from "stripe";
import { Router, NavigationExtras, ActivatedRoute, Data } from "@angular/router";
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay,
    DAYS_OF_WEEK,
    CalendarWeekViewEvent,
} from "angular-calendar";

import {
    getMonth,
    startOfMonth,
    startOfWeek,
    startOfDay,
    endOfWeek,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours,
} from "date-fns";
import RRule from "rrule";

// import { NgxSmartModalService, NgxSmartModalConfig } from "ngx-smart-modal";
import { Subject } from "rxjs/Subject";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { MdDialog, MdSnackBar, MdDialogConfig, MdSnackBarConfig } from "@angular/material";
import { ViewPeriod } from "calendar-utils";

// Local modules
import { AppState } from "../../../app/app.state";
import { Constants, Urls, colors, staffColors } from "../../../common/Constants";
import { IError } from "../../../interfaces/IError";
import {
    IUser,
    BeforeCalendarLoadMonth,
    BeforeCalendarLoadWeek,
    RecurringEvent,
    IOrgBasic,
    IAssignee,
    ITask,
    IBuildingSimple,
} from "../../../shared/SilverBrickTypes";
import { IUserState, UserStateType, IOrgState, OrgStateType, SessionStateType, ISessionState } from "../../../interfaces/IStoreState";
import { SessionActions } from "../../../store/actions/SessionActions";
import { UserActions } from "../../../store/actions/UserActions";
import { ITaskFeedJoin } from "../../../interfaces/IResponse";

import { OrgActions } from "../../../store/actions/OrgActions";
import { UserUtil } from "../../../common/UserUtil";
import { IPaginateQueryStrings, IUserDialogResponse, IGetBldgHelper, ITaskDialogResponse } from "../../../interfaces/IParams";
import { AudienceChooserComponent } from "../../audience-chooser/audience-chooser.component";

// import { UserDetailComponent } from '../components/user-details/user-details.component';
import { RecurringTaskDetailsComponent } from "../../../components/recurring-task-details/recurring-task-details.component";
import { SessionData } from "../../../providers/SessionData";
import { FirebaseAuth } from "../../../services/FirebaseAuth";
import { Controls } from "../../../providers/Controls";
import { RouteUtil } from "../../../common/RouteUtil";
export declare type WeekdayStr = "SU" | "MO" | "TU" | "WE" | "TH" | "FR" | "SA";
declare var google: any;

@Component({
    selector: "main-calendar",
    templateUrl: "./main-calendar.component.html",
    styleUrls: ["./main-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainCalendarComponent {
    @ViewChild("modalContent") modalContent: TemplateRef<any>;
    private _sessionStateSubscription: Subscription;
    private _commStateSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _usersSubscription: Subscription;
    private _screenSize: number = window.screen.width;

    public UserState$: Observable<IUserState>;
    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public IsLoading: boolean = false;
    public loading$: Observable<boolean>;
    public tasks$: Observable<Array<ITask>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public SessionState$: Observable<ISessionState>;
    public OrgState$: Observable<IOrgState>;
    public ListView: boolean = false;
    public CloseButton: string = "Will Do";
    public ModalTitle: string = "Setup Payment Info?";
    public ModalMessage: string = "";
    public view: string = "month";
    public ShowingMap: boolean = false;
    public SelectedDate: Date = null;
    public profile$: Observable<IUser>;
    public selectedStaff$: Observable<IUser>;
    public searchType$: Observable<string>;
    public searchRef: string = "";
    public FirstDay = DAYS_OF_WEEK.SUNDAY;
    public users$: Observable<Array<IUser>>;
    public IsReAssigning: boolean = false;
    public mapEvents$: Observable<Array<RecurringEvent>>;
    public WeekDays: WeekdayStr[] = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"];
    public DurationOptions: string[] = ["day(s)", "week(s)", "month(s)", "year(s)", "forever"];
    public FrequencyOptions: string[] = ["Daily", "Weekly", "Monthly", "Yearly"];
    public actions: CalendarEventAction[] = [
        {
            label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event, date }: { event: RecurringEvent; date: Date }): void => {
                // this.RecurringTask(event.task, date);
                this.handleEvent("Edit", event);
            },
        },
        {
            label: '<i style="color: #fff !important;" class="fa fa-fw fa-gear"></i>',
            onClick: ({ event, date }: { event: RecurringEvent; date: Date }): void => {
                // this.RecurringTask(event.task, date);

                // this.IsReAssigning = !this.IsReAssigning;
                this.handleEvent("Assign", event);
            },
        },
    ];
    public SelectedTask: ITask = null;
    public selectedStaff: IAssignee[] = [];
    public recurringEvents: RecurringEvent[] = [
        // {
        //     title: 'Trash (68 4th Place)',
        //     color: colors.yellow,
        //     rrule: {
        //         freq: RRule.MONTHLY,
        //         bymonthday: 5,
        //     },
        //     start: new Date(),
        //     actions: this.actions,
        //     resizable: {
        //         beforeStart: true,
        //         afterEnd: true
        //     },
        //     displayTitle: 'Trash (68 4th Place)'
        // }
    ];

    public todayDate: Date = new Date(Date.now());
    public viewDate: Date = new Date(this.todayDate.getFullYear(), this.todayDate.getMonth(), this.todayDate.getDate(), 12, 0);

    public modalData: {
        action: string;
        event: RecurringEvent;
    };

    public eventActions: CalendarEventAction[] = [
        {
            label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
            onClick: ({ event }: { event: RecurringEvent }): void => {
                this.handleEvent("Clicked", event);
            },
        },
    ];

    public refresh: Subject<any> = new Subject();

    public allEvents: RecurringEvent[] = [];
    public activeDayIsOpen: boolean = false;
    public isNewTask: boolean = false;
    public updateParams: BeforeCalendarLoadMonth | BeforeCalendarLoadWeek = null;

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsAdmin(): boolean {
        return null != this.sessionData && null != this.sessionData.user && this.sessionData.user.isAdmin;
    }

    constructor(
        public controls: Controls,
        public orgActions: OrgActions,
        public sessionData: SessionData,
        public userActions: UserActions,
        public modal: NgbModal,
        public firebaseAuth: FirebaseAuth,
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        public store: Store<AppState>,
        private cdr: ChangeDetectorRef,
        public sessionActions: SessionActions
    ) {
        this.AllBuildings$ = this.store.select((state) => state.orgState.allBuildings);
        this.users$ = this.store.select((state) => state.sessionState.usersScope);
        this.error$ = this.store.select((state) => state.usersState.error);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        this.OrgState$ = this.store.select((state) => state.orgState);
        this.SessionState$ = this.store.select((state) => state.sessionState);
        this.tasks$ = this.store.select((state) => state.sessionState.tasks);
        this.UserState$ = this.store.select((state) => state.usersState);
        this.profile$ = this.store.select((state) => state.authState.profile);
        this.selectedStaff$ = this.store.select((state) => state.sessionState.selectedStaff);
        this.mapEvents$ = this.store.select((state) => state.sessionState.mapEvents);
        this.searchType$ = this.store.select((state) => state.sessionState.searchType);
    }

    ngOnInit(): void {
        this.Initialize();
    }

    ngOnDestroy(): void {
        this.Unitialize();
    }

    @Output() notifyParent: EventEmitter<boolean> = new EventEmitter();
    StopLoading(): void {
        this.notifyParent.emit(false);
    }

    public async Initialize(): Promise<void> {
        this.store.dispatch(this.sessionActions.ChangeActiveDate(this.viewDate));
        if (null != this.sessionData.user) {
            if (!this.sessionData.user.isAdmin) {
                if (this.sessionData.user.role === "Tenant") {
                } else if (this.sessionData.user.role === "Landlord") {
                } else {
                    let bldgHelper: IGetBldgHelper = {
                        orgID: this.sessionData.org.id,
                        buildingID: this.sessionData.org.buildings[0].id,
                    };
                    this.IsLoading = true;
                    // // // // // console.log("bldgHelper", bldgHelper);
                    this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                }
            } else {
                this.IsLoading = true;
                // // // // // console.log("GetAllHistory");
                this.store.dispatch(this.userActions.GetUsers(null));
                // this.store.dispatch(this.orgActions.GetAllOrgsScope());
                //
            }
        }

        this._sessionStateSubscription = this.SessionState$.subscribe(
            async (state: ISessionState) => {
                if (null != state) {
                    switch (state.type) {
                        case SessionStateType.DELETE_RECURRING_TASK_SUCCEEDED:
                        {
                            if ((null != state.deletedTaskID)) {
                                let newAllEvents: RecurringEvent[] = [];
                                Object.assign({}, newAllEvents, this.allEvents);
                                newAllEvents = newAllEvents.filter((evt: RecurringEvent) => {
                                    return (evt.task.id !== state.deletedTaskID);
                                });
                                Object.assign({}, this.allEvents, newAllEvents);
                                // console.log("DELETE_RECURRING_TASK_SUCCEEDED", this.allEvents.length, state.deletedTaskID);
                                // this.updateCalendarEvents();
                                this.cdr.detectChanges();
                            }
                            break;
                        }

                        case SessionStateType.UPDATE_RECURRING_TASKS_SUCCEEDED:
                        {
                            if ((null != state.updatedTask)) {

                                this.allEvents = this.allEvents.map((evt: RecurringEvent) => {
                                    return evt.task.id === state.updatedTask.id ? Object.assign({}, evt, { task: state.updatedTask }) : evt;
                                });
                            }
                            break;
                        }

                        case SessionStateType.CREATE_RECURRING_TASKS_SUCCEEDED:
                        {
                            let task: ITask = state.recurringTasks[0];
                            let color: string = staffColors["notFound"];
                            if (null != task.assignees && task.assignees.length > 0) {
                                if (null != state.calendarColors) {
                                    color =
                                        null != state.calendarColors[task.assignees[0].uid]
                                            ? state.calendarColors[task.assignees[0].uid]
                                            : staffColors["notFound"];
                                } else {
                                    color =
                                        null != staffColors[task.assignees[0].uid] ? staffColors[task.assignees[0].uid] : staffColors["notFound"];
                                }
                            }
                            let newEvent: RecurringEvent = {
                                start: new Date(task.startDate),
                                title: `${task.subject}`,
                                actions: this.actions,
                                color: color,
                                resizable: {
                                    beforeStart: true,
                                    afterEnd: true,
                                },
                                meta: `${task.subject}`,
                                task: task,
                                displayTitle: `${task.subject}`,
                                draggable: true,
                            };
                            let matchDate: string = `${this.viewDate.getMonth() + 1}_${this.viewDate.getDate()}_${this.viewDate.getFullYear()}`;
                            if (null != task.completedDates && task.completedDates.includes(matchDate)) {
                                newEvent.completedToday = true;
                                newEvent.task.completedToday = true;
                            }
                            // // // // // console.log("newEvent", newEvent.start, newEvent.end);
                            // if (null != task.endDate) {
                            //    newEvent.end = new Date(task.endDate);
                            // }
                            switch (task.repeatInterval) {
                                case "Daily": {
                                    newEvent.rrule = {
                                        freq: RRule.DAILY,
                                        interval: task.repeatQuantity,
                                        until: new Date(task.endDate),
                                    };
                                    // // // // console.log("Daily", newEvent.rrule);
                                    break;
                                }

                                case "Weekly": {
                                    let newWeeklyMonthlyDays = [];
                                    if (this.viewDate.getMonth() === 10) {
                                        newEvent.rrule = {
                                            freq: RRule.MONTHLY,
                                            interval: task.repeatQuantity,
                                            byweekday: [],
                                            until: new Date(task.endDate),
                                        };
                                        if (null != task.monthlyWeekDays) {
                                            // await weekAction;
                                            let month = this.viewDate.getMonth();
                                            let d = new Date(this.viewDate.getFullYear(), month + 1, 0).getDate();
                                            let weekCount: number = this._weekCount(this.viewDate.getFullYear(), month);
                                            // // // console.log("weekCount", weekCount);
                                            for (let i = 1; i < 7; i++) {
                                                for (let day of task.monthlyWeekDays) {
                                                    // let weekAction = task.monthlyWeekDays.forEach((day: number) => {
                                                    let newDay: number = day;
                                                    if (i > 1) {
                                                        if (newDay === 6) {
                                                            newDay = 0;
                                                        } else {
                                                            newDay += 1;
                                                        }
                                                    }

                                                    if (null != RRule[this.WeekDays[newDay]]) {
                                                        if (i > 1) {
                                                            let dayOfWeeks = new Date(this.viewDate.getFullYear(), month + 1, 0).getDay();
                                                            if (i < weekCount) {
                                                                newEvent.rrule.byweekday.push(RRule[this.WeekDays[newDay]].nth(i));
                                                            } else if (i === weekCount && (newDay === dayOfWeeks || newDay - 1 === dayOfWeeks)) {
                                                                let newerDay: number = newDay + 1;
                                                                newEvent.rrule.byweekday.push(RRule[this.WeekDays[newerDay]].nth(i));
                                                            } else {
                                                            }
                                                        } else {
                                                            if (newDay !== 0) {
                                                                // if (d === this.viewDate.getDate()) {
                                                                newEvent.rrule.byweekday.push(RRule[this.WeekDays[newDay]].nth(i));
                                                                // }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            // await weekAction;
                                        }
                                    } else {
                                        if (null != task.monthlyWeekDays) {
                                            for (let day of task.monthlyWeekDays) {
                                                // let weekAction = task.monthlyWeekDays.forEach((day: number) => {
                                                let newDay: number = day;
                                                if (newDay === 0) {
                                                    newDay = 6;
                                                } else {
                                                    newDay -= 1;
                                                }
                                                newWeeklyMonthlyDays.push(newDay);
                                            }
                                            newEvent.rrule = {
                                                freq: RRule.WEEKLY,
                                                interval: task.repeatQuantity,
                                                byweekday: newWeeklyMonthlyDays,
                                                until: new Date(task.endDate),
                                            };
                                        }
                                    }
                                    // // // // // console.log("Weekly", newEvent.rrule);
                                    break;
                                }

                                case "Monthly": {
                                    if (task.monthlyConvention === "Day of month") {
                                        if (null != task.monthlyDays) {
                                            let newMonthlyDays = [];
                                            for (let day of task.monthlyDays) {
                                                newMonthlyDays.push(day + 1);
                                            }
                                            newEvent.rrule = {
                                                freq: RRule.MONTHLY,
                                                interval: task.repeatQuantity,
                                                bymonthday: newMonthlyDays,
                                                until: new Date(task.endDate),
                                            };
                                        }
                                    } else {
                                        let weekCount = 4;
                                        let dayCount = 1;
                                        newEvent.rrule = {
                                            freq: RRule.MONTHLY,
                                            interval: task.repeatQuantity,
                                            byweekday: [],
                                            until: new Date(task.endDate),
                                        };
                                        if (null != task.monthlyNWeekDays) {
                                            let keys = Object.keys(task.monthlyNWeekDays);
                                            for (let key of keys) {
                                                if (null != task.monthlyNWeekDays[Number(key)]) {
                                                    for (let day of task.monthlyNWeekDays[Number(key)]) {
                                                        if (null != day) {
                                                            newEvent.rrule.byweekday.push(RRule[this.WeekDays[day]].nth(Number(key) + 1));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // // // // // console.log("Monthly", newEvent.rrule);
                                    break;
                                }

                                case "Yearly": {
                                    let newMonthlyDays = [];
                                    for (let day of task.monthlyDays) {
                                        newMonthlyDays.push(day + 1);
                                    }
                                    newEvent.rrule = {
                                        freq: RRule.YEARLY,
                                        interval: task.repeatQuantity,
                                        bymonthday: newMonthlyDays,
                                        until: new Date(task.endDate),
                                        bymonth: new Date(task.startDate).getMonth(),
                                    };
                                    // // // // // console.log("Yearly", newEvent.rrule);
                                    break;
                                }

                                case "Once": {
                                    break;
                                }

                                default: {
                                    break;
                                }
                            }
                            this.recurringEvents.push(newEvent);
                            const startOfPeriod: any = {
                                month: startOfMonth,
                                week: startOfWeek,
                                day: startOfDay,
                            };

                            const endOfPeriod: any = {
                                month: endOfMonth,
                                week: endOfWeek,
                                day: endOfDay,
                            };
                            let date = this.viewDate;
                            date.setHours(12);
                            let endDate = this.viewDate;
                            endDate.setHours(12);
                            let newAllEvents: RecurringEvent[] = [];
                            Object.assign({}, newAllEvents, this.allEvents);
                            if (newEvent.task.repeatInterval !== "Once") {
                                const rule: RRule = new RRule(
                                    Object.assign({}, newEvent.rrule, {
                                        dtstart: startOfPeriod[this.view](date),
                                        until: endOfPeriod[this.view](endDate),
                                    })
                                );
                                let startHours: number = Number(newEvent.task.startTime.split(":", 2)[0]);
                                let endHours: number = Number(newEvent.task.endTime.split(":", 2)[0]);
                                let startMinutes: number = Number(newEvent.task.startTime.split(":", 2)[1]);
                                let endMinutes: number = Number(newEvent.task.endTime.split(":", 2)[1]);
                                let count: number = 0;
                                for (let date of rule.all()) {
                                    let matchDate = `${date.getMonth()}_${date.getDate()}_${date.getFullYear()}`;
                                    // // // console.log("skip", matchDate, ((null != event.task.skipDates) && (!event.task.skipDates.includes(matchDate))), (null == event.task.skipDates), event.task.subject, event.task.skipDates);
                                    if ((null != newEvent.task.skipDates && !newEvent.task.skipDates.includes(matchDate)) || null == newEvent.task.skipDates) {
                                        newEvent.currentDate = date;
                                        // // // console.log("event", event.task.subject, event.task.buildingName);
                                        newAllEvents.push(
                                            Object.assign({}, newEvent, {
                                                start: new Date(date.getFullYear(), date.getMonth(), date.getDate(), startHours, startMinutes),
                                                end: new Date(date.getFullYear(), date.getMonth(), date.getDate(), endHours, endMinutes),
                                            })
                                        );
                                    }
                                    setTimeout(() => {
                                        if (this.recurringEvents.length < 25) {
                                            // console.log("allEvents", this.allEvents);
                                        }
                                    }, 500);
                                }
                            } else {
                                newEvent.currentDate = date;
                                newAllEvents.push(newEvent);
                            }
                            Object.assign({}, this.allEvents, newAllEvents);
                            this.IsLoading = false;
                            this.cdr.detectChanges();
                            break;
                        }

                        case SessionStateType.GET_RECURRING_TASKS_SUCCEEDED:
                        case SessionStateType.SEARCH_TASKS_SUCCEEDED: {
                            this.IsLoading = true;
                            this.recurringEvents = [];
                            // // console.log("SessionStateType", SessionStateType[state.type]);
                            let eventAction = state.recurringTasks.forEach(async (task: ITask) => {
                                let color: string = staffColors["notFound"];
                                if (null != task.assignees && task.assignees.length > 0) {
                                    if (null != state.calendarColors) {
                                        color =
                                            null != state.calendarColors[task.assignees[0].uid]
                                                ? state.calendarColors[task.assignees[0].uid]
                                                : staffColors["notFound"];
                                    } else {
                                        color =
                                            null != staffColors[task.assignees[0].uid] ? staffColors[task.assignees[0].uid] : staffColors["notFound"];
                                    }
                                }
                                let newEvent: RecurringEvent = {
                                    start: new Date(task.startDate),
                                    title: `${task.subject}`,
                                    actions: this.actions,
                                    color: color,
                                    resizable: {
                                        beforeStart: true,
                                        afterEnd: true,
                                    },
                                    meta: `${task.subject}`,
                                    task: task,
                                    displayTitle: `${task.subject}`,
                                    draggable: true,
                                };
                                let matchDate: string = `${this.viewDate.getMonth() + 1}_${this.viewDate.getDate()}_${this.viewDate.getFullYear()}`;
                                if (null != task.completedDates && task.completedDates.includes(matchDate)) {
                                    newEvent.completedToday = true;
                                    newEvent.task.completedToday = true;
                                }
                                // // // // // console.log("newEvent", newEvent.start, newEvent.end);
                                // if (null != task.endDate) {
                                //    newEvent.end = new Date(task.endDate);
                                // }
                                switch (task.repeatInterval) {
                                    case "Daily": {
                                        newEvent.rrule = {
                                            freq: RRule.DAILY,
                                            interval: task.repeatQuantity,
                                            until: new Date(task.endDate),
                                        };
                                        // // // // console.log("Daily", newEvent.rrule);
                                        break;
                                    }

                                    case "Weekly": {
                                        let newWeeklyMonthlyDays = [];
                                        if (this.viewDate.getMonth() === 10) {
                                            newEvent.rrule = {
                                                freq: RRule.MONTHLY,
                                                interval: task.repeatQuantity,
                                                byweekday: [],
                                                until: new Date(task.endDate),
                                            };
                                            if (null != task.monthlyWeekDays) {
                                                // await weekAction;
                                                let month = this.viewDate.getMonth();
                                                let d = new Date(this.viewDate.getFullYear(), month + 1, 0).getDate();
                                                let weekCount: number = this._weekCount(this.viewDate.getFullYear(), month);
                                                // // // console.log("weekCount", weekCount);
                                                for (let i = 1; i < 7; i++) {
                                                    for (let day of task.monthlyWeekDays) {
                                                        // let weekAction = task.monthlyWeekDays.forEach((day: number) => {
                                                        let newDay: number = day;
                                                        if (i > 1) {
                                                            if (newDay === 6) {
                                                                newDay = 0;
                                                            } else {
                                                                newDay += 1;
                                                            }
                                                        }

                                                        if (null != RRule[this.WeekDays[newDay]]) {
                                                            if (i > 1) {
                                                                let dayOfWeeks = new Date(this.viewDate.getFullYear(), month + 1, 0).getDay();
                                                                if (i < weekCount) {
                                                                    newEvent.rrule.byweekday.push(RRule[this.WeekDays[newDay]].nth(i));
                                                                } else if (i === weekCount && (newDay === dayOfWeeks || newDay - 1 === dayOfWeeks)) {
                                                                    let newerDay: number = newDay + 1;
                                                                    newEvent.rrule.byweekday.push(RRule[this.WeekDays[newerDay]].nth(i));
                                                                } else {
                                                                }
                                                            } else {
                                                                if (newDay !== 0) {
                                                                    // if (d === this.viewDate.getDate()) {
                                                                    newEvent.rrule.byweekday.push(RRule[this.WeekDays[newDay]].nth(i));
                                                                    // }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                // await weekAction;
                                            }
                                        } else {
                                            if (null != task.monthlyWeekDays) {
                                                for (let day of task.monthlyWeekDays) {
                                                    // let weekAction = task.monthlyWeekDays.forEach((day: number) => {
                                                    let newDay: number = day;
                                                    if (newDay === 0) {
                                                        newDay = 6;
                                                    } else {
                                                        newDay -= 1;
                                                    }
                                                    newWeeklyMonthlyDays.push(newDay);
                                                }
                                                newEvent.rrule = {
                                                    freq: RRule.WEEKLY,
                                                    interval: task.repeatQuantity,
                                                    byweekday: newWeeklyMonthlyDays,
                                                    until: new Date(task.endDate),
                                                };
                                            }
                                        }
                                        // // // // // console.log("Weekly", newEvent.rrule);
                                        break;
                                    }

                                    case "Monthly": {
                                        if (task.monthlyConvention === "Day of month") {
                                            if (null != task.monthlyDays) {
                                                let newMonthlyDays = [];
                                                for (let day of task.monthlyDays) {
                                                    newMonthlyDays.push(day + 1);
                                                }
                                                newEvent.rrule = {
                                                    freq: RRule.MONTHLY,
                                                    interval: task.repeatQuantity,
                                                    bymonthday: newMonthlyDays,
                                                    until: new Date(task.endDate),
                                                };
                                            }
                                        } else {
                                            let weekCount = 4;
                                            let dayCount = 1;
                                            newEvent.rrule = {
                                                freq: RRule.MONTHLY,
                                                interval: task.repeatQuantity,
                                                byweekday: [],
                                                until: new Date(task.endDate),
                                            };
                                            if (null != task.monthlyNWeekDays) {
                                                let keys = Object.keys(task.monthlyNWeekDays);
                                                for (let key of keys) {
                                                    if (null != task.monthlyNWeekDays[Number(key)]) {
                                                        for (let day of task.monthlyNWeekDays[Number(key)]) {
                                                            if (null != day) {
                                                                newEvent.rrule.byweekday.push(RRule[this.WeekDays[day]].nth(Number(key) + 1));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // // // // // console.log("Monthly", newEvent.rrule);
                                        break;
                                    }

                                    case "Yearly": {
                                        let newMonthlyDays = [];
                                        for (let day of task.monthlyDays) {
                                            newMonthlyDays.push(day + 1);
                                        }
                                        newEvent.rrule = {
                                            freq: RRule.YEARLY,
                                            interval: task.repeatQuantity,
                                            bymonthday: newMonthlyDays,
                                            until: new Date(task.endDate),
                                            bymonth: new Date(task.startDate).getMonth(),
                                        };
                                        // // // // // console.log("Yearly", newEvent.rrule);
                                        break;
                                    }

                                    case "Once": {
                                        break;
                                    }

                                    default: {
                                        break;
                                    }
                                }
                                this.recurringEvents.push(newEvent);
                                if (this.recurringEvents.length === state.recurringTasks.length) {
                                    this.updateCalendarEvents();
                                }
                            });
                            await eventAction;

                            // setTimeout(() => {
                            //     this.updateCalendarEvents();
                            //     // this.IsLoading = false;
                            // }, 3000);
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                }
            },
            (error: any) => {
                this.IsLoading = false;
                // // // // console.log("Error - " + error);
            }
        );

        this._userStateSubscription = this.UserState$.subscribe((state: IUserState) => {
            if (null != state) {
                switch (state.type) {
                    default: {
                        break;
                    }
                }
            }
        });
    }

    public AddStaffMember(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        this.store.dispatch(this.userActions.GetUsers(null));
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = true;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SelectedStaff = this.selectedStaff;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: any) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                let assignee: IAssignee = {
                    name: `${user.firstName} ${user.lastName}`,
                    uid: user.uid,
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                    // // console.log("selectedStaff", this.selectedStaff);
                    this.cdr.detectChanges();
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                    // // console.log("selectedStaff", this.selectedStaff);
                }
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        this.controls
            .ShowDialog(`Remove staff?`, "Please confirm that you want to remove: " + `${staff.name}`, "Remove", "Cancel")
            .subscribe((response: any) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                            return assignee.uid !== staff.uid;
                        });
                        // // console.log("newStaff", newStaff);
                        this.selectedStaff = newStaff;
                        this.cdr.detectChanges();
                    } else {
                    }
                }
            });
    }

    public ViewChange(event: any): void {
        console.log("ViewChange", event);
        if (typeof event === 'string') {
            if (event === "month") {
                this.ShowingMap = false;
            }
        } else {
            this.store.dispatch(this.sessionActions.ChangeActiveDate(event));
        }
        this.store.dispatch(this.sessionActions.UpdateEvents());
    }

    public SwitchMap(evt: boolean): void {
        // console.log("SwitchMap", evt);
        this.ShowingMap = evt;
        this.store.dispatch(this.sessionActions.UpdateMapEvents());
    }

    public onDateChanged(event: any): void {
        // console.log("onDateChanged", event);
        let newDate: Date = new Date(event);
        Object.assign({}, this.viewDate, newDate);
        if (this.view === "month") {
            this.store.dispatch(this.sessionActions.UpdateEvents());
        } else {
            this.store.dispatch(this.sessionActions.ChangeActiveDate(this.viewDate));
        }
    }

    public SearchTasks(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // // console.log("newVal", newVal);
            this.store.dispatch(this.sessionActions.SearchTasks(newVal));
        }
    }

    public ChangeSearch(str: string): void {
        this.store.dispatch(this.sessionActions.ChangeSearch(str));
        setTimeout(() => {
            this.store.dispatch(this.sessionActions.SearchTasks(this.searchRef));
        }, 250);
    }

    public ScopeBuildingTasks(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: "",
        };
        this.store.dispatch(this.sessionActions.ScopeBuildingTasks(helper));
    }

    public Unitialize(): void {
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }

        if (null != this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
        this.IsLoading = false;
        this.searchRef = "";
    }

    public dayClicked({ date, allEvents }: { date: Date; allEvents: RecurringEvent[] }): void {
        // if (this.isNewTask) {
        if (isSameMonth(date, this.viewDate)) {
            if (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) {
                // // // // // console.log("isSame", date);
                // this.RecurringTask(null, date);
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                // this.viewDate = date;
            }
        }
    }

    public GetBuildingName(building: ITask): any {
        if (null != building.location) {
            return building.location;
        } else {
            return `${building.address.line1}, Brooklyn, NY ${building.address.zip}`;
        }
    }

    public ScopeTasks(user: IUser): void {
        this.IsLoading = true;
        this.store.dispatch(this.sessionActions.ScopeStaffTasks(user));
    }

    public AssignRecurringTask(task: ITask): void {
        if (null != this.selectedStaff && this.selectedStaff.length > 0) {
            let staffStr: string = "";
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `\n${staff.name}`;
            });
            this.controls.ShowAssignDialog(staffStr).subscribe((response: any) => {
                if (null != response.input && response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    task.assignees = this.selectedStaff;
                    let helper: any = {
                        userID: "",
                        authorID: this.sessionData.user.uid,
                        task: task,
                    };
                    if (null != response.input && response.input !== "") {
                        // console.log("data", response.input);
                        helper.message = response.input;
                    }
                    this.store.dispatch(this.sessionActions.AssignTask(helper));

                    setTimeout(() => {
                        this.store.dispatch(this.sessionActions.GetRecurringTasks());
                        this.IsReAssigning = false;
                        // this.
                        this.controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                    }, 700);
                } else {
                }
            });
        } else {
            this.controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public RecurringTask(task: ITask = null, date: any = null): void {
        // // // console.log("RecurringTask", task, date);
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(RecurringTaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this.store.dispatch(this.sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
            if (null != date) {
                dialogRef.componentInstance.newDate = date.date;
            }
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }
        if (date) {
            dialogRef.componentInstance.SelectedDate = date;
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RefreshRecurringTasks(): void {
        if (!this.sessionData.user.isAdmin) {
            if (this.sessionData.user.role === "Tenant") {
            } else {
                let bldgHelper: IGetBldgHelper = {
                    orgID: this.sessionData.org.id,
                    buildingID: this.sessionData.org.buildings[0].id,
                };
                this.IsLoading = true;
                // // // // // console.log("bldgHelper", bldgHelper);
                this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
            }
        } else {
            this.IsLoading = true;
            // // // // // console.log("GetAllHistory");
            // this.store.dispatch(this.sessionActions.GetUsersScope(null));
            //
            this.store.dispatch(this.sessionActions.GetRecurringTasks());
        }
    }

    public eventDropped(day: any, event: any): void {
        // // // console.log("eventDropped", day, event);
    }

    public eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {
        // // // console.log("eventTimesChanged");
        let matchedEvent: RecurringEvent = this.recurringEvents.find((events: RecurringEvent) => {
            return event.task.id === events.task.id;
        });
        // // // // // console.log("matchedEvent", matchedEvent, newStart, newEnd);
        let oldEvent: ITask = null != matchedEvent ? Object.assign({}, matchedEvent.task) : Object.assign({}, event.task);
        
        if (oldEvent.recurring) {
            let oldTime: Date = new Date(matchedEvent.task.startDate);
            let skipDate = `${event.start.getMonth()}_${event.start.getDate()}_${event.start.getFullYear()}`;
            // // // console.log("eventTimesChanged", skipDate);
            if (null != oldEvent.skipDates) {
                oldEvent.skipDates.push(skipDate);
            } else {
                oldEvent.skipDates = [skipDate];
            }
            let newNonSkip = `${newStart.getMonth()}_${newStart.getDate()}_${newStart.getFullYear()}`;
            if (null != oldEvent.skipDates) {
                if (oldEvent.skipDates.includes(newNonSkip)) {
                    oldEvent.skipDates = oldEvent.skipDates.filter((dte: string) => {
                        return (dte !== newNonSkip);
                    });
                }
            }
            // // // console.log("old task", oldEvent, oldEvent.startDate);
            this.store.dispatch(this.sessionActions.UpdateTask(oldEvent));
            setTimeout(() => {
                let newEvent: ITask = Object.assign({}, event.task);
                Object.assign({}, newEvent, {
                    startDate: newStart.getTime(),
                    durationAmount: "day(s)",
                    durationQuantity: 0,
                    repeatInterval: "Once",
                    subject: `Date Change: ${newEvent.subject}`,
                    description: `Date changed once for this occurance: ${newStart.toDateString()} ${newEvent.description}`,
                    status: "Assigned",
                    recurring: oldEvent.recurring,
                });
                newEvent.startDate = newStart.getTime();
                newEvent.durationAmount = "day(s)";
                newEvent.durationQuantity = 0;
                newEvent.repeatInterval = "Once";
                newEvent.subject = `Date Change: ${newEvent.subject}`;
                newEvent.description = `Date changed once for this occurance: ${newStart.toDateString()} ${newEvent.description}`;
                newEvent.status = "Assigned";
                newEvent.recurring = oldEvent.recurring;
                newEvent.completedDates = [];
                let endDate: Date = new Date(newEvent.startDate);
                endDate.setDate(endDate.getDate() + 1);
                newEvent.endDate = endDate.getTime();
                // // // // // console.log("new task", newEvent);
                this.store.dispatch(this.sessionActions.CreateTask(newEvent));
                if (null != this.sessionData.user) {
                    this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    
                }
            }, 500);
        } else {
            let newEvent: ITask = Object.assign({}, event.task);
            Object.assign({}, newEvent, {
                startDate: newStart.getTime(),
            });
            newEvent.startDate = newStart.getTime();
            newEvent.completedDates = [];
            let endDate: Date = new Date(newEvent.startDate);
            endDate.setDate(endDate.getDate() + 1);
            newEvent.endDate = endDate.getTime();
            this.store.dispatch(this.sessionActions.UpdateTask(newEvent));
            if (null != this.sessionData.user) {
                if (!this.sessionData.user.isAdmin) {
                    if (this.sessionData.user.role === "Tenant") {
                    } else {
                        let bldgHelper: IGetBldgHelper = {
                            orgID: this.sessionData.org.id,
                            buildingID: this.sessionData.org.buildings[0].id,
                        };
                        // // // // // console.log("bldgHelper", bldgHelper);
                        this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                    }
                } else {
                    // // // // // console.log("GetAllHistory");
                    //
                    this.store.dispatch(this.sessionActions.GetRecurringTasks());
                }
            }
        }
        // this.refresh.next();
    }

    public handleEvent(action: string, event: RecurringEvent): void {
        // window.location.href = event.meta;
        if (null != event) {
            // this.store.dispatch(this.userActions.GetUsers(null));
            // console.log("selectedStaff", event);
            this.SelectedTask = event.task;
            if (null != this.SelectedTask.assignees && this.SelectedTask.assignees.length > 0) {
                this.selectedStaff = this.SelectedTask.assignees;
            }
            this.modalData = { event, action };
            this.modal.open(this.modalContent, { size: "sm" });
        }
    }

    public addEvent(): void {
        this.allEvents.push({
            title: "New event",
            start: startOfDay(new Date()),
            end: endOfDay(new Date()),
            color: colors.red,
            draggable: true,
            resizable: {
                beforeStart: true,
                afterEnd: true,
            },
        });
        this.refresh.next();
    }

    public ToggleView(): void {
        this.ListView = !this.ListView;
    }

    public handleTaskDetailResponse(response: ITaskDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD: {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, {
                    duration: 3000,
                });
                if (null != this.sessionData.user) {
                    if (!this.sessionData.user.isAdmin) {
                        if (this.sessionData.user.role === "Tenant") {
                        } else {
                            let bldgHelper: IGetBldgHelper = {
                                orgID: this.sessionData.org.id,
                                buildingID: this.sessionData.org.buildings[0].id,
                            };
                            // // // // // console.log("bldgHelper", bldgHelper);
                            this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        }
                    } else {
                        // // // // // console.log("GetAllHistory");
                        //
                        // this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    }
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE: {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, {
                    duration: 3000,
                    extraClasses: ["snackBarClass"],
                });
                if (null != this.sessionData.user) {
                    if (!this.sessionData.user.isAdmin) {
                        if (this.sessionData.user.role === "Tenant") {
                        } else {
                            let bldgHelper: IGetBldgHelper = {
                                orgID: this.sessionData.org.id,
                                buildingID: this.sessionData.org.buildings[0].id,
                            };
                            // // // // // console.log("bldgHelper", bldgHelper);
                            // this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        }
                    } else {
                        // // // // // console.log("GetAllHistory");
                        //
                        // this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    }
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE: {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, {
                    duration: 3000,
                });
                if (null != this.sessionData.user) {
                    if (!this.sessionData.user.isAdmin) {
                        if (this.sessionData.user.role === "Tenant") {
                        } else {
                            let bldgHelper: IGetBldgHelper = {
                                orgID: this.sessionData.org.id,
                                buildingID: this.sessionData.org.buildings[0].id,
                            };
                            // // // // console.log("bldgHelper", bldgHelper);
                            // this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        }
                    } else {
                        // // // // console.log("GetAllHistory");
                        //
                        // this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    }
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_CANCEL: {
                break;
            }

            default: {
                break;
            }
        }
    }

    public IsCompleted(task: ITask, date: any): boolean {
        let newDate: string = `${date.date.getMonth() + 1}_${date.date.getDate()}_${date.date.getFullYear()}`;
        // // // // // console.log("IsCompleted", newDate, task, (null != task.completedDates) && (task.completedDates.length > 0) && (task.completedDates.includes(newDate)));
        if (null != task.completedDates && task.completedDates.length > 0 && task.completedDates.includes(newDate)) {
            return true;
        } else {
            return false;
        }
    }

    closeOpenMonthViewDay() {
        this.activeDayIsOpen = false;
    }

    public updateCalendarEvents($event: BeforeCalendarLoadMonth | BeforeCalendarLoadWeek = null): void {
        // // console.log("updateCalendarEvents", $event);
        this.IsLoading = true;
        this.allEvents = [];
        const startOfPeriod: any = {
            month: startOfMonth,
            week: startOfWeek,
            day: startOfDay,
        };

        const endOfPeriod: any = {
            month: endOfMonth,
            week: endOfWeek,
            day: endOfDay,
        };
        let date = this.viewDate;
        date.setHours(12);
        let endDate = this.viewDate;
        endDate.setHours(12);
        for (let event of this.recurringEvents) {
            if (event.task.repeatInterval !== "Once") {
                const rule: RRule = new RRule(
                    Object.assign({}, event.rrule, {
                        dtstart: startOfPeriod[this.view](date),
                        until: endOfPeriod[this.view](endDate),
                    })
                );
                let startHours: number = Number(event.task.startTime.split(":", 2)[0]);
                let endHours: number = Number(event.task.endTime.split(":", 2)[0]);
                let startMinutes: number = Number(event.task.startTime.split(":", 2)[1]);
                let endMinutes: number = Number(event.task.endTime.split(":", 2)[1]);
                let count: number = 0;
                for (let date of rule.all()) {
                    let matchDate = `${date.getMonth()}_${date.getDate()}_${date.getFullYear()}`;
                    // // // console.log("skip", matchDate, ((null != event.task.skipDates) && (!event.task.skipDates.includes(matchDate))), (null == event.task.skipDates), event.task.subject, event.task.skipDates);
                    if ((null != event.task.skipDates && !event.task.skipDates.includes(matchDate)) || null == event.task.skipDates) {
                        event.currentDate = date;
                        // // // console.log("event", event.task.subject, event.task.buildingName);
                        this.allEvents.push(
                            Object.assign({}, event, {
                                start: new Date(date.getFullYear(), date.getMonth(), date.getDate(), startHours, startMinutes),
                                end: new Date(date.getFullYear(), date.getMonth(), date.getDate(), endHours, endMinutes),
                            })
                        );
                    }
                    setTimeout(() => {
                        if (this.recurringEvents.length < 25) {
                            // console.log("allEvents", this.allEvents);
                        }
                    }, 500);
                }
            } else {
                let startHours: number = Number(event.task.startTime.split(":", 2)[0]);
                let endHours: number = Number(event.task.endTime.split(":", 2)[0]);
                let startMinutes: number = Number(event.task.startTime.split(":", 2)[1]);
                let endMinutes: number = Number(event.task.endTime.split(":", 2)[1]);
                event.currentDate = event.start;
                let dDate = new Date(event.start);
                // console.log("one time event", dDate, event.task);
                this.allEvents.push(
                    Object.assign({}, event, {
                                start: new Date(dDate.getFullYear(), dDate.getMonth(), dDate.getDate(), startHours, startMinutes),
                                end: new Date(dDate.getFullYear(), dDate.getMonth(), dDate.getDate(), endHours, endMinutes),
                            }));
            }
        }

        // RouteUtil.CalculateRoute('driving', this.allEvents);
        this._sortBy(this.allEvents);
        this.IsLoading = false;
        this.cdr.detectChanges();
    }

    private _weekCount(year: number, month_number: number) {
        // month_number is in the range 1..12
        let firstOfMonth = new Date(year, month_number - 1, 1);
        let lastOfMonth = new Date(year, month_number, 0);

        let used = firstOfMonth.getDay() + lastOfMonth.getDate();

        return Math.ceil(used / 7);
    }

    private _sortBy(users: RecurringEvent[]): RecurringEvent[] {
        let newUsers: RecurringEvent[] = users.sort((user1, user2) => {
            if (user1.start < user2.start) {
                return -1;
            }

            if (user1.start > user2.start) {
                return 1;
            }

            return 0;
        });
        return newUsers;
    }

    private _getMultiplier(task: ITask): void {}
}