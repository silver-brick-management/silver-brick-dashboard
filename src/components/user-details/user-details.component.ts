// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Component, Optional, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

// Local modules
import { AppState } from '../../app/app.state';
import { IUserState, IOrgState } from '../../interfaces/IStoreState';

// Action modules
import { OrgActions } from '../../store/actions/OrgActions';
import { UserActions } from '../../store/actions/UserActions';

// Component Modules
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { CollapsibleControlComponent } from '../collapsible-control/collapsible-control.component';

// Type related
import {
    IUser,
    IOrgSimple,
    IUserOrg,
    IOrgBasic,
    IUserBuilding,
    IBuildingSimple,
    IBuildingBasic
} from '../../shared/SilverBrickTypes';
import { IUserDialogResponse, IUserDialogBase, IFormSelctedValue } from '../../interfaces/IParams';
import { IError } from '../../interfaces/IError';
import { Constants, Urls } from '../../common/Constants';

// Utils and Helpers
import { OrgBldgUtil } from '../../common/OrgBldgUtil';
import { ArrayUtil } from '../../common/ArrayUtil';
import { Controls } from '../../providers/Controls';
import { FormValidators } from '../../common/Validators';

export interface UserFormTask {
    completed: FormArray;
    pending: FormArray;
}

@Component({
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDetailComponent {
    private _orgSubscription: Subscription;
    private _showingTasks: boolean = false;
    private _orgs: IOrgBasic[] = [];   
    private _user: IUser;
    private _selectedOrgId: string;
    private _selectedBuildingId: string;
    private _userFormBuildings: FormArray;
    private _buildingIdNameMap: Map<string, string>;
    private _selectedOrgBuildings: Array<IBuildingSimple> = [];
    private _userStateError: IError;
    private _isUserStateLoading: boolean;
    private _buildingUserView: boolean;
    private _userDetailTitle: string = "New User";
    private _userFormCertifications: FormArray;
    private _selectedUserTasksSubscription: Subscription;
    private _certificationsSubscription: Subscription;
    public calendarColor: string = "#fff";
    public calendarTextColor: string = "#000";
    public userForm = this._formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        phone: [''],
        email: ['', Validators.email],
        role: ['Staff', Validators.required],
        password: [''],
        fcmRegToken: [''],
        address: this._formBuilder.group({
            line1: [''],
            line2: [''],
            city: [''],
            state: [''],
            zip: ['']
        }),
        calendarColor: [''],
        calendarTextColor: [''],
        isAdmin: [false, Validators.required],
        photoURL: [''],
        orgs: this._formBuilder.group({
            id: ['', Validators.required],
            isAdmin: [false, Validators.required],
            buildings: this._bindUserFormBuildingArray()
        }, {validator: FormValidators.CheckBuildingArrayLength})
    });

    public orgs$: Observable<IOrgState>;

    get orgs(): IOrgBasic[] {
        return this._orgs;
    }

    set orgs(orgs: IOrgBasic[]) {
        this._orgs = orgs;
    }

    get user(): IUser {
        return this._user;
    }

    set user(user: IUser) {
        this._user = user;
    }

    get selectedOrgId(): string {
        return this._selectedOrgId;
    }

    set selectedOrgId(id: string) {
        this._selectedOrgId = id;
    }

    get selectedBuildingId(): string {
        return this._selectedBuildingId;
    }

    set selectedBuildingId(id: string) {
        this._selectedBuildingId = id;
    }

    get userFormCerts(): FormArray {
        return this._userFormCertifications;
    }

    set userFormCerts(form: FormArray) {
        this._userFormCertifications = form;
    }

    get userFormBuildings(): FormArray {
        return this._userFormBuildings;
    }

    set userFormBuildings(form: FormArray) {
        this._userFormBuildings = form;
    }

    get buildingIdNameMap(): Map<string, string> {
        return this._buildingIdNameMap;
    }

    set buildingIdNameMap(map: Map<string, string>) {
        this._buildingIdNameMap = map;
    }

    get selectedOrgBuildings(): Array<IBuildingSimple> {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(buildings: Array<IBuildingSimple>) {
        this._selectedOrgBuildings = buildings;
    }

    get userStateError(): IError {
        return this._userStateError;
    }

    set userStateError(error: IError) {
        this._userStateError = error;
    }

    get isUserStateLoading(): boolean {
        return this._isUserStateLoading;
    }

    set isUserStateLoading(loading: boolean) {
        this._isUserStateLoading = loading;
    }

    get isBuildingUserView(): boolean {
        return this._buildingUserView;
    }

    set isBuildingUserView(view: boolean) {
        this._buildingUserView = view;
    }

    get userDetailTitle(): string {
        return this._userDetailTitle;
    }

    set userDetailTitle(title: string) {
        this._userDetailTitle = title;
    }

    get IsShowingTasks(): boolean {
        return this._showingTasks;
    }

    set IsShowingTasks(tasks: boolean) {
        this._showingTasks = tasks;
    }


    // We hide extras if called from profile
    // E.g Activate, deactivate, delete
    public showExtras: boolean = true;
    public isTenant: boolean = false;
    public userState$: Observable<IUserState>;

    constructor(
        private _controls: Controls,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _userActions: UserActions,
        @Optional() public dialogRef: MdDialogRef<UserDetailComponent>,
        private _orgActions: OrgActions) {
        this.orgs$ = this._store.select(state => state.orgState);
        this.userState$ = this._store.select(state => state.usersState);
    }

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {
        this._store.dispatch(this._orgActions.GetAllOrgs());
        if (this.user) {
            this.userDetailTitle = `${this.user.firstName} ${this.user.lastName}`;
            // Set form values from existing
            this._orgSubscription = this.orgs$
            .subscribe((orgState: IOrgState) => {
                if ((null != orgState.orgs) && (orgState.orgs.length > 0)) {
                    this.orgs = orgState.orgs;
                    this.selectedOrgId = this.user.orgs.id;
                    // this._selectedBuildingId = this.selectedOrgId = this.user.orgs.buildings[0].id;
                    // console.log("User", this.user.orgs);
                    this.userForm.patchValue({
                        firstName: this.user.firstName,
                        lastName: this.user.lastName,
                        phone: this.user.phone ? this.user.phone : "",
                        email: this.user.email,
                        role: this.user.role ? this.user.role : "",
                        fcmRegToken: this.user.fcmRegToken ? this.user.fcmRegToken : "",
                        calendarColor: this.user.calendarColor ?  this.user.calendarColor : "#fff",
                        calendarTextColor: this.user.calendarTextColor ? this.user.calendarTextColor : "#000",
                        address: {
                            line1: this.user.address ? this.user.address.line1 : "",
                            line2: this.user.address ? this.user.address.line2 : "",
                            city: this.user.address ? this.user.address.city : "",
                            state: this.user.address ? this.user.address.state : "",
                            zip: this.user.address ? this.user.address.zip : ""
                        },
                        isAdmin: this.user.isAdmin ? this.user.isAdmin : false,
                        orgs: {
                            buildings: this.user.orgs.buildings ? this.user.orgs.buildings : [],
                            isAdmin: this.user.orgs.isAdmin,
                            id: this.user.orgs.id
                        },
                        photoURL: this.user.photoURL ? this.user.photoURL : Constants.DEFAULT_PROFILE_PHOTO_PATH
                    });
                    this._setSelectedOrgBuildings(this.user.orgs.id);
                    this._createBuildingIdNameMap();
                    // Set building form array from existing
                    this.userFormBuildings.controls = [];
                    if (null != this.user.orgs.buildings) {
                        for (const userBuilding of this.user.orgs.buildings) {
                            // console.log("userBuilding", userBuilding);
                            this.userFormBuildings.push(this._createBuilding(userBuilding.id, userBuilding.isAdmin));
                            // Remove existing buildings from the unselected list
                            this.selectedOrgBuildings = this.selectedOrgBuildings.filter((selectedBuilding) => {
                                return selectedBuilding.id !== userBuilding.id;
                            });
                        }
                    }
                }
            });
        } else {

            this.userForm.patchValue({
                password: `silverbrick`,
                role: 'Staff'
            });
            this._orgSubscription = this.orgs$
            .subscribe((orgState: IOrgState) => {
                if ((null != orgState.orgs) && (orgState.orgs.length > 0)) {
                    this.orgs = orgState.orgs;
                }
            });
        }
    }

    public ChangeCalendarText(val: string): void {
        this.calendarColor = val;
        console.log("ChangeCalendarText", val, this.calendarColor);
        this.user.calendarColor = val;
    }


    /* View Methods */

    public addBuilding(value: IFormSelctedValue): void {
        this.userFormBuildings.push(this._createBuilding(value.id));
        // Removes any selected buildings from the
        // unselected list so we dont have dupes
        this.selectedOrgBuildings.splice(value.index, 1);
    }

    public removeBuilding(value: IFormSelctedValue): void {
        this.userFormBuildings.removeAt(value.index);
        // Re-add any removed builds back to the selected list
        if (this.selectedOrgBuildings) {
            const buildingObj: IBuildingSimple = {
                id: value.id,
                name: this.getBuildingNameFromId(value.id)
            };
            this.selectedOrgBuildings.push(buildingObj);
        } else {
            // Selected building not set yet
            // Do nothing
        }
    }

    public onOrgChange(value: IFormSelctedValue): void {
        // Since I am not using the change event of the
        // select due to needing a complext value, this
        // is an optimization to only perform work if
        // the org is actually changed.
        if (value.id !== this.selectedOrgId) {
            this.selectedOrgId = value.id;
            // Extract the building list from the selected org
            const newBuildings: IBuildingSimple[] = this.orgs[value.index].buildings;
            // console.log("newBuildings", newBuildings);
            this.selectedOrgBuildings = [...newBuildings];
            this._createBuildingIdNameMap();
            // Clear the building form array to avoid
            // cross org contamination
            this._resetUserFormBuildingArray();
        } else {
            // Selected org has not changed
            // Do nothing
        }
    }

    public saveAction(): void {
        if ((this.userForm.valid && this.userForm.dirty) || this.user) {
            let newUser: IUser = this.userForm.value;
            let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
            newUser.calendarColor = this.calendarColor;
            newUser.calendarTextColor = this.calendarTextColor;
            // console.log("newUser", newUser);
            // Perform update if we detect a user object
            if (this.user) {
                action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                newUser.uid = this.user.uid;
                // console.log("User", newUser);
                // if (newUser.orgs.buildings) {
                //     let buildings: IUserBuilding[] = ArrayUtil.Transform(this.user.orgs.buildings);
                //     newUser.orgs.buildings.forEach((building: IUserBuilding) => {
                //         if (this.user.orgs.buildings) {
                //             let newBuildings: IUserBuilding[] = ArrayUtil.Transform(newUser.orgs.buildings);
                //             this.user.orgs.buildings.forEach((userBuilding: IUserBuilding) => {
                //                 if (building.id === userBuilding.id) {
                //                     newUser.orgs.buildings.map((building: IUserBuilding) => {
                //                         building.audiences = userBuilding.audiences;
                //                     });
                //                     // console.log("User Building!", newUser.orgs.buildings);
                //                 }
                //             });
                //             // console.log("New User's buildings!!! ", newUser.orgs.buildings);
                //         }
                //     })
                // }
            } else {

                // User object isnt set
                // Do nothing
            }

            const newPassword: string = this.userForm.get("password").value;
            // Since password is part of the form, it will always exist.
            // Only need to check for empty string
            if (newPassword.trim() === "") {
                // Setting the password field to undefined allows JSON.Stringify to
                // remove the field. This ensures the server doesnt set passwords on
                // updates
                newUser.password = undefined;
            } else {
                // New password is set
                // Let it through
            }

            // Create a IUser object and add it to the dialogResult
            const dialogResponse: IUserDialogResponse = {
                action: action,
                user: newUser
            };
            this._handleUserDetailAction(dialogResponse);
            // this.dialogRef.close(dialogResponse);
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public activateAction(): void {
        if (this.user) {
            const dialogResponse: IUserDialogResponse = {
                action: Constants.DIALOG_RESPONSE_ACTION_ACTIVATE,
                user: this.user
            };
            this._handleUserDetailAction(dialogResponse);
            // this.dialogRef.close(dialogResponse);
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public deactivateAction(): void {
        if (this.user) {
            const dialogResponse: IUserDialogResponse = {
                action: Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE,
                user: this.user
            };
            this._handleUserDetailAction(dialogResponse);
            // this.dialogRef.close(dialogResponse);
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public deleteAction(): void {
        if (this.user) {
            // Ask user if they want to close since they have changed something
            this._controls.
            ShowDialog(
                "Delete User?",
                "Are you sure you want to delete this user?",
                "Delete",
                "Cancel")
            .subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        const dialogResponse: IUserDialogResponse = {
                            action: Constants.DIALOG_RESPONSE_ACTION_DELETE,
                            user: this.user
                        };
                        this._handleUserDetailAction(dialogResponse);
                        // this.dialogRef.close(dialogResponse);
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

    public closeAction(): void {
        if (this.userForm.dirty) {
            // Ask user if they want to close since they have changed something
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

    public getBuildingNameFromId(id: string): string {
        let result = Constants.STR_NA;
        if (this.buildingIdNameMap) {
            result = this.buildingIdNameMap.get(id);
            result = result ? result : Constants.STR_NA;
        } else {
            // Building name map not set yet
            // Do nothing
        }
        return result;
    }

    private _bindUserFormCertsArray(): FormArray {
        this.userFormCerts = this._formBuilder.array([
            // if we need to create a default, we call
            // create building here.
            // this.createBuilding("default")
        ]);
        return this.userFormCerts;
    }

    private _bindUserFormBuildingArray(): FormArray {
        this.userFormBuildings = this._formBuilder.array([
            // if we need to create a default, we call
            // create building here.
            // this.createBuilding("default")
        ]);
        return this.userFormBuildings;
    }

    // Initially tried this.userFormBuildings.reset();
    // but that nulls out the values without resetting the length
    // This allows clearing of the array while keeping the binding in tact
    private _resetUserFormBuildingArray(): void {
        while (this.userFormBuildings.length > 0) {
            this.userFormBuildings.removeAt(this.userFormBuildings.length - 1);
        }
    }

    // Creates a building group in the building
    // form array
    private _createBuilding(id: string, isAdmin: boolean = false): FormGroup {
        return this._formBuilder.group({
            id: [id, Validators.minLength(2)],
            isAdmin: [isAdmin, Validators.required],
        });
    }

    // Creates a map of the building id to building name
    // of the selected orgs. This allows us to show the
    // building name in the selected buildings list.
    private _createBuildingIdNameMap(): void {
        this.buildingIdNameMap = new Map<any, any>();
        for (const building of this.selectedOrgBuildings) {
            this.buildingIdNameMap.set(building.id, building.name);
            // console.log("this.buildingIdNameMap", this.buildingIdNameMap, building);
        }
    }

    private _setSelectedOrgBuildings(id: string) {
        let selectedOrg = this.orgs.filter((org) => {
            return org.id === id;
        });
        if (selectedOrg.length > 0) {
            let buildingArray: IBuildingSimple[];
            if (this.isBuildingUserView) {
                buildingArray = selectedOrg[0].buildings;
            } else {
                buildingArray = selectedOrg[0].buildings;
            }
            this.selectedOrgBuildings = [...buildingArray];
        }
    }

    private _dismissDialog(): void {
        const dialogResponse: IUserDialogResponse = {
            action: Constants.DIALOG_RESPONSE_ACTION_CANCEL,
            user: null
        };
        if (null != this._orgSubscription) {
            this._orgSubscription.unsubscribe();
            this._orgSubscription = null;
        }
        this.orgs = [];
        this.user = null;
        this.selectedOrgBuildings = [];
        this.dialogRef.close(dialogResponse);
    }

    private _handleUserDetailAction(response: IUserDialogResponse) {
        // Prepare state subscription
        // Skip the initial subscription state and the dispatch action loading change
        // as we only care about final pings after the action is complete
        // Take 1 to account for the final result once action is complete.
        // Take 1 automatically unsubscribes after the 1st ping
        // ALT: Skip(1) & Take(2) and add a isLoading state to track the initial
        // dispatch action & final result once action is complete
        this.userState$.skip(2).take(1).subscribe((currentUserState) => {
            if (!currentUserState.error) {
                this.dialogRef.close(response);
            } else {
                // There was an error, stay on the page
            }
        });
        // Dispatch action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                this._store.dispatch(this._userActions.AddUser(response.user));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                this._store.dispatch(this._userActions.UpdateUser(response.user));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE:
            {
                this._store.dispatch(this._userActions.DeleteUser(response.user.uid));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_ACTIVATE:
            {
                this._store.dispatch(this._userActions.ActivateUser(response.user.uid));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE:
            {
                this._store.dispatch(this._userActions.DeactivateUser(response.user.uid));
                break;
            }

            default:
            {
                break;
            }
        }
    }
}
