// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { storage, database } from "firebase";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialogRef, MdDialog, MdDialogConfig } from "@angular/material";
import { Component, Optional, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import Viewer from 'viewerjs/dist/viewer.esm';
import 'viewerjs/dist/viewer.min.css';

// Local modules
import { AppState } from "../../app/app.state";
import { ISessionState, IOrgState, IUserState } from "../../interfaces/IStoreState";
import { SessionActions } from "../../store/actions/SessionActions";
import { UserActions } from "../../store/actions/UserActions";
import { OrgActions } from "../../store/actions/OrgActions";

// Component Modules
import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { CollapsibleControlComponent } from "../collapsible-control/collapsible-control.component";
import { BuildingUsersComponent } from "../building-users/building-users.component";
import { OrgBldgUtil } from "../../common/OrgBldgUtil";
import { StringUtil } from "../../common/StringUtil";
import { FirebaseStorage } from "../../services/FirebaseStorage";

// Type Related Modules
import { IOrgSimple, IAssignee, IUserOrg, IOrgBasic, IUserBuilding, IUser, IBuildingBasic, IBuilding, IBuildingConfig, IBuildingInfo, ITask, IBuildingSimple, ITaskFeed, ISilverBrickUser } from "../../shared/SilverBrickTypes";
import { IBldgDialogResponse, IBuildingDialogResponse, IInputDialogBase, IUserDialogBase, IFormSelctedValue, IGetBldgHelper, ITaskDialogResponse, IAudienceDialogResponse } from "../../interfaces/IParams";
import { IError } from "../../interfaces/IError";
import { ITaskFeedJoin, AddMessageHelper, AssignTaskHelper } from "../../interfaces/IResponse";
import { Constants, STATE_LIST, PROPERTY_TYPES, TASK_STATUS, TASK_TYPES, URGENCY, MAINTENANCE_REQUESTS } from "../../common/Constants";

import { SessionData } from "../../providers/SessionData";

// Utils and Helper modules
import { Controls } from "../../providers/Controls";
import { CalendarProvider } from "../../providers/CalendarProvider";
import { FormValidators } from "../../common/Validators";
import { AudienceChooserComponent } from '../audience-chooser/audience-chooser.component';

@Component({
    templateUrl: "./task-details.component.html",
    styleUrls: ["./task-details.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskDetailsComponent implements OnInit, OnDestroy {
    private _isUploading: boolean;
    private _uploadStatus: string;
    private _isUploadDone: boolean;
    private _uploadPreview: string;
    private _progressPercent: number;
    private _canCancelUpload: boolean;
    private _hideScrollDownButtom: boolean;
    private _imageDatas: File;
    private _uploadTask: storage.UploadTask;
    private _participantIDs: string[] = [];
    private _messageStateSubscription: Subscription;
    private _imageData: File;
    private _photoModified: boolean;
    private _uploadPreviews: string[] = [];
    private _imageDataArr: File[] = [];
    private _uploadUrls: string[] = [];

    private _orgStateSubscription: Subscription;
    private _orgs: IOrgBasic[] = [];
    private _screenSize: number = window.screen.width;
    private _task: ITask = null;
    private _userFormBuildings: FormArray;
    private _buildingIdNameMap: Map<string, string>;
    private _selectedOrgBuildings: Array<IBuildingSimple> = [];
    private _selectedOrgId: string;
    private _selectedBuildingId: string;
    private _hasSelectedAudience: boolean = false;

    public IsEditMode: boolean = false;
    public newTaskFeed: ITaskFeed = {
        audience: "",
        orgID: "",
        buildingID: "",
        assignees: [],
        clientUnit: "",
        type: "",
        timestamp: 0,
        taskID: "",
        message: "",
        category: "",
        subCategory: "",
        approved: true,
        photoGallery: []
    };

    public SharedWithTenant: boolean = false;
    public SharedWithLandlord: boolean = false;
    public SharedWithStaff: boolean = false;
    public building: IBuildingSimple = null;

    public taskForm = this._formBuilder.group({
        assignID: [""],
        assignName: [""],
        authorName: ["", Validators.required],
        authorID: ["", Validators.required],
        active: [true],
        isRecurring: [false],
        icon: [""],
        status: ["", Validators.required],
        deleted: [false],
        notes: [""],
        id: [""],
        type: [""],
        photoURL: [""],
        category: ["", Validators.required],
        availability: [""],
        subCategory: [""],
        buildingName: [""],
        buildingID: [""],
        orgID: [""],
        clientName: [""],
        clientID: [""],
        clientUnit: [""],
        subject: ["", Validators.required],
        description: ["", Validators.required],
        permissionToEnter: [false],
        havePets: [false],
        recurring: [false]
    });

    public settings: any = {
        bigBanner: true,
        defaultOpen: false,
        timePicker: true,
        closeOnSelect: true,
    };
    public activeTabIndex: number = 0;
    public orgID: string = "";
    public buildingID: string = "";
    public SelectedDate: Date = null;

    public UnitNumber: string = '';
    public isNewTask: boolean = false;
    public IsAddingUnit: boolean = false;
    public stateList: string[] = STATE_LIST;
    public selectedCategory: string = "Choose a Category";
    public selectedUrgency: string = "Choose a Level";
    public selectedSubCategory: string = "Choose a Sub-Category";
    public selectedStatus: string = "Choose a Status";
    public selectedPropertyType: string = "";
    public userStateError: IError;
    public isOrgStateLoading: boolean;
    public tenants$: Observable<IUser[]>;

    public orgs$: Observable<IOrgState>;
    public taskFeed$: Observable<ITaskFeed[]>;
    public userLoading$: Observable<boolean>;

    public taskDetailTitle: String = "";
    public showExtras: boolean = true;
    public ScheduleListForms: FormArray;
    public SessionState$: Observable<ISessionState>;
    public users$: Observable<Array<IUser>>;
    public staffUsers$: Observable<Array<IUser>>;
    public propertyTypes: string[] = PROPERTY_TYPES;
    public taskStatus: string[] = TASK_STATUS;
    public taskTypes: string[] = TASK_TYPES;
    public maintenanceReqs: string[] = MAINTENANCE_REQUESTS;
    public urgencyLevels: string[] = URGENCY;
    public searchString: string = "";
    public selectedTenant: IUser = null;
    public selectedStaff: IAssignee[] = [];
    public isSearching: boolean = false;
    public newDate: Date = null;
    public IsReAssigning: boolean = false;
    public searchStaffStr: string = "";

    get UploadPreviews(): string[] {
        return this._uploadPreviews;
    }

    set UploadPreviews(uploadPreview: string[]) {
        this._uploadPreviews = uploadPreview;
    }

    get ImageArray(): File[] {
        return this._imageDataArr;
    }

    set ImageArray(newValue: File[]) {
        this._imageDataArr = newValue;
    }

    get HasSelectedAudience(): boolean {
        return this._hasSelectedAudience;
    }

    set HasSelectedAudience(newValue: boolean) {
        this._hasSelectedAudience = newValue;
    }

    get PhotoModified(): boolean {
        return this._photoModified;
    }

    set PhotoModified(newValue: boolean) {
        this._photoModified = newValue;
    }

    get IsUploading(): boolean {
        return this._isUploading;
    }

    set IsUploading(isUploading: boolean) {
        this._isUploading = isUploading;
    }

    get UploadStatus(): string {
        return this._uploadStatus;
    }

    set UploadStatus(uploadStatus: string) {
        this._uploadStatus = uploadStatus;
    }

    get IsUploadDone(): boolean {
        return this._isUploadDone;
    }

    set IsUploadDone(isUploadDone: boolean) {
        this._isUploadDone = isUploadDone;
    }

    get UploadPreview(): string {
        return this._uploadPreview;
    }

    set UploadPreview(uploadPreview: string) {
        this._uploadPreview = uploadPreview;
    }

    get ProgressPercent(): number {
        return this._progressPercent;
    }

    set ProgressPercent(progressPercent: number) {
        this._progressPercent = progressPercent;
    }

    get CanCancelUpload(): boolean {
        return this._canCancelUpload;
    }

    set CanCancelUpload(canCancelUpload: boolean) {
        this._canCancelUpload = canCancelUpload;
    }
    
    get HideScrollDownButtom(): boolean {
        return this._hideScrollDownButtom;
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get task(): ITask {
        return this._task;
    }

    set task(newValue: ITask) {
        this._task = newValue;
    }

    get orgs(): IOrgBasic[] {
        return this._orgs;
    }

    set orgs(orgs: IOrgBasic[]) {
        this._orgs = orgs;
    }

    get selectedOrgId(): string {
        return this._selectedOrgId;
    }

    set selectedOrgId(id: string) {
        this._selectedOrgId = id;
    }

    get selectedBuildingId(): string {
        return this._selectedBuildingId;
    }

    set selectedBuildingId(id: string) {
        this._selectedBuildingId = id;
    }

    get userFormBuildings(): FormArray {
        return this._userFormBuildings;
    }

    set userFormBuildings(form: FormArray) {
        this._userFormBuildings = form;
    }

    get buildingIdNameMap(): Map<string, string> {
        return this._buildingIdNameMap;
    }

    set buildingIdNameMap(map: Map<string, string>) {
        this._buildingIdNameMap = map;
    }

    get selectedOrgBuildings(): Array<IBuildingSimple> {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(buildings: Array<IBuildingSimple>) {
        this._selectedOrgBuildings = buildings;
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant'));
    }

    get IsNotLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Landlord'));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    constructor(
        private _dialog: MdDialog,
        private _controls: Controls,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _sessionActions: SessionActions,
        private _userActions: UserActions,
        private cdr: ChangeDetectorRef,
        private _orgActions: OrgActions,
        public sessionData: SessionData,
        public firebaseStorage: FirebaseStorage,
        public calendarProvider: CalendarProvider,
        @Optional() public dialogRef: MdDialogRef<TaskDetailsComponent>) {
        this.orgs$ = this._store.select((state) => state.orgState);
        this.userLoading$ = this._store.select((state) => state.usersState.loading);
        this.SessionState$ = this._store.select((state) => state.sessionState);
        this.tenants$ = this._store.select((state) => state.usersState.tenants);
        this.users$ = this._store.select((state) => state.usersState.searchTenants);
        this.staffUsers$ = this._store.select((state) => state.usersState.searchStaff);
        this.taskFeed$ = this._store.select((state) => state.sessionState.taskFeed);
        this._imageDatas = null;
    }

    ngOnInit(): void {
        const todayData: Date = new Date(Date.now());
        const newData: string = new Date(todayData.getFullYear(), todayData.getMonth(), todayData.getDate() + 5, todayData.getHours(), 0, 0, 0).toString();
        this.taskForm.patchValue({
            dueDate: newData,
        });
    }

    ngOnDestroy(): void {}

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {
        let helper: IGetBldgHelper = {
            orgID: this.sessionData.user.orgs.id,
            buildingID: this.sessionData.user.orgs.buildings[0].id,
        };
        if ((null != this.sessionData.user)) {
            if (this.IsAdmin && this.isNewTask) {
                this._store.dispatch(this._orgActions.GetAllOrgs());
                this._store.dispatch(this._userActions.GetAllTenants());
                // this._store.dispatch(this._userActions.GetUsers(null));
            } else if (this.sessionData.user.role === 'Landlord') {
                
            }
        }

        if (this.IsLandlord || !this.IsNotTenant) {
            this.SharedWithStaff = true;
        }
        if (!this.isNewTask) {
            // let helper: ITaskFeedJoin = {
            //     orgID: this.task.orgID,
            //     buildingID: this.task.buildingID,
            //     taskID: this.task.id,
            // };
            this.orgID = this.task.orgID;
            this.buildingID = this.task.buildingID;
            this.newTaskFeed.orgID = this.task.orgID;
            this.newTaskFeed.buildingID = this.task.buildingID;
            this.newTaskFeed.taskID = this.task.id;
            this.newTaskFeed.assignees = this.task.assignees;
            this.newTaskFeed.authorID = this.sessionData.user.uid;
            this.newTaskFeed.authorName = `${this.sessionData.user.firstName} ${this.sessionData.user.lastName}`;
            this.newTaskFeed.clientUnit = this.task.clientUnit;
            this.newTaskFeed.category = this.task.category;
            this.newTaskFeed.subCategory = this.task.subCategory;
            this.newTaskFeed.status = this.task.status;
            this.newTaskFeed.photoGallery = [];
            this.newTaskFeed.type = "update";
            if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
                this.newTaskFeed.approved = false;
            } else {
                this.newTaskFeed.approved = true;
            }
            if ((null != this.task.assignees) && (this.task.assignees.length > 0)) {
                this.selectedStaff = this.task.assignees;
            }
            // this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            this._orgStateSubscription = this.orgs$.subscribe((orgState: IOrgState) => {
                if (null != orgState.orgs && orgState.orgs.length > 0) {
                    this.orgs = orgState.orgs;
                    this.selectedCategory = this.task.category;
                    if (this.task.category === "Maintenance Request") {
                        this.selectedSubCategory = this.task.subCategory;
                    }
                    if (null != this.task.clientID) {
                        let match: any = {
                            uid: this.task.clientID,
                            firstName: this.task.clientName.split(" ", 2)[0],
                            lastName: this.task.clientName.split(" ", 2)[1],
                            clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                            orgs: {
                                id: this.task.orgID,
                                buildings: [
                                    {
                                        name: this.task.buildingName,
                                        id: this.task.buildingID,
                                    },
                                ],
                            },
                        };
                        this.selectedTenant = match;
                    }


                    this.taskForm.patchValue({
                        authorName: this.task.authorName ? this.task.authorName : "",
                        authorID: this.task.authorID ? this.task.authorID : "",
                        clientID: this.task.clientID ? this.task.clientID : "",
                        clientName: this.task.clientName ? this.task.clientName : "",
                        clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                        active: this.task.active ? this.task.active : true,
                        icon: this.task.icon ? this.task.icon : "",
                        status: this.task.status ? this.task.status : "",
                        deleted: this.task.deleted ? this.task.deleted : false,
                        notes: this.task.notes ? this.task.notes : "",
                        date: this.task.date ? this.task.date : 0,
                        id: this.task.id ? this.task.id : "",
                        type: this.task.type ? this.task.type : "",
                        category: this.task.category ? this.task.category : "",
                        subCategory: this.task.subCategory ? this.task.subCategory : "",
                        orgID: this.task.orgID ? this.task.orgID : "",
                        buildingID: this.task.buildingID ? this.task.buildingID : "",
                        buildingName: this.task.buildingName ? this.task.buildingName : "",
                        availability: this.task.availability ? this.task.availability : "",
                        subject: this.task.subject ? this.task.subject : "",
                        description: this.task.description ? this.task.description : "",
                        photos: this.task.photoURL ? this.task.photoURL : "",
                        files: this.task.files ? this.task.files : "",
                        lastUpdated: this.task.lastUpdated ? this.task.lastUpdated : 0,
                        buildingAddress: this.task.buildingAddress ? this.task.buildingAddress : "",
                        permissionToEnter: this.task.permissionToEnter ? this.task.permissionToEnter : false,
                        havePets: this.task.havePets ? this.task.havePets : false,
                        recurring: this.task.recurring ? this.task.recurring : true
                    });
                    console.log("taskForm", this.taskForm.value);
                }
            });
        } else {
            this.taskDetailTitle = "New Request";
            if (this.sessionData.user.role === "Tenant") {
                this.SelectTenant(this.sessionData.user);
            }
            if ((this.sessionData.user.role === 'Landlord') && (this.sessionData.user.orgs.buildings.length === 1)) {
                this.building = {
                    orgID: this.sessionData.user.orgs.id,
                    id: this.sessionData.user.orgs.buildings[0].id,
                    name: this.sessionData.user.orgs.buildings[0].name
                };

                this.taskForm.patchValue({
                    orgID: this.sessionData.user.orgs.id,
                    buildingID: this.sessionData.user.orgs.buildings[0].id,
                    buildingName: this.sessionData.user.orgs.buildings[0].name,
                    clientID: this.sessionData.user.orgs.buildings[0].id
                });
            };
            this.taskForm.patchValue({
                authorName: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
                authorID: this.sessionData.user.uid,
                status: "New",
                deleted: false,
                active: true,
                recurring: false
            });
        }
    }

    public AddUnit(): void {
        if (this.IsAddingUnit) {
            this.taskForm.patchValue({
                clientID: this.UnitNumber,
                clientUnit: this.UnitNumber
            });
        }
        this.IsAddingUnit = !this.IsAddingUnit;
    }

    public AddBuilding(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = false;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SearchingBuildings = false;
        dialogRef.componentInstance.LandlordSearch = true;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IBuildingDialogResponse) => {
            if (response) {
                let building: IBuildingSimple = <IBuildingSimple>response.selected;
                this.building = building;
                this.taskForm.patchValue({
                    clientID: building.id,
                    clientName: building.name,
                    clientUnit: "",
                    orgID: building.orgID,
                    buildingID: building.id,
                    buildingName: building.name,
                });
                this.cdr.detectChanges();
                console.log("selectedBuilding", this.building);
                this.isSearching = false;
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public DeleteTask(): void {
        this._controls.ShowDialog('Delete Task?', `Please confirm you want to delete this task. This action cannot be undone`, 'Delete', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                let dialogResponse: ITaskDialogResponse = {
                    task: this.task,
                    action: Constants.DIALOG_RESPONSE_ACTION_DELETE,
                };
                this._handleTaskDetailAction(dialogResponse);
                this.dialogRef.close(dialogResponse);
            }
        });
    }

    public OpenImage(event: HTMLElement): void {
        const viewer = new Viewer(event, {
            inline: false,
            zIndex: 9999,
            title: 0,
            toolbar: {
                zoomIn: 1,
                zoomOut: 1,
                oneToOne: 1,
                reset: 1,
                prev: 0,
                play: {
                  show: 0,
                  size: 'large',
                },
                next: 0,
                rotateLeft: 4,
                rotateRight: 4,
                flipHorizontal: 4,
                flipVertical: 4,
            },
            viewed() {
                viewer.zoomTo(0.2);
            },
        });
        viewer.show();
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public UnitNo(event: string): void {
        if (null != event) {
            this.taskForm.patchValue({
                clientName: event
            })
        }
    }

    public SwitchEditMode(): void {
        if (this.IsEditMode && this.taskForm.dirty) {
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        }
        this.IsEditMode = !this.IsEditMode;
    }

    public GetName(user: IUser): string {
        return `${user.firstName} ${ user.lastName }`;
    }

    public ReAssignStaff(): void {
        this.IsReAssigning = !this.IsReAssigning;
        this.cdr.detectChanges();
    }

    public SelectTenant(user: IUser): void {
        if (user !== this.selectedTenant) {
            this.selectedTenant = user;
            this.taskForm.patchValue({
                clientID: user.uid,
                clientName: user.firstName + " " + user.lastName,
                clientUnit: user.unit ? user.unit : "",
                orgID: user.orgs.id,
                buildingID: user.orgs.buildings[0].id,
                buildingName: user.orgs.buildings[0].name,
            });
            console.log("SelectTenant", this.taskForm.value);
            this.isSearching = false;
            this.searchString = "";
        } else {
            this._controls.ShowAlert("Already Selected", "This tenant is currently selected. Please choose a different one.", "Dismiss");
        }
    }

    public AddStaffMember(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        // this._store.dispatch(this._userActions.GetUsers(null));
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = true;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SelectedStaff = this.selectedStaff;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                console.log("Chosen person", user);
                let assignee: IAssignee = {
                    name: `${ user.firstName } ${user.lastName}`,
                    uid: user.uid
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                }
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        this._controls.ShowDialog(`Remove staff?`, "Please confirm that you want to remove: " + `${ staff.name}`, "Remove", "Cancel")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                        return (assignee.uid !== staff.uid);
                    });
                    console.log("newStaff", newStaff);
                    this.selectedStaff = newStaff;
                    this.cdr.detectChanges();
                } else {

                }
            }
        });
    }

    public AddTenant(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        if (this.sessionData.user.isAdmin) {
            this._store.dispatch(this._userActions.GetAllTenants());
        } else {
            this._store.dispatch(this._userActions.GetAllTenantsLandlord());
        }
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = false;
        dialogRef.componentInstance.SearchingTenant = true;
        dialogRef.componentInstance.SelectedTenant = this.selectedTenant;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                this.selectedTenant = user;
                this.cdr.detectChanges();
                this.taskForm.patchValue({
                    clientID: user.uid,
                    clientName: user.firstName + " " + user.lastName,
                    clientUnit: user.unit ? user.unit : "",
                    orgID: user.orgs.id,
                    buildingID: user.orgs.buildings[0].id,
                    buildingName: user.orgs.buildings[0].name,
                });
                this.isSearching = false;
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public SearchTenants(event: string): void {
        if (null != event) {
            let newVal: string = event;
            console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllTenants(newVal));
            if (newVal.trim() === "") {
                this.isSearching = false;
            } else {
                this.isSearching = true;
            }
        }
    }

    public GetAudience(id: string): string {
        let audience: string = Constants.STR_AUDIENCE_LANDLORD_ONLY;
        switch (id) {
            case Constants.STR_AUDIENCE_TENANT_AND_LANDLORD:
            {
                return "the tenant and landlord(s)";
            }

            case Constants.STR_AUDIENCE_TENANT_AND_STAFF:
            {
                return "the tenant and the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_STAFF_ONLY:
            {
                return "the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_TENANT_ONLY:
            {
                return "the tenant";
            }

            case Constants.STR_AUDIENCE_LANDLORD_ONLY:
            {
                return "the landlord(s)";
            }

            default:
            {
                return "the tenant";
            }
        }
    }

    public AssignTask(): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `\n${staff.name}`
            });
            this._controls.ShowAssignDialog(staffStr)
            .subscribe((response: IInputDialogBase) => {
                if ((null != response.input) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                    this.task.status = "Assigned";
                    this.task.assignees = this.selectedStaff;
                    let helper: AssignTaskHelper = {
                        userID: '',
                        authorID: this.sessionData.user.uid,
                        task: this.task
                    };

                    if ((null != response.input) && (response.input !== '')) {
                        console.log("data", response.input);
                        helper.message = response.input;
                    }
                    this._store.dispatch(this._sessionActions.AssignTask(helper));
                    setTimeout(() => {
                        if ((null != this.sessionData.user) && (!this.sessionData.user.isAdmin)) {
                            
                        } else {
                            this._store.dispatch(this._sessionActions.GetTasks());
                            // this._store.dispatch(this._userActions.GetUsers(null));
                        }
                        this.IsReAssigning = false;
                        this.closeAction();
                        this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                    }, 700);
                }
                       
            });
        } else {
            this._controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public SearchStaff(event: string): void {
        if (null != event) {
            let newVal: string = event;
            console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllStaff(newVal));
            if (newVal.trim() === "") {
                this.isSearching = false;
            } else {
                this.isSearching = true;
            }
        }
    }

    public onSubCategoryChange(type: string): void {
        this.selectedSubCategory = type;
    }

    public onUrgencyChange(type: string): void {
        this.selectedUrgency = type;
    }

    public onCategoryChange(state: string): void {
        this.selectedCategory = state;
    }

    public onStatusChange(state: string): void {
        this.selectedStatus = state;
    }

    public onDateSelect(event: any): void {
        // console.log("onDateSelect", event);
        this.SelectedDate = event;
    }

    public GetIconName(activity: ITaskFeed): string {
        let iconName: string = "chat";
        switch (activity.type) {
            case "update": {
                iconName = "chat";
                break;
            }

            case "insert_photo": {
                iconName = "insert_photo";
                break;
            }

            case "new": {
                iconName = "add";
                break;
            }

            case "assign": {
                iconName = "assignment";
                break;
            }

            case "complete": {
                iconName = "check";
                break;
            }

            default: {
                break;
            }
        }
        return iconName;
    }

    public onOrgChange(value: IFormSelctedValue): void {
        // Since I am not using the change event of the
        // select due to needing a complext value, this
        // is an optimization to only perform work if
        // the org is actually changed.
        if (value.id !== this.selectedOrgId) {
            this.selectedOrgId = value.id;
            // Extract the building list from the selected org
            const newBuildings: IBuildingSimple[] = this.orgs[value.index].buildings;
            this.selectedOrgBuildings = [...newBuildings];
            this._createBuildingIdNameMap();
            // Clear the building form array to avoid
            // cross org contamination
            this._resetUserFormBuildingArray();
        } else {
            // Selected org has not changed
            // Do nothing
        }
    }

    public async saveAction(): Promise<void> {
        if (this.taskForm.valid && this.taskForm.dirty) {
            const newTask: ITask = this.taskForm.value;
            newTask.status = "New";
            this.buildingID = newTask.buildingID;
            this.orgID = newTask.orgID;
            // if ((this.sessionData.user.role = 'Landlord') && ((null != this.UnitNumber) && (this.UnitNumber.trim() !== ''))) {
            //     newTask.clientID = ;        
            // }
            newTask.scheduled = false;
            newTask.scheduling = false;
            console.log("New Task", newTask);
            if (null != newTask.clientID && newTask.clientID !== "") {
                // if ((null != newTask.assignID) && (newTask.assignName !== '')) {
                let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
                if (!this.isNewTask) {
                    action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                } else {
                    newTask.date = new Date(Date.now()).getTime();
                    // Create a IUser object and add it to the dialogResult
                }

                if (this._photoModified) {
                    newTask.photoURL = await this._uploadPhoto();
                }
                newTask.recurring = false;
                let dialogResponse: ITaskDialogResponse = {
                    task: newTask,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
                this.dialogRef.close(dialogResponse);
                // } else {
                //     this._controls.ShowAlert("Missing Staff", "Please make sure you have assigned this task to a staff member", "Dismiss");
                // }
            } else {
                this._controls.ShowAlert("Missing Tenant", "Please make sure you have chosen a tenant for this task", "Dismiss");
            }
        } else {
            // Dismiss the dialog since no changes were detected
            this._dismissDialog();
        }
    }

    public closeAction(): void {
        if (this.taskForm.dirty) {
            // Ask user if they want to close since they have changed something
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

    public ApproveHistory(history: ITaskFeed): void {
        let audience: string = this.GetAudience(history.audience);
        this._controls.ShowDialog('Approve Note?', `Please confirm you want to approve this note. It will be send out to the audience of ${audience}`, 'Approve', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                let helper: AddMessageHelper = {
                    message: history,
                    taskID: history.taskID
                };
                this._store.dispatch(this._sessionActions.ApproveHistory(helper));
                setTimeout(() => {
                    this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                }, 1000);
            }
        });          
    }

    public UpdateHistory(history: ITaskFeed, photo: string): void {
        let audience: string = this.GetAudience(history.audience);

        this._controls.ShowDialog('Delete Photo', `Please confirm you want to delete this photo. This action cannot be undone`, 'Delete', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                        history.photoGallery = history.photoGallery.filter((pht: string) => {
                    return (pht !== photo);
                });
                this.cdr.detectChanges();
                console.log("UpdateHistory", history);
                let helper: AddMessageHelper = {
                    message: history,
                    taskID: history.taskID
                };
                this._store.dispatch(this._sessionActions.UpdateHistory(helper));
                setTimeout(() => {
                    this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                }, 1000);
            }
        });          
    }

    public async SendText(): Promise<void> {
        // if (this.SocketReconnected) {
        if (this.newTaskFeed) {
            // Create a partial message object for the object clone
            if ((this.IsStaff) && (!this.sessionData.user.isAdmin)) {
                this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
            } else if (this.sessionData.user.isAdmin) {   
                if (this.task.recurring) {
                    if (this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                } else {
                    if (this.SharedWithTenant && this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                    } else if (this.SharedWithTenant && !this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_ONLY;
                    } else if (!this.SharedWithTenant && this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                }
            } else if (this.sessionData.user.role === "Landlord") {
                if (this.task.recurring) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                } else {
                    if (this.SharedWithTenant && this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    } else if (this.SharedWithTenant && !this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    } else if (!this.SharedWithTenant && this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    }
                }
            } else {
                // this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
            }
            if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
                this.newTaskFeed.approved = false;
            } else {
                this.newTaskFeed.approved = true;
            }
            let self = this;
            this.newTaskFeed.timestamp = new Date(Date.now()).getTime();
            if (this._photoModified) {
                this.newTaskFeed.category = this.task.category;
                this.newTaskFeed.subCategory = this.task.subCategory;
                this.newTaskFeed.role = this.sessionData.user.role;
                // this.newTaskFeed.photoURL = await this._uploadPhoto();
                this.newTaskFeed.type = "insert_photo";
                let self = this;
                self._uploadMultipleFiles()
                .then(() => {
                    setTimeout(function() {
                        this._controls.ShowAlert("Message Added", "Message successfully added", 'Dismiss');
                        // Create a new message object from the default message object with an override of the text message
                        
                    }, (this._imageDataArr.length * 500));
                });
                
            } else {

                const helper: AddMessageHelper = {
                    message: this.newTaskFeed,
                    taskID: this.newTaskFeed.taskID,
                };
                this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                // Clear the text message field
                this.newTaskFeed.message = "";
                this._controls.ShowAlert("Message Added", "Message successfully added", 'Dismiss');
            }
        } else {
            // Text message field is empty
        }
        // } else {
        //     this.controls.ShowAlert('Reconnecting...', 'One moment while we connect your Platform experience', 'Thanks');
        // }
    }

    /* Helper Methods */

    private _dismissDialog(): void {
        const dialogResponse: IBldgDialogResponse = {
            action: Constants.DIALOG_RESPONSE_ACTION_CANCEL,
            building: null,
        };
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.dialogRef.close(dialogResponse);
    }

    // Initially tried this.userFormBuildings.reset();
    // but that nulls out the values without resetting the length
    // This allows clearing of the array while keeping the binding in tact
    private _resetUserFormBuildingArray(): void {
        while (this.userFormBuildings.length > 0) {
            this.userFormBuildings.removeAt(this.userFormBuildings.length - 1);
        }
    }

    // Creates a map of the building id to building name
    // of the selected orgs. This allows us to show the
    // building name in the selected buildings list.
    private _createBuildingIdNameMap(): void {
        this.buildingIdNameMap = new Map<any, any>();
        for (const building of this.selectedOrgBuildings) {
            this.buildingIdNameMap.set(building.id, building.name);
        }
    }

    private _handleTaskDetailAction(response: ITaskDialogResponse) {
        this.SessionState$.skip(2)
            .take(1)
            .subscribe((currentSessionState) => {
                if (!currentSessionState.error) {
                    this.dialogRef.close(response);
                } else {
                    // There was an error, stay on the page
                }
            });
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD: {
                this._store.dispatch(this._sessionActions.CreateTask(response.task));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE: {
                this._store.dispatch(this._sessionActions.UpdateTask(response.task));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE: {
                this._store.dispatch(this._sessionActions.DeleteTask(response.task));
                break;
            }

            default: {
                break;
            }
        }
    }

    public async handleFileInput(files: FileList): Promise<void> {
        for (let i = 0; i < files.length; i++) {
            this._imageDataArr.push(files.item(i));
        }

        this._photoModified = true;
        //this._uploadPreview = URL.createObjectURL(this._imageData);
        // this._setPhoto(this._imageData);
    }

    /* Helper Methods */
    public readURL(event: any): void {
        console.log("EVENT: ", event);
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                console.log("TARGET: ", e.target.result);
                this._uploadPreview = <string>e.target.result;
                console.log("preview: ", this.UploadPreview);
                this.cdr.detectChanges();
            };
            reader.readAsDataURL(file);
        }
    }

    public readMultiFiles(files: any[]): void {
        let reader = new FileReader();
        let self = this;
        function readFile(index: number) {
            if( index >= files.length ) return;
            let file = files[index];
            reader.onload = function(e: any) { 
                // get file content  
                self._uploadPreviews.push(<string>e.target.result);
                readFile(index + 1);
            }
            reader.readAsBinaryString(file);
        }
        readFile(0);
    }

    public CancelImageUpload(): void {
        if (this._uploadTask) {
            this._uploadTask.cancel();
        } else {
            // Upload task not set yet
        }
        this._finishImageSend(false);
    }

    public RemoveImage(): void {
        // Remove image data
        if (this._imageData) {
            this._imageData = null;
            this._photoModified = true;
        }
        // Hide the remove button & show add button
    }

    private async _uploadPhoto(): Promise<string> {
        console.log("_uploadPhoto", this._uploadPreview);
        let self = this;
        return new Promise<string>((resolve, reject) => {
            if (!this._imageDatas) {
                // Return empty string
                // Set in updatedProfile object to clear image
                resolve("");
            } else {
                // Set the upload preview
                // Show the upload UI
                this.IsUploading = true;
                // Generate filename and remove dashes
                // Generate full file path
                const fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
                let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
                const fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
                // Start image upload process and get upload task
                const contentType: string = "image.png";
                const format: string = "base64";
                this._uploadTask = this.firebaseStorage.Upload(fullPath, this._imageDatas, contentType);
                // Enable the cancel button now that the upload task is available
                this.CanCancelUpload = true;
                // Listen for state changes, error, completion of the upload
                this._uploadTask.on(
                    storage.TaskEvent.STATE_CHANGED,
                    (snapshot: firebase.storage.UploadTaskSnapshot) => {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    },
                    (error) => {
                        let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                        switch (error.name) {
                            case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                                // User doesn't have permission to access the object
                                message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                                break;
                            }

                            case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                                // User canceled the upload
                                message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                        self._controls.ShowDialog(error.name, message, "Ok", "Dismiss");
                        reject(message);
                    },
                    () => {
                        this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                        // Update UI with finishing and remove cancel button
                        this.IsUploadDone = true;
                        this.CanCancelUpload = false;
                        // Create a partial message object for the message clone
                        this.newTaskFeed.photoURL = fullPath;
                        // Create a new message object from the default message object with an override of the text message
                        // const helper: AddMessageHelper = {
                        //     message: this.newTaskFeed,
                        //     taskID: this.newTaskFeed.taskID
                        // };
                        // this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                        // // Create a partial message object for the message clone
                        // this._finishImageSend(true);
                        resolve(this._uploadTask.snapshot.downloadURL);
                        // Update the photoURL path
                    }
                );
            }
        });
    }

    public async _uploadMultipleFiles(): Promise<void> {
        const promises: any[] = [];
        let self = this;
        let count: number = (this._imageDataArr.length - 1);
        this._imageDataArr.forEach(file => {
            this.IsUploading = true;
            let fileName: string = (null != this.sessionData.user) ? this.sessionData.user.uid + StringUtil.GenerateUUID() + Date.now() : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.sessionData.user) ? this.sessionData.user.uid : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.task.orgID}/visits/${this.task.buildingID}/${taskID}/${fileName}.png`;
            console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            let uploadTask: storage.UploadTask = this.firebaseStorage.Upload(fullPath, file, format);
            console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, (snapshot: any) => {
               let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (error: any) => {
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;
                    }

                    case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;
                    }

                    default: {
                        break;
                    }
                }
                self._controls.ShowAlert(error.name, message, "Ok");
                
            },
            () => {
                this._uploadUrls.push(uploadTask.snapshot.downloadURL);
                this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                // Update UI with finishing and remove cancel button
                console.log("uploadUrls", this.newTaskFeed.photoGallery);
                count = count - 1;
                // Create a partial message object for the message clone
                // // Create a partial message object for the message clone
                // Update the photoURL path
            });

         });
         Promise.all(promises)
            .then(() => {
               this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                console.log("Upload done", this.newTaskFeed.photoGallery);
                const helper: AddMessageHelper = {
                    message: this.newTaskFeed,
                    taskID: this.newTaskFeed.taskID,
                };
                this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                // Clear the text message field
                this.newTaskFeed.message = "";
                this._uploadPreviews = [];
                this._imageDataArr = [];
                this._finishImageSend(true);
            })
            .catch(err => console.log(err.code));
    }

    private _finishImageSend(isDone: boolean): void {
        // Update UI with done
        this.UploadStatus = isDone ? "Done!" : "Canceled!";
        // keep the done UI for 1 second
        setTimeout(() => {
            // Hides all upload UI elements
            // Re-enables the image upload button
            this.IsUploading = false;
            // Reset the image done flag and status
            this.IsUploadDone = false;
            this.UploadStatus = Constants.STR_FINISHING;
            // Resets the progress bar
            this.ProgressPercent = 0;
            // Reset upload preview
            this.UploadPreview = null;
            // Clear upload task
            this._uploadTask = null;
            this._photoModified = false;
        }, 1000);
    }
}

