// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialogRef, MdSnackBar, MdDialog, MdDialogConfig } from '@angular/material';
import { Component, Optional, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

// Local modules
import { AppState } from '../../app/app.state';
import { IUserState, IOrgState } from '../../interfaces/IStoreState';

// Action modules
import { SessionActions } from '../../store/actions/SessionActions';
import { UserActions } from '../../store/actions/UserActions';
import { OrgActions } from '../../store/actions/OrgActions';

// Component Modules
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { CollapsibleControlComponent } from '../collapsible-control/collapsible-control.component';
import { UserDetailComponent } from '../user-details/user-details.component';
import { SessionData } from "../../providers/SessionData";

// Type related
import {
    IUser,
    IAssignee,
    IOrgSimple,
    IOrgBasic,
    ISilverBrickUser,
    IUserOrg,
    IBuildingSimple,
    IBuildingBasic
} from '../../shared/SilverBrickTypes';
import {
    IUserDialogResponse,
    IUserDialogBase,
    IFormSelctedValue,
    IBuildingDialogResponse,
    IGetBldgHelper,
    IAudienceDialogResponse
} from '../../interfaces/IParams';
import { IError } from '../../interfaces/IError';
import { Constants } from '../../common/Constants';

// Utils and Helpers
import { OrgBldgUtil } from '../../shared/OrgBldgUtil';
import { Controls } from '../../providers/Controls';
import { FormValidators } from '../../common/Validators';

@Component({
    templateUrl: './audience-chooser.component.html',
    styleUrls: ['./audience-chooser.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AudienceChooserComponent {
    public uidOrEmail: string = "";
    public buildingDetailTitle: string = "Building Users";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public users$: Observable<Array<IUser>>;
    public userSubscription: Subscription = null;
    private _orgID: string = "";
    private _buildingID: string = "";
    private _buildingName: string = "Building";
    private _selectedOrg: IOrgBasic = null;
    private _selectedOrgBuildings: IBuildingSimple[] = [];
    private _screenSize: number = window.screen.width;
    public searchRef: string = "";
    public tenants$: Observable<IUser[]>;
    public usersScope$: Observable<IUser[]>;
    public staffUsers$: Observable<Array<IUser>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public RequestsTitle: string = 'Picker';
    public SearchingStaff: boolean = false;
    public SearchingTenant: boolean = false;
    public SelectedStaff: IAssignee[] = [];
    public ScopingTasks: boolean = false;
    public SelectedTenant: IUser = null;
    public SearchingBuildings: boolean = false;
    public LandlordSearch: boolean = false;


    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get orgID(): string {
        return this._orgID;
    }

    set orgID(id: string) {
        this._orgID = id;
    }

    get buildingID(): string {
        return this._buildingID;
    }

    set buildingID(id: string) {
        this._buildingID = id;
    }

    get buildingName(): string {
        return this._buildingName;
    }

    set buildingName(id: string) {
        this._buildingName = id;
    }

    get selectedOrg(): IOrgBasic {
        return this._selectedOrg;
    }

    set selectedOrg(org: IOrgBasic) {
        this._selectedOrg = org;
    }

    get selectedOrgBuildings(): IBuildingSimple[] {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(bldg: IBuildingSimple[]) {
        this._selectedOrgBuildings = bldg;
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _store: Store<AppState>,
        private _controls: Controls,
        private _sessionActions: SessionActions,
        private _orgActions: OrgActions,
        public sessionData: SessionData,
        private _userActions: UserActions,
         @Optional() public dialogRef: MdDialogRef<AudienceChooserComponent>) {
        this.staffUsers$ = this._store.select((state) => state.usersState.searchStaff);
        this.tenants$ = this._store.select((state) => state.usersState.searchTenants);
        this.usersScope$ = this._store.select((state) => state.sessionState.usersScope);
        this.users$ = this._store.select(state => state.usersState.buildingUsers);
        this.AllBuildings$ = this._store.select((state) => state.orgState.searchBuildings);
        this.error$ = this._store.select(state => state.usersState.error);
        this.loading$ = this._store.select(state => state.usersState.loading);
    }

    public PopulateForm(): void {
        if (this.SearchingStaff) {
            this.RequestsTitle = 'Select Staff Member';
        }
        if (this.SearchingTenant) {
            this.RequestsTitle = 'Select A Tenant';
        }

        if (this.ScopingTasks) {
            this.RequestsTitle = 'Staff Schedule';
        }

        if (this.SearchingBuildings) {
            this.RequestsTitle = 'Select a Building';
        }
    }

    /* View Methods */

    public CloseAction(user: IUser): void {
        let response: IAudienceDialogResponse = {
            selected: user,
            action: Constants.DIALOG_RESPONSE_ACTION_OK
        };
        this._cleanUp();
        this.dialogRef.close(response);
    }

    public Dismiss(): void {
        this._cleanUp();
        this.dialogRef.close();
    }

    public scopeStaff(user: IUser): void {
        let assignee: IAssignee = {
            name: `${user.firstName} ${user.lastName}`,
            uid: user.uid
        };
        if (null != user) {
            this._store.dispatch(this._sessionActions.ScopeStaffTasks(user));
            this._cleanUp();
            this.dialogRef.close();
        } else {
            this._controls.ShowAlert('Already Selected', 'This staff member is already selected', 'Dismiss');
        }
    }

    public chooseStaff(user: IUser): void {
        let assignee: IAssignee = {
            name: `${user.firstName} ${user.lastName}`,
            uid: user.uid
        };
        let match: IAssignee = this.SelectedStaff.find((assign: IAssignee) => {
            return (assign.uid === user.uid);
        });
        if (((null != this.SelectedStaff) && (null == match)) || ((null != this.SelectedStaff) && (this.SelectedStaff.length === 0))) {
            this.CloseAction(user);
        } else {
            this._controls.ShowAlert('Already Selected', 'This staff member is already selected', 'Dismiss');
        }
    }

    public chooseBuilding(user: IBuildingSimple): void {
        let response: IBuildingDialogResponse = {
            selected: user,
            action: Constants.DIALOG_RESPONSE_ACTION_OK
        };
        this._cleanUp();
        this.dialogRef.close(response);
    }

    public chooseTenant(user: IUser): void {
        if ((null != this.SelectedTenant) && (this.SelectedTenant.uid !== user.uid) || (null == this.SelectedTenant)) {
            this.CloseAction(user);
        } else {
            this._controls.ShowAlert('Already Selected', 'This tenant is already selected', 'Dismiss');
        }
    }

    public GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public SearchUsers(event: string): void {
        if (null != event) {
            let newVal: string = event;
            console.log("newVal", newVal);
            if (this.SearchingTenant) {
                this._store.dispatch(this._userActions.SearchAllTenants(newVal));
            } else if (this.SearchingStaff) {
                this._store.dispatch(this._userActions.SearchAllStaff(newVal));
            } else if (this.ScopingTasks) {
                this._store.dispatch(this._sessionActions.ScopeStaff(newVal));
            } else if (this.SearchingBuildings) {
                this._store.dispatch(this._orgActions.SearchBuildings(newVal));
            }
        }
    }

    private _cleanUp(): void {
        this.SearchingStaff = false;
        this.SearchingTenant = false;
        this.SelectedStaff = [];
        this.SelectedTenant = null;
        this._store.dispatch(this._sessionActions.CleanUpSearch());
        this._store.dispatch(this._userActions.CleanUpSearch());
        this._store.dispatch(this._orgActions.CleanUpSearch());
    }

    /* Helper Methods */

    handleUserDetailResponse(response: IUserDialogResponse) {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            case Constants.DIALOG_RESPONSE_ACTION_ACTIVATE:
            case Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE:
            {
                const msg: string = `${response.user.firstName} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                break;
            }
            default:
            {
                break;
            }
        }
    }

    public IsAdmin(user: IUser): boolean {
        return user.orgs.buildings[0].isAdmin;
    }
}
