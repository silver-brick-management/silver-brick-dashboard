// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialogRef, MdSnackBar, MdDialog, MdDialogConfig } from '@angular/material';
import { Component, Optional, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

// Local modules
import { AppState } from '../../app/app.state';
import { IUserState, IOrgState } from '../../interfaces/IStoreState';

// Action modules
import { OrgActions } from '../../store/actions/OrgActions';
import { UserActions } from '../../store/actions/UserActions';

// Component Modules
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { CollapsibleControlComponent } from '../collapsible-control/collapsible-control.component';
import { UserDetailComponent } from '../user-details/user-details.component';

// Type related
import {
    IUser,
    IOrgSimple,
    IOrgBasic,
    ISilverBrickUser,
    IBuildingSimple,
    IBuildingBasic
} from '../../shared/SilverBrickTypes';
import {
    IUserDialogResponse,
    IUserDialogBase,
    IFormSelctedValue,
    IGetBldgHelper
} from '../../interfaces/IParams';
import { IError } from '../../interfaces/IError';
import { Constants } from '../../common/Constants';

// Utils and Helpers
import { OrgBldgUtil } from '../../shared/OrgBldgUtil';
import { Controls } from '../../providers/Controls';
import { FormValidators } from '../../common/Validators';

@Component({
    templateUrl: './building-users.component.html',
    styleUrls: ['./building-users.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingUsersComponent {
    public uidOrEmail: string = "";
    public buildingDetailTitle: string = "Building Users";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public users$: Observable<Array<IUser>>;
    public userSubscription: Subscription = null;
    private _orgID: string = "";
    private _buildingID: string = "";
    private _buildingName: string = "Building";
    private _selectedOrg: IOrgBasic = null;
    private _selectedOrgBuildings: IBuildingSimple[] = [];
    private _screenSize: number = window.screen.width;
    
    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get orgID(): string {
        return this._orgID;
    }

    set orgID(id: string) {
        this._orgID = id;
    }

    get buildingID(): string {
        return this._buildingID;
    }

    set buildingID(id: string) {
        this._buildingID = id;
    }

    get buildingName(): string {
        return this._buildingName;
    }

    set buildingName(id: string) {
        this._buildingName = id;
    }

    get selectedOrg(): IOrgBasic {
        return this._selectedOrg;
    }

    set selectedOrg(org: IOrgBasic) {
        this._selectedOrg = org;
    }

    get selectedOrgBuildings(): IBuildingSimple[] {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(bldg: IBuildingSimple[]) {
        this._selectedOrgBuildings = bldg;
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _store: Store<AppState>,
        private _userActions: UserActions,
        private _orgActions: OrgActions,
         @Optional() public dialogRef: MdDialogRef<BuildingUsersComponent>) {

        this.users$ = this._store.select(state => state.usersState.buildingUsers);
        this.error$ = this._store.select(state => state.usersState.error);
        this.loading$ = this._store.select(state => state.usersState.loading);
    }

    public PopulateForm(): void {
        const helper: IGetBldgHelper = {
            orgID: this.orgID,
            buildingID: this.buildingID
        };
        this._store.dispatch(this._userActions.GetBuildingUsers(helper));
    }

    /* View Methods */

    expandUser(user?: IUser): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '900px';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(UserDetailComponent, dialogConfig);
        if (user) {
            dialogRef.componentInstance.user = user;
            dialogRef.componentInstance.isBuildingUserView = true;
        } else {
            // No user to push to the dialog
            // Do nothing
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IUserDialogResponse) => {
            if (response) {
                this.handleUserDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    searchUidOrEmail(): void {
        if (this.uidOrEmail.trim()) {
            this._store.dispatch(this._userActions.GetUser(this.uidOrEmail));
        } else {
            // No string detected
            // Do nothing
        }
    }

    public closeAction(): void {
        this.dialogRef.close();
    }

    public deleteUsers(): void {
        console.log("Deleting Users");
        this.userSubscription = this.users$
        .subscribe((users: IUser[]) => {
            if ((null != users) && (users.length > 0)) {
                console.log("Deleting Users", users.length);
                users.forEach((user: IUser) => {
                    this._store.dispatch(this._userActions.DeleteUser(user.uid));
                });
            } else {
                console.log("Deleting Users, users length is 0 or is null");
            }
        });
    }

    /* Helper Methods */

    handleUserDetailResponse(response: IUserDialogResponse) {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            case Constants.DIALOG_RESPONSE_ACTION_ACTIVATE:
            case Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE:
            {
                const msg: string = `${response.user.firstName} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                break;
            }
            default:
            {
                break;
            }
        }
    }

    public IsAdmin(user: IUser): boolean {
        return user.orgs.buildings[0].isAdmin;
    }
}
