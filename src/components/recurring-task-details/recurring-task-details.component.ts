// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { storage, database } from "firebase";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialogRef, MdDialog, MdDialogConfig } from "@angular/material";
import { Component, Optional, OnInit, OnDestroy, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from "@angular/forms";
import Viewer from 'viewerjs/dist/viewer.esm';
import 'viewerjs/dist/viewer.min.css';

// Local modules
import { AppState } from "../../app/app.state";
import { ISessionState, IOrgState, IUserState } from "../../interfaces/IStoreState";
import { SessionActions } from "../../store/actions/SessionActions";
import { UserActions } from "../../store/actions/UserActions";
import { OrgActions } from "../../store/actions/OrgActions";

// Component Modules
import { ConfirmDialogComponent } from "../confirm-dialog/confirm-dialog.component";
import { CollapsibleControlComponent } from "../collapsible-control/collapsible-control.component";
import { BuildingUsersComponent } from "../building-users/building-users.component";
import { OrgBldgUtil } from "../../common/OrgBldgUtil";
import { StringUtil } from "../../common/StringUtil";
import { FirebaseStorage } from "../../services/FirebaseStorage";
import { AudienceChooserComponent } from '../audience-chooser/audience-chooser.component';

// Type Related Modules
import {
    IOrgSimple,
    IUserOrg,
    IOrgBasic,
    IUserBuilding,
    IUser,
    IBuildingBasic,
    IBuilding,
    IBuildingConfig,
    RecurringEvent,
    IBuildingInfo,
    ITask,
    IBuildingSimple,
    IAssignee,
    ITaskFeed,
    ISilverBrickUser
} from "../../shared/SilverBrickTypes";
import {
    IBldgDialogResponse,
    IUserDialogBase,
    IFormSelctedValue,
    IInputDialogBase,
    IAudienceDialogResponse,
    IGetBldgHelper,
    ITaskDialogResponse,
    IBuildingDialogResponse
} from "../../interfaces/IParams";
import { IError } from "../../interfaces/IError";
import { ITaskFeedJoin, AddMessageHelper, AssignTaskHelper } from "../../interfaces/IResponse";
import { Constants, STATE_LIST, PROPERTY_TYPES, TASK_STATUS, TASK_TYPES, URGENCY, MAINTENANCE_REQUESTS } from "../../common/Constants";
import { TimeUtil } from "../../common/TimeUtil";
import { SessionData } from "../../providers/SessionData";

// Utils and Helper modules
import { Controls } from "../../providers/Controls";
import { CalendarProvider } from "../../providers/CalendarProvider";
import { FormValidators } from "../../common/Validators";

@Component({
    templateUrl: "./recurring-task-details.component.html",
    styleUrls: ["./recurring-task-details.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecurringTaskDetailsComponent implements OnInit, OnDestroy {
   
    private _isUploading: boolean;
    private _uploadStatus: string;
    private _isUploadDone: boolean;
    private _uploadPreview: string;
    private _progressPercent: number;
    private _canCancelUpload: boolean;
    private _hideScrollDownButtom: boolean;
    private _imageDatas: File;
    private _uploadTask: storage.UploadTask;
    private _participantIDs: string[] = [];
    private _messageStateSubscription: Subscription;
    private _imageData: File;
    private _photoModified: boolean;
    private _uploadPreviews: string[] = [];
    private _imageDataArr: File[] = [];
    private _uploadUrls: string[] = [];
    private _taskImageDataArr: File[] = [];
    private _taskUploadPreviews: string[] = [];
    private _taskUploadUrls: string[] = []; 

    private _orgStateSubscription: Subscription;
    private _orgs: IOrgBasic[] = [];
    private _screenSize: number = window.screen.width;
    private _task: ITask = null;
    private _userFormBuildings: FormArray;
    private _buildingIdNameMap: Map<string, string>;
    private _selectedOrgBuildings: Array<IBuildingSimple> = [];
    private _selectedOrgId: string;
    private _selectedBuildingId: string;
    public CurrentDate: Date =  new Date(Date.now());
    public IsEditMode: boolean = false;
    public newTaskFeed: ITaskFeed = {
        audience: "",
        orgID: "",
        buildingID: "",
        assignees: [],
        clientUnit: "",
        type: "",
        timestamp: 0,
        taskID: "",
        message: "",
        category: "",
        subCategory: "",
        approved: true,
        photoGallery: []
    };
    public minDate = new Date(Date.now());
    public SharedWithTenant: boolean = false;
    public SharedWithLandlord: boolean = false;
    public SharedWithStaff: boolean = false;
    public taskForm = this._formBuilder.group({
        authorName: [""],
        authorID: [""],
        active: [true],
        icon: [""],
        status: [""],
        deleted: [false],
        notes: [""],
        id: [""],
        type: [""],
        photoURL: [""],
        category: [""],
        subCategory: [""],
        buildingName: [""],
        buildingID: [""],
        scheduled: [true],
        orgID: [""],
        clientName: [""],
        clientID: [""],
        clientUnit: [""],
        subject: ["", Validators.required],
        description: ["", Validators.required],
        permissionToEnter: [false],
        havePets: [false],
        recurring: [true],
        startDate: [this.minDate],
        durationAmount: ['', Validators.required],
        durationQuantity: [1],
        startTime: ['', Validators.required],
        endTime: ['', Validators.required],
        clockInTime: [0],
        clockOutTime: [0],
        repeats: ['', Validators.required],
        repeatInterval: ['Weekly'],
        repeatQuantity: [1],
        monthlyConvention: ['Day of month']
    });

    public InvoicingFrequency: string[] = [ 'Monthly on the last day of the month', 'After each visit', 'As needed-no reminders', 'Once when job is closed' ];
    public InvoiceTotal: number = 0;
    public repeatDays: number[] = [];
    public monthlyDays: number[] = [];
    public monthlyWeekDays: number[] = [];
    public monthlyNWeekDays: number[][] = [[], [], [], []];

    public WeekDays: string[] = ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'];
    public MonthDays: string[] = [];
    public RepeatOptions: string[] = ['As needed, we won\'t prompt you'];
    public DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)', 'forever'];
    public FrequencyOptions: string[] = ['Daily', 'Weekly', 'Monthly', 'Yearly', 'Once'];
    public MonthlyConventions: string[] = ['Day of month', 'Day of week'];
    public ByNWeekDays: string[][] = [['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa'], ['Su', 'M', 'Tu', 'W', 'Th', 'F', 'Sa']];
    public monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "January"];
    public settings: any = {
        bigBanner: true,
        format: 'hh:mm a',
        defaultOpen: false,
        timePicker: true,
        closeOnSelect: true,
    };

    public message: string = '';

    public orgID: string = "";
    public buildingID: string = "";
    public SelectedDate: Date = null;
    public isNewTask: boolean = false;
    public building: IBuildingSimple = null;
    public stateList: string[] = STATE_LIST;
    public selectedCategory: string = "Choose a Category";
    public selectedUrgency: string = "Choose a Level";
    public selectedSubCategory: string = "Choose a Sub-Category";
    public selectedStatus: string = "Choose a Status";
    public selectedPropertyType: string = "";
    public userStateError: IError;
    public isOrgStateLoading: boolean;
    public orgs$: Observable<IOrgState>;
    public taskFeed$: Observable<ITaskFeed[]>;
    public AllBuildings$: Observable<IBuildingSimple[]>;

    public taskDetailTitle: String = "";
    public showExtras: boolean = true;
    public ScheduleListForms: FormArray;
    public SessionState$: Observable<ISessionState>;
    public users$: Observable<Array<IUser>>;
    public staffUsers$: Observable<Array<IUser>>;
    public propertyTypes: string[] = PROPERTY_TYPES;
    public taskStatus: string[] = TASK_STATUS;
    public taskTypes: string[] = TASK_TYPES;
    public maintenanceReqs: string[] = MAINTENANCE_REQUESTS;
    public urgencyLevels: string[] = URGENCY;
    public searchString: string = "";
    public selectedTenant: IUser = null;
    public selectedStaff: IAssignee[] = [];
    public isSearching: boolean = false;
    public SelectedRepeat: string = '';
    public SelectedRepeatQuantity: number = 1;
    public SelectedFrequency: string = 'Weekly';
    public SelectedMonthlyConvention: string = 'Day of month';
    public SelectedDuration: string = 'forever';
    public searchStaffStr: string = "";
    public newDate: Date = null;
    public IsReAssigning: boolean = false;
    public activeTabIndex: number = 0;
    
    public CurrentMonthDayCount: number = 30;
    public DAYS_IN_MONTH = [
        31,
        28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31
    ];

    get UploadPreviews(): string[] {
        return this._uploadPreviews;
    }

    set UploadPreviews(uploadPreview: string[]) {
        this._uploadPreviews = uploadPreview;
    }

    get TaskUploadPreviews(): string[] {
        return this._taskUploadPreviews;
    }

    set TaskUploadPreviews(uploadPreview: string[]) {
        this._taskUploadPreviews = uploadPreview;
    }

    get ImageArray(): File[] {
        return this._imageDataArr;
    }

    set ImageArray(newValue: File[]) {
        this._imageDataArr = newValue;
    }

    get TaskImageArray(): File[] {
        return this._taskImageDataArr;
    }

    set TaskImageArray(newValue: File[]) {
        this._taskImageDataArr = newValue;
    }

    get PhotoModified(): boolean {
        return this._photoModified;
    }

    set PhotoModified(newValue: boolean) {
        this._photoModified = newValue;
    }

    get IsUploading(): boolean {
        return this._isUploading;
    }

    set IsUploading(isUploading: boolean) {
        this._isUploading = isUploading;
    }

    get UploadStatus(): string {
        return this._uploadStatus;
    }

    set UploadStatus(uploadStatus: string) {
        this._uploadStatus = uploadStatus;
    }

    get IsUploadDone(): boolean {
        return this._isUploadDone;
    }

    set IsUploadDone(isUploadDone: boolean) {
        this._isUploadDone = isUploadDone;
    }

    get UploadPreview(): string {
        return this._uploadPreview;
    }

    set UploadPreview(uploadPreview: string) {
        this._uploadPreview = uploadPreview;
    }

    get ProgressPercent(): number {
        return this._progressPercent;
    }

    set ProgressPercent(progressPercent: number) {
        this._progressPercent = progressPercent;
    }

    get CanCancelUpload(): boolean {
        return this._canCancelUpload;
    }

    set CanCancelUpload(canCancelUpload: boolean) {
        this._canCancelUpload = canCancelUpload;
    }
    
    get HideScrollDownButtom(): boolean {
        return this._hideScrollDownButtom;
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get task(): ITask {
        return this._task;
    }

    set task(newValue: ITask) {
        this._task = newValue;
    }

    get orgs(): IOrgBasic[] {
        return this._orgs;
    }

    set orgs(orgs: IOrgBasic[]) {
        this._orgs = orgs;
    }

    get selectedOrgId(): string {
        return this._selectedOrgId;
    }

    set selectedOrgId(id: string) {
        this._selectedOrgId = id;
    }

    get selectedBuildingId(): string {
        return this._selectedBuildingId;
    }

    set selectedBuildingId(id: string) {
        this._selectedBuildingId = id;
    }

    get userFormBuildings(): FormArray {
        return this._userFormBuildings;
    }

    set userFormBuildings(form: FormArray) {
        this._userFormBuildings = form;
    }

    get buildingIdNameMap(): Map<string, string> {
        return this._buildingIdNameMap;
    }

    set buildingIdNameMap(map: Map<string, string>) {
        this._buildingIdNameMap = map;
    }

    get selectedOrgBuildings(): Array<IBuildingSimple> {
        return this._selectedOrgBuildings;
    }

    set selectedOrgBuildings(buildings: Array<IBuildingSimple>) {
        this._selectedOrgBuildings = buildings;
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role !== 'Landlord') && (this.sessionData.user.role !== 'Tenant')));
    }

    get IsNotLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Landlord'));
    }

    constructor(
        private dialog: MdDialog,
        private _controls: Controls,
        private _store: Store<AppState>,
        private _formBuilder: FormBuilder,
        private _sessionActions: SessionActions,
        private _userActions: UserActions,
        private _orgActions: OrgActions,
        private cdr: ChangeDetectorRef,
        public sessionData: SessionData,
        public firebaseStorage: FirebaseStorage,
        @Optional() public dialogRef: MdDialogRef<RecurringTaskDetailsComponent>) {
        this.orgs$ = this._store.select((state) => state.orgState);
        this.AllBuildings$ = this._store.select((state) => state.orgState.allBuildings);
        this.SessionState$ = this._store.select((state) => state.sessionState);
        this.users$ = this._store.select((state) => state.usersState.searchTenants);
        this.staffUsers$ = this._store.select((state) => state.usersState.users);
        this.taskFeed$ = this._store.select((state) => state.sessionState.taskFeed);
        this._imageDatas = null;
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {}

    // Extension to the constructor that allows
    // the calling coponent to set the initial values for the
    // user form if possible.
    public PopulateForm(): void {
        this.minDate.setMonth(this.minDate.getMonth() - 3);
        // this.MonthDays = Array(5).fill().map((x,i)=>i); // [0,1,2,3,4]
        let helper: IGetBldgHelper = {
            orgID: this.sessionData.user.orgs.id,
            buildingID: this.sessionData.user.orgs.buildings[0].id,
        };
        this._store.dispatch(this._userActions.GetUsers(null));
        if (this.isNewTask && this.sessionData.user.isAdmin) {
            this._store.dispatch(this._orgActions.GetAllOrgsScope());
        }
        let currentDate: Date = new Date(Date.now());
        this.CurrentMonthDayCount = this.DAYS_IN_MONTH[currentDate.getMonth()];
        let currentHour: number = currentDate.getHours();
        let newHour: number = currentHour + 1;
        let day: string = this._getDay(currentDate.getDay());
        this.RepeatOptions.push(`Weekly on ${ day }`);
        this.RepeatOptions.push(`Every 2 weeks on ${ day }`);
        this.RepeatOptions.push(`Monthly on the ${currentDate.getDate() }${ this._nth(currentDate.getDate()) } day of the month`);
        this.RepeatOptions.push(`Custom`);
        this.SelectedRepeat = this.RepeatOptions[4];
        this.taskForm.patchValue({
            repeats: this.RepeatOptions[2],
            durationAmount: this.DurationOptions[4],
            durationQuantity: 1
        });
        console.log("this.task", this.task);
        let timeViews = document.getElementsByClassName('time-view');
        // console.log("timeViews", timeViews);
        if ((null != timeViews) && (null != timeViews[0])) {
            timeViews[0].className = 'time-view time-view-visible';
        }
        if (!this.isNewTask) {
            this.taskForm.patchValue({
                authorName: this.task.authorName ? this.task.authorName : "",
                authorID: this.task.authorID ? this.task.authorID : "",
                clientID: this.task.clientID ? this.task.clientID : "",
                clientName: this.task.clientName ? this.task.clientName : "",
                clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                active: this.task.active ? this.task.active : true,
                icon: this.task.icon ? this.task.icon : "",
                status: this.task.status ? this.task.status : "",
                deleted: this.task.deleted ? this.task.deleted : false,
                notes: this.task.notes ? this.task.notes : "",
                date: this.task.date ? this.task.date : 0,
                id: this.task.id ? this.task.id : "",
                type: this.task.type ? this.task.type : "",
                category: this.task.category ? this.task.category : "",
                subCategory: this.task.subCategory ? this.task.subCategory : "",
                orgID: this.task.orgID ? this.task.orgID : "",
                buildingID: this.task.buildingID ? this.task.buildingID : "",
                buildingName: this.task.buildingName ? this.task.buildingName : "",
                subject: this.task.subject ? this.task.subject : "",
                description: this.task.description ? this.task.description : "",
                photos: this.task.photoURL ? this.task.photoURL : "",
                files: this.task.files ? this.task.files : "",
                lastUpdated: this.task.lastUpdated ? this.task.lastUpdated : 0,
                buildingAddress: this.task.buildingAddress ? this.task.buildingAddress : "",
                permissionToEnter: this.task.permissionToEnter ? this.task.permissionToEnter : false,
                havePets: this.task.havePets ? this.task.havePets : false,
                startDate: this.task.startDate ? new Date(this.task.startDate) : currentDate,
                startTime: this.task.startTime ? this.task.startTime : `${currentDate.getHours()}:00`,
                recurring: this.task.recurring ? this.task.recurring : true,
                durationAmount:  this.task.durationAmount ? this.task.durationAmount : currentDate,
                durationQuantity:  this.task.durationQuantity ? this.task.durationQuantity : currentDate,
                endTime:  this.task.endTime ? this.task.endTime : `${currentDate.getHours() + 1}:00`,
                clockInTime:  this.task.clockInTime ? this.task.clockInTime : 0,
                clockOutTime:  this.task.clockOutTime ? this.task.clockOutTime : 0,
                repeats: this.task.repeats ? this.task.repeats : '',
                repeatDays: this.task.repeatDays ? this.task.repeatDays : [''],
                repeatInterval: this.task.repeatInterval ? this.task.repeatInterval : 'Weekly',
                repeatQuantity: this.task.repeatQuantity ? this.task.repeatQuantity : 1
            });
            // console.log("taskForm", this.taskForm.value);
            this.building = {
                id: this.task.buildingID,
                name: this.task.buildingName
            };

            let helper: ITaskFeedJoin = {
                orgID: this.task.orgID,
                buildingID: this.task.buildingID,
                taskID: this.task.id,
            };
            this.orgID = this.task.orgID;
            this.buildingID = this.task.buildingID;
            this.newTaskFeed.orgID = this.task.orgID;
            this.newTaskFeed.buildingID = this.task.buildingID;
            this.newTaskFeed.taskID = this.task.id;
            this.newTaskFeed.assignees = this.task.assignees;
            this.newTaskFeed.authorID = this.sessionData.user.uid;
            this.newTaskFeed.authorName = `${this.sessionData.user.firstName} ${this.sessionData.user.lastName}`;
            this.newTaskFeed.assignees = this.task.assignees;
            this.newTaskFeed.clientUnit = this.task.clientUnit;
            this.newTaskFeed.category = this.task.category;
            this.newTaskFeed.subCategory = this.task.subCategory;
            this.newTaskFeed.photoGallery = [];
            this.newTaskFeed.status = this.task.status;
            this.newTaskFeed.type = "update";
            if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
                this.newTaskFeed.approved = false;
            } else {
                this.newTaskFeed.approved = true;
            }
            if ((null != this.task.assignees) && (this.task.assignees.length > 0)) {
                this.selectedStaff = this.task.assignees;
            }
            this.SelectedDuration = this.task.durationAmount;
            this.SelectedFrequency = this.task.repeatInterval;
            this.SelectedRepeat = this.task.repeats;
            this.SelectedDate = new Date(this.task.startDate);
            this.SelectedRepeat = this.task.repeats;
            if (this.SelectedFrequency === this.FrequencyOptions[2]) {
                this.SelectedMonthlyConvention = this.task.monthlyConvention;
                if (this.SelectedMonthlyConvention === 'Day of month') {
                    this.monthlyDays = this.task.monthlyDays;
                } else {
                    // let weekCount = 0;
                    // for (let week of this.task.monthlyNWeekDays) {
                    //     if (null != week) {
                    //         this.monthlyNWeekDays[weekCount] = week;
                    //     }
                    //     weekCount = weekCount + 1;
                    // }
                    // this.monthlyNWeekDays = this.task.monthlyNWeekDays;
                    if (null != this.task.monthlyNWeekDays) {
                        let keys = Object.keys(this.task.monthlyNWeekDays);
                        for (let key of keys) {
                            if (null != this.task.monthlyNWeekDays[Number(key)]) {
                                // let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `
                                for (let day of this.task.monthlyNWeekDays[Number(key)]) {
                                    if (null != day) {
                                        this.monthlyNWeekDays[Number(key)].push(Number(day));
                                        // let dayStr: string = `${weekStr} ${this.GetDay(Number(key))}, `;
                                        // repeat += dayStr;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (this.SelectedFrequency === this.FrequencyOptions[1]) {
                this.monthlyWeekDays = this.task.monthlyWeekDays;
                // this.taskForm.patchValue({
                //     monthlyConvention
                // })
            }
            
            this._orgStateSubscription = this.orgs$.subscribe((orgState: IOrgState) => {
                if (null != orgState.orgs && orgState.orgs.length > 0) {
                    this.orgs = orgState.orgs;
                    this.selectedCategory = this.task.category;
                    if (this.task.category === "Maintenance Request") {
                        this.selectedSubCategory = this.task.subCategory;
                    }
                    if (null != this.task.clientID) {
                        let match: any = {
                            uid: this.task.clientID,
                            firstName: this.task.clientName.split(" ", 2)[0],
                            lastName: this.task.clientName.split(" ", 2)[1],
                            clientUnit: this.task.clientUnit ? this.task.clientUnit : "",
                            orgs: {
                                id: this.task.orgID,
                                buildings: [
                                    {
                                        name: this.task.buildingName,
                                        id: this.task.buildingID,
                                    },
                                ],
                            },
                        };
                        this.selectedTenant = match;
                    }
                }
            });
        } else {
            this.taskDetailTitle = "New Recurring Job";
            this.taskForm.patchValue({
                authorName: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
                authorID: this.sessionData.user.uid,
                category: "Recurring Task",
                status: "New",
                deleted: false,
                active: true,
                recurring: true,
                startDate: currentDate,
                startTime: `${ currentHour }:00`,
                endTime: `${ newHour }:00`
            });
        }
    }

    public GetNewDate(): string {
        let text: string = '';
        let taskDate: Date = new Date(this.task.startDate);
        switch (this.task.durationAmount) {
            case this.DurationOptions[0]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setDate(endDate.getDate() + Number(this.task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Daily", endDate);
                return text;
            }

            case this.DurationOptions[1]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setDate(endDate.getDate() + (Number(this.task.durationQuantity) * 7));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Weekly", endDate);
                return text;
            }

            case this.DurationOptions[2]:
            {
                let date: Date = new Date(this.task.startDate);
                let currentDate = date.getDate();
                // Set to day 1 to avoid forward
                date.setDate(1);
                // Increase month by 1
                date.setMonth(date.getMonth() + Number(this.task.durationQuantity));
                // console.log("Monthly", date);
                // Get max # of days in this new month
                let daysInMonth = new Date(taskDate.getFullYear(), date.getMonth(), 0).getDate();
                // Set the date to the minimum of current date of days in month
                date.setDate(Math.min(currentDate, daysInMonth));
                // date.setMonth(date.getMonth() + task.durationQuantity);
                text = `until ${ this._getMonthDayYear(date) }`;
                // console.log("Monthly", date);
                return text;
            }

            case this.DurationOptions[3]:
            {
                let endDate: Date = new Date(this.task.startDate);
                endDate.setFullYear(endDate.getFullYear() + Number(this.task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Yearly", endDate);
                return text;
            }

            case this.DurationOptions[4]:
            {
                text = ` forever`;
                // console.log("Yearly", endDate);
                return text;
            }
            
            default:
            {
                break;
            }
        }
    }

    public GetYearRepeat(): string {
        let repeat: string = '';
        let date: Date = new Date(this.task.startDate);
        repeat = `the ${ this.Nth(date.getDate()) } day of ${ this.monthNames[date.getMonth()] }`;
        return repeat;
    }

    public FormatTime(time: string): string {
        let newTime: string = time.split(":", 2)[0];
        let numberTime: number = Number(newTime);
        if (Number(newTime) > 12) {
            newTime = `${(numberTime - 12)}:${ time.split(":", 2)[1]} PM`;
            return newTime;
        } else if (Number(newTime) === 12) {
            newTime = `${(numberTime)}:${ time.split(":", 2)[1]} PM`;
            return newTime;
        } else {
            newTime = `${(numberTime)}:${ time.split(":", 2)[1]} AM`;
            return newTime;
        }
    }

    public GetHowOftenRepeat(): string {
        let repeatInterval: string = this.SelectedFrequency;
        let weekly: string  = (repeatInterval === 'Weekly') ? 'week(s)' : 'month(s)';
        let repeatQuantity: number = this.taskForm.get("repeatQuantity").value;
        if (repeatQuantity > 1) {
            return `every ${ repeatQuantity } ${ weekly }`;
        } else {
            return `${ repeatInterval.toLowerCase() }`;
        }
    }

    public GetName(user: IUser): string {
        return `${user.firstName} ${ user.lastName }`;
    }

    public ChooseDay(day: number): void {
        if (!this.monthlyWeekDays.includes(Number(day))) {
            this.monthlyWeekDays.push(day);
        } else {
            this.monthlyWeekDays = this.monthlyWeekDays.filter((days: number) => {
                return (day !== days);
            });
        }
    }

    public ChooseConvention(choice: string): void {
        this.SelectedMonthlyConvention = choice;
        this.taskForm.patchValue({
            monthlyConvention: choice
        });
    }

    public IncludesDay(day: number): boolean {
        return this.monthlyWeekDays.includes(day);
    }

    public GetDay(day: number): string {
        return this._getDay(day);
    }

    public Nth(day: number): string {
        return this._nth(day);
    }

    public GetWeek(): string {
        let repeat: string = ``;
        if (null == this.task) {
            if (null != this.monthlyNWeekDays) {
                let keys = Object.keys(this.monthlyNWeekDays);
                for (let key of keys) {
                    if (null != this.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of this.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }              
        } else {
            if (null != this.task.monthlyNWeekDays) {
                let keys = Object.keys(this.task.monthlyNWeekDays);
                
                for (let key of keys) {
                    if (null != this.task.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of this.task.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }
        }
        return repeat;
    }

    public ReAssignStaff(): void {
        this.IsReAssigning = !this.IsReAssigning;
        this.cdr.detectChanges();
    }

    public ChooseMonthDay(day: number): void {
        if (!this.monthlyDays.includes(Number(day))) {
            this.monthlyDays.push(day);
        } else {
            this.monthlyDays = this.monthlyDays.filter((days: number) => {
                return (day !== days);
            });
        }
    }

    public ChooseNWeekMonthDay(day: number, week: number): void {
        if ((null != this.monthlyNWeekDays[week]) && (!this.monthlyNWeekDays[week].includes(Number(day)))) {
            this.monthlyNWeekDays[week].push(day);
        } else {
            this.monthlyNWeekDays[week] = this.monthlyNWeekDays[week].filter((days: number) => {
                return (day !== days);
            });
        }
        // // console.log("this.task", this.taskForm.valid, this.taskForm.errors);
    }

    public IncludesNWeekMonthDay(day: number, week: number): boolean {
        return (null != this.monthlyNWeekDays[week]) ? this.monthlyNWeekDays[week].includes(day) : false;
    }

    public IncludesMonthDay(day: number): boolean {
        return this.monthlyDays.includes(day);
    }

    public OpenImage(event: HTMLElement): void {
        // console.log("event", event);
        const viewer = new Viewer(event, {
            inline: false,
            zIndex: 9999,
            title: 0,
            toolbar: {
                zoomIn: 1,
                zoomOut: 1,
                oneToOne: 1,
                reset: 1,
                prev: 0,
                play: {
                  show: 0,
                  size: 'large',
                },
                next: 0,
                rotateLeft: 4,
                rotateRight: 4,
                flipHorizontal: 4,
                flipVertical: 4,
            },
            viewed() {
                viewer.zoomTo(0.5);
            },
        });
        viewer.show();
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public SwitchEditMode(): void {
        if (this.IsEditMode && this.taskForm.dirty) {
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        }
        this.IsEditMode = !this.IsEditMode;
    }

    public SelectBuilding(building: IBuildingSimple): void {
        if (building !== this.building) {
            this.building = building;
            this.taskForm.patchValue({
                clientID: building.id,
                clientName: building.name,
                clientUnit: "",
                orgID: building.orgID,
                buildingID: building.id,
                buildingName: building.name,
            });
            // console.log("SelectBuilding", this.taskForm.value);
            this.isSearching = false;
            // this.searchString = "";
        } else {
            this._controls.ShowAlert("Already Selected", "This tenant is currently selected. Please choose a different one.", "Dismiss");
        }
    }

    public onFrequencyChange(frequency: string): void {
        this.SelectedFrequency = frequency;
        let match: number = this.FrequencyOptions.findIndex((fre: string) => {
            return (fre === frequency);
        });

        let match2: number = this.DurationOptions.findIndex((dur: string) => {
            return (dur === this.SelectedDuration);
        });

        if (match > match2) {
            this.SelectedDuration = this.DurationOptions[match];
        }
    }

    public ChangeTime(event: any): void {
        // console.log("event", event);
    }

    public SearchTenants(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllTenants(newVal));
            if (newVal.trim() === "") {
                this.isSearching = false;
            } else {
                this.isSearching = true;
            }
        }
    }

    public MarkComplete(): void {
        let task: ITask = Object.assign({}, this.task);
        this._controls.ShowDialog("Complete Request?", "Please confirm you want to mark this request as completed.", "Confirm", "Cancel").subscribe((response: any) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                } else {
                    task.active = false;
                    task.status = "Completed";
                    let date: string = `${this.newDate.getMonth() + 1}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                    if (null != task.completedDates) {
                        task.completedDates.push(date);
                    } else {
                        task.completedDates = [];
                        task.completedDates.push(date);
                    }
                    // this._store.dispatch(this._sessionActions.CompleteTask(task));
                    setTimeout(() => {
                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                    }, 700);
                }
            } else {
                // No response detected, its safer to stay and not loose the user's changes
                return;
            }
        });
    }

    public MarkRecurringComplete(): void {
        let task: ITask = Object.assign({}, this.task);
        this._controls.ShowDialog("Complete Job?", "Please confirm you want to mark this job as completed.", "Confirm", "Cancel").subscribe((response: any) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    task.status = "Assigned";
                    let date: string = `${this.newDate.getMonth() + 1}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                    if (null != task.completedDates) {
                        task.completedDates.push(date);
                    } else {
                        task.completedDates = [];
                        task.completedDates.push(date);
                    }
                    this._store.dispatch(this._sessionActions.CompleteRecurringTask(task));
                    setTimeout(() => {

                            this._store.dispatch(this._sessionActions.GetRecurringTasks());
                        
                    }, 700);
                } else {
                }
            }
        });

    }

    public ReOpenRecurring(): void {
        let task: ITask = Object.assign({}, this.task);
        this._controls.ShowDialog("Un-Mark Complete?", "Please confirm you want to un-mark this task as complete.", "Confirm", "Cancel").subscribe((response: any) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    let date: string = `${this.newDate.getMonth() + 1}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                    if (null != task.completedDates) {
                        task.completedDates = task.completedDates.filter((oldDate: string) => {
                            return (oldDate !== date);
                        });
                    }
                    task.status = "In Progress";
                    this._store.dispatch(this._sessionActions.ReopenRecurringTask(task));
                    setTimeout(() => {

                            this._store.dispatch(this._sessionActions.GetRecurringTasks());

                        
                    }, 700);
                }
            }
        });
       
    }

    public ReOpen(): void {
        let task: ITask = Object.assign({}, this.task);
        this._controls.ShowDialog("Un-Mark Complete?", "Please confirm you want to un-mark this task as complete.", "Confirm", "Cancel").subscribe((response: any) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    task.active = true;
                    task.status = "In Progress";
                    this._store.dispatch(this._sessionActions.ReopenTask(task));
                    setTimeout(() => {
                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                    }, 700);
                }
            }
        });
    }

    public AssignTask(): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `\n${staff.name}`
            });
            this._controls.ShowAssignDialog(staffStr)
            .subscribe((response: IInputDialogBase) => {
                if ((null != response.input) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                    this.task.status = "Assigned";
                    this.task.assignees = this.selectedStaff;
                    let helper: AssignTaskHelper = {
                        userID: '',
                        authorID: this.sessionData.user.uid,
                        task: this.task
                    };

                    if ((null != response.input) && (response.input !== '')) {
                        // console.log("data", response.input);
                        helper.message = response.input;
                    }
                    this._store.dispatch(this._sessionActions.AssignTask(helper));
                    setTimeout(() => {
                        
                        this.IsReAssigning = false;
                        this.closeAction();
                        this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                        if ((null != this.sessionData.user)) {
                            this._store.dispatch(this._sessionActions.GetTasks());
                            // this._store.dispatch(this._userActions.GetUsers(null));
                        } else {
                            
                        }
                    }, 700);
                }
                       
            });
        } else {
            this._controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public AssignRecurringTask(): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `\n${staff.name}`
            });
            this._controls.ShowAssignDialog(staffStr)
            .subscribe((response: IInputDialogBase) => {
                if ((null != response.input) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                    this.task.assignees = this.selectedStaff;
                    let helper: AssignTaskHelper = {
                        userID: '',
                        authorID: this.sessionData.user.uid,
                        task: this.task,
                    };
                    if ((null != response.input) && (response.input !== '')) {
                        // console.log("data", response.input);
                        helper.message = response.input;
                    }
                    this._store.dispatch(this._sessionActions.AssignTask(helper));
     
                    setTimeout(() => {
                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                        this.IsReAssigning = false;
                        this.closeAction();
                        this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                    }, 700);
                } else {
                    // let oldEvent: ITask = Object.assign({}, this.task);
                    // let oldTime: Date = new Date(this.task.startDate);
                    // let skipDate = `${this.newDate.getMonth()}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                    // // console.log("eventTimesChanged", skipDate);
                    // if ((null != oldEvent.skipDates)) {
                    //     oldEvent.skipDates.push(skipDate);
                    // } else {
                    //     oldEvent.skipDates = [ skipDate ];
                    // }

                    // // console.log("old task", oldEvent, oldEvent.startDate);
                    // this._store.dispatch(this._sessionActions.UpdateTask(oldEvent));

                    // let newEvent: ITask = Object.assign({}, this.task);
                    // Object.assign({}, newEvent, {
                    //     startDate: this.newDate.getTime(),
                    //     durationAmount: 'day(s)',
                    //     durationQuantity: 0,
                    //     repeatInterval: "Once",
                    //     status: "Assigned",
                    //     subject: `Re-Assign: ${newEvent.subject}`,
                    //     description: `Reassigned task once for date: ${this.newDate.toDateString()} ${newEvent.description}`,
                    //     assignees: this.selectedStaff,
                    //     recurring: false
                    // });
                    // newEvent.startDate = this.newDate.getTime();
                    // newEvent.durationAmount = 'day(s)';
                    // newEvent.durationQuantity = 0;
                    // newEvent.repeatInterval = "Once";
                    // newEvent.status = "Assigned";
                    // newEvent.subject = `Re-Assign: ${newEvent.subject}`;
                    // newEvent.description = `Reassigned task once for date: ${this.newDate.toDateString()} ${newEvent.subject}`;
                    // newEvent.assignees = this.selectedStaff;
                    // newEvent.recurring = false;
                    // newEvent.completedDates = [];
                    // let endDate: Date = new Date(newEvent.startDate);
                    // endDate.setDate(endDate.getDate() + 1);
                    // newEvent.endDate = endDate.getTime();
                    // // console.log("new task", newEvent);
                    // this._store.dispatch(this._sessionActions.CreateTask(newEvent));
                    // if ((null != this.sessionData.user)) {
                    //     this._store.dispatch(this._sessionActions.GetRecurringTasks());
                    //     this.IsReAssigning = false;
                    //     this.closeAction();
                    //     this._controls.ShowAlert("Successfully Re-Assigned", "Successfully re-assigned the task", "Thanks");
                    // }
                }
            });
        } else {
            this._controls.ShowAlert("No One Selected", "Please choose at least one staff member to assign.", "Dismiss");
        }
    }

    public DeleteTask(): void {
        this._controls.ShowDialog('Delete Task?', `Please confirm you want to delete this task. This action cannot be undone`, 'Delete', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                let dialogResponse: ITaskDialogResponse = {
                    task: this.task,
                    action: Constants.DIALOG_RESPONSE_ACTION_DELETE,
                };
                this._handleTaskDetailAction(dialogResponse);
                this.dialogRef.close(dialogResponse);
            }
        });
    }

    public ChangeDateTask(): void {
        this._controls.ShowChangeDateDialog(this.newDate)
        .subscribe((response: IInputDialogBase) => {
            let matchedEvent: ITask = this.task;
            // console.log("response", response);
            // // // // console.log("matchedEvent", matchedEvent, newStart, newEnd);
            let oldEvent: ITask = this.task;
            if ((null != response.input) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                let oldTime: Date = new Date(matchedEvent.startDate);
                let skipDate = `${this.newDate.getMonth()}_${this.newDate.getDate()}_${this.newDate.getFullYear()}`;
                // // console.log("eventTimesChanged", skipDate);
                if ((null != oldEvent.skipDates)) {
                    oldEvent.skipDates.push(skipDate);
                } else {
                    oldEvent.skipDates = [ skipDate ];
                }
                let newNonSkip = `${response.date.getMonth()}_${response.date.getDate()}_${response.date.getFullYear()}`;
                if (null != oldEvent.skipDates) {
                    if (oldEvent.skipDates.includes(newNonSkip)) {
                        oldEvent.skipDates = oldEvent.skipDates.filter((dte: string) => {
                            return (dte !== newNonSkip);
                        });
                    }
                }
                // console.log("old task", oldEvent, oldEvent.startDate);
                this._store.dispatch(this._sessionActions.UpdateTask(oldEvent));
                setTimeout(() => {
                    let newEvent: ITask = Object.assign({}, this.task);
                    Object.assign({}, newEvent, {
                        startDate: response.date.getTime(),
                        durationAmount: 'day(s)',
                        durationQuantity: 0,
                        repeatInterval: "Once",
                        subject: `Date Change: ${newEvent.subject}`,
                        description: `Date changed once for this occurance: ${response.date.toDateString()} ${newEvent.description}`,
                        status: "Assigned",
                        recurring: this.task.recurring
                    });
                    newEvent.startDate = response.date.getTime();
                    newEvent.durationAmount = 'day(s)';
                    newEvent.durationQuantity = 0;
                    newEvent.repeatInterval = "Once";
                    newEvent.subject = `Date Change: ${newEvent.subject}`;
                    newEvent.description = `Date changed once for this occurance: ${response.date.toDateString()} ${newEvent.description}`;
                    newEvent.status = "Assigned";
                    newEvent.recurring = this.task.recurring;
                    newEvent.completedDates = [];
                    let endDate: Date = new Date(newEvent.startDate);
                    endDate.setDate(endDate.getDate() + 1);
                    newEvent.endDate = endDate.getTime();
                    console.log("new task", newEvent);
                    this._store.dispatch(this._sessionActions.CreateTask(newEvent));
                    if ((null != this.sessionData.user)) {
                        this._store.dispatch(this._sessionActions.GetRecurringTasks());
                        setTimeout(() => {
                            this.closeAction();
                        }, 750);
                    }
                }, 500);
            } else {
                
            }
        });
    }

    public AddStaffMember(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        // this._store.dispatch(this._userActions.GetUsers(null));
        const dialogRef = this.dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = true;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SelectedStaff = this.selectedStaff;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                // console.log("Chosen person", user);
                let assignee: IAssignee = {
                    name: `${ user.firstName } ${user.lastName}`,
                    uid: user.uid
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                    // console.log("selectedStaff", this.selectedStaff);
                    this.cdr.detectChanges();
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                    // console.log("selectedStaff", this.selectedStaff);
                }
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        this._controls.ShowDialog(`Remove staff?`, "Please confirm that you want to remove: " + `${ staff.name}`, "Remove", "Cancel")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                        return (assignee.uid !== staff.uid);
                    });
                    // console.log("newStaff", newStaff);
                    this.selectedStaff = newStaff;
                    this.cdr.detectChanges();
                } else {

                }
            }
        });
    }

    public AddTenant(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        if (this.sessionData.user.isAdmin) {
            this._store.dispatch(this._userActions.GetAllTenants());
        } else {
            this._store.dispatch(this._userActions.GetAllTenantsLandlord());
        }
        const dialogRef = this.dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = false;
        dialogRef.componentInstance.SearchingTenant = true;
        dialogRef.componentInstance.SelectedTenant = this.selectedTenant;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                this.selectedTenant = user;
                this.cdr.detectChanges();
                // console.log("selectedTenant", this.selectedTenant);
                this.taskForm.patchValue({
                    clientID: user.uid,
                    clientName: user.firstName + " " + user.lastName,
                    clientUnit: user.unit ? user.unit : "",
                    orgID: user.orgs.id,
                    buildingID: user.orgs.buildings[0].id,
                    buildingName: user.orgs.buildings[0].name,
                });
                this.isSearching = false;
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public AddBuilding(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        this._store.dispatch(this._orgActions.GetAllOrgsScope());
        const dialogRef = this.dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = false;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SearchingBuildings = true;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IBuildingDialogResponse) => {
            if (response) {
                let building: IBuildingSimple = <IBuildingSimple>response.selected;
                this.building = building;
                this.taskForm.patchValue({
                    clientID: building.id,
                    clientName: building.name,
                    clientUnit: "",
                    orgID: building.orgID,
                    buildingID: building.id,
                    buildingName: building.name,
                });
                this.cdr.detectChanges();
                // console.log("selectedTenant", this.selectedTenant);
                this.isSearching = false;
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public GetAudience(id: string): string {
        let audience: string = "landlord";
        switch (id) {
            case Constants.STR_AUDIENCE_TENANT_AND_LANDLORD:
            {
                return "the tenant and landlord(s)";
            }

            case Constants.STR_AUDIENCE_TENANT_AND_STAFF:
            {
                return "the tenant and the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_STAFF_ONLY:
            {
                return "the Silver Brick team";
            }

            case Constants.STR_AUDIENCE_TENANT_ONLY:
            {
                return "the tenant";
            }

            case Constants.STR_AUDIENCE_LANDLORD_ONLY:
            {
                return "the landlord(s)";
            }

            default:
            {
                return "the tenant";
            }
        }
    }

    public SearchStaff(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // console.log("newVal", newVal);
            this._store.dispatch(this._userActions.SearchAllStaff(newVal));
            if (newVal.trim() === "") {
                this.isSearching = false;
            } else {
                this.isSearching = true;
            }
        }
    }

    public onSubCategoryChange(type: string): void {
        this.selectedSubCategory = type;
    }

    public onUrgencyChange(type: string): void {
        this.selectedUrgency = type;
    }

    public onCategoryChange(state: string): void {
        this.selectedCategory = state;
    }

    public onStatusChange(state: string): void {
        this.selectedStatus = state;
    }

    public onDateSelect(event: any): void {
        console.log("onDateSelect", event);
        this.SelectedDate = event;
    }

    public GetIconName(activity: ITaskFeed): string {
        let iconName: string = "chat";
        switch (activity.type) {
            case "update": {
                iconName = "chat";
                break;
            }

            case "insert_photo": {
                iconName = "insert_photo";
                break;
            }

            case "new": {
                iconName = "add";
                break;
            }

            case "assign": {
                iconName = "assignment";
                break;
            }

            case "complete": {
                iconName = "check";
                break;
            }

            default: {
                break;
            }
        }
        return iconName;
    }

    public onRepeatChange(repeat: string): void {
        // console.log("onRepeatChange", repeat);
        this.SelectedRepeat = repeat;
    }

    public ChangeRepeatQuantity(quantity: number): void {
        this.SelectedRepeatQuantity = quantity;
    }

    public onDurationChange(duration: string): void {
        console.log("onDurationChange", duration);
        this.SelectedDuration = duration;
    }

    public onOrgChange(value: IFormSelctedValue): void {
        // Since I am not using the change event of the
        // select due to needing a complext value, this
        // is an optimization to only perform work if
        // the org is actually changed.
        if (value.id !== this.selectedOrgId) {
            this.selectedOrgId = value.id;
            // Extract the building list from the selected org
            const newBuildings: IBuildingSimple[] = this.orgs[value.index].buildings;
            this.selectedOrgBuildings = [...newBuildings];
            this._createBuildingIdNameMap();
            // Clear the building form array to avoid
            // cross org contamination
            this._resetUserFormBuildingArray();
        } else {
            // Selected org has not changed
            // Do nothing
        }
    }

    public async saveAction(): Promise<void> {
        let newTask: ITask = this.taskForm.value;
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            newTask.assignees = this.selectedStaff;
        }
        this.buildingID = newTask.buildingID;
        this.orgID = newTask.orgID;
        newTask.recurring = true;
        newTask.scheduled = true;
        newTask.scheduling = false;
        if (null != this.SelectedDuration) {
            newTask.durationAmount = this.SelectedDuration;
        } else {
            newTask.durationAmount = this.DurationOptions[4];
        }
        if (null != this.SelectedFrequency) {
            newTask.repeatInterval = this.SelectedFrequency;
        }
        if (null != this.SelectedRepeat) {
            newTask.repeats = this.SelectedRepeat;
        }
        if (null != this.SelectedDate) {
            newTask.startDate = this.SelectedDate.getTime();
        }
        if (null != this.SelectedRepeatQuantity) {
            newTask.repeatQuantity = this.SelectedRepeatQuantity;
        }
        if ((null == newTask.clientID) || (newTask.clientID === '')) {
            this._controls.ShowAlert("Missing Building", "Please choose a building", "Dismiss");
            return;
        } else if ((null == newTask.assignees) || (newTask.assignees.length === 0)) {
            this._controls.ShowAlert("Missing Staff", "Please assign this request to a staff member", "Dismiss");
            return;
        } else if ((null == newTask.startTime) || (newTask.startTime === "") || (null == newTask.endTime) || (newTask.endTime === "")) {
            this._controls.ShowAlert("Missing start/end time", "Please choose a start/end time", "Dismiss");
            return;
        } else if ((null == newTask.durationAmount) || (newTask.durationAmount === "")) {
            this._controls.ShowAlert("Missing duration", "Please choose a duration", "Dismiss");
            return;
        } else if ((null == newTask.subject) || (newTask.subject === "")) {
            this._controls.ShowAlert("Missing subject", "Please enter a subject", "Dismiss");
            return;
        } else if ((null == newTask.description) || (newTask.description === "")) {
            this._controls.ShowAlert("Missing description", "Please enter a description", "Dismiss");
            return;
        } else if ((null == newTask.category) || (newTask.category === "")) {
            this._controls.ShowAlert("Missing Category", "Please choose a category", "Dismiss");
            return;
        } else {
            if (newTask.repeats === "Custom") {
                switch (this.SelectedFrequency) {
                    case this.FrequencyOptions[0]:
                    {
                        
                        break;
                    }

                    case this.FrequencyOptions[1]:
                    {
                        newTask.monthlyWeekDays = this.monthlyWeekDays;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.FrequencyOptions[2]:
                    {
                        newTask.monthlyConvention = this.SelectedMonthlyConvention;
                        if (newTask.monthlyConvention === 'Day of month') {
                            newTask.monthlyDays = this.monthlyDays;
                            if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        } else {
                            newTask.monthlyNWeekDays = this.monthlyNWeekDays;
                            if ((null == newTask.monthlyNWeekDays) || (null != newTask.monthlyNWeekDays) && (newTask.monthlyNWeekDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        }
                        // // console.log("monthly", newTask.monthlyNWeekDays, newTask.monthlyConvention);
                        break;
                    }

                    case this.FrequencyOptions[3]:
                    {
                        newTask.monthlyDays = [new Date(Date.now()).getDate()];
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            } else {
                let currentDate: Date = new Date(Date.now());
                let day: string = this._getDay(currentDate.getDay());
                switch (newTask.repeats) {
                    case this.RepeatOptions[1]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        newTask.repeatQuantity = 1;
                        break;
                    }

                    case this.RepeatOptions[2]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        newTask.repeatQuantity = 2;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.RepeatOptions[3]:
                    {
                        newTask.monthlyDays = [currentDate.getDate() - 1];
                        newTask.repeatInterval = "Monthly";
                        newTask.repeatQuantity = 1;
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }

            // console.log("New Task", newTask);
            let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
            if (!this.isNewTask) {
                newTask.status = this.task.status;
                action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
            } else {
                newTask.status = "New";
                newTask.date = new Date(Date.now()).getTime();
                // Create a IUser object and add it to the dialogResult
            }
            let self = this;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            if (this._photoModified) {
                this._uploadMultipleFilesTask(newTask)
                .then(() => {
                    setTimeout(function() {
                        self._controls.ShowAlert("Task Added", "Task successfully added", 'Dismiss');
                        // Create a new message object from the default message object with an override of the text message
                        
                    }, (this._taskImageDataArr.length * 500));
                });
                // newTask.photoURL = await this._uploadPhotoTask();
            } else {
                let dialogResponse: ITaskDialogResponse = {
                    task: newTask,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
                this.dialogRef.close(dialogResponse);
            }
            console.log("task", newTask);
        }
    }

    public async UpdateAction(): Promise<void> {
        let newTask: ITask = this.task;
        let newerTask: ITask = this.taskForm.value;
        newTask.subject = newerTask.subject;
        newTask.description = newerTask.description;
        if (null != this.SelectedDuration) {
            newTask.durationAmount = this.SelectedDuration;
        }
            newTask.repeatInterval = this.SelectedFrequency;
        if (null != this.SelectedRepeat) {
            newTask.repeats = this.SelectedRepeat;
        }
        if (null != this.SelectedDate) {
            newTask.startDate = this.SelectedDate.getTime();
        }
        if (null != this.SelectedRepeatQuantity) {
            newTask.repeatQuantity = this.SelectedRepeatQuantity;
        }
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            newTask.assignees = this.selectedStaff;
        }
        this.buildingID = newTask.buildingID;
        this.orgID = newTask.orgID;
        if ((null == newTask.clientID) || (newTask.clientID === '')) {
            this._controls.ShowAlert("Missing Building", "Please choose a building", "Dismiss");
            return;
        } else if ((null == newTask.assignees) || (newTask.assignees.length === 0)) {
            this._controls.ShowAlert("Missing Staff", "Please assign this request to a staff member", "Dismiss");
            return;
        } else if ((null == newTask.startTime) || (newTask.startTime === "") || (null == newTask.endTime) || (newTask.endTime === "")) {
            this._controls.ShowAlert("Missing start/end time", "Please choose a start/end time", "Dismiss");
            return;
        } else if ((null == newTask.durationAmount) || (newTask.durationAmount === "")) {
            this._controls.ShowAlert("Missing duration", "Please choose a duration", "Dismiss");
            return;
        } else if ((null == newTask.subject) || (newTask.subject === "")) {
            this._controls.ShowAlert("Missing subject", "Please enter a subject", "Dismiss");
            return;
        } else if ((null == newTask.description) || (newTask.description === "")) {
            this._controls.ShowAlert("Missing description", "Please enter a description", "Dismiss");
            return;
        } else if ((null == newTask.category) || (newTask.category === "")) {
            this._controls.ShowAlert("Missing Category", "Please choose a category", "Dismiss");
            return;
        } else {
            if (newTask.repeats === "Custom") {
                switch (this.SelectedFrequency) {
                    case this.FrequencyOptions[0]:
                    {
                        
                        break;
                    }

                    case this.FrequencyOptions[1]:
                    {
                        newTask.monthlyWeekDays = this.monthlyWeekDays;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.FrequencyOptions[2]:
                    {
                        newTask.monthlyConvention = this.SelectedMonthlyConvention;
                        if (newTask.monthlyConvention === 'Day of month') {
                            newTask.monthlyDays = this.monthlyDays;
                            if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        } else {
                            newTask.monthlyNWeekDays = this.monthlyNWeekDays;
                            if ((null == newTask.monthlyNWeekDays) || (null != newTask.monthlyNWeekDays) && (newTask.monthlyNWeekDays.length === 0)) {
                                this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                                return;
                            }
                        }
                        // console.log("monthly", newTask.monthlyNWeekDays, newTask.monthlyConvention);
                        break;
                    }

                    case this.FrequencyOptions[3]:
                    {
                        newTask.monthlyDays = [new Date(Date.now()).getDate()];
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            } else {
                let currentDate: Date = new Date(Date.now());
                let day: string = this._getDay(currentDate.getDay());
                switch (newTask.repeats) {
                    case this.RepeatOptions[1]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        newTask.repeatQuantity = 1;
                        break;
                    }

                    case this.RepeatOptions[2]:
                    {
                        newTask.monthlyWeekDays = [currentDate.getDay()];
                        newTask.repeatInterval = "Weekly";
                        newTask.repeatQuantity = 2;
                        if ((null == newTask.monthlyWeekDays) || (null != newTask.monthlyWeekDays) && (newTask.monthlyWeekDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }

                    case this.RepeatOptions[3]:
                    {
                        newTask.monthlyDays = [currentDate.getDate() - 1];
                        newTask.repeatInterval = "Monthly";
                        newTask.repeatQuantity = 1;
                        if ((null == newTask.monthlyDays) || (null != newTask.monthlyDays) && (newTask.monthlyDays.length === 0)) {
                            this._controls.ShowAlert("Missing Days", 'There are no days assigned', "Try Again");
                            return;
                        }
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
            let action: string = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
            newTask.status = this.task.status;
            let self = this;
            console.log("new taks", newTask);
            if (this._photoModified) {
                this.task.photoGallery = [];
                this._uploadMultipleFilesTask(newTask)
                .then(() => {
                    setTimeout(function() {
                        self._controls.ShowAlert("Task Updated", "Task successfully updated", 'Dismiss');
                        // Create a new message object from the default message object with an override of the text message
                        
                    }, (this._taskImageDataArr.length * 500));
                });
                // newTask.photoURL = await this._uploadPhotoTask();
            } else {
                let dialogResponse: ITaskDialogResponse = {
                    task: newTask,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse);
                this.dialogRef.close(dialogResponse);
            }
        }
    }

    public closeAction(): void {
        if (this.taskForm.dirty) {
            // Ask user if they want to close since they have changed something
            this._controls.ShowLeaveDialog().subscribe((response: IUserDialogBase) => {
                if (response) {
                    if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                        this._dismissDialog();
                    } else {
                        // User chose either cancel or chosen response is other.
                        // At that point, its safer to stay and not loose the user's changes
                        return;
                    }
                } else {
                    // No response detected, its safer to stay and not loose the user's changes
                    return;
                }
            });
        } else {
            this._dismissDialog();
        }
    }

    public ApproveHistory(history: ITaskFeed): void {
        let audience: string = this.GetAudience(history.audience);
        this._controls.ShowDialog('Approve Note?', `Please confirm you want to approve this note. It will be send out to the audience of ${audience}`, 'Approve', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                let helper: AddMessageHelper = {
                    message: history,
                    taskID: history.taskID
                };
                this._store.dispatch(this._sessionActions.ApproveHistory(helper));
                setTimeout(() => {
                    this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                }, 1000);
            }
        });          
    }
    
    public UpdateHistory(history: ITaskFeed, photo: string): void {
        let audience: string = this.GetAudience(history.audience);
        
        this._controls.ShowDialog('Delete Photo?', `Please confirm you want to delete this photo. This action cannot be undone`, 'Delete', 'Cancel')
        .subscribe((response: IUserDialogBase) => {
            if ((null != response) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                history.photoGallery = history.photoGallery.filter((pht: string) => {
                    return (pht !== photo);
                });
                this.cdr.detectChanges();
                console.log("UpdateHistory", history);
                let helper: AddMessageHelper = {
                    message: history,
                    taskID: history.taskID
                };
                this._store.dispatch(this._sessionActions.UpdateHistory(helper));
                setTimeout(() => {
                    this._store.dispatch(this._sessionActions.GetTaskFeed({ orgID: history.orgID, buildingID: history.buildingID, taskID: history.taskID }));
                }, 1000);
            }
        });          
    }

    public async SendText(): Promise<void> {
        // if (this.SocketReconnected) {
        if (this.newTaskFeed) {
            // Create a partial message object for the object clone
            if ((this.IsStaff) && (!this.sessionData.user.isAdmin)) {
                this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
            } else if (this.sessionData.user.isAdmin) {   
                if (this.task.recurring) {
                    if (this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                } else {
                    if (this.SharedWithTenant && this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
                    } else if (this.SharedWithTenant && !this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_ONLY;
                    } else if (!this.SharedWithTenant && this.SharedWithLandlord) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_LANDLORD_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    }
                }
            } else if (this.sessionData.user.role === "Landlord") {
                if (this.task.recurring) {
                    this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                } else {
                    if (this.SharedWithTenant && this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    } else if (this.SharedWithTenant && !this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    } else if (!this.SharedWithTenant && this.SharedWithStaff) {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_STAFF_ONLY;
                    } else {
                        this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_STAFF;
                    }
                }
            } else {
                // this.newTaskFeed.audience = Constants.STR_AUDIENCE_TENANT_AND_LANDLORD;
            }
            if ((!this.sessionData.user.isAdmin) && (this.sessionData.user.role === 'Staff')) {
                this.newTaskFeed.approved = false;
            } else {
                this.newTaskFeed.approved = true;
            }
            // console.log("audience", this.newTaskFeed.audience, this.task.recurring);
            this.newTaskFeed.timestamp = new Date(Date.now()).getTime();
            if (this._photoModified) {
                this.newTaskFeed.category = this.task.category;
                this.newTaskFeed.subCategory = this.task.subCategory;
                this.newTaskFeed.role = this.sessionData.user.role;
                // this.newTaskFeed.photoURL = await this._uploadPhoto();
                this.newTaskFeed.type = "insert_photo";
                let self = this;
                this._uploadMultipleFiles()
                .then(() => {
                    setTimeout(function() {
                        this._controls.ShowAlert("Message Added", "Message successfully added", 'Dismiss');
                        // Create a new message object from the default message object with an override of the text message
                        
                    }, (this._imageDataArr.length * 500));
                });
                
            } else {

                const helper: AddMessageHelper = {
                    message: this.newTaskFeed,
                    taskID: this.newTaskFeed.taskID,
                };
                this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                // Clear the text message field
                this.newTaskFeed.message = "";
                this._controls.ShowAlert("Message Added", "Message successfully added", 'Dismiss');
            }
        } else {
            // Text message field is empty
        }
        // } else {
        //     this.controls.ShowAlert('Reconnecting...', 'One moment while we connect your Platform experience', 'Thanks');
        // }
    }

    /* Helper Methods */

    private _dismissDialog(): void {
        const dialogResponse: IBldgDialogResponse = {
            action: Constants.DIALOG_RESPONSE_ACTION_CANCEL,
            building: null,
        };
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.dialogRef.close(dialogResponse);
    }

    // Initially tried this.userFormBuildings.reset();
    // but that nulls out the values without resetting the length
    // This allows clearing of the array while keeping the binding in tact
    private _resetUserFormBuildingArray(): void {
        while (this.userFormBuildings.length > 0) {
            this.userFormBuildings.removeAt(this.userFormBuildings.length - 1);
        }
    }

    // Creates a map of the building id to building name
    // of the selected orgs. This allows us to show the
    // building name in the selected buildings list.
    private _createBuildingIdNameMap(): void {
        this.buildingIdNameMap = new Map<any, any>();
        for (const building of this.selectedOrgBuildings) {
            this.buildingIdNameMap.set(building.id, building.name);
        }
    }

    private _handleTaskDetailAction(response: ITaskDialogResponse) {
        this.SessionState$.skip(2)
            .take(1)
            .subscribe((currentSessionState) => {
                if (!currentSessionState.error) {
                    this.dialogRef.close(response);
                } else {
                    // There was an error, stay on the page
                }
            });
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD: {
                this._store.dispatch(this._sessionActions.CreateTask(response.task));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE: {
                this._store.dispatch(this._sessionActions.UpdateTask(response.task));
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE: {
                this._store.dispatch(this._sessionActions.DeleteTask(response.task));
                break;
            }

            default: {
                break;
            }
        }
    }

    public async handleFileInput(files: FileList): Promise<void> {
        for (let i = 0; i < files.length; i++) {
            this._imageDataArr.push(files.item(i));
        }

        this._photoModified = true;
        //this._uploadPreview = URL.createObjectURL(this._imageData);
        // this._setPhoto(this._imageData);
    }

    public RemoveImageTask(img: string, index: number): void {
        let taskUploadUrls = this._taskUploadUrls.splice(index, 1);
        let taskImageDataArr = this._taskImageDataArr.splice(index, 1);
        let taskUploadPreviews = this._taskUploadPreviews.splice(index, 1);
        this._taskImageDataArr = taskImageDataArr;
        this._taskUploadPreviews = taskUploadPreviews;
        this._taskUploadUrls = taskUploadUrls;
        // console.log("RemoveImageTask", this._taskUploadPreviews, this._taskImageDataArr);
        this.cdr.detectChanges();
    }

    /* Helper Methods */
    public readURL(event: any): void {
        // console.log("EVENT: ", event);
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                // console.log("TARGET: ", e.target.result);
                this._uploadPreviews.push(<string>e.target.result);
                // console.log("preview: ", this.UploadPreview);
                this.cdr.detectChanges();
            };
            reader.readAsDataURL(file);
        }
    }

    public async handleFileInputTask(files: FileList): Promise<void> {
        // this._imageData = files.item(0);
        for (let i = 0; i < files.length; i++) {
            this._taskImageDataArr.push(files.item(i));
        }
        this._photoModified = true;
        //this._uploadPreview = URL.createObjectURL(this._imageData);
        // this._setPhoto(this._imageData);
    }

    /* Helper Methods */
    public readURLTask(event: any): void {
        // console.log("EVENT: ", event);
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                // console.log("TARGET: ", e.target.result);
                this._taskUploadPreviews.push(<string>e.target.result);
                // this._uploadPreview = <string>e.target.result;
                // console.log("preview: ", this.UploadPreviews);
                this.cdr.detectChanges();
            };
            reader.readAsDataURL(file);
        }
    }

    public readMultiFiles(files: any[]): void {
        let reader = new FileReader();
        let self = this;
        function readFile(index: number) {
            if( index >= files.length ) return;
            let file = files[index];
            reader.onload = function(e: any) {  
                // get file content  
                self._uploadPreviews.push(<string>e.target.result);
                readFile(index + 1);
            }
            reader.readAsBinaryString(file);
        }
        readFile(0);
    }

    public CancelImageUpload(): void {
        if (this._uploadTask) {
            this._uploadTask.cancel();
        } else {
            // Upload task not set yet
        }
        this._finishImageSend(false);
    }

    public RemoveImage(): void {
        // Remove image data
        if (this._imageData) {
            this._imageData = null;
            this._photoModified = true;
        }
        // Hide the remove button & show add button
    }

    private async _uploadPhotoTask(): Promise<string> {
        // console.log("_uploadPhoto", this._uploadPreview);
        return new Promise<string>((resolve, reject) => {
            if (!this._imageData) {
                // Return empty string
                // Set in updatedProfile object to clear image
                resolve("");
            } else {
                // Set the upload preview
                // Show the upload UI
                this.IsUploading = true;
                // Generate filename and remove dashes
                // Generate full file path
                const fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
                let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
                const fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
                // Start image upload process and get upload task
                const contentType: string = "image.png";
                const format: string = "base64";
                this._uploadTask = this.firebaseStorage.Upload(fullPath, this._imageData, contentType);
                // Enable the cancel button now that the upload task is available
                this.CanCancelUpload = true;
                // Listen for state changes, error, completion of the upload
                this._uploadTask.on(
                    storage.TaskEvent.STATE_CHANGED,
                    (snapshot: firebase.storage.UploadTaskSnapshot) => {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    },
                    (error) => {
                        let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                        switch (error.name) {
                            case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                                // User doesn't have permission to access the object
                                message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                                break;
                            }

                            case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                                // User canceled the upload
                                message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                        this._controls.ShowDialog(error.name, message, "Ok", "Dismiss");
                        reject(message);
                    },
                    () => {
                        this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                        // Update UI with finishing and remove cancel button
                        this.IsUploadDone = true;
                        this.CanCancelUpload = false;
                        // Create a partial message object for the message clone
                        // Create a new message object from the default message object with an override of the text message
                        // const helper: AddMessageHelper = {
                        //     message: this.newTaskFeed,
                        //     taskID: this.newTaskFeed.taskID
                        // };
                        // this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                        // // Create a partial message object for the message clone
                        // this._finishImageSend(true);
                        resolve(this._uploadTask.snapshot.downloadURL);
                        // Update the photoURL path
                    }
                );
            }
        });
    }

    private async _uploadPhoto(): Promise<string> {
        // console.log("_uploadPhoto", this._uploadPreview);
        return new Promise<string>((resolve, reject) => {
            if (!this._imageDatas) {
                // Return empty string
                // Set in updatedProfile object to clear image
                resolve("");
            } else {
                // Set the upload preview
                // Show the upload UI
                this.IsUploading = true;
                // Generate filename and remove dashes
                // Generate full file path
                const fileName: string = (null != this.task) ? (this.task.id + StringUtil.GenerateUUID() + Date.now()) : StringUtil.GenerateUUID() + Date.now();
                let taskID: string = (null != this.task) ? this.task.id : StringUtil.GenerateUUID() + Date.now();
                const fullPath: string = `/orgs/${this.orgID}/tasks/${this.buildingID}/${taskID}/${fileName}.png`;
                // Start image upload process and get upload task
                const contentType: string = "image.png";
                const format: string = "base64";
                this._uploadTask = this.firebaseStorage.Upload(fullPath, this._imageDatas, contentType);
                // Enable the cancel button now that the upload task is available
                this.CanCancelUpload = true;
                // Listen for state changes, error, completion of the upload
                this._uploadTask.on(
                    storage.TaskEvent.STATE_CHANGED,
                    (snapshot: firebase.storage.UploadTaskSnapshot) => {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                    },
                    (error) => {
                        let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                        switch (error.name) {
                            case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                                // User doesn't have permission to access the object
                                message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                                break;
                            }

                            case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                                // User canceled the upload
                                message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                                break;
                            }

                            default: {
                                break;
                            }
                        }
                        this._controls.ShowDialog(error.name, message, "Ok", "Dismiss");
                        reject(message);
                    },
                    () => {
                        this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                        // Update UI with finishing and remove cancel button
                        this.IsUploadDone = true;
                        this.CanCancelUpload = false;
                        // Create a partial message object for the message clone
                        this.newTaskFeed.photoURL = fullPath;
                        // Create a new message object from the default message object with an override of the text message
                        // const helper: AddMessageHelper = {
                        //     message: this.newTaskFeed,
                        //     taskID: this.newTaskFeed.taskID
                        // };
                        // this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                        // // Create a partial message object for the message clone
                        // this._finishImageSend(true);
                        resolve(this._uploadTask.snapshot.downloadURL);
                        // Update the photoURL path
                    }
                );
            }
        });
    }

    public async _uploadMultipleFilesTask(task: ITask): Promise<void> {
        const promises: any[] = [];
        let count: number = (this._taskImageDataArr.length - 1);
        this._taskImageDataArr.forEach(file => {
            this.IsUploading = true;
            let fileName: string = (null != this.sessionData.user) ? this.sessionData.user.uid + StringUtil.GenerateUUID() + Date.now() : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.sessionData.user) ? this.sessionData.user.uid : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${task.orgID}/visits/${task.buildingID}/${taskID}/${fileName}.png`;
            // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            let uploadTask: storage.UploadTask = this.firebaseStorage.Upload(fullPath, file, format);
            // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, (snapshot: any) => {
               let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (error: any) => {
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;
                    }

                    case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;
                    }

                    default: {
                        break;
                    }
                }
                this._controls.ShowAlert(error.name, message, "Ok");
                
            },
            () => {
                this._taskUploadUrls.push(uploadTask.snapshot.downloadURL);
                if (null == task.photoGallery) {
                    task.photoGallery = [];
                }
                task.photoGallery.push(uploadTask.snapshot.downloadURL);
                // Update UI with finishing and remove cancel button
                // console.log("uploadUrls", task.photoGallery);
                count = count - 1;
                // Create a partial message object for the message clone
                // // Create a partial message object for the message clone
                // Update the photoURL path
            });

         });
         Promise.all(promises)
            .then(() => {
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done", task.photoGallery);
                let action: string = Constants.DIALOG_RESPONSE_ACTION_ADD;
                if (!this.isNewTask) {
                    action = Constants.DIALOG_RESPONSE_ACTION_UPDATE;
                } else {
                    // Create a IUser object and add it to the dialogResult
                }
                let dialogResponse: ITaskDialogResponse = {
                    task: task,
                    action: action,
                };
                this._handleTaskDetailAction(dialogResponse)
                this._dismissDialog();
                // Clear the text message field
                this.newTaskFeed.message = "";
                this._taskUploadPreviews = [];
                this._taskImageDataArr = [];
                this._taskUploadUrls = [];
                this._finishImageSend(true);
            })
            .catch(err => console.log(err.code));
    }

    public async _uploadMultipleFiles(): Promise<void> {
        const promises: any[] = [];
        let count: number = (this._imageDataArr.length - 1);
        this._imageDataArr.forEach(file => {
            this.IsUploading = true;
            let fileName: string = (null != this.sessionData.user) ? this.sessionData.user.uid + StringUtil.GenerateUUID() + Date.now() : StringUtil.GenerateUUID() + Date.now();
            let taskID: string = (null != this.sessionData.user) ? this.sessionData.user.uid : StringUtil.GenerateUUID() + Date.now();
            let fullPath: string = `/orgs/${this.task.orgID}/visits/${this.task.buildingID}/${taskID}/${fileName}.png`;
            // console.log("fullPath", fullPath);
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            let uploadTask: storage.UploadTask = this.firebaseStorage.Upload(fullPath, file, format);
            // console.log("uploadTask", uploadTask);
            promises.push(uploadTask);
            uploadTask.on(storage.TaskEvent.STATE_CHANGED, (snapshot: any) => {
               let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (error: any) => {
                let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                switch (error.name) {
                    case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                        // User doesn't have permission to access the object
                        message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                        break;
                    }

                    case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                        // User canceled the upload
                        message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                        break;
                    }

                    default: {
                        break;
                    }
                }
                this._controls.ShowAlert(error.name, message, "Ok");
                
            },
            () => {
                this._uploadUrls.push(uploadTask.snapshot.downloadURL);
                this.newTaskFeed.photoGallery.push(uploadTask.snapshot.downloadURL);
                // Update UI with finishing and remove cancel button
                // console.log("uploadUrls", this.newTaskFeed.photoGallery);
                count = count - 1;
                // Create a partial message object for the message clone
                // // Create a partial message object for the message clone
                // Update the photoURL path
            });

         });
         Promise.all(promises)
            .then(() => {
                this.IsUploadDone = true;
                this.CanCancelUpload = false;
                this._photoModified = false;
                this._isUploading = false;
                // console.log("Upload done", this.newTaskFeed.photoGallery);
                const helper: AddMessageHelper = {
                    message: this.newTaskFeed,
                    taskID: this.newTaskFeed.taskID,
                };
                this._store.dispatch(this._sessionActions.AddTaskMessage(helper));
                // Clear the text message field
                this.newTaskFeed.message = "";
                this._uploadPreviews = [];
                this._imageDataArr = [];
                this._finishImageSend(true);
            })
            .catch(err => console.log(err.code));
    }

    private _finishImageSend(isDone: boolean): void {
        // Update UI with done
        this.UploadStatus = isDone ? "Done!" : "Canceled!";
        // keep the done UI for 1 second
        setTimeout(() => {
            // Hides all upload UI elements
            // Re-enables the image upload button
            this.IsUploading = false;
            // Reset the image done flag and status
            this.IsUploadDone = false;
            this.UploadStatus = Constants.STR_FINISHING;
            // Resets the progress bar
            this.ProgressPercent = 0;
            // Reset upload preview
            this.UploadPreview = null;
            // Clear upload task
            this._uploadTask = null;
            this._photoModified = false;
        }, 1000);
    }

    private _getDay(day: number): string {
        switch (day) {
            case 0:
            {
                return 'Sunday';
            }

            case 1:
            {
                return 'Monday';
            }

            case 2:
            {
                return 'Tuesday';
            }

            case 3:
            {
                return 'Wednesday';
            }

            case 4:
            {
                return 'Thursday';
            }

            case 5:
            {
                return 'Friday';
            }

            case 6:
            {
                return 'Saturday';
            }
            
            default:
            {
                break;
            }
        }
    }

    private _getMonthDayYear(curDate: Date): string {
        let newDate: string = "";
        newDate = `${this.monthNames[curDate.getMonth() + 1]} ${curDate.getDate()}${ this._nth(curDate.getDate()) }, ${curDate.getFullYear()}`;
        return newDate;
    }

    private _nth(d: number): string {
      if (d > 3 && d < 21) {
          return 'th';
      } else {
          // console.log("_nth", d, d % 10);
          switch (d % 10) {
            case 1:
            {
                return "st";
            }

            case 2:
            {
                return "nd";
            }

            case 3:
            {
                return "rd";
            }

            default:
            {
                return "th";
            }
          }
      }
    }
}

