// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, Input, trigger, state, animate, transition, style, AUTO_STYLE } from '@angular/core';

// Local modules
import { Constants } from '../../common/Constants';

@Component({
    selector: 'collapsible-control',
    template: `
    <div class="collapsible-header" fxLayout="row" (click)="changeState()">
    <ng-content select=".collapsible-control-header"></ng-content>
    <md-icon class="margin-vcenter" [@iconChange]="collapseState">keyboard_arrow_down</md-icon>
    </div>
    <div class="collapsible-content" [@collapseChange]="collapseState">
    <ng-content select=".collapsible-control-content"></ng-content>
    </div>
    `,
    animations: [
    trigger('collapseChange', [
        state(Constants.STR_ZERO,
            style({ height: '0', display: 'none' }),
            ),
        state(Constants.STR_ONE,
            style({ height: AUTO_STYLE, display: AUTO_STYLE, })
            ),
        transition(Constants.STR_ONE + ' => ' + Constants.STR_ZERO, [
            style({ overflow: 'hidden' }),
            animate('.25s ease-in', style({ height: '0' })),
            ]),
        transition(Constants.STR_ZERO + ' => ' + Constants.STR_ONE, [
            style({ overflow: 'hidden' }),
            animate('.25s ease-out', style({ height: AUTO_STYLE })),
            ])
        ]),
    trigger('iconChange', [
        state(Constants.STR_ONE,
            style({ transform: 'rotate( -180deg )' })
            ),
        state(Constants.STR_ZERO,
            style({ transform: 'rotate( 0deg )' })
            ),
        transition('* => *', animate('.25s'))
        ])
    ]
})
export class CollapsibleControlComponent {
    @Input()
    collapseState: string = Constants.STR_ZERO;

    changeState(): void {
        if (this.collapseState === Constants.STR_ZERO) {
            this.collapseState = Constants.STR_ONE;
        } else {
            this.collapseState = Constants.STR_ZERO;
        }
    }
}
