// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Injectable, Component, OnInit, OnChanges, OnDestroy, ViewChild, Output, Optional, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { ICard, customers, IShippingInformation } from "stripe";
import { Router, NavigationExtras, ActivatedRoute, Data } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from "@angular/forms";
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay,
    CalendarWeekViewEvent
} from "angular-calendar";
import {
    getMonth,
    startOfMonth,
    startOfWeek,
    startOfDay,
    endOfWeek,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours } from "date-fns";
import RRule from 'rrule';
// import { NgxSmartModalService, NgxSmartModalConfig } from "ngx-smart-modal";
import { Subject } from "rxjs/Subject";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { MdDialog, MdSnackBar, MdDialogConfig, MdSnackBarConfig } from "@angular/material";
import { ViewPeriod } from 'calendar-utils';

// Local modules
import { AppState } from "../app/app.state";
import { Constants, Urls, colors } from "../common/Constants";
import { IError } from "../interfaces/IError";
import {
    IUser,
    BeforeCalendarLoadMonth,
    BeforeCalendarLoadWeek,
    RecurringEvent,
    IOrgBasic,
    ITask
} from "../shared/SilverBrickTypes";
import { IUserState, UserStateType, IOrgState, OrgStateType, SessionStateType, ISessionState } from "../interfaces/IStoreState";
import { SessionActions } from "../store/actions/SessionActions";
import { UserActions } from "../store/actions/UserActions";
import { ITaskFeedJoin } from "../interfaces/IResponse";

import { OrgActions } from "../store/actions/OrgActions";
import { UserUtil } from "../common/UserUtil";
import { IPaginateQueryStrings, IUserDialogResponse, IGetBldgHelper, ITaskDialogResponse } from "../interfaces/IParams";
// import { UserDetailComponent } from '../components/user-details/user-details.component';
import { RecurringTaskDetailsComponent } from '../components/recurring-task-details/recurring-task-details.component';
import { SessionData } from "../providers/SessionData";
import { FirebaseAuth } from '../services/FirebaseAuth';
import { Controls } from '../providers/Controls';

// The purpose of this file is to have a shared subscription to the session state that will
// allow for easy access to important properties that a user will need throughout their time in the app.
// These properties are typically static in nature.



@Injectable()
export class CalendarProvider {
    private _sessionStateSubscription: Subscription;
    private _commStateSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _usersSubscription: Subscription;
    private _screenSize: number = window.screen.width;

    public UserState$: Observable<IUserState>;
    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public tasks$: Observable<Array<ITask>>;
    public SessionState$: Observable<ISessionState>;
    public OrgState$: Observable<IOrgState>;
    public ListView: boolean = false;
    public CloseButton: string = 'Will Do';
    public ModalTitle: string = 'Setup Payment Info?';
    public ModalMessage: string = '';
    public view: string = "month";
    public SelectedDate: Date = null;
    public profile$: Observable<IUser>;
    public DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)'];
    public FrequencyOptions: string[] = ['Daily', 'Weekly', 'Monthly', 'Yearly'];
    public actions: CalendarEventAction[] = [
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
        }
    },
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
            this.allEvents = this.allEvents.filter(iEvent => iEvent !== event);
            this.handleEvent("Deleted", event);
        }
    }
    ];
    public recurringEvents: RecurringEvent[] = [
        {
            title: 'Trash (68 4th Place)',
            color: colors.yellow,
            rrule: {
                freq: RRule.MONTHLY,
                bymonthday: 5,
            },
            start: new Date(),
            actions: this.actions,
            resizable: {
                beforeStart: true,
                afterEnd: true
            },
            displayTitle: 'Trash (68 4th Place)'
        }
    ];

    public viewDate: Date = new Date();

    public modalData: {
        action: string;
        event: CalendarEvent;
    };

    

    public eventActions: CalendarEventAction[] = [
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
        }
    },
    {
        label: '<i style="color: #fff !important;" class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
            this.allEvents = this.allEvents.filter(iEvent => iEvent !== event);
            this.handleEvent("Deleted", event);
        }
    }
    ];

    public refresh: Subject<any> = new Subject();

    public allEvents: CalendarEvent[] = [];
    public activeDayIsOpen: boolean = false;
    public isNewTask: boolean = false;

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    constructor(
        public controls: Controls,
        public orgActions: OrgActions,
        public sessionData: SessionData,
        public userActions: UserActions,
        public modal: NgbModal,
        public firebaseAuth: FirebaseAuth,
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        public store: Store<AppState>,
        @Optional() private cdr: ChangeDetectorRef,
        public sessionActions: SessionActions) {
        this.error$ = this.store.select(state => state.usersState.error);
        this.loading$ = this.store.select(state => state.sessionState.loading);
        this.OrgState$ = this.store.select(state => state.orgState);
        this.SessionState$ = this.store.select(state => state.sessionState);
        this.tasks$ = this.store.select(state => state.sessionState.tasks);
        this.UserState$ = this.store.select(state => state.usersState);
        this.profile$ = this.store.select(state => state.authState.profile);
    }

    public Initialize(): void {
        // if ((null != this.sessionData.user)) {
        //     if (!this.sessionData.user.isAdmin) {
        //         if (this.sessionData.user.role === "Tenant") {
        //         } else {

        //             let bldgHelper: IGetBldgHelper = {
        //                 orgID: this.sessionData.org.id,
        //                 buildingID: this.sessionData.org.buildings[0].id
        //             };
        //             console.log("bldgHelper", bldgHelper);
        //             this.store.dispatch(this.sessionActions.GetRecurringStaffTasks(this.sessionData.user.uid));

        //         }
        //     } else {
        //         console.log("GetAllHistory");
        //         this.store.dispatch(this.sessionActions.GetRecurringTasks());
        //     }
        // }

        // this._sessionStateSubscription = this.SessionState$.subscribe((state: ISessionState) => {
        //     if (null != state) {
        //         switch (state.type) {
        //             case SessionStateType.GET_RECURRING_TASKS_SUCCEEDED:
        //             {
        //                 this.recurringEvents = [];
        //                 console.log("SessionStateType", SessionStateType[state.type]);
        //                 state.recurringTasks.forEach((task: ITask) => {
        //                     let newEvent: RecurringEvent = {
        //                         start: new Date(task.startDate),
        //                         title: `${ task.subject }`,
        //                         actions: this.actions,
        //                         color: colors.blue,
        //                         resizable: {
        //                             beforeStart: true,
        //                             afterEnd: true
        //                         },
        //                         meta: `${ task.subject }`,
        //                         task: task,
        //                         displayTitle: `${ task.subject }`
        //                     };
        //                     switch (task.durationAmount) {
        //                         case this.DurationOptions[0]:
        //                         {
        //                             let endDate: Date = new Date(task.startDate);
        //                             endDate.setDate(endDate.getDate() + task.durationQuantity);
        //                             newEvent.end = endDate;
        //                             console.log("Daily", endDate);
        //                             break;
        //                         }

        //                         case this.DurationOptions[1]:
        //                         {
        //                             let endDate: Date = new Date(task.startDate);
        //                             endDate.setDate(endDate.getDate() + (task.durationQuantity * 7));
        //                             newEvent.end = endDate;
        //                             console.log("Weekly", endDate);
        //                             break;
        //                         }

        //                         case this.DurationOptions[2]:
        //                         {
        //                             let endDate: Date = new Date(task.startDate);
        //                             endDate.setMonth(endDate.getMonth() + 1 + task.durationQuantity);
        //                             newEvent.end = endDate;
        //                             console.log("Monthly", endDate);
        //                             break;
        //                         }

        //                         case this.DurationOptions[3]:
        //                         {
        //                             let endDate: Date = new Date(task.startDate);
        //                             endDate.setFullYear(endDate.getFullYear() + task.durationQuantity);
        //                             newEvent.end = endDate;
        //                             console.log("Yearly", endDate);
        //                             break;
        //                         }
                                
        //                         default:
        //                         {
        //                             break;
        //                         }
        //                     }
        //                     switch (task.repeatInterval) {
        //                         case "Daily":
        //                         {
        //                             newEvent.rrule = {
        //                                 freq: RRule.DAILY
        //                             };
        //                             console.log("Daily", newEvent.rrule);
        //                             break;
        //                         }

        //                         case "Weekly":
        //                         {
        //                             newEvent.rrule = {
        //                                 freq: RRule.WEEKLY,
        //                                 byweekday: task.monthlyWeekDays
        //                             };
        //                             console.log("Weekly", newEvent.rrule);
        //                             break;
        //                         }

        //                         case "Monthly":
        //                         {
        //                             newEvent.rrule = {
        //                                 freq: RRule.MONTHLY,
        //                                 bymonthday: task.monthlyDays
        //                             };
        //                             console.log("Monthly", newEvent.rrule);
        //                             break;
        //                         }

        //                         case "Yearly":
        //                         {
        //                             newEvent.rrule = {
        //                                 freq: RRule.YEARLY,
        //                                 bymonthday: task.monthlyDays,
        //                                 bymonth: new Date(task.startDate).getMonth()
        //                             };
        //                             console.log("Yearly", newEvent.rrule);
        //                             break;
        //                         }
                                
        //                         default:
        //                         {
        //                             break;
        //                         }
        //                     }
        //                     this.recurringEvents.push(newEvent);
        //                 });
        //                 break;
        //             }
                    
        //             default:
        //             {
        //                 break;
        //             }
        //         }
        //     }
        // },
        // (error: any) => {
        //     console.log("Error - " + error);
        // });

        // this._userStateSubscription = this.UserState$
        // .subscribe((state: IUserState) => {
        //     if (null != state) {
        //         switch (state.type) {


        //             default:
        //             {
        //                 break;
        //             }
        //         }
        //     }
        // });
    }

    public Unitialize(): void {
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }

        if (null != this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public dayClicked({ date, allEvents }: { date: Date; allEvents: CalendarEvent[] }): void {
        if (this.isNewTask) {
            if (isSameMonth(date, this.viewDate)) {
                if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true)) {
                    console.log("isSame", date);
                    this.RecurringTask(null, date);
                    this.activeDayIsOpen = false;
                } else {
                    this.activeDayIsOpen = true;
                    this.viewDate = date;
                }
            }
        } else {
            this.viewDate = date;
        }
    }

    public RecurringTask(task: ITask = null, date: Date = null): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(RecurringTaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this.store.dispatch(this.sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }
        if (date) {
            dialogRef.componentInstance.SelectedDate = date;
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public eventTimesChanged({ event, newStart, newEnd, oldStart }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.handleEvent("Dropped or resized", event);
        this.refresh.next();
    }

    public handleEvent(action: string, event: CalendarEvent): void {
        window.location.href = event.meta;
        // this.modalData = { event, action };
        // this.modal.open(this.modalContent, { size: 'lg' });
    }

    public addEvent(): void {
        this.allEvents.push({
            title: "New event",
            start: startOfDay(new Date()),
            end: endOfDay(new Date()),
            color: colors.red,
            draggable: true,
            resizable: {
                beforeStart: true,
                afterEnd: true
            }
        });
        this.refresh.next();
    }

    public ToggleView(): void {
        this.ListView = !this.ListView;
    }

    handleTaskDetailResponse(response: ITaskDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                if ((null != this.sessionData.user)) {
                    if (!this.sessionData.user.isAdmin) {
                        if (this.sessionData.user.role === "Tenant") {
                        } else {

                            let bldgHelper: IGetBldgHelper = {
                                orgID: this.sessionData.org.id,
                                buildingID: this.sessionData.org.buildings[0].id
                            };
                            console.log("bldgHelper", bldgHelper);
                            this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        }
                    } else {
                        console.log("GetAllHistory");
                        this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    }
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000 });
                if ((null != this.sessionData.user)) {
                    if (!this.sessionData.user.isAdmin) {
                        if (this.sessionData.user.role === "Tenant") {
                        } else {

                            let bldgHelper: IGetBldgHelper = {
                                orgID: this.sessionData.org.id,
                                buildingID: this.sessionData.org.buildings[0].id
                            };
                            console.log("bldgHelper", bldgHelper);
                            this.store.dispatch(this.sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        }
                    } else {
                        console.log("GetAllHistory");
                        this.store.dispatch(this.sessionActions.GetRecurringTasks());
                    }
                }
                break;
            }

            default:
            {
                break;
            }
        }
    }



    public updateCalendarEvents($event: BeforeCalendarLoadMonth | BeforeCalendarLoadWeek = null): void {
        console.log("updateCalendarEvents", $event);
        this.allEvents = [];
        const startOfPeriod: any = {
            month: startOfMonth,
            week: startOfWeek,
            day: startOfDay
        };

        const endOfPeriod: any = {
            month: endOfMonth,
            week: endOfWeek,
            day: endOfDay
        };

        // switch (this.view) {
        //     case "month":
        //     {   
        //         let event: BeforeCalendarLoadMonth = <BeforeCalendarLoadMonth>$event; 
        //         startOfPeriod.month = startOfMonth(event.body[0].date);
        //         endOfPeriod.month = startOfMonth(event.header.pop().date);
        //         break;
        //     }

        //     case "week":
        //     {   
        //         let event: BeforeCalendarLoadWeek = <BeforeCalendarLoadMonth>$event; 
        //         startOfPeriod.month = startOfMonth(event.body[0].date);
        //         endOfPeriod.month = startOfMonth(event.body.pop().date);
        //         break;
        //     }
            
        //     default:
        //         // code...
        //         break;
        // }
        let date = this.viewDate;
        let endDate = this.viewDate;
        if (null != $event) {
            date = $event.body[0].date;
            endDate = $event.body.pop().date;
        }
        for (let event of this.recurringEvents) {
            const rule: RRule = new RRule(
                Object.assign({}, event.rrule, {
                    dtstart: startOfPeriod[this.view](date),
                    until: endOfPeriod[this.view](endDate)
                })
                );

            for (let date of rule.all()) {
                this.allEvents.push(
                    Object.assign({}, event, {
                        start: new Date(date)
                    })
                    );
                // console.log("this.allEvents", rule, event);
            }
        };
        this.cdr.detectChanges();
    }
}