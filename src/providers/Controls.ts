// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { MdDialogRef, MdDialog, MdDialogConfig } from "@angular/material";
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";

// Local modules
import { IUserDialogBase, IInputDialogBase } from "../interfaces/IParams";
import { ConfirmDialogComponent } from "../components/confirm-dialog/confirm-dialog.component";
import { Constants } from "../common/Constants";

@Injectable()
export class Controls {
    public loadingTimeoutHandler: number;
    constructor(public dialog: MdDialog) {}

    public ShowDialog(title: string, message: string, ok: string, cancel: string): Observable<IUserDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = title;
        dialogRef.componentInstance.Message = message;
        dialogRef.componentInstance.OkString = ok;
        dialogRef.componentInstance.CancelString = cancel;

        return dialogRef.afterClosed();
    }

    public ShowAlert(title: string, message: string, ok: string): Observable<IUserDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.height = '40%';
        dialogConfig.position = {
            top: '10%'
        };

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = title;
        dialogRef.componentInstance.Message = message;
        dialogRef.componentInstance.OkString = ok;

        return dialogRef.afterClosed();
    }

    public ShowLeaveDialog(): Observable<IUserDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = Constants.UNSAVED_CHANGES_TITLE;
        dialogRef.componentInstance.Message = Constants.UNSAVED_CHANGES_MSG;
        dialogRef.componentInstance.OkString = Constants.UNSAVED_CHANGES_OK;
        dialogRef.componentInstance.CancelString = Constants.UNSAVED_CHANGES_CANCEL;

        return dialogRef.afterClosed();
    }

    public ShowAssignDialog(name: string): Observable<IInputDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = "Assign Request?";
        dialogRef.componentInstance.IsAssigning = true;
        dialogRef.componentInstance.Message = "Confirm you would like to assign this request to " + name;
        dialogRef.componentInstance.OkString = "Assign Request";
        dialogRef.componentInstance.CancelString = "Cancel";

        return dialogRef.afterClosed();
    }

    public ShowChangeDateDialog(date: Date = null): Observable<IInputDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = "Change Date?";
        if (null != date) {
            dialogRef.componentInstance.SelectedDate = date;
        }
        dialogRef.componentInstance.IsRescheduling = true;
        dialogRef.componentInstance.Message = "Please confirm you are changing the date for just this occurance";
        dialogRef.componentInstance.OkString = "Confirm";
        dialogRef.componentInstance.CancelString = "Cancel";
        dialogRef.componentInstance.SecondOkString = "All";

        return dialogRef.afterClosed();
    }

    public ShowAssignRecurringDialog(name: string): Observable<IInputDialogBase> {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = "Re-Assign this task";
        dialogRef.componentInstance.IsInputting = true;
        dialogRef.componentInstance.Message = "Re-assign this task to " + name + "just once or all future events?";
        dialogRef.componentInstance.OkString = "All Events";
        dialogRef.componentInstance.CancelString = "Once";

        return dialogRef.afterClosed();
    }

    private _clearLoadingTimeout(): void {
        if (this.loadingTimeoutHandler) {
            window.clearTimeout(this.loadingTimeoutHandler);
            this.loadingTimeoutHandler = null;
        } else {
            // Loading timeout already cleared
        }
    }
}

