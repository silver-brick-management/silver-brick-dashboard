// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { IConfigContext } from '../interfaces/IConfigContext';

export const environment = {
    production: true
};

export class ConfigContext {
    static readonly FirebaseAPIKey: string = "AIzaSyD0C_SCgt4kdYqR31VCcCJXrkLbDcck28g";

    static readonly FirebaseAuthDomain: string = "silver-brick.firebaseapp.com";

    static readonly FirebaseDatabaseURL: string = "https://silver-brick-default-rtdb.firebaseio.com";

    static readonly FirebaseStorageBucket: string = "silver-brick.appspot.com";

    static readonly FirebaseMessengerSenderID: string = "766315901859";
    // static readonly SilverBrickApiURL: string = "http://localhost:3000/";
    static readonly SilverBrickApiURL: string = "https://api.silverbrickmanagement.com:3000/";
}

