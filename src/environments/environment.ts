// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

import { IConfigContext } from '../interfaces/IConfigContext';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: true
};

export class ConfigContext {
    static readonly FirebaseAPIKey: string = "AIzaSyD0C_SCgt4kdYqR31VCcCJXrkLbDcck28g";

    static readonly FirebaseAuthDomain: string = "silver-brick.firebaseapp.com";

    static readonly FirebaseDatabaseURL: string = "https://silver-brick-default-rtdb.firebaseio.com";

    static readonly FirebaseStorageBucket: string = "silver-brick.appspot.com";

    static readonly FirebaseMessengerSenderID: string = "766315901859";
    // static readonly SilverBrickApiURL: string = "http://localhost:3000/";
    static readonly SilverBrickApiURL: string = "https://api.silverbrickmanagement.com:3000/";
}

