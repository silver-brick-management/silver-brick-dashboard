
import { IBuildingBasic, IBuildingSimple, IOrgBasic } from './SilverBrickTypes';

export class OrgBldgUtil {
	static Transform(value: any): IBuildingSimple[] {
        let keys = Object.keys(value);
        let bldgArray: IBuildingSimple[] = [];
       // let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        keys.forEach((key: string) => {
        	let newBldg: IBuildingSimple = {
        		id: key,
        		name: value[key]
        	};
        	bldgArray.push(newBldg);
        	console.log("bldgArray", bldgArray, newBldg);
        });

        return bldgArray;
    }

    static SortByName(orgs: IOrgBasic[]): IOrgBasic[] {
        let newUsers: IOrgBasic[] = orgs.sort((user1, user2) => {
            if (`${user1.name}` < `${user2.name}`) {
                return -1;
            }

            if (`${user1.name}` > `${user2.name}`) {
                return 1;
            }

            return 0;
        });
        return newUsers;
    }

    static TransformBasic(value: any): IBuildingBasic[] {
        if (!value || (null == value)) {
            const newBldg: IBuildingBasic[] = [];
            return newBldg;
        }
        let keys = Object.keys(value);
        let newKeys: IBuildingBasic[] = keys.map(k => value[k]);
        
        return newKeys;
    }
}