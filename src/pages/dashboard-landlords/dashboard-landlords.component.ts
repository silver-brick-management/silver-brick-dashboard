// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
// import { IgxExcelExporterService, IgxExcelExporterOptions } from "igniteui-angular";

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, Urls } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IUser, IBuildingSimple, IUserOrg, IOrgBasic } from '../../shared/SilverBrickTypes';
import { IUserState, IAuthState, AuthStateType, UserStateType } from '../../interfaces/IStoreState';
import { UserActions } from '../../store/actions/UserActions';
import { SessionActions } from '../../store/actions/SessionActions';
import { UserUtil } from '../../common/UserUtil';
import { IPaginateQueryStrings, IUserDialogResponse, IGetBldgHelper } from '../../interfaces/IParams';
import { SessionData } from '../../providers/SessionData';
import { LandlordDetailComponent } from '../../components/landlord-details/landlord-details.component';
import { OrgActions } from "../../store/actions/OrgActions";

@Component({
    selector: 'app-dashboard-landlords',
    templateUrl: './dashboard-landlords.component.html',
    styleUrls: ['./dashboard-landlords.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardLandlordsComponent implements OnInit {
    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public limit$: Observable<string>;
    public next$: Observable<string>;
    public previous$: Observable<string>;
    public loading$: Observable<boolean>;
    public users$: Observable<Array<IUser>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public userState$: Observable<IUserState>;
    private _authSubscription: Subscription;
    private _userStateSubscription: Subscription;
    public authState$: Observable<IAuthState>;
    public orgID: string = '';
    public buildingID: string = '';
    private _firstLoad: boolean = true;
    private _screenSize: number = window.screen.width;
    
    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _orgActions: OrgActions,
        private _sessionActions: SessionActions,
        public sessionData: SessionData,
        private _store: Store<AppState>,
        private _userActions: UserActions) {
        this.AllBuildings$ = this._store.select((state) => state.orgState.allBuildings);
        this.users$ = this._store.select(state => state.usersState.allLandlords);
        this.userState$ = this._store.select(state => state.usersState);
        this.error$ = this._store.select(state => state.usersState.error);
        this.loading$ = this._store.select(state => state.usersState.loading);
        this.authState$ = this._store.select(state => state.authState);
    }

    ngOnInit(): void {
        this._authSubscription = this.authState$
        .subscribe((state: IAuthState) => {
            if (null != state) {
                if (null != state.profile) {
                    this.orgID = this.sessionData.org.id;
                    this.buildingID = this.sessionData.org.buildings[0].id;
                    let bldgHelper: IGetBldgHelper = {
                        orgID: this.sessionData.org.id,
                        buildingID: this.sessionData.org.buildings[0].id
                    };
                    console.log("bldgHelper", bldgHelper);
                    this._store.dispatch(this._userActions.GetAllLandlords());
                }
            }
        });
        this._userStateSubscription = this.userState$
        .subscribe((state: IUserState) => {
            if (null != state) {
                switch (state.type) {
                    case UserStateType.SEND_USER_INVITE_SUCCEEDED:
                    {
                        if (null != state.invitedUser) {
                            const msg: string = `${state.invitedUser.firstName} has been sent an invite`;
                            this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackbar-class'] });
                        }
                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        });
    }

    ngOnDestroy(): void {
        if (this._authSubscription) {
            this._authSubscription.unsubscribe();
            this._authSubscription = null;
        }

        if (this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public SendInvite(user: IUser): void {
        this._store.dispatch(this._userActions.SendUserInvite(user));
    }

    public ScopeTenants(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: ""
        };
        this._store.dispatch(this._userActions.ScopeBuildingLandlords(helper));
    }

    public RefreshTenants(): void {
        if (!this.sessionData.user.isAdmin) {
            if (this.sessionData.user.role === "Tenant") {
            } else {
                this.orgID = this.sessionData.org.id;
                this.buildingID = this.sessionData.org.buildings[0].id;
                let bldgHelper: IGetBldgHelper = {
                    orgID: this.sessionData.org.id,
                    buildingID: this.sessionData.org.buildings[0].id
                };
                console.log("bldgHelper", bldgHelper);
            }
        } else {
            console.log("GetAllHistory");
            this._store.dispatch(this._orgActions.GetAllOrgsScope());
            this._store.dispatch(this._userActions.GetAllLandlords());
        }
    }

    public sortTable(): void {
        console.log("sortTable");
        this._store.dispatch(this._userActions.SortByName());
    }

    public GetName(user: IUser): string {
        return `${user.firstName} ${ user.lastName }`;
    }


    /* View Methods */

    GetBuilding(orgs: IUserOrg): string {
        if ((null != orgs.buildings) && (orgs.buildings.length > 0)) {
            return orgs.buildings[0].name;
        } else {
            return 'N/A';
        }
    }

    expandUser(user?: IUser): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '1000px';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(LandlordDetailComponent, dialogConfig);
        if (user) {
            dialogRef.componentInstance.user = user;
            dialogRef.componentInstance.isBuildingUserView = false;
        } else {
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
           // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IUserDialogResponse) => {
            if (response) {
                this.handleUserDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    searchUidOrEmail(): void {
        if (this.uidOrEmail.trim()) {
            this._store.dispatch(this._userActions.GetUser(this.uidOrEmail));
        } else {
            // No string detected
            // Do nothing
        }
    }

    onLimitChange(value: string): void {
        const paginateQueryStrings: IPaginateQueryStrings = {
            limit: value
        };
        this._store.dispatch(this._userActions.GetUsers(paginateQueryStrings));
    }

    getMoreUsersNext(): void {
        this.next$.first().subscribe((url) => {
            if (url) {
                this._store.dispatch(this._userActions.GetUsersFromFullUrl(url));
            } else {
                // No next URL
                // Do nothing
            }
        });
    }

    getMoreUsersPrev(): void {
        this.previous$.first().subscribe((url) => {
            if (url) {
                this._store.dispatch(this._userActions.GetUsersFromFullUrl(url));
            } else {
                // No previous URL
                // Do nothing
            }
        });
    }

    handleUserDetailResponse(response: IUserDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            case Constants.DIALOG_RESPONSE_ACTION_ACTIVATE:
            case Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE:
            {
                setTimeout(() => {
                    const msg: string = `${response.user.firstName} ${response.action}`;
                    this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                    this._store.dispatch(this._userActions.GetAllLandlords());
                }, 1000);
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                setTimeout(() => {
                    const msg: string = `${response.user.firstName} ${response.action}`;
                    this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                    let bldgHelper: IGetBldgHelper = {
                        orgID: this.sessionData.org.id,
                        buildingID: this.sessionData.org.buildings[0].id
                    };
                    console.log("bldgHelper", bldgHelper);
                    this._store.dispatch(this._userActions.GetAllLandlords());
                }, 1000);
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public IsBdlgAdmin(user: IUser): boolean {
        if (null != user.orgs) {
            return user.orgs.buildings[0].isAdmin;
        } else {
            return false;
        }
    }
}
