import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardLandlordsComponent } from './dashboard-landlords.component';

describe('DashboardLandlordsComponent', () => {
  let component: DashboardLandlordsComponent;
  let fixture: ComponentFixture<DashboardLandlordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardLandlordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardLandlordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
