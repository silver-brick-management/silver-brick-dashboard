// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************
// Node modules
import { storage, database } from "firebase";
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialog, MdSnackBar, MdDialogConfig } from "@angular/material";
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import Viewer from "viewerjs/dist/viewer.esm";
import "viewerjs/dist/viewer.min.css";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IMessage, IUser, IAssignee } from "../../shared/SilverBrickTypes";
import { IMessageState, MessageStateType } from "../../interfaces/IStoreState";
import { MessageActions, MessageActionTypes } from "../../store/actions/MessageActions";
import { SessionData } from "../../providers/SessionData";

import { IFormSelctedValue, IUserDialogBase, IAudienceDialogResponse, IOrgDialogResponse, IBldgDialogResponse, IGetBldgHelper } from "../../interfaces/IParams";
import { CollapsibleControlComponent } from "../../components/collapsible-control/collapsible-control.component";
import { OrgBldgUtil } from "../../shared/OrgBldgUtil";
import { Controls } from "../../providers/Controls";
import { UserChatRoom, AddMessageHelper, NewChatRoomHelper, MessageParticipant } from "../../interfaces/IResponse";
import { FirebaseStorage } from "../../services/FirebaseStorage";
import { StringUtil } from "../../common/StringUtil";
import { AudienceChooserComponent } from '../../components/audience-chooser/audience-chooser.component';
import { UserActions } from "../../store/actions/UserActions";

export interface IPartialChatMessage {
    text: string;
    type?: string;
}

@Component({
    selector: "app-dashboard-chat",
    templateUrl: "./dashboard-chat.component.html",
    styleUrls: ["./dashboard-chat.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardChatComponent implements OnInit {
    private _textMessage: string;
    private _screenSize: number = window.screen.width;
    private _isUploading: boolean;
    private _uploadStatus: string;
    private _isUploadDone: boolean;
    private _uploadPreview: string;
    private _progressPercent: number;
    private _canCancelUpload: boolean;
    private _hideScrollDownButtom: boolean;
    private _uploadTask: storage.UploadTask;
    private _defaultMessage: IMessage;
    private _participantIDs: string[] = [];
    private _messageParticipants: MessageParticipant[] = [];
    private _messageStateSubscription: Subscription;
    private _imageData: File;
    private _photoModified: boolean;

    public selectedStaff: IAssignee[] = [];
    public ChatRooms$: Observable<UserChatRoom[]>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public SocketReconnected: boolean = false;
    public Messages$: Observable<IMessage[]>;
    public Error: IError;
    public IsNewChatRoom: boolean = false;
    public IsLoading: boolean;
    public HasMoreMessages: boolean;
    public RoomID: string = "";

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsUploading(): boolean {
        return this._isUploading;
    }

    set IsUploading(isUploading: boolean) {
        this._isUploading = isUploading;
    }

    get UploadStatus(): string {
        return this._uploadStatus;
    }

    set UploadStatus(uploadStatus: string) {
        this._uploadStatus = uploadStatus;
    }

    get IsUploadDone(): boolean {
        return this._isUploadDone;
    }

    set IsUploadDone(isUploadDone: boolean) {
        this._isUploadDone = isUploadDone;
    }

    get UploadPreview(): string {
        return this._uploadPreview;
    }

    set UploadPreview(uploadPreview: string) {
        this._uploadPreview = uploadPreview;
    }

    get ProgressPercent(): number {
        return this._progressPercent;
    }

    set ProgressPercent(progressPercent: number) {
        this._progressPercent = progressPercent;
    }

    get TextMessage(): string {
        return this._textMessage;
    }

    set TextMessage(message: string) {
        this._textMessage = message;
    }

    get CanCancelUpload(): boolean {
        return this._canCancelUpload;
    }

    set CanCancelUpload(canCancelUpload: boolean) {
        this._canCancelUpload = canCancelUpload;
    }

    get HideScrollDownButtom(): boolean {
        return this._hideScrollDownButtom;
    }

    set HideScrollDownButtom(hideScrollDownButtom: boolean) {
        this._hideScrollDownButtom = hideScrollDownButtom;
    }

    get MessageParticipants(): MessageParticipant[] {
        return this._messageParticipants;
    }

    set MessageParticipants(users: MessageParticipant[]) {
        this._messageParticipants = users;
    }

    constructor(
        private _dialog: MdDialog,
        private _snackBar: MdSnackBar,
        public store: Store<AppState>,
        public controls: Controls,
        public firebaseStorage: FirebaseStorage,
        public userActions: UserActions,
        public sessionData: SessionData,
        public cdr: ChangeDetectorRef,
        public messageActions: MessageActions) {
        this.error$ = this.store.select((state) => state.messageState.error);
        this.loading$ = this.store.select((state) => state.messageState.loading);
        this.ChatRooms$ = this.store.select((state) => state.messageState.chatRooms);
        this.Messages$ = this.store.select((state) => state.messageState.messages);
    }
    ngOnInit() {
        this.store.dispatch(this.messageActions.GetChatRooms());
        this._messageStateSubscription = this.store
            .select((state) => state.messageState)
            .subscribe((messageState) => {
                if (messageState) {
                    // Set params
                    this.Error = messageState.error;
                    this.IsLoading = messageState.loading;
                    this.HasMoreMessages = messageState.hasMoreMessages;
                    // Additional logic (scroll)
                    switch (messageState.type) {
                        case MessageStateType.LOADING: {
                            console.log("Loading");
                            this.IsLoading = true;
                            this.SocketReconnected = true;
                            break;
                        }
                        case MessageStateType.JOIN_SUCCEEDED: {
                            // We should scoll to the bottom on all initial load
                            // TODO: Add logic to remove the loading UI after the scroll.
                            // So might initially have a detached loading flag that eventually
                            // is connected to the state loading flag
                            setTimeout(() => {
                                this.SocketReconnected = true;
                                this.IsLoading = false;
                                // this.content.scrollToBottom(1);
                            }, 1000);
                            break;
                        }
                        case MessageStateType.ADD_MESSAGE_SUCCESS: {
                            // Only auto scroll if the user is at the bottom
                            // Do not auto scroll if the user has scroll up reading older chats
                            console.log("Proper add chat scroll logic coming soon");
                            setTimeout(() => {
                                this.SocketReconnected = true;
                                this.IsLoading = false;
                                // this.content.scrollToBottom(1);
                            }, 1000);
                            break;
                        }
                        case MessageStateType.GET_MESSAGES_SUCCESS:
                        case MessageStateType.NONE:
                        default: {
                            setTimeout(() => {
                                this.IsLoading = false;
                                if (!messageState.isInitialLoad) {
                                    // this.content.scrollToBottom(1);
                                }
                            }, 4000);
                            break;
                        }
                    }
                } else {
                    // Chat state is undefined for some reason
                }
            });
    }

    ngOnDestroy(): void {
        this.store.dispatch(this.messageActions.LeaveMessageRoom());
        if (this._messageStateSubscription != null) {
            this._messageStateSubscription.unsubscribe();
            this._messageStateSubscription = null;
        }
        this.SocketReconnected = false;
    }

    public SwitchNewChat(): void {
        this.IsNewChatRoom = !this.IsNewChatRoom;
        if (this.IsNewChatRoom) {
            this.RoomID = "";
        }
    }

    public SelectChatRoom(room: UserChatRoom): void {
        this.store.dispatch(this.messageActions.JoinMessageRoom(room.roomID));
        this.RoomID = room.roomID;
        this.MessageParticipants.forEach((participant: MessageParticipant) => {
            participant.uid === this.sessionData.userID ? console.log("we matchy") : this._participantIDs.push(participant.uid);
        });
        this._defaultMessage = {
            date: database.ServerValue.TIMESTAMP,
            authorID: this.sessionData.userID,
            authour: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
            text: null,
            photoURL: `/users/${this.sessionData.userID}/${this.sessionData.userID}.png`,
            type: "text",
        };
    }

    public RefreshChat(): void {
        this.store.dispatch(this.messageActions.GetChatRooms());
    }

    public AddStaffMember(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        this.store.dispatch(this.userActions.GetUsers(null));
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = true;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SelectedStaff = this.selectedStaff;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                console.log("Chosen person", user);
                let assignee: IAssignee = {
                    name: `${ user.firstName } ${user.lastName}`,
                    uid: user.uid
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                    console.log("selectedStaff", this.selectedStaff);
                    this.cdr.detectChanges();
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                    console.log("selectedStaff", this.selectedStaff);
                }
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        this.controls.ShowDialog(`Remove staff?`, "Please confirm that you want to remove: " + `${ staff.name}`, "Remove", "Cancel")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                        return (assignee.uid !== staff.uid);
                    });
                    console.log("newStaff", newStaff);
                    this.selectedStaff = newStaff;
                    this.cdr.detectChanges();
                } else {

                }
            }
        });
    }

    public IsLink(txt: string): boolean {
        return txt.includes("https") || txt.includes("http");
    }

    public OpenImage(event: HTMLElement): void {
        const viewer = new Viewer(event, {
            inline: false,
            zIndex: 9999,
            title: 0,
            toolbar: {
                zoomIn: 1,
                zoomOut: 1,
                oneToOne: 1,
                reset: 1,
                prev: 0,
                play: {
                    show: 0,
                    size: "large",
                },
                next: 0,
                rotateLeft: 4,
                rotateRight: 4,
                flipHorizontal: 4,
                flipVertical: 4,
            },
            viewed() {
                viewer.zoomTo(0.2);
            },
        });
        viewer.show();
    }

    public LoadMoreMessages(infiniteScroll: any): void {
        console.log("Begin load more messages operation");
        if (null != this.RoomID) {
            // Get current cursor and dispatch get history action
            this.store
                .select((state) => state.messageState.cursor)
                .first()
                .subscribe((cursor: string) => {
                    if (cursor) {
                        this.store.dispatch(this.messageActions.GetMessageHistory(this.RoomID, cursor));
                        infiniteScroll.complete();
                    } else {
                        infiniteScroll.complete();
                        // Cursor doesnt exist, probably due to no more messages
                    }
                });
        } else {
            infiniteScroll.complete();
        }
    }

    public StartChat(): void {
        if (this.TextMessage) {
            // if (this.ChatTopic !== 'Select a topic') {
                // Create a partial message object for the object clone
                const partialChatMessage: IPartialChatMessage = {
                    text: this.TextMessage,
                    type: "text"
                };
                let messageParticipants: MessageParticipant[] = [];
                if (null != this.selectedStaff) {
                    this.selectedStaff.forEach((participant: IAssignee) => {
                        let newParticipant: MessageParticipant = {
                            uid: participant.uid,
                            photoURL: `/users/${participant.uid}/${participant.uid}.${Constants.STR_PNG}`,
                            authour: participant.name,
                            role: "Staff"
                        };
                        messageParticipants.push(newParticipant);
                    });
                }
                // add myself to the chat
                const myself: MessageParticipant = {
                    uid: this.sessionData.userID,
                    photoURL: `/users/${this.sessionData.userID}/${this.sessionData.userID}.${Constants.STR_PNG}`,
                    authour: this.sessionData.user.firstName + " " + this.sessionData.user.lastName,
                    role: this.sessionData.user.role
                };
                messageParticipants.push(myself);
                this._participantIDs.push(myself.uid);

                // Create a new message object from the default message object with an override of the text message
                let message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);
                message.date = new Date(Date.now()).getTime();
                message.authour = this.sessionData.user.firstName + " " + this.sessionData.user.lastName;
                const helper: NewChatRoomHelper = {
                    message: message,
                    roomID: this.RoomID,
                    participants: messageParticipants,
                    authorID: this.sessionData.userID
                };

                console.log("HELPER: ", helper);
                this.store.dispatch(this.messageActions.NewChatRoom(helper));
                
                
                // Clear the text message field
                this.TextMessage = "";
                setTimeout(() => {
                    this.controls.ShowAlert(`Room started`, `New chat room started`, "Thanks");
                    this.store.dispatch(this.messageActions.GetChatRooms());
                    this.SwitchNewChat();
                }, 500);
            // } else {
            //     this.controls.ShowAlert("Select a topic", "Please select a topic", "Thanks");
            // }
        } else {
            this.controls.ShowAlert("Enter a message", "Please enter a message", "Thanks");
            // Text message field is empty
        }
    }

    public SendText(): void {
        if (this.TextMessage) {
            // Create a partial message object for the object clone
            const partialChatMessage: IPartialChatMessage = {
                text: this.TextMessage,
                type: "text",
            };
            // Create a new message object from the default message object with an override of the text message
            const message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);

            // Clear the text message field
            this.TextMessage = "";
        } else {
            // Text message field is empty
        }
    }

    public async handleFileInput(files: FileList): Promise<void> {
        this._imageData = files.item(0);
        console.log("File", this._imageData);
        this._photoModified = true;
        await this._uploadPhoto(this._imageData);
        // this._uploadPreview = URL.createObjectURL(this._imageData);
        // this._setPhoto(this._imageData);
    }

    /* Helper Methods */
    public readURL(event: any): void {
        console.log("EVENT: ", event);
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = (e: any) => {
                console.log("TARGET: ", e.target.result);
                this.UploadPreview = <string>e.target.result;
                console.log("preview: ", this.UploadPreview);
            };
            reader.readAsDataURL(file);
        }
    }

    public CancelImageUpload(): void {
        if (this._uploadTask) {
            this._uploadTask.cancel();
        } else {
            // Upload task not set yet
        }
        this._finishImageSend(false);
    }

    public RemoveImage(): void {
        // Remove image data
        if (this._imageData) {
            this._imageData = null;
            this._photoModified = true;
        }
        // Hide the remove button & show add button
    }

    private async _uploadPhoto(imageData: File): Promise<string> {
        console.log("_uploadPhoto", imageData);
        return new Promise<string>((resolve, reject) => {
            // Set the upload preview
            // Show the upload UI
            this.IsUploading = true;
            // Generate filename and remove dashes
            // Generate full file path
            const fileName: string = this.RoomID + StringUtil.GenerateUUID() + Date.now();
            const fullPath: string = `/orgs/${this.sessionData.orgID}/messages/${this.sessionData.buildingID}/${this.RoomID}/${fileName}.png`;
            // Start image upload process and get upload task
            const contentType: string = "image.png";
            const format: string = "base64";
            this._uploadTask = this.firebaseStorage.Upload(fullPath, imageData, contentType);
            // Enable the cancel button now that the upload task is available
            this.CanCancelUpload = true;
            // Listen for state changes, error, completion of the upload
            this._uploadTask.on(
                storage.TaskEvent.STATE_CHANGED,
                (snapshot: firebase.storage.UploadTaskSnapshot) => {
                    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                    // this.ProgressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                },
                (error) => {
                    let message = Constants.MESSAGE_IMAGE_UPLOAD_ERROR;
                    switch (error.name) {
                        case Constants.ERROR_TYPE_STORAGE_UNAUTHORIZED: {
                            // User doesn't have permission to access the object
                            message = Constants.MESSAGE_IMAGE_UPLOAD_UNAUTHORIZED;
                            break;
                        }

                        case Constants.ERROR_TYPE_STORAGE_CANCELED: {
                            // User canceled the upload
                            message = Constants.MESSAGE_IMAGE_UPLOAD_CANCELED;
                            break;
                        }

                        default: {
                            break;
                        }
                    }
                    this.controls.ShowDialog(error.name, message, "Ok", "Dismiss");
                },
                () => {
                    this.UploadPreview = this._uploadTask.snapshot.downloadURL;
                    // Update UI with finishing and remove cancel button
                    this.IsUploadDone = true;
                    this.CanCancelUpload = false;
                    // Create a partial message object for the message clone
                    const partialChatMessage: IPartialChatMessage = {
                        text: fullPath,
                        type: "photo",
                    };
                    // Create a new message object from the default message object with an override of the text message
                    const message: IMessage = Object.assign({}, this._defaultMessage, partialChatMessage);

                    // Create a partial message object for the message clone
                    this._finishImageSend(true);
                    // Update the photoURL path
                }
            );
        });
    }

    private _finishImageSend(isDone: boolean): void {
        // Update UI with done
        this.UploadStatus = isDone ? "Done!" : "Canceled!";
        // keep the done UI for 1 second
        setTimeout(() => {
            // Hides all upload UI elements
            // Re-enables the image upload button
            this.IsUploading = false;
            // Reset the image done flag and status
            this.IsUploadDone = false;
            this.UploadStatus = Constants.STR_FINISHING;
            // Resets the progress bar
            this.ProgressPercent = 0;
            // Reset upload preview
            this.UploadPreview = null;
            // Clear upload task
            this._uploadTask = null;
            this._photoModified = false;
        }, 1000);
    }
}
