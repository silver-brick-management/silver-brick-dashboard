import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTenantsComponent } from './dashboard-tenants.component';

describe('DashboardTenantsComponent', () => {
  let component: DashboardTenantsComponent;
  let fixture: ComponentFixture<DashboardTenantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTenantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTenantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
