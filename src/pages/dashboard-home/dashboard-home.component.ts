// *********************************************************
//
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.
//
// *********************************************************

// Node modules
import { FirebaseStorage } from "../../services/FirebaseStorage";
import { Store } from "@ngrx/store";
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialog, MdSnackBar, MdDialogConfig, MdSnackBarConfig, MdSelect} from "@angular/material";
import { NgForm } from '@angular/forms';
import {
    Chart,
    ChartAnimationObject,
    ChartAnimationOptions,
    ChartOptions,
    ChartDataSets,
    ChartXAxe,
    ChartLegendLabelOptions
} from "chart.js";


// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IAuthState, AuthStateType, UserStateType, IUserState, ISessionState, SessionStateType } from "../../interfaces/IStoreState";
import { IAssignee, IUser, IBuildingBasic, IBuildingSimple, ITaskFeed, IUserOrg, ITask } from "../../shared/SilverBrickTypes";
import { UserActions } from "../../store/actions/UserActions";
import { AuthActions } from "../../store/actions/AuthActions";
import { OrgActions } from "../../store/actions/OrgActions";
import { SessionActions } from "../../store/actions/SessionActions";
import { IPaginateQueryStrings, IInputDialogBase, ITaskDialogResponse, IGetBldgHelper, IUserDialogBase, IAudienceDialogResponse } from "../../interfaces/IParams";
import { AssignTaskHelper, ITimeCardToday, ITaskFeedJoin, GetTimeCardHelper } from "../../interfaces/IResponse";
import { TaskDetailsComponent } from "../../components/task-details/task-details.component";
import { SessionData } from "../../providers/SessionData";
import { TimeUtil } from "../../common/TimeUtil";
import { Controls } from "../../providers/Controls";
import { ConfirmDialogComponent } from "../../components/confirm-dialog/confirm-dialog.component";
import { UserApi } from "../../services/UserApi";
import { AudienceChooserComponent } from '../../components/audience-chooser/audience-chooser.component';
import { RecurringTaskDetailsComponent } from '../../components/recurring-task-details/recurring-task-details.component';

@Component({
    selector: "app-dashboard-home",
    templateUrl: "./dashboard-home.component.html",
    styleUrls: ["./dashboard-home.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardHomeComponent implements OnInit {
    private _sessionStateSubscription: Subscription;
    private _user: IUser = null;
    private _authSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _screenSize: number = window.screen.width;
    private _credentials: any = {
        email: '',
        password: ''
    };

    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public usersLoading$: Observable<boolean>;
    public SelectedStaff: IUser = null;
    public tasks$: Observable<Array<ITask>>;
    public userState$: Observable<IUserState>;
    public activeTabIndex: number = 0;
    public authState$: Observable<IAuthState>;
    public sessionState$: Observable<ISessionState>;
    public staffUsers$: Observable<Array<IUser>>;
    public taskFeed$: Observable<ITaskFeed[]>;
    public timeCards$: Observable<ITimeCardToday[]>;
    public IsFirstTime$: Observable<IUser>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public orgID: string = '';
    public buildingID: string = '';
    public SelectedDate: Date = null;
    public IsScheduling: boolean = false;
    public isFirstLoad: boolean = true;
    public selectedStaff: IAssignee[] = [];
    public isLoading: boolean = false;


    get user(): IUser {
        return this._user;
    }

    set user(use: IUser) {
        this._user = use;
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsStaff(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && ((this.sessionData.user.role === 'Staff') && (!this.sessionData.user.isAdmin)));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && this.sessionData.user.isAdmin);
    }

    get IsNotTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Tenant'));
    }

    get IsTenant(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Tenant'));
    }

    get IsNotLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Landlord'));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    public TimeStamp(time: number): string {
        return TimeUtil.timeSince(time);
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _firebaseStorage: FirebaseStorage,
        public sessionData: SessionData,
        private _controls: Controls,
        private _userApi: UserApi,
        private cdr: ChangeDetectorRef,
        private _store: Store<AppState>,
        private _sessionActions: SessionActions,
        private _authActions: AuthActions,
        private _orgActions: OrgActions,
        private _userActions: UserActions) {
        this.AllBuildings$ = this._store.select((state) => state.orgState.allBuildings);
        this.staffUsers$ = this._store.select((state) => state.usersState.users);
        this.tasks$ = this._store.select((state) => state.sessionState.tasks);
        this.userState$ = this._store.select((state) => state.usersState);
        this.error$ = this._store.select((state) => state.usersState.error);
        this.loading$ = this._store.select((state) => state.sessionState.loading);
        this.usersLoading$ = this._store.select((state) => state.usersState.loading);
        this.authState$ = this._store.select((state) => state.authState);
        this.sessionState$ = this._store.select((state) => state.sessionState);
        this.IsFirstTime$ = this._store.select(state => state.authState.profile);
        this.taskFeed$ = this._store.select(state => state.sessionState.taskFeed);
        this.timeCards$ = this._store.select(state => state.usersState.timeCards);
    }

    ngOnInit(): void {
        this.SelectedDate = new Date(Date.now());
        if (this.sessionData.isFirstTime) {
            this._userStateSubscription = this.userState$
            .subscribe((state: IUserState) => {
                if (null != state) {
                    switch (state.type) {
                        case UserStateType.UPDATE_USER_SUCCEEDED:
                        {
                            if (null != this.sessionData.user) {
                                this.sessionData.user.isFirstTime = false;
                            }
                            this._controls.ShowAlert('Password successfully set!', 'You have successfully set your password', 'Thanks');
                            break;
                        }

                        default:
                        {
                            break;
                        }
                    }
                }
            });
        } else {
            this._sessionStateSubscription = this.sessionState$.subscribe((state: ISessionState) => {
                if (null != state) {
                    switch (state.type) {

                        case SessionStateType.GET_USERS_SUCCEEDED:
                        {
                            if (null != state.user) {
                                if (this.isFirstLoad) {
                                    if (!state.user.isAdmin) {
                                       
                                    } else {
                                        let date: Date = new Date(Date.now());
                                        let helper: GetTimeCardHelper = {
                                            day: date.getDate(),
                                            month: date.getMonth() + 1,
                                            year: date.getFullYear(),
                                            uid: this.sessionData.userID,
                                            date: date.getTime()
                                        };
                                        this._store.dispatch(this._userActions.GetAllTimeCards(helper));
                                        this.isLoading = true;
                                        // this._store.dispatch(this._sessionActions.GetRecurringTasks());
                                        this.isFirstLoad = false;
                                    }
                                }
                            }
                            break;
                        }

                        default:
                        {
                            break;
                        }
                    }
                }
            });

            this._authSubscription = this.authState$
            .subscribe((state: IAuthState) => {
                if (null != state) {

                }
            });
        }
    }

    ngOnDestroy(): void {
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }

        if (null != this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public StopLoading(): void {
        this.isLoading = false;
    }

    public TimeSince(date: number): string {
        return TimeUtil.timeSince(date);
    }

    public SelectStaff(user: IUser, task: ITask): void {
        this.SelectedStaff = user;
        console.log("SelectedStaff", this.SelectedStaff);
    }

    public AddStaffMember(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        this._store.dispatch(this._userActions.GetUsers(null));
        const dialogRef = this._dialog.open(AudienceChooserComponent, dialogConfig);
        dialogRef.componentInstance.SearchingStaff = true;
        dialogRef.componentInstance.SearchingTenant = false;
        dialogRef.componentInstance.SelectedStaff = this.selectedStaff;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IAudienceDialogResponse) => {
            if (response) {
                let user: IUser = <IUser>response.selected;
                console.log("Chosen person", user);
                let assignee: IAssignee = {
                    name: `${ user.firstName } ${user.lastName}`,
                    uid: user.uid
                };
                if (null != this.selectedStaff) {
                    this.selectedStaff.push(assignee);
                    console.log("selectedStaff", this.selectedStaff);
                    this.cdr.detectChanges();
                } else {
                    this.selectedStaff = [];
                    this.selectedStaff.push(assignee);
                    this.cdr.detectChanges();
                    console.log("selectedStaff", this.selectedStaff);
                }
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RemoveStaff(staff: IAssignee): void {
        this._controls.ShowDialog(`Remove staff?`, "Please confirm that you want to remove: " + `${ staff.name}`, "Remove", "Cancel")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    let newStaff = this.selectedStaff.filter((assignee: IAssignee) => {
                        return (assignee.uid !== staff.uid);
                    });
                    console.log("newStaff", newStaff);
                    this.selectedStaff = newStaff;
                    this.cdr.detectChanges();
                } else {

                }
            }
        });
    }

    public ScheduleTask(task: ITask): void {
        if ((null != this.selectedStaff) && (this.selectedStaff.length > 0)) {
            let staffStr: string = '';
            this.selectedStaff.forEach((staff: IAssignee) => {
                staffStr = staffStr + `\n${staff.name}`
            });
            this._controls.ShowAssignDialog(staffStr)
            .subscribe((response: IInputDialogBase) => {
                if ((null != response.input) && (response.action === Constants.DIALOG_RESPONSE_ACTION_OK)) {
                    let parts: string[] = task.startTime.split(":", 2);
                    let newHours: number = Number(parts[0]) + 1;
                    task.endTime = `${newHours}:${parts[1]}`;
                    task.startDate = this.SelectedDate.getTime();
                    task.durationAmount = 'day(s)';
                    task.durationQuantity = 1;
                    task.repeats = 'As needed, we won\'t prompt you';
                    task.repeatInterval = 'Once';
                    task.repeatQuantity = 1;
                    task.scheduled = true;
                    task.status = "Assigned";
                    if (null != this.selectedStaff) {
                        task.assignees = this.selectedStaff;
                    }
                    let helper: AssignTaskHelper = {
                        userID: '',
                        authorID: this.sessionData.user.uid,
                        task: task,
                    };
                    if ((null != response.input) && (response.input !== '')) {
                        console.log("data", response.input);
                        helper.message = response.input;
                    }
                    const msg: string = 'Task successfully scheduled & assigned';
                    this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                    this.switchScheduling(task);
                    this._store.dispatch(this._sessionActions.AssignTask(helper));
                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                    this.SelectedStaff = null;
                    this.SelectedDate = null;
                    }
                           
                });
        } else {
            this._controls.ShowAlert("Missing Info", "Please selected a staff member and a date", "Will Do");
        }
    }

    public switchScheduling(task: ITask): void {
        task.scheduling = !task.scheduling;
        this.cdr.detectChanges();
    }

    public onDateSelect(event: any): void {
        console.log("onDateSelect", event);
        this.SelectedDate = event;
    }

    public ScopeTasks(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: ""
        };
        this._store.dispatch(this._sessionActions.ScopeBuildingTasks(helper));
    }

    public ScopeHistory(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: ""
        };
        this._store.dispatch(this._sessionActions.ScopeBuildingHistory(helper));
    }

    public SetPassword(form: NgForm): void {
        if (form.valid) {
            const passwords: any = form.value;
            console.log("Passwords", passwords);
            const password: string = <string>passwords.inputPassword;
            const confirmPassword: string = <string>passwords.confirmPassword;
            if (password === confirmPassword) {
                if (password.length > 6) {
                    const subscription: Subscription = this.authState$
                    .subscribe((state: IAuthState) => {
                        if ((null != state) && (null != state.profile)) {
                            this._credentials.email = state.profile.email;
                            this._credentials.password = password;
                            const newProfile: IUser = state.profile;
                            newProfile.password = password;
                            newProfile.isFirstTime = false;
                            console.log("Setting Password! ");
                            this._store.dispatch(this._userActions.UpdateUserPassword(newProfile));
                        }
                    });

                } else {
                    this._controls.ShowDialog("Password too short", "Passwords must be longer than 6 characters", "Try Again", "Dismiss");
                }
            } else {
                this._controls.ShowDialog("Passwords do not match!", "Please make sure your passwords match", "Try Again", "Dismiss");
            }
        } else {
            this._controls.ShowDialog("Incorrect Info", "Please verify your account info.", "Try Again", "Dismiss");
        }
    }

    public AssignTask(user: IUser, task: ITask): void {
        this._controls.ShowAssignDialog(`${ user.firstName} ${ user.lastName }`)
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    task.status = "Assigned";
                    
                    let helper: AssignTaskHelper = {
                        userID: user.uid,
                        authorID: this.sessionData.user.uid,
                        task: task,
                    };
                    this._store.dispatch(this._sessionActions.AssignTask(helper));
                    let response: ITaskDialogResponse = {
                        action: Constants.DIALOG_RESPONSE_ACTION_UPDATE,
                        task: task,
                    };
                    this.handleTaskDetailResponse(response);
                } else {
                    // User chose either cancel or chosen response is other.
                    // At that point, its safer to stay and not loose the user's changes
                    return;
                }
            } else {
                // No response detected, its safer to stay and not loose the user's changes
                return;
            }
        });
    }

    /* View Methods */

    public RefreshTasks(): void {
        if (!this.sessionData.user.isAdmin) {
            if ((this.sessionData.user.role === "Tenant")) {
                this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
            }  else if (this.sessionData.user.role === "Landlord") {
                this._store.dispatch(this._sessionActions.GetLandlordTasks());
            } else {
                this.orgID = this.sessionData.org.id;
                this.buildingID = this.sessionData.org.buildings[0].id;
                let bldgHelper: IGetBldgHelper = {
                    orgID: this.sessionData.org.id,
                    buildingID: this.sessionData.org.buildings[0].id
                };
                console.log("bldgHelper", bldgHelper);
                this._store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
            }
        } else {
            console.log("GetAllHistory");
            this._store.dispatch(this._orgActions.GetAllOrgsScope());
            this._store.dispatch(this._sessionActions.GetRecurringTasks());
            this._store.dispatch(this._sessionActions.GetTasks());
        }
    }

    public RefreshTimeCards(): void {
        let date: Date = new Date(Date.now());
        let helper: GetTimeCardHelper = {
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
            uid: this.sessionData.userID,
            date: date.getTime()
        };
        this._store.dispatch(this._userActions.GetAllTimeCards(helper));
    }

    public RefreshHistory(): void {
        if (!this.sessionData.user.isAdmin) {
            if ((this.sessionData.user.role === "Tenant") || (this.sessionData.user.role === "Landlord")) {
            } else {
                this.orgID = this.sessionData.org.id;
                this.buildingID = this.sessionData.org.buildings[0].id;
                let bldgHelper: IGetBldgHelper = {
                    orgID: this.sessionData.org.id,
                    buildingID: this.sessionData.org.buildings[0].id
                };
                console.log("bldgHelper", bldgHelper);
                this._store.dispatch(this._sessionActions.GetStaffHistory(this.sessionData.user.uid));
            }
        } else {
            console.log("GetAllHistory");
            this._store.dispatch(this._orgActions.GetAllOrgsScope());
            // 
        }
    }

    GetBuilding(orgs: IUserOrg): string {
        if (null != orgs.buildings && orgs.buildings.length > 0) {
            return orgs.buildings[0].name;
        } else {
            return "N/A";
        }
    }

    public GetTimeSince(time: number): string {
        return TimeUtil.timeSince(time);
    }

    expandTask(task?: ITask): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        
        const dialogRef = this._dialog.open(TaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            if ((this.sessionData.user.role === 'Landlord') && (this.sessionData.user.orgs.buildings.length === 1)) {
                dialogRef.componentInstance.building = {
                    orgID: this.sessionData.user.orgs.id,
                    id: this.sessionData.user.orgs.buildings[0].id,
                    name: this.sessionData.user.orgs.buildings[0].name
                };
            };
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RecurringTask(task: ITask = null): void {
        // // console.log("RecurringTask", task, date);
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        
        const dialogRef = this._dialog.open(RecurringTaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
           
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    handleTaskDetailResponse(response: ITaskDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                if (this.sessionData.user.isAdmin) {
                    this._store.dispatch(this._sessionActions.GetTasks());
                } else {
                    let bldgHelper: IGetBldgHelper = {
                        orgID: this.sessionData.org.id,
                        buildingID: this.sessionData.org.buildings[0].id,
                    };
                    console.log("bldgHelper", bldgHelper);
                    this._store.dispatch(this._sessionActions.GetStaffBookings(bldgHelper));
                    this._store.dispatch(this._userActions.GetBuildingTenants(bldgHelper));
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                this._store.dispatch(this._sessionActions.GetTasks());
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                if (this.sessionData.user.isAdmin) {
                    this._store.dispatch(this._sessionActions.GetTasks());
                    
                } else {
                    if ((this.sessionData.user.role === "Tenant")) {
                        this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                    } else if (this.sessionData.user.role === "Landlord") {
                        let bldgHelper: IGetBldgHelper = {
                            orgID: this.sessionData.org.id,
                            buildingID: this.sessionData.org.buildings[0].id
                        };
                        this._store.dispatch(this._sessionActions.GetBuildingTasks(bldgHelper));
                    } else {
                        this.orgID = this.sessionData.org.id;
                        this.buildingID = this.sessionData.org.buildings[0].id;
                        let bldgHelper: IGetBldgHelper = {
                            orgID: this.sessionData.org.id,
                            buildingID: this.sessionData.org.buildings[0].id
                        };
                        console.log("bldgHelper", bldgHelper);
                        this._store.dispatch(this._orgActions.GetAllOrgsScope());
                        this._store.dispatch(this._sessionActions.GetStaffTasks(this.sessionData.user.uid));
                        this._store.dispatch(this._sessionActions.GetStaffHistory(this.sessionData.user.uid));
                    }
                }
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public NewTime(duration: number): string {
        const seconds = (duration / 1000) % 60 > 1 ? (duration / 1000) % 60 : 0;
        const minutes = (duration / (1000 * 60)) % 60 > 1 ? (duration / (1000 * 60)) % 60 : 0;

        const strMinutes = minutes > 0 ? Math.round(minutes) + ":" : "";
        const strSeconds = seconds > 0 ? Math.round(seconds) : 0;

        return strMinutes + strSeconds;
    }

    private _renderCalendar(): void {}
}

