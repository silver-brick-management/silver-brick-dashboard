// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, Urls } from '../../common/Constants';
import { IUser, ITaskFeed } from '../../shared/SilverBrickTypes';
import { IUserDialogResponse, IUserDialogBase, IGetBldgHelper } from '../../interfaces/IParams';
import { AuthActions } from '../../store/actions/AuthActions';
import { Controls } from '../../providers/Controls';
import { SessionData } from '../../providers/SessionData';
import { SessionActions } from "../../store/actions/SessionActions";
import { TimeUtil } from "../../common/TimeUtil";
import { UserActions } from "../../store/actions/UserActions";
import { UserDetailComponent } from '../../components/user-details/user-details.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
    public isNavbarToggled = true;
    public profile$: Observable<IUser>;
    public taskFeed$: Observable<ITaskFeed[]>;
    public profileSubscrition: Subscription;
    public ProfilePic: string = Urls.DEFAULT_PROFILE_URL;
    public IsFirstTime$: Observable<IUser>;

    private _screenSize: number = window.screen.width;
    get Email(): string {
        return (null != this._sessionData.email) ? this._sessionData.email : '';
    }

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsStaff(): boolean {
        return ((null != this._sessionData) && (null != this._sessionData.user) && ((this._sessionData.user.role === 'Staff') && (!this._sessionData.user.isAdmin)));
    }

    get IsNotTenant(): boolean {
        return ((null != this._sessionData) && (null != this._sessionData.user) && ((this._sessionData.user.role !== 'Landlord') && (this._sessionData.user.role !== 'Tenant')));
    }

    get IsTenant(): boolean {
        return ((null != this._sessionData) && (null != this._sessionData.user) && ((this._sessionData.user.role === 'Tenant')));
    }

    get IsLandlord(): boolean {
        return ((null != this._sessionData) && (null != this._sessionData.user) && ((this._sessionData.user.role === 'Landlord')));
    }

    public isLoading$: Observable<boolean>;

    constructor(
        private _dialog: MdDialog,
        private _controls: Controls,
        private _userActions: UserActions,
        private _sessionActions: SessionActions,
        private _snackbar: MdSnackBar,
        private _sessionData: SessionData,
        private _store: Store<AppState>,
        private _authActions: AuthActions) {
        this.profileSubscrition = null;
        this.IsFirstTime$ = this._store.select(state => state.authState.profile);
        this.taskFeed$ = this._store.select(state => state.sessionState.taskFeed);
        this.isLoading$ = this._store.select(state => state.authState.loading);
        this.profile$ = this._store.select(state => state.authState.profile);
    }

    ngOnInit(): void {
        
        this.profileSubscrition = this.profile$
        .subscribe((profile: IUser) => {
            // if (null != profile) {
            //     if (!profile.isAdmin) {
            //         if ((profile.role === "Tenant")) {
            //             this._store.dispatch(this._sessionActions.GetTenantTasks(profile.uid));
            //         } else if (profile.role === "Landlord") {
                        
            //             this._store.dispatch(this._sessionActions.GetLandlordTasks());
            //         } else {
            //             let bldgHelper: IGetBldgHelper = {
            //                 orgID: this._sessionData.org.id,
            //                 buildingID: this._sessionData.org.buildings[0].id
            //             };
            //             console.log("bldgHelper", bldgHelper);
            //             this._store.dispatch(this._sessionActions.GetStaffTasks(profile.uid));
            //             this._store.dispatch(this._sessionActions.GetStaffHistory(profile.uid));
            //         }
            //     } else {
            //         // this._store.dispatch(this._sessionActions.GetAllHistory());
            //         this._store.dispatch(this._sessionActions.GetTasks());
            //         this._store.dispatch(this._userActions.GetUsers(null));
            //     }
            //     this.ProfilePic = (null != profile.fullPhotoURL) ? profile.fullPhotoURL : Urls.DEFAULT_PROFILE_URL;
            // } else {
            //     console.log("Profile is null");
            // }
        });
    }

    public GetIconName(activity: ITaskFeed): string {
        let iconName: string = "chat";
        switch (activity.type) {
            case "update":
            {
                iconName = "chat";
                break;
            }

            case "insert_photo":
            {
                iconName = 'insert_photo';
                break;
            }

            case "new":
            {
                iconName = "add";
                break;
            }

            case "assign":
            {
                iconName = "assignment";
                break;
            }

            case "complete":
            {
                iconName = "check";
                break;
            }
            
            default:
            {
                break;
            }
        }
        return iconName;
    }

    public OpenMobile(): void {
        if (window.screen.width < 767) {
            //window.open('pltfm://com.platform.community');
        }
    }

    blah(what: string): void {
        console.log(what);
    }

    public RefreshTasks(): void {
        this._store.dispatch(this._sessionActions.GetTasks());
    }

    public ExpandProfile(): void {
        if (this.profile$) {
            this.profile$.first().subscribe((user) => {
                const dialogConfig = new MdDialogConfig();
                if (this.IsMobile) {
                    dialogConfig.width = '1000px';
                    dialogConfig.height = '650px';
                } else {
                    dialogConfig.width = '70%';
                    dialogConfig.height = '650px';
                }
                
                const dialogRef = this._dialog.open(UserDetailComponent, dialogConfig);
                dialogRef.componentInstance.user = user;
                dialogRef.componentInstance.showExtras = false;
                dialogRef.componentInstance.PopulateForm();
                dialogRef.afterClosed().subscribe((response: IUserDialogResponse) => {
                    if (response) {
                        this.handleUserDetailResponse(response);
                    } else {
                        // Do nothing as there is no response
                    }
                });
            });
        } else {
            this._snackbar.open(Constants.ERROR_MESSAGE_NO_PROFILE, Constants.STR_DISMISS, {
                duration: 3000
            });
        }
    }

    public GetTimeSince(time: number): string {
        return TimeUtil.timeSince(time);
    }

    public ToggleMenu(): void {
        let menu: HTMLElement = document.getElementById('left_col');
        menu.style.display = (menu.style.display === "block") ? "none" : "block";
    }

    public Logout(): void {
        this._controls.ShowDialog("Logout?", "Are you sure you want to logout?", "Yes", "No")
        .subscribe((response: IUserDialogBase) => {
            if (response) {
                if (response.action === Constants.DIALOG_RESPONSE_ACTION_OK) {
                    this._store.dispatch(this._authActions.Logout());
                } else {
                    // User chose either cancel or chosen response is other.
                    // At that point, its safer to stay and not loose the user's changes
                    return;
                }
            } else {
                // No response detected, its safer to stay and not loose the user's changes
                return;
            }
        });
    }


    /* Helper Methods */

    handleUserDetailResponse(response: IUserDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            case Constants.DIALOG_RESPONSE_ACTION_ACTIVATE:
            case Constants.DIALOG_RESPONSE_ACTION_DEACTIVATE:
                const msg: string = `${response.user.firstName} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;

            default:
                break;
        }
    }
}
