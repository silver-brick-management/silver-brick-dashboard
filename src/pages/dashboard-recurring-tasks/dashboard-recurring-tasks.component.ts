// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
// import { IgxExcelExporterService, IgxExcelExporterOptions } from "igniteui-angular";

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, staffColors, Urls } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IUser, IAssignee, IBuildingSimple, ITask, IUserOrg, IOrgBasic } from '../../shared/SilverBrickTypes';
import { IUserState, IAuthState, AuthStateType, UserStateType, ISessionState, SessionStateType } from '../../interfaces/IStoreState';
import { UserActions } from '../../store/actions/UserActions';
import { SessionActions } from '../../store/actions/SessionActions';
import { UserUtil } from '../../common/UserUtil';
import { IPaginateQueryStrings, ITaskDialogResponse, IGetBldgHelper, IUserDialogBase } from '../../interfaces/IParams';
import { SessionData } from '../../providers/SessionData';
import { TaskDetailsComponent } from '../../components/task-details/task-details.component';
import { Controls } from '../../providers/Controls';
import { AssignTaskHelper, ITaskFeedJoin } from '../../interfaces/IResponse';
import { OrgActions } from "../../store/actions/OrgActions";
import { RecurringTaskDetailsComponent } from '../../components/recurring-task-details/recurring-task-details.component';

@Component({
    selector: 'app-dashboard-recurring-tasks',
    templateUrl: './dashboard-recurring-tasks.component.html',
    styleUrls: ['./dashboard-recurring-tasks.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardRecurringTasksComponent implements OnInit {
    private _authSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _sessionStateSubscription: Subscription;
    private _firstLoad: boolean = true;
    private _screenSize: number = window.screen.width;
    public monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "January" ];

    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public tasks$: Observable<Array<ITask>>;
    public completedTasks$: Observable<Array<ITask>>;
    public userState$: Observable<IUserState>;
    public authState$: Observable<IAuthState>;
    public sessionState$: Observable<ISessionState>;
    public staffUsers$: Observable<Array<IUser>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public orgID: string = '';
    public buildingID: string = '';
    public DurationOptions: string[] = ['day(s)', 'week(s)', 'month(s)', 'year(s)', 'forever'];
    public FrequencyOptions: string[] = ['Daily', 'Weekly', 'Monthly', 'Yearly', 'Once'];
    public searchType$: Observable<string>;
    public searchRef: string = '';
    public calendarColors$: Observable<any>;
    public selectedStaff$: Observable<IUser>;

    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsNotLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Landlord'));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _controls: Controls,
        private _orgActions: OrgActions,
        public sessionData: SessionData,
        private _store: Store<AppState>,
        private _sessionActions: SessionActions,
        private _userActions: UserActions) {
        this.selectedStaff$ = this._store.select((state) => state.sessionState.selectedStaff);
        this.AllBuildings$ = this._store.select((state) => state.orgState.allBuildings);
        this.staffUsers$ = this._store.select(state => state.usersState.users);
        this.tasks$ = this._store.select(state => state.sessionState.recurringTasks);
        this.completedTasks$ = this._store.select(state => state.sessionState.completedTasks);
        this.userState$ = this._store.select(state => state.usersState);
        this.error$ = this._store.select(state => state.usersState.error);
        this.loading$ = this._store.select(state => state.sessionState.loading);
        this.authState$ = this._store.select(state => state.authState);
        this.searchType$ = this._store.select((state) => state.sessionState.searchType);
        this.sessionState$ = this._store.select(state => state.sessionState);
        this.calendarColors$ = this._store.select(state => state.sessionState.calendarColors);
    }

    ngOnInit(): void {
        this._store.dispatch(this._sessionActions.GetCalendarColors());
        this._store.dispatch(this._userActions.GetUsers(null));
        this._authSubscription = this.authState$
        .subscribe((state: IAuthState) => {
            if (null != state) {
                if (null != state.profile) {
                    this._store.dispatch(this._sessionActions.GetRecurringTasks());
                }
            }
        });

        this._sessionStateSubscription = this.sessionState$
        .subscribe((state: ISessionState) => {
            if (null != state) {
                switch (state.type) {
                    case SessionStateType.ASSIGN_TASKS_SUCCEEDED:
                    {

                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        });
    }

    ngOnDestroy(): void {
        if (this._authSubscription) {
            this._authSubscription.unsubscribe();
            this._authSubscription = null;
        }

        if (this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public ScopeTasks(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: ""
        };
        this._store.dispatch(this._sessionActions.ScopeBuildingTasks(helper));
    }

    public RefreshTasks(): void {
        this._store.dispatch(this._sessionActions.GetRecurringTasks());
    }

    public sortTable(): void {
        this._store.dispatch(this._userActions.SortByName());
    }

    /* View Methods */

    GetBuilding(orgs: IUserOrg): string {
        if ((null != orgs.buildings) && (orgs.buildings.length > 0)) {
            return orgs.buildings[0].name;
        } else {
            return 'N/A';
        }
    }

    public RecurringTask(task: ITask = null): void {
        // // console.log("RecurringTask", task, date);
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        
        const dialogRef = this._dialog.open(RecurringTaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
           
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public ScopeStaffTasks(user: IUser): void {
        // this.IsLoading = true;
        this._store.dispatch(this._sessionActions.ScopeStaffTasks(user));
    }

    public ScopeTaskType(type: string): void {
        console.log("ScopeTaskType", type);
        this._store.dispatch(this._sessionActions.ScopeByTaskType(type));
    }

    public GetNewDate(task: ITask): string {
        let text: string = '';
        let taskDate: Date = new Date(task.startDate);
        switch (task.durationAmount) {
            case this.DurationOptions[0]:
            {
                let endDate: Date = new Date(task.startDate);
                endDate.setDate(endDate.getDate() + Number(task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Daily", endDate);
                return text;
            }

            case this.DurationOptions[1]:
            {
                let endDate: Date = new Date(task.startDate);
                endDate.setDate(endDate.getDate() + (Number(task.durationQuantity) * 7));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Weekly", endDate);
                return text;
            }

            case this.DurationOptions[2]:
            {
                let date: Date = new Date(task.startDate);
                let currentDate = date.getDate();
                // Set to day 1 to avoid forward
                date.setDate(1);
                // Increase month by 1
                date.setMonth(date.getMonth() + Number(task.durationQuantity));
                // console.log("Monthly", date);
                // Get max # of days in this new month
                let daysInMonth = new Date(taskDate.getFullYear(), date.getMonth(), 0).getDate();
                // Set the date to the minimum of current date of days in month
                date.setDate(Math.min(currentDate, daysInMonth));
                // date.setMonth(date.getMonth() + task.durationQuantity);
                text = `until ${ this._getMonthDayYear(date) }`;
                // console.log("Monthly", date);
                return text;
            }

            case this.DurationOptions[3]:
            {
                let endDate: Date = new Date(task.startDate);
                endDate.setFullYear(endDate.getFullYear() + Number(task.durationQuantity));
                text = `until ${ this._getMonthDayYear(endDate) }`;
                // console.log("Yearly", endDate);
                return text;
            }

            case this.DurationOptions[4]:
            {
                text = ` forever`;
                // console.log("Yearly", endDate);
                return text;
            }
            
            default:
            {
                break;
            }
        }
    }

    public Nth(day: number): string {
        return this._nth(day);
    }

    public GetYearRepeat(task: ITask): string {
        let repeat: string = '';
        let date: Date = new Date(task.startDate);
        repeat = `the ${ this.Nth(date.getDate()) } day of ${ this.monthNames[date.getMonth()] }`;
        return repeat;
    }

    public GetHowOftenRepeat(frequency: string, repeatQuantity: number): string {
        let repeatInterval: string = frequency;
        let weekly: string  = (repeatInterval === 'Weekly') ? 'week(s)' : 'month(s)';
        if (repeatQuantity > 1) {
            return `every ${ repeatQuantity } ${ weekly }`;
        } else {
            return `${ repeatInterval.toLowerCase() }`;
        }
    }

    private _getMonthDayYear(curDate: Date): string {
        let newDate: string = "";
        newDate = `${this.monthNames[curDate.getMonth() + 1]} ${curDate.getDate()}${ this._nth(curDate.getDate()) }, ${curDate.getFullYear()}`;
        return newDate;
    }

    public SearchTasks(event: string): void {
        if (null != event) {
            let newVal: string = event;
            // console.log("newVal", newVal);
            this._store.dispatch(this._sessionActions.SearchTasks(newVal));
        }
    }

    public ChangeSearch(str: string): void {
        this._store.dispatch(this._sessionActions.ChangeSearch(str));
    }

    handleTaskDetailResponse(response: ITaskDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                this._store.dispatch(this._sessionActions.GetRecurringTasks());
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                this._store.dispatch(this._sessionActions.GetRecurringTasks());
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public GetYearMonthDay(data: string): string {
        if (null != data) {
            let parts: string = data.replace("_", "/");
            parts = parts.replace("_", "/");
            return parts;
        }
    }

    public GetDay(day: number): string {
        return this._getDay(day);
    }

    private _getDay(day: number): string {
        switch (day) {
            case 0:
            {
                return 'Sunday';
            }

            case 1:
            {
                return 'Monday';
            }

            case 2:
            {
                return 'Tuesday';
            }

            case 3:
            {
                return 'Wednesday';
            }

            case 4:
            {
                return 'Thursday';
            }

            case 5:
            {
                return 'Friday';
            }

            case 6:
            {
                return 'Saturday';
            }
            
            default:
            {
                break;
            }
        }
    }

    public GetStaffBGColor(assignees: IAssignee[], colors: any): string {
        let assignee: IAssignee = assignees[0];
        return ((null != assignee) && (null != colors[assignee.uid]) && (null != colors[assignee.uid]) && (colors[assignee.uid].primary)) ? colors[assignee.uid].primary : staffColors["notFound"].primary;
    }

    public GetStaffTextColor(assignees: IAssignee[], colors: any): string {
        let assignee: IAssignee = assignees[0];
        return ((null != assignee) && (null != colors[assignee.uid]) && (null != colors[assignee.uid]) && (colors[assignee.uid].secondary)) ? colors[assignee.uid].secondary : staffColors["notFound"].secondary;
    }

    public IsBdlgAdmin(user: IUser): boolean {
        if (null != user.orgs) {
            return user.orgs.buildings[0].isAdmin;
        } else {
            return false;
        }
    }

    public GetWeek(task: any): string {
        let repeat: string = ``;
        if (null == task) {
            if (null != task.monthlyNWeekDays) {
                let keys = Object.keys(task.monthlyNWeekDays);
                for (let key of keys) {
                    if (null != task.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of task.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }              
        } else {
            if (null != task.monthlyNWeekDays) {
                let keys = Object.keys(task.monthlyNWeekDays);
                
                for (let key of keys) {
                    if (null != task.monthlyNWeekDays[Number(key)]) {
                        let weekStr: string = `${Number(key) + 1}${this.Nth(Number(key) + 1)} `;
                        for (let day of task.monthlyNWeekDays[Number(key)]) {
                            if (null != day) {
                                let dayStr: string = `${weekStr} ${this.GetDay(Number(day))}, `;
                                repeat += dayStr;
                            }
                        }
                    }
                }
            }
        }
        return repeat;
    }

    private _nth(d: number): string {
      if (d > 3 && d < 21) {
          return 'th';
      } else {
          
          switch (d % 10) {
            case 1:
            {
                return "st";
            }

            case 2:
            {
                return "nd";
            }

            case 3:
            {
                return "rd";
            }

            default:
            {
                return "th";
            }
          }
      }
    }
}
