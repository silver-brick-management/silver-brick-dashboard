import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRecurringTasksComponent } from './dashboard-recurring-tasks.component';

describe('DashboardRecurringTasksComponent', () => {
  let component: DashboardRecurringTasksComponent;
  let fixture: ComponentFixture<DashboardRecurringTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRecurringTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRecurringTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
