// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialog, MdSnackBar, MdDialogConfig } from "@angular/material";
import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { DndDropEvent, DropEffect } from "ngx-drag-drop";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants } from "../../common/Constants";
import { IError } from "../../interfaces/IError";
import { IBooking } from "../../shared/SilverBrickTypes";
import { ISessionState, SessionStateType } from "../../interfaces/IStoreState";
import { SessionActions } from "../../store/actions/SessionActions";
import { IFormSelctedValue, IOrgDialogResponse, IBldgDialogResponse, IGetBldgHelper } from "../../interfaces/IParams";
import { CollapsibleControlComponent } from "../../components/collapsible-control/collapsible-control.component";
import { OrgDetailComponent } from "../../components/org-details/org-details.component";
import { BldgDetailComponent } from "../../components/building-details/building-details.component";
import { BuildingUsersComponent } from "../../components/building-users/building-users.component";

import { OrgBldgUtil } from "../../shared/OrgBldgUtil";

@Component({
    selector: "app-dashboard-schedule",
    templateUrl: "./dashboard-schedule.component.html",
    styleUrls: ["./dashboard-schedule.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardScheduleComponent implements OnInit {
    public bookings$: Observable<Array<IBooking>>;
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public sessionState$: Observable<ISessionState>;
    private _sessionStateSubscription: Subscription;
    draggableListLeft = [
        {
            content: "HVAC",
            effectAllowed: "move",
            disable: false,
            handle: false,
            subCategory: 'HVAC',
            clientName: 'Newest Tenant',
            buildingName: '38 New York Avenue',
            clientUnit: '4B'
        },
        {
            content: "Electrical",
            effectAllowed: "move",
            disable: false,
            handle: false,
            subCategory: 'Electrical',
            clientName: 'Tom Brady',
            buildingName: '68 4th Place',
            clientUnit: '5D'
        },
        {
            content: "HVAC",
            effectAllowed: "move",
            disable: false,
            handle: false,
            subCategory: 'HVAC',
            clientName: 'Newer Tenant',
            buildingName: '68 4th Place',
            clientUnit: '24B'
        },
        {
            content: "Key & Lock",
            effectAllowed: "move",
            disable: false,
            handle: false,
            subCategory: 'Key & Lock',
            clientName: 'Newest Tenant',
            buildingName: '38 New York Avenue',
            clientUnit: '1C'
        },
    ];

    draggableListRight = [
        {
            content: "Plumbing",
            effectAllowed: "move",
            disable: false,
            handle: false,
            subCategory: 'Plumbing',
            clientName: 'Newest Tenant',
            buildingName: '38 New York Avenue',
            clientUnit: '2F'
        },
    ];

    draggableListAdam: any[] = [];

    layout: any;
    horizontalLayoutActive: boolean = false;
    private currentDraggableEvent: DragEvent;
    private currentDragEffectMsg: string;
    private readonly verticalLayout = {
        container: "row",
        list: "column",
        dndHorizontal: false,
    };
    private readonly horizontalLayout = {
        container: "row",
        list: "row",
        dndHorizontal: true,
    };

    constructor(private _dialog: MdDialog, private _snackBar: MdSnackBar, public store: Store<AppState>, public sessionActions: SessionActions) {
        this.error$ = this.store.select((state) => state.sessionState.error);
        this.bookings$ = this.store.select((state) => state.sessionState.bookings);
        this.loading$ = this.store.select((state) => state.sessionState.loading);
        this.sessionState$ = this.store.select((state) => state.sessionState);
        this.setHorizontalLayout(this.horizontalLayoutActive);
    }

    setHorizontalLayout(horizontalLayoutActive: boolean) {
        this.layout = horizontalLayoutActive ? this.horizontalLayout : this.verticalLayout;
    }

    onDragStart(event: DragEvent) {
        this.currentDragEffectMsg = "";
        this.currentDraggableEvent = event;

        this._snackBar.dismiss();
        this._snackBar.open("Assigning...", undefined, { duration: 2000 });
    }

    onDragged(item: any, list: any[], effect: DropEffect) {
        this.currentDragEffectMsg = `Assigning....`;

        if (effect === "move") {
            const index = list.indexOf(item);
            list.splice(index, 1);
        }
    }

    onDragEnd(event: DragEvent) {
        this.currentDraggableEvent = event;
        this._snackBar.dismiss();
        this._snackBar.open(this.currentDragEffectMsg || `Assigned`, undefined, { duration: 2000 });
    }

    onDrop(event: DndDropEvent, list?: any[]) {
        if (list && (event.dropEffect === "copy" || event.dropEffect === "move")) {
            let index = event.index;

            if (typeof index === "undefined") {
                index = list.length;
            }

            list.splice(index, 0, event.data);
        }
    }

    ngOnInit(): void {
        this.store.dispatch(this.sessionActions.GetBookings());
        this._sessionStateSubscription = this.sessionState$.subscribe((sessionState: ISessionState) => {
            if (null != sessionState) {
            } else {
            }
        });
    }

    ngOnDestroy(): void {
        if (null != this._sessionStateSubscription) {
            this._sessionStateSubscription.unsubscribe();
            this._sessionStateSubscription = null;
        }
    }

    // onDraggableCopied(event: DragEvent) {
    //     console.log("draggable copied", JSON.stringify(event, null, 2));
    // }

    // onDraggableLinked(event: DragEvent) {
    //     console.log("draggable linked", JSON.stringify(event, null, 2));
    // }

    // onDraggableMoved(event: DragEvent) {
    //     console.log("draggable moved", JSON.stringify(event, null, 2));
    // }

    // onDragCanceled(event: DragEvent) {
    //     console.log("drag cancelled", JSON.stringify(event, null, 2));
    // }

    // onDragover(event: DragEvent) {
    //     console.log("dragover", JSON.stringify(event, null, 2));
    // }

    // onDrop(event: DndDropEvent) {
    //     console.log("dropped", JSON.stringify(event, null, 2));
    // }
}