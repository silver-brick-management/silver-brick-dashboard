// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
// import { IgxExcelExporterService, IgxExcelExporterOptions } from "igniteui-angular";

// Local modules
import { AppState } from '../../app/app.state';
import { Constants, Urls } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IUser, IBuildingSimple, ITask, IUserOrg, IOrgBasic } from '../../shared/SilverBrickTypes';
import { IUserState, IAuthState, AuthStateType, UserStateType, ISessionState, SessionStateType } from '../../interfaces/IStoreState';
import { UserActions } from '../../store/actions/UserActions';
import { SessionActions } from '../../store/actions/SessionActions';
import { UserUtil } from '../../common/UserUtil';
import { IPaginateQueryStrings, ITaskDialogResponse, IGetBldgHelper, IUserDialogBase } from '../../interfaces/IParams';
import { SessionData } from '../../providers/SessionData';
import { TaskDetailsComponent } from '../../components/task-details/task-details.component';
import { Controls } from '../../providers/Controls';
import { AssignTaskHelper, ITaskFeedJoin } from '../../interfaces/IResponse';
import { OrgActions } from "../../store/actions/OrgActions";
import { RecurringTaskDetailsComponent } from '../../components/recurring-task-details/recurring-task-details.component';

@Component({
    selector: 'app-dashboard-archive',
    templateUrl: './dashboard-archive.component.html',
    styleUrls: ['./dashboard-archive.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardArchiveComponent implements OnInit {
    private _authSubscription: Subscription;
    private _userStateSubscription: Subscription;
    private _sessionStateSubscription: Subscription;
    private _firstLoad: boolean = true;
    private _screenSize: number = window.screen.width;
    public activeTabIndex: number = 0;
    public uidOrEmail: string = "";
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public tasks$: Observable<Array<ITask>>;
    public recurringTasks$: Observable<Array<ITask>>;
    public completedTasks$: Observable<Array<ITask>>;
    public userState$: Observable<IUserState>;
    public authState$: Observable<IAuthState>;
    public sessionState$: Observable<ISessionState>;
    public staffUsers$: Observable<Array<IUser>>;
    public AllBuildings$: Observable<IBuildingSimple[]>;
    public orgID: string = '';
    public buildingID: string = '';
    
    
    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    get IsNotLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role !== 'Landlord'));
    }

    get IsLandlord(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.role === 'Landlord'));
    }

    get IsAdmin(): boolean {
        return ((null != this.sessionData) && (null != this.sessionData.user) && (this.sessionData.user.isAdmin));
    }

    constructor(
        private _dialog: MdDialog,
        private _snackbar: MdSnackBar,
        private _controls: Controls,
        private _orgActions: OrgActions,
        public sessionData: SessionData,
        private _store: Store<AppState>,
        private _sessionActions: SessionActions,
        private _userActions: UserActions) {
        this.AllBuildings$ = this._store.select((state) => state.orgState.allBuildings);
        this.staffUsers$ = this._store.select(state => state.usersState.users);
        this.tasks$ = this._store.select(state => state.sessionState.closedTasks);
        this.recurringTasks$ = this._store.select(state => state.sessionState.closedRecurringTasks);
        this.completedTasks$ = this._store.select(state => state.sessionState.completedTasks);
        this.userState$ = this._store.select(state => state.usersState);
        this.error$ = this._store.select(state => state.usersState.error);
        this.loading$ = this._store.select(state => state.sessionState.loading);
        this.authState$ = this._store.select(state => state.authState);
        this.sessionState$ = this._store.select(state => state.sessionState);
    }

    ngOnInit(): void {

        this._authSubscription = this.authState$
        .subscribe((state: IAuthState) => {
            if (null != state) {
                if (null != state.profile) {
                    this._store.dispatch(this._sessionActions.GetClosedTasks());
                    this._store.dispatch(this._userActions.GetUsers(null));
                }
            }
        });

        this._sessionStateSubscription = this.sessionState$
        .subscribe((state: ISessionState) => {
            if (null != state) {
                switch (state.type) {
                    case SessionStateType.ASSIGN_TASKS_SUCCEEDED:
                    {

                        break;
                    }
                    
                    default:
                    {
                        break;
                    }
                }
            }
        });
    }

    ngOnDestroy(): void {
        if (this._authSubscription) {
            this._authSubscription.unsubscribe();
            this._authSubscription = null;
        }

        if (this._userStateSubscription) {
            this._userStateSubscription.unsubscribe();
            this._userStateSubscription = null;
        }
    }

    public ScopeTasks(building: IBuildingSimple): void {
        let helper: IGetBldgHelper = {
            buildingID: building.id,
            orgID: ""
        };
        this._store.dispatch(this._sessionActions.ScopeBuildingTasks(helper));
    }

    public RefreshTasks(): void {
        this._store.dispatch(this._orgActions.GetAllOrgsScope());
        this._store.dispatch(this._sessionActions.GetClosedTasks());
        this._store.dispatch(this._userActions.GetUsers(null));
    }

    public sortTable(): void {
        console.log("sortTable");
        this._store.dispatch(this._userActions.SortByName());
    }

    /* View Methods */

    GetBuilding(orgs: IUserOrg): string {
        if ((null != orgs.buildings) && (orgs.buildings.length > 0)) {
            return orgs.buildings[0].name;
        } else {
            return 'N/A';
        }
    }

    expandTask(task?: ITask): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '1000px';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(TaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
           // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public RecurringTask(task: ITask = null): void {
        // // console.log("RecurringTask", task, date);
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = "1000px";
            dialogConfig.height = "650px";
        } else {
            dialogConfig.width = "70%";
            dialogConfig.height = "650px";
        }
        dialogConfig.disableClose = true;
        
        const dialogRef = this._dialog.open(RecurringTaskDetailsComponent, dialogConfig);
        if (task) {
            let helper: ITaskFeedJoin = {
                orgID: task.orgID,
                buildingID: task.buildingID,
                taskID: task.id,
            };
            this._store.dispatch(this._sessionActions.GetTaskFeed(helper));
            dialogRef.componentInstance.task = task;
           
            dialogRef.componentInstance.isNewTask = false;
            dialogRef.componentInstance.IsEditMode = false;
        } else {
            dialogRef.componentInstance.isNewTask = true;
            dialogRef.componentInstance.IsEditMode = true;
            //dialogRef.componentInstance.selectedOrgId = this.orgID;
            // dialogRef.componentInstance.selectedBuildingId = this.buildingID;
            // No user to push to the dialog
            // Do nothing
        }

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: ITaskDialogResponse) => {
            if (response) {
                this.handleTaskDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    handleTaskDetailResponse(response: ITaskDialogResponse): void {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                if (this.sessionData.user.isAdmin) {
                    this._store.dispatch(this._sessionActions.GetTasks());
                } else {
                    if (this.sessionData.user.role === 'Landlord') {
                        this._store.dispatch(this._sessionActions.GetLandlordTasks());
                    } else {
                        let bldgHelper: IGetBldgHelper = {
                            orgID: this.sessionData.org.id,
                            buildingID: this.sessionData.org.buildings[0].id
                        };
                        console.log("bldgHelper", bldgHelper);
                        this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                    }
                }
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `${response.task.subject} ${response.action}`;
                this._snackbar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                if (this.sessionData.user.isAdmin) {
                    this._store.dispatch(this._sessionActions.GetTasks());
                } else {
                    if (this.sessionData.user.role === 'Landlord') {
                        this._store.dispatch(this._sessionActions.GetLandlordTasks());
                    } else {
                        let bldgHelper: IGetBldgHelper = {
                            orgID: this.sessionData.org.id,
                            buildingID: this.sessionData.org.buildings[0].id
                        };
                        console.log("bldgHelper", bldgHelper);
                        this._store.dispatch(this._sessionActions.GetTenantTasks(this.sessionData.user.uid));
                    }
                }
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public GetYearMonthDay(data: string): string {
        if (null != data) {
            let parts: string = data.replace("_", "/");
            parts = parts.replace("_", "/");
            return parts;
        }
    }

    public GetDay(day: string): string {
        let split: string[] = day.split("_", 3);
        return `${new Date(Number(split[2]), Number(split[1]), Number(split[0]))}`;
    }

    public IsBdlgAdmin(user: IUser): boolean {
        if (null != user.orgs) {
            return user.orgs.buildings[0].isAdmin;
        } else {
            return false;
        }
    }
}
