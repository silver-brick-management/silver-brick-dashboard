// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs/Rx';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

// Local modules
import { AppState } from '../../app/app.state';
import { Constants } from '../../common/Constants';
import { IError } from '../../interfaces/IError';
import { IOrgBasic, IUser, IOrgInfo, IBuildingSimple, IBuildingBasic } from '../../shared/SilverBrickTypes';
import { IOrgState, OrgStateType } from '../../interfaces/IStoreState';
import { OrgActions } from '../../store/actions/OrgActions';
import { SessionActions } from '../../store/actions/SessionActions';
import { UserActions } from '../../store/actions/UserActions';
import { IFormSelctedValue, IOrgDialogResponse, IBldgDialogResponse, IGetBldgHelper } from '../../interfaces/IParams';
import { CollapsibleControlComponent } from '../../components/collapsible-control/collapsible-control.component';
import { OrgDetailComponent } from '../../components/org-details/org-details.component';
import { BldgDetailComponent } from '../../components/building-details/building-details.component';
import { BuildingUsersComponent } from '../../components/building-users/building-users.component';

import { OrgBldgUtil } from '../../shared/OrgBldgUtil';

@Component({
    selector: 'app-dashboard-orgs',
    templateUrl: './dashboard-orgs.component.html',
    styleUrls: ['./dashboard-orgs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardOrgsComponent implements OnInit {
    public allOrgs$: Observable<Array<IOrgBasic>>;
    public Orgs: Array<IOrgBasic> = [];
    public error$: Observable<IError>;
    public loading$: Observable<boolean>;
    public orgState$: Observable<IOrgState>;
    public orgSearch: string = "";
    public searchRef: string = "";
    public hasBuildings: number = 0;
    private _orgStateSubscription: Subscription;
    public Landlords$: Observable<IUser[]>;
    private _screenSize: number = window.screen.width;
    
    get IsMobile(): boolean {
        return this._screenSize < 768;
    }

    public orgHasBuildings(org: IOrgBasic): boolean {
        if ((org.buildings.length > 0) && (null != org.buildings)) {
            this.hasBuildings = 1;
            return true;
        } else {
            this.hasBuildings = 0;
            return false;
        }
    }

    constructor(
        private _dialog: MdDialog,
        private _snackBar: MdSnackBar,
    	public store: Store<AppState>,
        public sessionActions: SessionActions,
    	public orgActions: OrgActions) {
        this.error$ = this.store.select(state => state.orgState.error);
    	this.allOrgs$ =  this.store.select(state => state.orgState.searchOrgs);
        this.loading$ = this.store.select(state => state.orgState.loading);
        this.orgState$ = this.store.select(state => state.orgState);
        this.Landlords$ = this.store.select(state => state.usersState.allLandlords);
    }

    ngOnInit() {
    	this.store.dispatch(this.orgActions.GetAllOrgs());
        this._orgStateSubscription = this.orgState$
        .subscribe((orgState: IOrgState) => {
            if ((null != orgState.orgs) && (orgState.orgs.length > 0)) {
                this.Orgs = orgState.orgs;
                switch (orgState.type) {
                    case OrgStateType.NONE:
                    case OrgStateType.LOADING:
                    {
                        break;
                    }
                    
                    case OrgStateType.UPDATE_BLDG_SUCCEEDED:
                    case OrgStateType.ADD_BLDG_SUCCEEDED:
                    {
                        this.store.dispatch(this.orgActions.GetAllOrgs());
                        // for (let org of orgState.orgs) {
                        //     if ((null != org.buildings) && (org.buildings)) {
                        //         org.buildings = OrgBldgUtil.Transform(org.buildings);
                        //     } else {

                        //     }
                        // }
                        break;
                    }

                    case OrgStateType.GET_ALL_ORGS_SUCCEEDED:
                    {
                        // for (let org of orgState.orgs) {
                        //     if ((null != org.buildings) && (org.buildings)) {
                        //         org.buildings = OrgBldgUtil.Transform(org.buildings);
                        //     } else {

                        //     }
                        // }
                        break;
                    }

                    default:
                    {
                        break;
                    }
                }
            } else {
                
            }
        });
    }

    ngOnDestroy(): void {
        if (null != this._orgStateSubscription) {
            this._orgStateSubscription.unsubscribe();
            this._orgStateSubscription = null;
        }
        this.store.dispatch(this.orgActions.CleanUpSearch());
    }

    public SearchOrgs(event: string): void {
        if (null != event) {
            let newVal: string = event;
            console.log("newVal", newVal);
            this.store.dispatch(this.orgActions.SearchOrgs(newVal));
        }
    }

    public RefreshOrgs(): void {
        this.store.dispatch(this.orgActions.GetAllOrgs());
    }

    public searchByName(): void {
        if (this.orgSearch.trim()) {
        } else {
            // No string detected
            // Do nothing
        }
    }

    expandOrg(org: IOrgBasic): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;
        this.store.dispatch(this.orgActions.GetOrgInfo(org.id));
        const dialogRef = this._dialog.open(OrgDetailComponent, dialogConfig);
        dialogRef.componentInstance.orgID = org.id;
        const selectedOrg: IOrgInfo = {
            name: org.name,
            id: org.id,
            phone: '',
            lastModified: 0,
            address: null
        };
        dialogRef.componentInstance.org = selectedOrg;
        dialogRef.componentInstance.orgID = org.id;

        dialogRef.componentInstance.PopulateForm();
        dialogRef.afterClosed().subscribe((response: IOrgDialogResponse) => {
            if (response) {
                this._handleOrgDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    expandBldg(bldg: IBuildingSimple, orgID: string): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;
        const buildingHelper: IGetBldgHelper = {
            orgID: orgID,
            buildingID: bldg.id
        };
        this.store.dispatch(this.orgActions.GetBldg(buildingHelper));
        const dialogRef = this._dialog.open(BldgDetailComponent, dialogConfig);
        dialogRef.componentInstance.orgID = orgID;
        dialogRef.componentInstance.isNewBuilding = false;
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IBldgDialogResponse) => {
            if (response) {
                this._handleBldgDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public buildingAction(orgID: string): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;
        this.store.dispatch(this.orgActions.GetAllOrgs());
        const dialogRef = this._dialog.open(BldgDetailComponent, dialogConfig);
        dialogRef.componentInstance.orgID = orgID;

        dialogRef.componentInstance.isNewBuilding = true;

        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IBldgDialogResponse) => {
            if (response) {
                this._handleBldgDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public buildingUsers(bldg: IBuildingSimple, orgID: string): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(BuildingUsersComponent, dialogConfig);
        dialogRef.componentInstance.orgID = orgID;
        dialogRef.componentInstance.buildingID = bldg.id;
        dialogRef.componentInstance.buildingName = bldg.name;
        dialogRef.componentInstance.PopulateForm();
    }

    public newOrg(): void {
        const dialogConfig = new MdDialogConfig();
        if (this.IsMobile) {
            dialogConfig.width = '100%';
            dialogConfig.height = '650px';
        } else {
            dialogConfig.width = '70%';
            dialogConfig.height = '650px';
        }
        dialogConfig.disableClose = true;
        this.store.dispatch(this.orgActions.GetAllOrgs());
        const dialogRef = this._dialog.open(OrgDetailComponent, dialogConfig);
        dialogRef.componentInstance.org = null;
        dialogRef.componentInstance.PopulateForm();

        dialogRef.afterClosed().subscribe((response: IOrgDialogResponse) => {
            if (response) {
                this._handleOrgDetailResponse(response);
            } else {
                // Do nothing as there is no response
            }
        });
    }

    /* Helper Methods */

    private _handleOrgDetailResponse(response: IOrgDialogResponse) {
        // Notify of action
        switch (response.action) {
            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `Addition of a new Org named ${response.org.name} was successful`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `Update of a existing Org named ${response.org.name} was successful`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_DELETE:
            {
                const msg: string = `${response.org.name} deleted successfully`;
                this.store.dispatch(this.sessionActions.GetTasks());
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;
            }

            default:
            {
                break;
            }
        }
    }

    private _handleBldgDetailResponse(response: IBldgDialogResponse) {
        // Notify of action
        switch (response.action) {

            case Constants.DIALOG_RESPONSE_ACTION_UPDATE:
            {
                const msg: string = `Update of a existing Building named ${response.building.building.info.name} was successful`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;
            }

            case Constants.DIALOG_RESPONSE_ACTION_ADD:
            {
                const msg: string = `${response.building.building.info.name} added successfully`;
                this._snackBar.open(msg, Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                break;
            }

            default:
            {
                break;
            }
        }
    }

}
