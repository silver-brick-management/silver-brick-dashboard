import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardOrgsComponent } from './dashboard-orgs.component';

describe('DashboardOrgsComponent', () => {
  let component: DashboardOrgsComponent;
  let fixture: ComponentFixture<DashboardOrgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardOrgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardOrgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
