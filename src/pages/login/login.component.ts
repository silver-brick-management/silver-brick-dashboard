// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from '@ngrx/store';
import { Router, NavigationExtras } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MdDialog, MdSnackBar, MdDialogConfig } from '@angular/material';
import * as firebase from 'firebase';

// Local modules
import { Controls } from '../../providers/Controls';
import { AppState } from '../../app/app.state';
import { IError } from '../../interfaces/IError';
import { ICredentials } from '../../interfaces/IParams';
import { AuthActions } from '../../store/actions/AuthActions';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { IUserDialogBase, IInputDialogBase } from '../../interfaces/IParams';
import {
    Constants,
    Urls
} from '../../common/Constants';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

    public errorMessage: string;
    public isLoading$: Observable<boolean>;
    public error$: Observable<IError>;
    public LoggingIn: boolean = false;

    constructor(
        private _dialog: MdDialog,
        public controls: Controls,
        private _snackbar: MdSnackBar,
        private _router: Router,
        private _store: Store<AppState>,
        private _authActions: AuthActions) {

        this.isLoading$ = this._store.select(state => state.authState.loading);
        this.error$ = this._store.select(state => state.authState.error);
    }

    ngOnInit(): void {

    }

    public ResetPassword(): void {
        const dialogConfig = new MdDialogConfig();
        dialogConfig.disableClose = true;

        const dialogRef = this._dialog.open(ConfirmDialogComponent, dialogConfig);
        // TODO: Move these into Constants
        dialogRef.componentInstance.Title = 'Reset Your Password';
        dialogRef.componentInstance.Message = 'We will send you an email with the steps to reset it.';
        dialogRef.componentInstance.OkString = 'Submit';
        dialogRef.componentInstance.IsInputting = true;
        dialogRef.componentInstance.CancelString = 'No';
        dialogRef.afterClosed().subscribe((response: IInputDialogBase) => {
            if (response.input) {
                console.log("Password Reset Email!", response.input);
                // this._firebaseAuth.ResetPassword(response.input);
                firebase.auth().sendPasswordResetEmail(response.input).then(() => {
                    this._snackbar.open('Password reset email sent!', Constants.STR_DISMISS, { duration: 3000, extraClasses: ['snackBarClass'] });
                });
            } else {
                // Do nothing as there is no response
            }
        });
    }

    public ChangeLogin(): void {
        this.LoggingIn = !this.LoggingIn;
    }

    public onSubmit(loginForm: NgForm): void {
        console.log("Form Submitted");
        const credentials: ICredentials = loginForm.value;
        if (loginForm.valid) {
            this._store.dispatch(this._authActions.LoginFirebase(credentials));
        } else {
            // Login form isnt valid yet
            // Do nothing
        }
    }
}
