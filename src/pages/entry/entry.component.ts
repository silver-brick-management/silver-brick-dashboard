// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';

// Local modules
import { AppState } from '../../app/app.state';
import { IAuthState, AuthStateType } from '../../interfaces/IStoreState';
import { Constants, Urls } from '../../common/Constants';
import { AuthActions } from '../../store/actions/AuthActions';


@Component({
    selector: 'app-entry',
    templateUrl: './entry.component.html',
    styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {

    private _authStateSubscription: Subscription;
	public authState$: Observable<IAuthState>;
    public LoadingText: string = Constants.STR_LOADING;
    constructor(public store: Store<AppState>, public router: Router, public authActions: AuthActions) {
        this.authState$ =  this.store.select(state => state.authState);
    }

    ngOnInit(): void {
		this._authStateSubscription = this.authState$
		.subscribe((currentAuthState: IAuthState) => {
            console.log("Entry.ts Current auth state - " + AuthStateType[currentAuthState.type]);
            switch (currentAuthState.type) {
                // Idle states
                case AuthStateType.LOGOUT_SUCCEEDED:
                {
                    this.LoadingText = "LOG OUT SUCCESSFUL!";
                    this.router.navigate(['/login'])
                    break;
                }

                case AuthStateType.LOGIN_FAILED:
                {
                    this.LoadingText = "LOGIN FAILED!";
                    break;
                }

                case AuthStateType.LOGIN_SUCCESS:
                {
                	this.LoadingText = "LOGIN SUCCESS!";
                	break;
                }

                case AuthStateType.NONE:
                case AuthStateType.LOADING:
                default:
                {
                    this.LoadingText = Constants.STR_LOADING;
                    break;
                }
            }
        },
        (error) => {
            console.log('Entry.ts: Auth subscription failed! Go log in');
        });
    }

    ngOnDestroy(): void {
        if (null != this._authStateSubscription) {
            this._authStateSubscription.unsubscribe();
            this._authStateSubscription = null;
        }
    }
}
