// ****************************************************************
//
// This file is subject to the terms and conditions defined in file
// 'LICENSE.txt', which can be found at the root of this project.
//
// ****************************************************************

// Node modules
import { Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs/Rx";
import { MdDialog, MdSnackBar, MdDialogConfig } from "@angular/material";
import { Component, OnInit, ChangeDetectionStrategy, Optional, ChangeDetectorRef, ViewChild, TemplateRef } from "@angular/core";
import { NgxSmartModalService, NgxSmartModalConfig } from "ngx-smart-modal";
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from "date-fns";
import { Subject } from "rxjs/Subject";
import { NgbModal, NgbModalOptions } from "@ng-bootstrap/ng-bootstrap";
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewDay } from "angular-calendar";

// Local modules
import { AppState } from "../../app/app.state";
import { Constants, Urls, colors } from "../../common/Constants";
import { Controls } from "../../providers/Controls";

import { IError } from "../../interfaces/IError";
import { IUser, IOrgBasic, ITask } from "../../shared/SilverBrickTypes";
import { IUserState, UserStateType, IOrgState, OrgStateType, SessionStateType, ISessionState } from "../../interfaces/IStoreState";
import { SessionActions } from "../../store/actions/SessionActions";
import { OrgActions } from "../../store/actions/OrgActions";
import { UserUtil } from "../../common/UserUtil";
import { IPaginateQueryStrings, IUserDialogResponse, IGetBldgHelper } from "../../interfaces/IParams";
// import { UserDetailComponent } from '../../components/user-details/user-details.component';
import { SessionData } from "../../providers/SessionData";
import { FirebaseAuth } from '../../services/FirebaseAuth';
import { CalendarProvider } from "../../providers/CalendarProvider";
import { UserActions } from "../../store/actions/UserActions";

@Component({
    selector: "app-dashboard-calendar",
    templateUrl: "./dashboard-calendar.component.html",
    styleUrls: ["./dashboard-calendar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardCalendarComponent implements OnInit {
    public loading$: Observable<boolean>;
    constructor(
        private _controls: Controls,
        private dialog: MdDialog,
        private snackbar: MdSnackBar,
        private _orgActions: OrgActions,
        private _cdr: ChangeDetectorRef,
        public userActions: UserActions,
        private _sessionData: SessionData,
        public _modal: NgbModal,
        public firebaseAuth: FirebaseAuth,
        private _store: Store<AppState>,
        private _sessionActions: SessionActions) {
        this.loading$ = this._store.select(state => state.sessionState.loading);
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {}



    // beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    // body.forEach(day => {
    //   day.badgeTotal = day.events.filter(
    //     event => event.meta.incrementsBadgeTotal
    //   ).length;
    // });
    // }
}

